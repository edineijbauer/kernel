var loading = "<div class='container pd-content30'><i class='container shoticon shoticon-loading' style='padding-left: 0; height: 50px; width: 60%; margin:10% 20%'></i></div>";
var inform;

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
}

function inputchange(t) {
    if (t.attr("type") === 'checkbox' && t.is(":checked") === false) {
        t.siblings("span").removeClass("inputSelect");
    } else {
        if (t.val().length !== 0) {
            t.siblings("span").addClass("inputSelect");
        } else {
            t.siblings("span").removeClass("inputSelect");
        }
    }
}

function inputs() {
    $("input").focus(function () {
        $(this).siblings("span").addClass("inputSelect");
    }).focusout(function () {
        var $this = $(this);
        setTimeout(function () {
            if (!$this.is(":focus")) {
                inputchange($this);
            }
        }, 200);
    }).each(function () {
        inputchange($(this));
    });

}
inputs();

function infor(mensagem, times) {
    if (!$("#main-infor").length) {
        $("body").append("<div id='main-infor' class='transition-easy'>" + mensagem + "</div>");
        setTimeout(function () {
            $("#main-infor").addClass("main-infor-open");
        },1);

        inform = setTimeout(function () {
            $("#main-infor").removeClass("main-infor-open");
            setTimeout(function () {
                $("#main-infor").remove();
            }, 400);
        }, (times === undefined ? 2500 : times * 1000));
    } else {
        clearTimeout(inform);
        $("#main-infor").html(mensagem);

        inform = setTimeout(function () {
            $("#main-infor").removeClass("main-infor-open");
            setTimeout(function () {
                $("#main-infor").remove();
            }, 400);
        }, (times === undefined ? 2500 : times * 1000));
    }
}

function closeLightBox() {
    if ($(".asLightBox").length === 1) {
        $("body").css("overflow-y", "scroll");
        $(".closeButton").addClass("ds-none");
        $(".offoscar").fadeOut(400);
    }
    $(".asLightBox").last().remove();
    $("#" + localStorage.lastIdLightBox).focus();
    localStorage.removeItem("lastIdLightBox");
}
function showOffoscar() {
    $(".offoscar").fadeIn(400);
    $(".closeButton").removeClass("ds-none");
}

function lightBox(id, title, html) {
    id = (id === "" ? "offoscar" : id);
    localStorage.lastIdLightBox = id;

    if (!$(".asLightBox").length) {
        setTimeout(function () {
            $("body").css("overflow-y", "hidden");
        }, 300);
        showOffoscar();
    }

    $(".offoscar").before("<div class='ps-fixed bg-white asLightBox' style='top:" + ($("#" + id).offset().top - $(document).scrollTop()) + "px; left:" + $("#" + id).offset().left + "px;'>" +
        "<div class='ps-absolute LightBoxTitle left upper font-size12 bg-primary container pd-small color-lightgray'>" +
        "<span class='fl-left pd-smallb LightBoxTitleName'>" + title + "</span>" +
        "<div class='transition-fast fl-right closeButton font-size12 pointer' onclick='closeLightBox()'>x</div>" +
        "</div>" +
        "<div class='container asLightBoxContent' style='max-height: 100%; overflow-x: hidden; overflow-y: scroll;'>" + html + "</div></div>");

    setTimeout(function () {
        $(".asLightBox").addClass("lightboxOpen");
    }, 1);
}

function lightBoxContent(html) {
    if ($(".asLightBox").length) {
        $(".asLightBoxContent").last().html(html);
    }
}

function lightBoxTitle(title) {
    if ($(".asLightBox").length) {
        $(".LightBoxTitleName").last().html(title);
    }
}

function setLightBoxPadding(e) {
    $(".asLightBox").last().css("padding", e);
}

function setLightBoxNoScrool() {
    $(".asLightBoxContent").last().css("overflow-y", "hidden");
}

function setLightBoxHeightAuto() {
    $(".asLightBox").last().css("height", "auto");
}
function setLightBoxBackground(e) {
    $(".asLightBox").last().css("background", e);
}

$(function () {
    $(".closeButton, .offoscar").click(function () {
        closeLightBox();
    });
});