<?php
ob_start();
require('./_app/Config.inc.php');
$read = new Read;

header("Content-Type: application/xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>' . '<rss version="2.0">';

echo "<channel>
    <title>" . SITENAME . "</title>
    <link>" . HOME . "</link>
    <description>" . SITEDESC . "</description>";

//######################  FEED POST  #########################//
$gal = new Read;
$read->ExeRead(PRE . "post", "ORDER BY id DESC limit 15");
if ($read->getResult()):
    foreach ($read->getResult() as $post):
        $categ = '';
        $read->ExeRead(PRE . "category_post", "WHERE post_id=:pk", "pk={$post['id']}");
        if ($read->getResult()):
            foreach ($read->getResult() as $c):
                $category = Check::getBanco(PRE . 'category', (int)$c['category_id'], 'category_title');
                $categ .= "<category>{$category}</category>";
            endforeach;
            unset($category, $c);
        endif;

        $link = HOME . "/" . $post['urlname'];
        $imagem = Check::getBanco(PRE . "gallery", (int)$post['imagem'], "cover");

        echo "
          <item>
            <title>" . $post['title'] . "</title>
            " . $categ . "
            <copyright>" . date('Y') . " " . SITENAME . ". All rights reserved.</copyright>
                
            <image>
                <url>" . HOME . "/uploads/" . $imagem . "</url>
                <title>" . $post['title'] . "</title>
                <link>" . $link . "/</link>
            </image>
            
            <language>pt-br</language>
            <pubDate>" . date('D, d M Y H:i:s', strtotime($post['date'])) . " +0000</pubDate>

            <link>" . $link . "/</link>
            <description><![CDATA[<img src='" . HOME . "/uploads/" . $imagem . "' alt='" . $post['title'] . "' title='" . $post['title'] . "' /><br>" . $post['descricao'] . "<br>]]></description>
          </item>
        ";
    endforeach;
endif;

//######################  FEED SMARTPHONE #########################//
$gal = new Read;
$read->ExeRead(PRE . "smartphone", "ORDER BY id DESC limit 5");
if ($read->getResult()):
    foreach ($read->getResult() as $post):
        $categ = "<category>{$post['sistema_operacional']}</category>";

        $link = HOME . "/smartphone/" . $post['urlname'];
        $imagem = Check::getBanco(PRE . "gallery", (int)$post['imagem'], "cover");

        $read->ExeRead(PRE . "smartphone_resumo", "WHERE smartphone = :sm", "sm={$post['id']}");
        if ($read->getResult()):

            $resumos = $read->getResult()[0];

            $read->ExeRead(PRE . "preco", "WHERE smartphone = :sm ORDER BY valor LIMIT 1", "sm={$post['id']}");
            if ($read->getResult()):
                $resumos['link'] = $read->getResult()[0]['link'];
                $resumos['preco'] = round($read->getResult()[0]['valor'] * DOLAR);
            endif;

            $resumos['alfandega'] = "R$" . $resumos['preco'] * 0.5 . " á R$" . $resumos['preco'] * 0.7;
            $resumos['total'] = "R$" . ($resumos['preco'] + ($resumos['preco'] * 0.65));
            $resumos['pontos'] = number_format(Check::getBanco(PRE . "preco_beneficio", (int)$post['id'], "pontos", "smartphone"), 1, '.', '');

            $view = new View();
            $resumo = $view->Retorna($resumos, $view->Load("especificacao/resumo"));

            $date = Check::getBanco(PRE . "preco_historico", (int)$post['id'], "dia", "smartphone");
            $post['title'] .= " - Informações Técnicas " . SITENAME;

            echo "
                <item>
                    <title>" . $post['title'] . "</title>
                    " . $categ . "
                    <copyright>" . date('Y') . " " . SITENAME . ". All rights reserved.</copyright>
                        
                    <image>
                        <url>" . HOME . "/uploads/" . $imagem . "</url>
                        <title>" . $post['title'] . "</title>
                        <link>" . $link . "/</link>
                    </image>
                    
                    <language>pt-br</language>
                    <pubDate>" . date('D, d M Y H:i:s', strtotime($date)) . " +0000</pubDate>
                    
                    <link>" . $link . "/</link>
                    <description><![CDATA[<img src='" . HOME . "/uploads/" . $imagem . "' alt='" . $post['title'] . "' title='" . $post['title'] . "' /><br>" . $resumo . "<br>]]></description>
                </item>
                ";
        endif;
    endforeach;
endif;

echo '</channel></rss>';



