<?php
$get = filter_input(INPUT_GET, 'i', FILTER_VALIDATE_INT);
$hoje = date('Y-m-d');
ob_start();
require('./_app/Config.inc.php');
$Session = new Session;
$read = new Read;
$mesmo = 'a';
$conta_e=0;

header("Content-Type: application/xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';

if(!empty($get)): ?>
	<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

	<?php

	$read = new Read;
	if($get === 1):
		//######################  HOME  #########################//
		echo "<url>
				<loc>".HOME."/</loc>
				<lastmod>".$hoje."</lastmod>
				<changefreq>daily</changefreq>
				<priority>1</priority>
		</url>";
		
		$read->ExeRead(PRE . "post", "ORDER BY date DESC limit 500");
		if($read->getResult()):
			foreach ($read->getResult() as $op):
				echo "<url>
						<loc>". HOME . "/" . $op['urlname'] . "/" ."</loc>
						<lastmod>".date('Y-m-d', strtotime($op['date']))."</lastmod>
						<changefreq>monthly</changefreq>
						<priority>0.8</priority>
				</url>";
			endforeach;
		endif; 
		
		$read->ExeRead(PRE . "smartphone", "ORDER BY id DESC limit 500");
		if($read->getResult()):
			foreach ($read->getResult() as $op):
				$date = date('Y-m-d', strtotime(Check::getBanco(PRE . "preco_historico", (int) $op['id'], "dia", "smartphone")));
				echo "<url>
						<loc>". HOME . "/" . $op['urlname'] . "/" ."</loc>
						<lastmod>" . $date . "</lastmod>
						<changefreq>monthly</changefreq>
						<priority>0.7</priority>
				</url>";
			endforeach;
		endif;   
		
		echo "</urlset>";
		
	else:
		//######################  POSTS  #########################//
		$offset = 1000*$get-1000;
		
		$read->ExeRead(PRE . "post", "ORDER BY date DESC limit 1000 offset $offset");
		if($read->getResult()):
			foreach ($read->getResult() as $op):
				echo "<url>
						<loc>". HOME . "/" . $op['urlname'] . "/" ."</loc>
						<lastmod>".date('Y-m-d', strtotime($op['date']))."</lastmod>
						<changefreq>monthly</changefreq>
						<priority>0.8</priority>
				</url>";
			endforeach;
		endif;  
		
		$read->ExeRead(PRE . "smartphone", "ORDER BY id DESC limit 1000 offset $offset");
		if($read->getResult()):
			foreach ($read->getResult() as $op):
				$date = date('Y-m-d', strtotime(Check::getBanco(PRE . "preco_historico", (int) $op['id'], "dia", "smartphone")));
				echo "<url>
						<loc>". HOME . "/" . $op['urlname'] . "/" ."</loc>
						<lastmod>" . $date . "</lastmod>
						<changefreq>monthly</changefreq>
						<priority>0.7</priority>
				</url>";
			endforeach;
		endif; 
		
		echo "</urlset>";
	endif;

else:

    echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $read->ExeRead(PRE . "post");
            $conta = ceil($read->getRowCount() / 1000);
			
            for($i=1;$i<=$conta;$i++):
                echo '<sitemap>
                        <loc>'.HOME.'/sitemap'.$i.'.xml</loc>
                        <lastmod>'.$hoje.'</lastmod>
                    </sitemap>';
            endfor;
        echo '</sitemapindex>';
endif;
?>


