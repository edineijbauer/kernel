<?php
ob_start();
require('./_app/Config.inc.php');
$read = new Read;

header("Content-Type: application/xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>' . '<rss version="2.0">';

echo "<channel>
    <title>" . SITENAME . "</title>
    <link>" . HOME . "</link>
    <description>" . SITEDESC . "</description>";

//######################  FEED POST  #########################//
$gal = new Read;
$read->ExeRead(PRE . "publicar", "WHERE status=1 && instant_article = 1 ORDER BY id DESC limit 25");
if ($read->getResult()):
    foreach ($read->getResult() as $postt):
        $read->ExeRead(PRE . "post", "WHERE publicar = :p ", "p={$postt['id']}");
        if ($read->getResult()):
            $post = $read->getResult()[0];
            $post['category'] = '';
            $read->ExeRead(PRE . "category_post", "WHERE post_id=:pk", "pk={$post['id']}");
            if ($read->getResult()):
                foreach ($read->getResult() as $c):
                    $category = Check::getBanco(PRE . 'category', (int)$c['category_id'], 'category_title');
                    $post['category'] .= "<category>{$category}</category>";
                endforeach;
                unset($category, $c);
            endif;

            $post['urlname'] = HOME . "/" . $post['urlname'];
            $post['imagem'] = HOME . "/uploads/" . Check::getBanco(PRE . "gallery", (int)$post['imagem'], "cover");
            $post['copyright'] = date('Y') . " " . SITENAME . ". All rights reserved.";
            $post['date'] = date('D, d M Y H:i:s', strtotime($postt['date'])) . " +0000";
            $post['content'] = "<img src='{$post['imagem']}' alt='{$post['title']}' title='{$post['title']}' /><br>{$post['descricao']}<br>";

            $views = new Tpl();
            $views->Show($post, $views->Load("feed/feed"));

        endif;
    endforeach;
endif;

echo '</channel></rss>';



