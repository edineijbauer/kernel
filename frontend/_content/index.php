<?php

ob_start();
require_once('../../backend/_app/Config.inc.php');

$Link = new Link;
?>
    <!DOCTYPE html>
    <html lang="pt-br">
    <meta charset="UTF-8">

    <?php
    $Link->getMetadados();



    ?>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    </head>

    <body>
        <?php
            require_once ($Link->getPatch() . DIRECTORY_SEPARATOR . $Link->getFile() .  '.php');
        ?>
    </body>
    </html>

<?php
ob_end_flush();
