<?php
ob_start();
require('../_app/Config.inc.php');
$Session = new Session;
require('../_app/Login.inc.php');

define("ADM", true);

$Link = new Link();

$read = new Read();
?>
    <!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset="UTF-8">

        <?php
        $Link->getMetadados();
        ?>

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="<?= HOME ?>/css/boot.css"/>
        <link rel="stylesheet" href="<?= REQUIRE_PATH ?>/css/style.css"/>
        <link rel="stylesheet" href="<?= HOME ?>/_app/PageNavigation/css/style.css"/>
        <script src="<?= HOME ?>/js/jquery.min.js"></script>
        <script>
            sessionStorage.removeItem("vertical");
            sessionStorage.removeItem("horizontal");</script>
        <?php include(REQUIRE_PATH . '/inc/head.php'); ?>

    </head>
    <body>
    <div class="offoscar" id="offoscar"></div>
    <input type="hidden" id="pre" value="<?=PRE?>" />

    <?php
    require(REQUIRE_PATH . '/inc/header.inc.php');

    if (!require($Link->getPatch())):
        WSErro('Erro ao incluir arquivo de navegação!', WS_ERROR, true);
    endif;

    if (isset($_SESSION['userlogin']['id'])):
        require(REQUIRE_PATH . '/inc/chatonline.inc.php');
    endif;
    ?>
    <script src="<?= HOME ?>/js/bootJs.js"></script>
    <script src="<?= HOME ?>/js/boot_functions.js"></script>
    <script src="<?= HOME ?>/_app/PageNavigation/js/page_navigate.js"></script>

    </body>

    </html>
<?php

ob_end_flush();
