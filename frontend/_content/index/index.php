<?php

require(REQUIRE_PATH . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . "header.inc.php");

if (!require($Link->getPatch())):
    WSErro('Erro ao incluir arquivo de navegação!', WS_ERROR, true);
endif;

require(REQUIRE_PATH . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . "footer.inc.php");