var loading = "<div class='container'><i class='container shoticon shoticon-loading' style='padding-left: 0;height: 50px;width: 100%;mar;margin-left: -15%;'></i></div>";

$("#copyImageToGallery").focus();
inputs();

function choiceCover(banco, id, retorno) {
    closeWindow(banco);
    getPage(banco, id, retorno);
}

function excluirGallery(id, banco) {
    if (confirm("Excluir Imagem da Galeria?")) {
        $.post("../requests/back/gallery/deletar.php", {e: id, b: banco});
        $("#imageG-" + id).toggle(200);
        infor("Imagem Deletada");
    }
}

function sendlink(id, funcao, banco) {
    var value = $("#copyImageToGallery").val();
    var folder = $("#pasta").val();
    if (value.length > 10) {

        $("#copyImageToGallery").val("");
        $("#galleryStatus").html(loading);

        $.post('../_imageUpload/getlink.php', {v: value, banco: banco, folder:folder}, function (g) {
            $.post('../_imageUpload/gallery.php', {id: id, e: funcao, banco: banco},
                function (h) {
                    $("#galleryBox").html(h);
                    $("#galleryStatus").html("");
                });

        });
    }
}

function removeAcento(strToReplace) {
    str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ&- ";
    str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUCe__";
    var nova = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
            nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
        } else {
            nova += strToReplace.substr(i, 1);
        }
    }
    return nova;
}

$(function () {

    var ul = $('#uploadBox ul');

    $('#drop a').click(function () {
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#uploadBox').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

            $("#galleryStatus").html(loading);

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' +
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function () {

                if (tpl.hasClass('working')) {
                    jqXHR.abort();
                }

                if ($("#acf").hasClass("func")) {
                    $("#acf").trigger("click");
                }

                tpl.fadeOut(function () {
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            data.files[0].name = removeAcento(data.files[0].name);
            var jqXHR = data.submit();
        },
        progress: function (e, data) {

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if (progress == 100) {
                data.context.removeClass('working');
                var d = new Date();
                var m = d.getMonth() + 1;
                if (m < 10) {
                    m = '0' + m;
                }
                setTimeout(function () {
                    // $("#uploadBox ul li div").last().html("<img src='../uploads/" + img + "' style='height:48px;width:48px;' />");
                    $("#uploadBox ul").html("");

                    var img = $("#pasta").val() + "/" + d.getFullYear() + "/" + m + "/" + removeAcento(data.files[0].name);
                    $.post('../_imageUpload/assets/getIdGallery.php', {cover: img, banco: $("#bancoUsoSendGallery").val()}, function (id) {
                        $.post('../_imageUpload/gallery.php', {id: $("#idinput").val(), e: $("#ff").val(), banco: $("#bancoUsoSendGallery").val()}, function (h) {
                            $("#galleryBox").html(h);
                            $("#galleryStatus").html("");
                        });
                    });

                }, 1000);
            }
        },
        fail: function (e, data) {
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});