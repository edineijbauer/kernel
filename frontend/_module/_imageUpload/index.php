<?php
ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

$funcao = filter_input(INPUT_GET, 'e', FILTER_DEFAULT);
$folder = filter_input(INPUT_GET, 'f', FILTER_DEFAULT);
$id = filter_input(INPUT_GET, 'id', FILTER_DEFAULT);

$opacity = 0;
$widthII = 30;

$read = new Read();
$read->ExeRead(PRE . "padrao", "WHERE user_id=:ui", "ui={$_SESSION['userlogin']['id']}");
if ($read->getResult()):
    foreach ($read->getResult() as $p):
        if ($p['name'] == "opacity-" . $funcao . '-' . $folder):
            $opacity = $p['padrao'] * 10;
        endif;
        if ($p['name'] == "width-" . $funcao . '-' . $folder):
            $widthII = $p['padrao'];
        endif;
    endforeach;
endif;
?><!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8"/>
        <title>Galeria de Imagens</title>

        <!-- Google web fonts -->
        <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> 

        <!-- The main CSS file -->
        <link rel="stylesheet" href="<?= HOME ?>/css/boot.css"/>
        <link href="assets/css/style.css" rel="stylesheet" />
        <link href="<?= HOME ?>/css/shoticon/material.css" rel="stylesheet" />
    </head>

    <script>

        function HeaderSelect(e){
            parent.close();
            if (e == '0') {
                $('#main-header', window.parent.document).css("background-image", "initial");
                parent.addPattern("headercolor", "#main-header{background-image: initial", 2); 
            } else {
                $('#main-header', window.parent.document).css("background-image", "url('tim.php?src=uploads/" + e + "&w=1920')").css("background-size", "cover").css("background-position", "center");
                parent.addPattern("headercolor", "#main-header{background-image: url('tim.php?src=uploads/" + e + "&w=1920');background-size: cover; background-position: center}", 2); 
            }
        }

        function ArtigoSelect(e){
            parent.close();
            if (e == '0') {
                $('#artigo', window.parent.document).css("background-image", "initial");
                parent.addPattern("artigocolor", "#artigo{background-image: initial", 2); 
            } else {
                $('#artigo', window.parent.document).css("background-image", "url('tim.php?src=uploads/" + e + "&h=1920')").css("background-size", "cover").css("background-position", "center");
                parent.addPattern("artigocolor", "#artigo{background-image: url('tim.php?src=uploads/" + e + "&h=1920');background-size: cover; background-position: center}", 2); 
            }
        }

        function LogoSelect(e){
            parent.close();
            if (e == '0') {
                $('#logotipoTitle', window.parent.document).css("background-image", "initial");
                parent.addPattern("logocolor", "#logotipoTitle{background-image: initial", 2); 
                parent.addPattern("logoimg", "<?=SITENAME?>", 1);
                
            } else {
                $('#logotipoTitle', window.parent.document).css("background-image", "url('tim.php?src=uploads/" + e + "&h=50')").css("background-size", "cover").css("background-position", "center");
                parent.addPattern("logocolor", "#logotipoTitle{background-image: url('tim.php?src=uploads/" + e + "&h=50');background-size: cover; background-position: center}", 2); 
                parent.addPattern("logoimg", "<img src='tim.php?src=uploads/" + e + "&h=50' style='visibility: hidden;' />", 1); 
            }
        }
        function infor(mensagem) {
            var times = 3000;
            $(".main-infor").removeClass("bounceOutLeft ds-none").addClass("bounceInLeft").html(mensagem);
            setTimeout(function() {
                $(".main-infor").removeClass("bounceInLeft").addClass("bounceOutLeft");
            }, times);
        }
        function opengal(e) {
            $(".btnchange").removeClass("active");
            if (!$(".sel").hasClass("active") && e == 0) {
                $(".sel").addClass("active");
                $("#sendSpace, #option").fadeOut(0);
                $("#gallerySpace").fadeIn(300);
            } else if (!$(".sen").hasClass("active") && e == 1) {
                $(".sen").addClass("active");
                $("#gallerySpace, #option").fadeOut(0);
                $("#sendSpace").fadeIn(300);
            } else if (!$(".opt").hasClass("active") && e == 2) {
                $(".opt").addClass("active");
                $("#gallerySpace, #sendSpace").fadeOut(0);
                $("#option").fadeIn(300);
            }
        }
        function sendlink() {
            var v = $(".inputchange").val();
            $(".inputchange").val("");
            if (!$('.sel').hasClass("active")) {
                $(".sel").trigger("click");
                $("#galleryBox").html("<div class='container pd-content20 al-center font-light'>enviando...</div>" + loading);
            }
            $.post('getlink.php', {v: v, pasta: '<?= $folder ?>'},
            function(g) {
                $.post('gallery.php', {e: '<?= $funcao ?>', f: '<?= $folder ?>', id: '<?= $id ?>'},
                function(h) {
                    $("#galleryBox").html(h);
                });

            });
        }
    </script>

    <body>

        <div style="width:100%;float:left;margin:0; background: #777; position: absolute; top: 0; left: 0;">
            <div class="btnimc" onclick="sendlink();">enviar</div>
            <input type="text" placeholder="link para imagem" class="inputchange" />
            <div class="btnchange sel" onclick="opengal(0);">Selecionar</div>
            <div class="btnchange sen active" onclick="opengal(1);">Enviar</div>
        </div>

        <div id="gallerySpace">

            <?php

            $bancoInfo = new tableStruct();
            $bancoInfo->setTable("gallery");
            $bancoInfo = $bancoInfo->getResult()[PRE . "gallery"];

            if ($folder == 'all'):
                $read->ExeRead(PRE . "gallery", "ORDER BY id DESC");
            else:
                $read->ExeRead(PRE . "gallery", "WHERE folder = :gf ORDER BY id DESC", "gf={$folder}");
            endif;

            if ($read->getResult()):

                echo "<div id='galleryBox' class='container ps-relative pd-medium' style='overflow-y: scroll; height: 700px;'>";

                echo '<div class="box box-5 bg-white border ps-relative border-bottom s11box-4 s8box-3 s6box-3 s4box-1 pointer inhover hovershadow transition-easy" id="imageG-0">'
                . '<img src="' . HOME . '/tim.php?src=' . HOME . '/css/boot/sistem/frame.png&w=330&h=180" onclick="' . $funcao . '(\'' . $bancoInfo['cover']['value'] . '\', 0, \'' . $id . '\');" class="container" />'
                . '<span class="container pd-medium al-center" style="height: 38px; line-height: 24px; overflow:hidden">Não usar</span>'
                . '</div>';

                foreach ($read->getResult() as $gal):
                    echo '<div class="box box-5 bg-white border ps-relative border-bottom s11box-4 s8box-3 s6box-3 s4box-1 pointer inhover hovershadow transition-easy" id="imageG-' . $gal['id'] . '">'
                    . '<img src="' . HOME . '/tim.php?src=' . HOME . '/uploads/' . $gal['cover'] . '&w=330&h=180" onclick="' . $funcao . '(\'' . $gal['cover'] . '\', ' . $gal['id'] . ', \'' . $id . '\');" class="container" />'
                    . '<span class="container pd-medium al-center" style="height: 38px; line-height: 24px; overflow:hidden">' . $gal['title'] . '</span>'
                    . '<button class="btn btn-staticwhite radius transition-easy inhover border-redhover pd-smallb font-size09 ps-absolute top right" title="excluir imagem" onclick="excluirGallery(' . $gal['id'] . ')" style="">x</button>'
                    . '</div>';
                endforeach;

                echo "</div>";

            else:
                echo "<div class='container content8'>";
                WSErro('Opa! Nenuma imagem nesta galeria "' . $folder . '"', WS_INFOR);
                echo "</div>";
            endif;
            ?>

        </div>

        <div id="sendSpace">
            <form id="uploadBox" method="post" action="upload.php" enctype="multipart/form-data">
                <div class="txtgallery">Enviar imagens para a Galeria</div>
                <div id="drop">
                    Joque aqui ou 

                    <a>Selecione</a>
                    <input type="file" name="upl" class="imageup" multiple />
                </div>

                <ul>
                    <!-- The file uploads will be shown here -->
                </ul>

                <input type="hidden" name="pasta" id="pasta" value="<?= $folder ?>" />
                <input type="hidden" id="ff" value="<?= $funcao ?>" />
                <input type="hidden" id="idinput" value="<?= $id ?>" />
                <input type="hidden" name="bancoUsoSendGallery" id="bancoUsoSendGallery" value="<?=PRE?>gallery" />

            </form>

            <span id="acf"></span>
        </div>

        <!-- JavaScript Includes -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.knob.js"></script>

        <!-- jQuery File Upload Dependencies -->
        <script src="assets/js/jquery.ui.widget.js"></script>
        <script src="assets/js/jquery.iframe-transport.js"></script>
        <script src="assets/js/jquery.fileupload.js"></script>

        <!-- Our main JS file -->
        <script src="<?= HOME ?>/js/bootJs.js"></script>
        <script src="assets/js/script.js"></script>
        <script src="<?= HOME ?>/js/boot_functions.js"></script>

    </body>
</html>