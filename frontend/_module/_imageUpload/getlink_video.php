<?php

ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

function CreateFolder($Folder) {
    if (!file_exists('../uploads/' . $Folder) && !is_dir('../uploads/' . $Folder)):
        mkdir('../uploads/' . $Folder, 0755);
    endif;
}

function FolderImg() {
    list($y, $m) = explode('/', date('Y/m'));
    CreateFolder("images");
    CreateFolder("images/{$y}");
    CreateFolder("images/{$y}/{$m}/");
    return "images/{$y}/{$m}/";
}

$link = filter_input(INPUT_POST, 'v', FILTER_DEFAULT);
$folder = filter_input(INPUT_POST, 'pasta', FILTER_DEFAULT);

if (Check::IsImage($link)):

    $generateName = rand(0, 9999) . md5($link) . base64_encode(date("Y-m-d H:i:s"));

    $g['embed'] = FolderImg() . $generateName . '.jpg';
    $g['title'] = str_replace(array('https://', 'http://', 'www.', '.com' . '.net', '.br', '.mp4', '.avi', '/'), array('', '', '', '', '', '', '', '', '-'), $link);

    $dest = "../uploads/" . $g['embed'];
    copy($link, $dest);

    $g['urlname'] = Check::Name($g['title']);
    $g['user_id'] = $_SESSION['userlogin']['id'];
    $g['folder'] = $folder;

    $create = new Create();
    $create->ExeCreate(PRE . "video", $g);

    echo $g['embed'];
endif;