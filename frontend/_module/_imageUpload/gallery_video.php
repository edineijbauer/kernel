<?php

ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

$u = filter_input(INPUT_POST, 'e', FILTER_DEFAULT);
$folder = filter_input(INPUT_POST, 'f', FILTER_DEFAULT);

$read->ExeRead(PRE . "video", "WHERE (user_id=:fg || user_id=0) && folder=:gf ORDER BY id DESC", "fg={$_SESSION['userlogin']['id']}&gf={$folder}");
if ($read->getResult()):

    echo '<div class="box box-5 bg-white border ps-relative border-bottom s11box-4 s8box-3 s6box-3 s4box-1 pointer inhover hovershadow transition-easy" onclick="' . $u . '(\'0\');" id="imageG-0">'
        . '<span class="container pd-medium al-center" style="height: 38px; line-height: 24px; overflow:hidden">Não usar</span>'
        . '</div>';

    foreach ($read->getResult() as $gal):
        echo '<div class="box box-5 bg-white border ps-relative border-bottom s11box-4 s8box-3 s6box-3 s4box-1 pointer inhover hovershadow transition-easy" onclick="' . $u . '(\'' . $gal['embed'] . '\');" id="imageG-'.$gal['id'].'">'
        . '<span class="container pd-medium al-center" style="height: 38px; line-height: 24px; overflow:hidden">' . $gal['title'] . '</span>'
        . '<button class="btn btn-staticwhite radius transition-easy inhover border-redhover pd-smallb font-size09 ps-absolute top right" title="excluir imagem" onclick="excluirVideo('. $gal['id'] .')" style="">x</button>'
        . '</div>';
    endforeach;
    
else:
    echo "<div class='container content8'>";
    WSErro('Opa! Nenuma imagem nesta galeria "'. $folder .'"', WS_INFOR);
    echo "</div>";
endif;