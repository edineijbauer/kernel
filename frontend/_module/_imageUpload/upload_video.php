<?php

ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

function CreateFolder($Folder) {
    if (!file_exists('../uploads/' . $Folder) && !is_dir('../uploads/' . $Folder)):
        mkdir('../uploads/' . $Folder, 0777);
    endif;
}

function CheckFolder($Folder) {
    list($y, $m) = explode('/', date('Y/m'));
    CreateFolder("{$Folder}");
    CreateFolder("{$Folder}/{$y}");
    CreateFolder("{$Folder}/{$y}/{$m}/");
    return $Folder . '/' . $y . '/' . $m . '/';
}

// A list of permitted file extensions
$allowed = array('mp4', 'avi');
$allowedt = array('-mp4', '-avi');
$allowedp = array('.mp4', '.avi');

if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {

    $folder = filter_input(INPUT_POST, 'pasta', FILTER_DEFAULT);

    $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

    if (!in_array(strtolower($extension), $allowed)) {
        echo '{"status":"error"}';
        exit;
    }
    
    $Format = array();
    $Format['a'] = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ&- ';
    $Format['b'] = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUCe__";
    $nome = strtr(utf8_decode($_FILES['upl']['name']), utf8_decode($Format['a']), $Format['b']);

    $local = CheckFolder($folder);
    if (move_uploaded_file($_FILES['upl']['tmp_name'], '../uploads/' . $local . str_replace($allowedt, $allowedp, $nome))) {
        $read = new Read();
        $up = new Update();
        $create = new Create();

        echo '{"status":"success"}';

        $g['embed'] = $local . str_replace($allowedt, $allowedp, $nome);
        $g['title'] = strip_tags(trim(str_replace($allowedp, array("", "", ""), $_FILES['upl']['name'])));
        $g['urlname'] = Check::Name($g['title']);
        $g['user_id'] = $_SESSION['userlogin']['id'];
        $g['folder'] = $folder;

        $create->ExeCreate(PRE . "video", $g);

        echo $g['embed'];
        
        exit;
    }
}

echo '{"status":"error"}';
exit;
