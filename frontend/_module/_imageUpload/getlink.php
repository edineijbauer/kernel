<?php

ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

function CreateFolder($Folder) {
    if (!file_exists('../uploads/' . $Folder) && !is_dir('../uploads/' . $Folder)):
        mkdir('../uploads/' . $Folder, 0755);
    endif;
}

function FolderImg($folder) {
    list($y, $m) = explode('/', date('Y/m'));
    CreateFolder($folder);
    CreateFolder("{$folder}/{$y}");
    CreateFolder("{$folder}/{$y}/{$m}/");
    return "{$folder}/{$y}/{$m}/";
}

$link = filter_input(INPUT_POST, 'v', FILTER_DEFAULT);
$banco = filter_input(INPUT_POST, 'banco', FILTER_DEFAULT);
$folder = filter_input(INPUT_POST, 'folder', FILTER_DEFAULT);

if (Check::IsImage($link)):

    $generateName = rand(0, 9999) . md5($link) . base64_encode(date("Y-m-d H:i:s"));

    $g['cover'] = FolderImg($folder) . $generateName . '.jpg';
    $g['title'] = str_replace(array('https://', 'http://', 'www.', '.com' . '.net', '.br', '.jpg', '.png', '.gif', '.org', '/'), array('', '', '', '', '', '', '', '', '', '', '-'), $link);

    $dest = "../uploads/" . $g['cover'];
    copy($link, $dest);

    $g['urlname'] = Check::Name($g['title']);
    $g['user'] = $_SESSION['userlogin']['id'];
    $g['folder'] = $banco;

    $create = new Create();
    $create->ExeCreate($banco, $g);

    echo $g['cover'];
endif;