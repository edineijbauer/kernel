<?php
ob_start();
require('../_app/Config.inc.php');
$Session = new Session;

$funcao = filter_input(INPUT_POST, 'e', FILTER_DEFAULT);
$id = filter_input(INPUT_POST, 'id', FILTER_DEFAULT);
$banco = filter_input(INPUT_POST, 'banco', FILTER_DEFAULT);

$read = new Read();
$read->ExeRead($banco, "ORDER BY id DESC");
if ($read->getResult()):
    $tpl = new View();
    $tpl->setBase("_imageUpload/assets/tpl");
    foreach ($read->getResult() as $gal):
        $gal['funcao'] = $funcao . "('{$banco}', '{$gal['id']}', '{$id}')";
        $tpl->Show($gal, $tpl->Load("imagens"));
    endforeach;
endif;