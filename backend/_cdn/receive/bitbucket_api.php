<?php
require_once '../../../../vendor/autoload.php';

$bb_user = 'galera.org@gmail.com';
$bb_pass = 'jax(69).';
$repo_slug = 'kernel';
$account_name = 'edineijbauer';

$src = new Bitbucket\API\Repositories\Src();
$src->setCredentials(new Bitbucket\API\Authentication\Basic($bb_user, $bb_pass));

/* 	Titulo Alterado	 */
$commits = new Bitbucket\API\Repositories\Commits();
$commits->setCredentials(new Bitbucket\API\Authentication\Basic($bb_user, $bb_pass));
$commits = json_decode($commits->all($account_name, $repo_slug, array('branch' => 'master'))->getContent());
$commits = $commits->values[0];

$commit = $commits->message;
$hash = substr($commits->hash, 0, 7);

$read = new Read();
$read->ExeRead(PRE . "update_sistem", "ORDER BY date DESC LIMIT 1");
if (!$read->getResult() || ($read->getResult() && $read->getResult()[0]['commit'] != $commit)):

    /* 	Arquivos Alterados	 */
    $changesets = new Bitbucket\API\Repositories\Changesets();
    $changesets->setCredentials(new Bitbucket\API\Authentication\Basic($bb_user, $bb_pass));
    $changesets = json_decode($changesets->get($account_name, $repo_slug, $hash)->getContent());

    foreach ($changesets->files as $changeFiles):
        if (!preg_match('/Connection\.inc/i', $changeFiles->file)):
            if ($changeFiles->type === "modified" || $changeFiles->type === "added"):

                if (preg_match('/\//i', $changeFiles->file)):
                    foreach (explode('/', $changeFiles->file) as $dir):
                        if (!preg_match("/\w+\.\w+/i", $dir)):
                            $diretorio = (!isset($diretorio) ? $dir : $diretorio . "/" . $dir);
                            if (!file_exists('../../' . $diretorio)):
                                mkdir('../../' . $diretorio, 0755);
                            endif;
                        endif;
                    endforeach;
                    unset($diretorio, $dir);
                endif;

                $fp = fopen('../../' . $changeFiles->file, "w");
                $escreve = fwrite($fp, $src->raw($account_name, $repo_slug, $hash, $changeFiles->file)->getContent());
                fclose($fp);

            elseif ($changeFiles->type === "removed" && file_exists('../../' . $changeFiles->file)):
                unlink('../../' . $changeFiles->file);

                if (preg_match('/\//i', $changeFiles->file)):
                    $dir = explode('/', $changeFiles->file);
                    for ($i = count($dir) - 1; $i > 0; $i--):
                        $dir_not = (!isset($dir_not) ? $dir[$i] : $dir[$i] . '/' . $dir_not);
                        $diretorio = str_replace($dir_not, "", $changeFiles->file);
                        if (count(scandir('../../' . $diretorio)) < 3):
                            rmdir('../../' . $diretorio);
                        endif;
                    endfor;
                    unset($diretorio, $dir_not, $dir);
                endif;
            endif;
        endif;
    endforeach;

    $dados['commit'] = $commit;
    $dados['date'] = date("Y-m-d H:i:s");

    $create = new Create();
    $create->ExeCreate(PRE . "update_sistem", $dados);

endif;
