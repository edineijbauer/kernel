<?php
/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 29/03/2017
 * Time: 20:17
 */

ob_start();
require('../../_app/Config.inc.php');
require('../../_app/Helpers/View.class.php');
require('../../_app/Helpers/Check.class.php');
require('../../_app/Conn/Read.class.php');

$email = new Email();
$email->setAssunto("Teste");
$email->setEmail("galera.org@gmail.com");
$email->setNome("Edinei");
$email->setMensagem("teste");

$view = new View();
$read = new Read();
$artigos = "";
$read->ExeRead(PRE . "publicar", "WHERE status = 1 ORDER BY date DESC LIMIT :offset, :limit", "offset=0&limit=10");
if ($read->getResult()):
    foreach ($read->getResult() as $post):
        $read->ExeRead(PRE . "post", "WHERE publicar = :p ", "p={$post['id']}");
        if ($read->getResult()):
            $review = $read->getResult()[0];
            $review['imagem'] = Check::getBanco(PRE . "gallery", (int)$review['imagem'], "cover");
            $review['descricao'] = Check::Words($review['descricao'], 15);
            $artigos .= $view->Retorna($review, $view->Load("artigo"));
        endif;
    endforeach;
endif;

$email->setHtml("<html>{$artigos}</html>");
$email->enviar();