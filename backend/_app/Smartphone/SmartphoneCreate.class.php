<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 11/04/2017
 * Time: 09:26
 */
class SmartphoneCreate {
    private $dados;
    private $extra;
    private $smartphone;

    /**
     * @param mixed $dados
     */
    public function setDados($dados) {
        $this->dados = $dados;
        if ($this->dados):
            $this->start();
        endif;
    }

    private function start() {
        $this->clearDados();
        $this->extractExtraInfo();
        $this->smartphone = $this->dados['smartphone'];
        $this->createSmartphonePart('camera_frontal', 'smart_camera', 'camera_front');
        $this->createSmartphonePart('camera_traseira', 'smart_camera', 'camera_back');
        $this->createSmartphonePart('filmagem_traseira', 'smart_filmadora', 'filmadora');
        $this->createAparencia();
        $this->createSmartphonePart('processador', 'smart_processador', 'processador');

        $this->createNucleos();
        $this->createMemorias();

        $this->createSmartphonePart('tela', 'smart_tela', 'tela');
        $this->createSmartphonePart('bateria', 'smart_bateria', 'bateria');
        $this->createSmartphonePartUnique('conectividade', 'smart_conectividade', 'conectividade');

        $this->createLinkExtra();
        $this->createSmartphone();

    }

    private function createSmartphonePartUnique($part, $table, $dado) {
        if (isset($this->dados[$dado])):
            $this->smartphone[$part] = $this->createBancoUnique($table, $this->dados[$dado]);
        endif;
    }

    private function createSmartphonePart($part, $table, $dado) {
        if (isset($this->dados[$dado])):
            $this->smartphone[$part] = $this->createBanco($table, $this->dados[$dado]);
        endif;
    }

    private function createBancoUnique($table, $dados) {
        $front = new Banco($table);
        $front->loadArray($dados);
        return $front->save();
    }

    private function createBanco($table, $dados) {
        $front = new Banco($table);
        $front->load('title', $dados['title']);
        $front->setDados($dados);
        return $front->save();
    }

    private function createMemorias() {
        if (isset($this->dados['ram']['capacidade'])):
            foreach ($this->dados['ram']['capacidade'] as $i => $ram):
                $dadosRam['capacidade'] = $ram;
                $dadosRam['title'] = $dadosRam['capacidade'] . "GB";
                $memorias["memoria_ram_{$i}"] = $this->createBanco('smart_memoria', $dadosRam);
                if (!isset($memorias['title'])):
                    $memorias['title'] = $dadosRam['capacidade'];
                endif;
            endforeach;

            if (isset($this->dados['armazenamento']['capacidade'])):
                foreach ($this->dados['armazenamento']['capacidade'] as $i => $rom):
                    $dadosRom['capacidade'] = $rom;
                    $dadosRom['title'] = $dadosRom['capacidade'] . "GB";
                    $memorias["armazenamento_{$i}"] = $this->createBanco('smart_memoria', $dadosRom);

                    if ($i === 0):
                        $memorias['title'] .= '/' . $dadosRom['capacidade'];
                    endif;
                endforeach;
            endif;

            if (isset($this->dados['armazenamento_expansivel']['capacidade'])):
                $this->dados['armazenamento_expansivel']['title'] = $this->dados['armazenamento_expansivel']['capacidade'] . "GB";
                $memorias['armazenamento_expansivel'] = $this->createBanco("smart_memoria", $this->dados['armazenamento_expansivel']);
            endif;

            $this->smartphone['memorias'] = $this->createBancoUnique('smart_memorias', $memorias);
        endif;
    }

    private function createAparencia() {
        if (isset($this->dados['aparencia_fisica'])):
            $front = new Banco('smart_aparencia_fisica');
            $front->loadArray(array("peso" => $this->dados['aparencia_fisica']['peso'], "tamanho" => $this->dados['aparencia_fisica']['tamanho']));
            if (!empty($this->dados['aparencia_fisica']['profundidade'])):
                $front->profundidade = $this->dados['aparencia_fisica']['profundidade'];
            endif;
            $this->smartphone['aparencia_fisica'] = $front->save();
        endif;
    }

    private function createNucleos() {
        if (isset($this->dados['nucleos']['title'])):
            $this->dados['nucleos']['processador'] = $this->smartphone['processador'];
            $this->createBanco('smart_nucleos', $this->dados['nucleos']);
        endif;
    }

    private function createLinkExtra() {
        $read = new Read();
        $create = new Create();
        foreach ($this->extra as $name => $item):
            $smartNome = ($name === "camera" ? "camera_traseira" : ($name === "fisico" ? "aparencia_fisica" : ($name === "sensores" ? "conectividade" : $name)));
            $banco = new Banco('extra_' . $name);
            foreach ($item as $it):
                $banco->load('title', $it);
                $link['extra_' . $name] = (int)$banco->save();
                $link['smart_' . $name] = (int)$this->smartphone[$smartNome];

                $read->ExeRead(PRE . "link_extra_" . $name, "WHERE extra_{$name} = :e && smart_{$name} = :s", "e={$link['extra_' . $name]}&s={$link['smart_' . $name]}");
                if (!$read->getResult()):
                    $create->ExeCreate(PRE . "link_extra_" . $name, $link);
                endif;

                unset($link, $linkExtra);

            endforeach;
        endforeach;
    }

    private function createSmartphone() {
        $banco = new Banco("smartphone");
        $banco->load("title", $this->smartphone['title']);
        $banco->setDados($this->smartphone);
        $banco->save();
    }

    private function extractExtraInfo() {
        if (isset($this->dados['camera_back']['caracteristicas_extras'])):
            $this->extra['camera'] = $this->dados['camera_back']['caracteristicas_extras'];
        endif;

        if (isset($this->dados['aparencia_fisica']['caracteristicas_extras'])):
            $this->extra['fisico'] = $this->dados['aparencia_fisica']['caracteristicas_extras'];
        endif;

        if (isset($this->dados['tela']['caracteristicas_extras'])):
            $this->extra['tela'] = $this->dados['tela']['caracteristicas_extras'];
        endif;

        if (isset($this->dados['conectividade']['caracteristicas_extras'])):
            $this->extra['sensores'] = $this->dados['conectividade']['sensores'];
        endif;

        if (isset($this->dados['conectividade']['caracteristicas_extras'])):
            $this->extra['conectividade'] = $this->dados['conectividade']['caracteristicas_extras'];
        endif;

        unset($this->dados['camera_back']['caracteristicas_extras'], $this->dados['aparencia_fisica']['caracteristicas_extras'], $this->dados['tela']['caracteristicas_extras'], $this->dados['conectividade']['sensores'], $this->dados['conectividade']['caracteristicas_extras']);
    }

    private function clearDados() {
        foreach ($this->dados as $table => $dado):
            if (empty($dado)):
                unset($this->dados[$table]);
            else:
                if (is_array($dado)):
                    foreach ($dado as $name => $value):
                        if (empty($value)):
                            unset($this->dados[$table][$name]);
                            if (empty($this->dados[$table])):
                                unset($this->dados[$table]);
                            endif;
                        endif;
                    endforeach;
                endif;
            endif;
        endforeach;
    }
}