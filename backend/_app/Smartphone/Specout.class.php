<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 09:37
 */
class Specout {
    private $camera_front;
    private $camera_back;
    private $filmadora;
    private $aparencia_fisica;
    private $processador;
    private $nucleos;
    private $ram;
    private $tela;
    private $bateria;
    private $armazenamento;
    private $armazenamento_expansivel;
    private $conectividade;
    private $preco;
    private $smartphone;
    private $review;
    private $cover;

    /**
     * Specout constructor.
     * @param $url
     */
    public function __construct() {
        $this->start();
    }

    private function getDados() {
        return array("camera_front" => $this->camera_front, "camera_back" => $this->camera_back, "filmadora" => $this->filmadora, "aparencia_fisica" => $this->aparencia_fisica, "processador" => $this->processador, "nucleos" => $this->nucleos, "ram" => $this->ram, "tela" => $this->tela, "bateria" => $this->bateria, "armazenamento" => $this->armazenamento, "armazenamento_expansivel" => $this->armazenamento_expansivel, "conectividade" => $this->conectividade, "preco" => $this->preco, "smartphone" => $this->smartphone, "smartphone_gallery" => $this->cover, "review" => $this->review);
    }

    private function start() {
        $craw = new SpecoutCraw();
        $site = file_get_contents($craw->getUrl());

        $this->review['geral'] = $this->getNota($site);
        $this->review['geral_reviews'] = $this->getReviews($site);
        $this->review['num_reviews'] = $this->getNumReviews($site);
        $this->cover = $this->getCover($site);
        $this->getSmartphone($site);
        $this->checkLinkGearBest($site);

        if (preg_match('/class=\"ddc-main-sections\">/i', $site) && preg_match('/class=\"detail-section ddc-ref-section/i', $site)):
            $site = '<div>' . explode('<div class="detail-section ddc-ref-section', explode('<div class="ddc-main-sections">', $site)[1])[0];
            $this->setDados($site);

            $create = new SmartphoneCreate();
            $create->setDados($this->getDados());
        endif;
    }

    private function setDados($site) {
        $this->getOverview($site);
        $this->getCamera($site);
        $this->getDisplay($site);
        $this->getBattery($site);
        $this->getDimensao($site);
        $this->getCpuRam($site);
        $this->getStorage($site);
        $this->getConectividade($site);
    }

    private function getOverview($site) {
        if (preg_match('/class=\"id-0/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-0', $site)[1])[0] . '</section>';
            $overview = new Overview($site);
            $this->smartphone['sistema_operacional'] = $overview->getOs();
            $this->smartphone['lancamento'] = $overview->getRelease();
            $this->smartphone['status'] = Check::statusPadrao($overview->getStatus());
            $this->smartphone['custom_rom'] = $overview->getRom();
            $this->preco['valor'] = $overview->getPrice();
        endif;
    }

    private function getCamera($site) {
        if (preg_match('/class=\"id-4/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-4', $site)[1])[0] . '</section>';
            $camera = new Camera($site);
            $this->camera_back = $camera->getDadosBack();
            $this->camera_front = $camera->getDadosFront();
            $this->filmadora = $camera->getDadosVideo();
        endif;
    }

    private function getDisplay($site) {
        if (preg_match('/class=\"id-5/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-5', $site)[1])[0] . '</section>';
            $display = new Display($site);
            $this->tela = $display->getDados();
        endif;
    }

    private function getBattery($site) {
        if (preg_match('/class=\"id-7/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-7', $site)[1])[0] . '</section>';
            $battery = new Battery($site);
            $this->bateria = $battery->getDados();
        endif;
    }

    private function getDimensao($site) {
        if (preg_match('/class=\"id-8/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-8', $site)[1])[0] . '</section>';
            $dimensao = new Dimension($site);
            $this->aparencia_fisica = $dimensao->getDados();
        endif;
    }

    private function getCpuRam($site) {
        if (preg_match('/class=\"id-10/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-10', $site)[1])[0] . '</section>';
            $cpu = new Cpu($site);
            $this->processador = $cpu->getProcessador();
            $this->ram = $cpu->getRam();
            $this->nucleos = $cpu->getNucleo();
        endif;
    }

    private function getStorage($site) {
        if (preg_match('/class=\"id-11/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-11', $site)[1])[0] . '</section>';
            $storage = new Storage($site);
            $this->armazenamento = $storage->getStorage();
            $this->armazenamento_expansivel = $storage->getExternal();
        endif;
    }

    private function getConectividade($site) {
        if (preg_match('/class=\"id-8/i', $site)):
            $site = '<section class="' . explode('</section>', explode('<section class="id-14', $site)[1])[0] . '</section>';
            $conection = new Conection($site);
            $this->conectividade = $conection->getDados();
        endif;
    }

    private function checkLinkGearBest($site) {
        if (preg_match('/href=\"http:\/\/www\.gearbest\.com\/cell-phones\//i', $site)):
            $this->preco['link'] = 'http://www.gearbest.com/cell-phones/' . explode('"', explode('href="http://www.gearbest.com/cell-phones/', $site)[1])[0];
            if (preg_match('/\?/i', $this->preco['link'])):
                $this->preco['link'] = explode('?', $this->preco['link'])[0];
            endif;
        endif;
    }

    private function getSmartphone($site) {
        if (preg_match('/detail={/i', $site)):
            $smart = json_decode('{' . explode('};', explode('detail={', $site)[1])[0] . '}');
            $this->smartphone['title'] = $smart->title;
            $this->smartphone['urlname'] = Check::Name($this->smartphone['title']);
            $this->smartphone['imagem'] = $this->cover[0]->src;
            unset($this->cover[0]);
        endif;

        if (isset($smart->reviews)):
            foreach ($smart->reviews as $i => $rev):
                $this->review[$i]['comment'] = $rev->review->text;
                $this->review[$i]['date'] = $rev->review->date;
                $this->review[$i]['nota'] = (int)$rev->rating->main * 2;
                $this->review[$i]['nome'] = $rev->user->name;
            endforeach;
        endif;
    }

    private function getCover($site) {
        if (preg_match('/dd_imgs=/i', $site)):
            $imgs = json_decode('{' . explode('};', explode('dd_imgs={', $site)[1])[0] . '}');
            $imgs = $imgs->main_header;
            return $imgs;
        else:
            return null;
        endif;
    }

    private function getNumReviews($site) {
        if (preg_match('/<div class=\"ur-num-rate link \">/i', $site)):
            return (int)(!preg_match('/class=\"ur-num-rate link empty\"/i', $site) ? str_replace(array('user reviews'), '', explode("</div>", explode('<div class="ur-num-rate link ">', $site)[1])[0]) : 0);
        else:
            return null;
        endif;
    }

    private function getReviews($site) {
        if (preg_match('/<div class=\"ur-avg\">/i', $site)):
            return (float)number_format(explode('</div>', explode('<div class="ur-avg">', $site)[1])[0] * 2, 1);
        else:
            return null;
        endif;
    }

    private function getNota($site) {
        if (preg_match('/class=\"js-vz-u\" data-u=\"23780\"/i', $site)):
            $nota = "<div>" . explode("</div>", explode("class=\"js-vz-u\" data-u=\"23780\"", $site)[1])[0];
            $nota = new Semantica($nota);
            $nota->getTagByName("noscript");
            return (float)number_format($nota->getConteudo() * 2, 1);
        else:
            return null;
        endif;
    }
}