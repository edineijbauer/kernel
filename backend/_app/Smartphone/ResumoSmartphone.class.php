<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 13/03/2017
 * Time: 10:10
 */
class ResumoSmartphone {

    private $id;
    private $smartphone;
    private $imagem;
    private $especificacao = "";
    private $resumo;

    /**
     * @param mixed $id
     */
    public function setId($id) {
        if (!$this->smartphone):
            $read = new Read();
            $read->ExeRead(PRE . "smartphone", "WHERE id=:id", "id={$id}");
            if ($read->getResult()):
                $this->id = $id;
                $this->smartphone = $read->getResult()[0];
                $this->start();
            endif;
        endif;
    }

    /**
     * @param mixed $smartphone
     */
    public function setSmartphone($smartphone) {
        if (!$this->id):
            $this->smartphone = $smartphone;
            $this->id = $this->smartphone['id'];
            $this->start();
        endif;
    }

    /**
     * @return mixed
     */
    public function getSmartphone() {
        return $this->smartphone;
    }

    /**
     * @return mixed
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * @return string
     */
    public function getEspecificacao() {
        return $this->especificacao;
    }

    /**
     * @return string
     */
    public function getResumo() {
        $view = new View();
        return $view->Retorna($this->resumo, $view->Load("especificacao/resumo"));
    }

    public function getResumoDados() {
        return $this->resumo;
    }

    private function start() {
        if ($this->id && $this->smartphone):
            $this->setPadraoResumo();
            $this->setImagem($this->smartphone['imagem']);
            $this->setEspecificacao();
            $this->setHardware();
            $this->setPreco();
            $this->saveSmartphoneResumo();
        endif;
    }

    private function saveSmartphoneResumo() {
        $dados['processador'] = $this->resumo['processador'];
        $dados['memoria'] = $this->resumo['memoria'];
        $dados['tela'] = $this->resumo['tela'];
        $dados['bateria'] = $this->resumo['bateria'];
        $dados['camera'] = $this->resumo['camera'];
        $dados['preco'] = $this->resumo['preco'];
        $resumo = new Banco("smartphone_resumo");
        $resumo->load("smartphone", $this->smartphone['id']);
        $resumo->setDados($dados);
        $resumo->save();
    }

    private function setPadraoResumo() {
        $this->resumo['info']['tela'] = "";
        $this->resumo['info']['processador'] = "";
        $this->resumo['info']['camera_back'] = "";
        $this->resumo['info']['camera_front'] = "";
        $this->resumo['info']['bateria'] = "";
        $this->resumo['info']['memoria_ram'] = "";
        $this->resumo['info']['memoria_rom'] = "";
    }

    private function setPreco() {
        $read = new Read();
        $read->ExeRead(PRE . "preco", "WHERE smartphone = :sm ORDER BY valor LIMIT 1", "sm={$this->id}");
        $this->resumo['preco'] = ($read->getResult() ? round($read->getResult()[0]['valor'] * DOLAR) : '-');
        $precoInfluencia = (!$this->resumo['preco'] ? 0 : ($this->resumo['preco'] > 1000 ? ($this->resumo['preco'] > 1500 ? 12 : 10) : ($this->resumo['preco'] > 600 ? 8 : ($this->resumo['preco'] > 400 ? 7 : ($this->resumo['preco'] < 50 ? 0 : 5)))));

        $precoInfluencia = $this->applyMarcaInfluenciaSobrePreco($precoInfluencia);
        $this->setValorInfluenciaResumo($precoInfluencia);

        $this->resumo['dolar'] = ($read->getResult() ? $read->getResult()[0]['valor'] : '-');
        $this->resumo['link'] = ($read->getResult() ? $read->getResult()[0]['link'] : '-');
    }

    private function applyMarcaInfluenciaSobrePreco($preco) {
        $read = new Read();
        $read->ExeRead(PRE . "marca_avaliacao", "WHERE marca=:id", "id={$this->smartphone['marca']}");
        if ($read->getResult()):
            return ((($read->getResult()[0]['qualidade'] + $read->getResult()[0]['acabamento'] + $read->getResult()[0]['custo_beneficio'] + $read->getResult()[0]['suporte']) / 4) + 1 + $preco) / 2;
        endif;
        return $preco;
    }

    private function setValorInfluenciaResumo($inf) {
        $preco_beneficio['pontos'] = 25;

        if (isset($this->resumo['processador']) && $this->resumo['processador'] !== "Sem informação"):
            $this->resumo['processador'] = ($inf > 0 ? ($this->resumo['processador'] + $inf) / 2 : $this->resumo['processador']);
            $preco_beneficio['pontos'] += $this->resumo['processador'] - 5;
            $this->resumo['processador'] = $this->getReview($this->resumo['processador'], $this->resumo['info']['processador']);
        endif;


        if (isset($this->resumo['memoria']) && $this->resumo['memoria'] !== "Sem informação"):
            $this->resumo['memoria'] = ($inf > 0 ? (($this->resumo['memoria'] * 2) + $inf) / 3 : $this->resumo['memoria']);
            $preco_beneficio['pontos'] += $this->resumo['memoria'] - 5;
            $this->resumo['memoria'] = $this->getReview($this->resumo['memoria'], $this->resumo['info']['memoria_ram'] . "GB / " . $this->resumo['info']['memoria_rom'] . "GB");
        endif;

        if (isset($this->resumo['camera']) && $this->resumo['camera'] !== "Sem informação"):
            $this->resumo['camera'] = ($inf > 0 ? ($this->resumo['camera'] + $inf) / 2 : $this->resumo['camera']);
            $preco_beneficio['pontos'] += $this->resumo['camera'] - 5;
            $this->resumo['camera'] = $this->getReview($this->resumo['camera'], $this->resumo['info']['camera_back'] . "MP / " . $this->resumo['info']['camera_front'] . "MP");
        endif;

        if (isset($this->resumo['bateria']) && $this->resumo['bateria'] !== "Sem informação"):
            $this->resumo['bateria'] = ($inf > 0 ? (($this->resumo['bateria'] * 3) + $inf) / 4 : $this->resumo['bateria']);
            $preco_beneficio['pontos'] += $this->resumo['bateria'] - 5;
            $this->resumo['bateria'] = $this->getReview($this->resumo['bateria'], $this->resumo['info']['bateria'] . " mAh");
        endif;

        if (isset($this->resumo['tela']) && $this->resumo['tela'] !== "Sem informação"):
            $this->resumo['tela'] = ($inf > 0 ? (($this->resumo['tela'] * 2) + $inf) / 3 : $this->resumo['tela']);
            $preco_beneficio['pontos'] += $this->resumo['tela'] - 5;
            $this->resumo['tela'] = $this->getReview($this->resumo['tela'], $this->resumo['info']['tela']);
        endif;

        $this->savePrecoBeneficio($preco_beneficio['pontos'] / 5);

        unset($this->resumo['info']);
    }

    private function savePrecoBeneficio($preco_beneficio) {

        $preco_beneficio['smartphone'] = $this->smartphone['id'];
        $preco_beneficio['valor'] = $this->resumo['preco'];

        $save = new Banco("preco_beneficio");
        $save->load("smartphone", $preco_beneficio['smartphone']);
        $save->setDados($preco_beneficio);
        $save->save();
    }

    private function setImagem($id) {
        $read = new Read();
        $read->ExeRead(PRE . "gallery", "WHERE id =:ji", "ji= {$id}");
        if ($read->getResult()):
            $this->imagem = $read->getResult()[0];
        endif;
    }

    private function setEspecificacao() {
        $smart['marca'] = Check::getBanco(PRE . "marca", (int)$this->smartphone['marca'], "title");
        $smart['sistema_operacional'] = $this->smartphone['sistema_operacional'];
        $smart['views'] = $this->smartphone['views'];
        $smart['caracteristicas_extras'] = $this->smartphone['caracteristicas_extras'];

        $view = new View();
        $this->especificacao .= $view->Retorna($smart, $view->Load("especificacao/smartphone"));
    }

    private function setResumoMemoria($capacidade) {
        $capacidade = (int)$capacidade;
        $this->resumo['memoria'] = ($capacidade > 2 ? ($capacidade > 3 ? 10 : 8) : ($capacidade > 1 ? 6 : ($capacidade === 1 ? 4 : 2)));
    }

    private function setResumoMemoriaArmazenamento($capacidade) {
        $capacidade = (int)$capacidade;
        $this->resumo['memoria'] = (($capacidade > 16 ? ($capacidade > 32 ? 10 : 8) : ($capacidade > 8 ? 6 : ($capacidade === 8 ? 4 : 2))) + (isset($this->resumo['memoria']) ? $this->resumo['memoria'] : 5)) / 2;
    }

    private function getPotenciaProcessador($nucleo) {
        $this->setResumoBateriaCortex($nucleo['cortex']);

        $velocidade = $nucleo['velocidade'] * $nucleo['nucleo'];
        $velocidade *= ($nucleo['cortex'] > 60 ? ($nucleo['cortex'] > 80 ? ($nucleo['cortex'] > 100 ? 1.2 : 1.13) : 1.07) : ($nucleo['cortex'] < 30 ? 0.9 : 1));
        return $velocidade * ($nucleo['nucleo'] > 2 ? ($nucleo['nucleo'] > 4 ? 0.6 : 1) : ($nucleo['nucleo'] > 1 ? 1.1 : 1.4));
    }

    private function convertNucleoNome($nucleo) {
        switch ($nucleo):
            case 1:
                return "Single-Core";
                break;
            case 2:
                return "Dual-Core";
                break;
            case 4:
                return "Quad-Core";
                break;
            case 6:
                return "Six-Core";
                break;
            case 8:
                return "Octa-Core";
                break;
            case 10:
                return "Deca-Core";
                break;
        endswitch;

        return "Dual-Core";
    }

    private function setResumoProcessador($processador) {
        $nucleosVelocidade = "";
        $read = new Read();
        $read->ExeRead(PRE . "smart_nucleos", "WHERE processador = :p", "p={$processador['id']}");
        if ($read->getResult()):
            foreach ($read->getResult() as $n):
                $this->resumo['processador'] = (isset($this->resumo['processador']) ? $this->resumo['processador'] * 0.6 : 0) + $this->getPotenciaProcessador($n);
                $nucleosVelocidade .= (empty($nucleosVelocidade) ? "" : " + ") . "{$this->convertNucleoNome($n['nucleo'])} {$n['velocidade']} GHz";
            endforeach;
        endif;

        $this->resumo['info']['processador'] = $processador['title'] . ' ' . $nucleosVelocidade;

        if (isset($this->resumo['processador'])):
            $this->setResumoBateriaNucleos($this->resumo['processador']);

            $this->resumo['processador'] += ($processador['bits'] > 32 ? 0.4 : 0);

            $this->resumo['processador'] = $this->resumo['processador'] > 8 ? ($this->resumo['processador'] > 12 ? 10 : 8) : ($this->resumo['processador'] > 6 ? 6 : ($this->resumo['processador'] < 4.5 ? 2 : 4));
        endif;
        return $nucleosVelocidade;
    }

    private function setResumoTela($tela) {
        $this->resumo['info']['tela'] = $tela['tamanho'] . '" ' . $tela['resolucao'];

        $tela['resolucao'] = $this->getResolution($tela['resolucao']);
        $review = (($tela['tamanho'] > 5.4) ? ($tela['resolucao'] > 1080 ? 10 : ($tela['resolucao'] > 720 ? 8 : 6)) : ($tela['tamanho'] > 4.6 ? ($tela['resolucao'] > 1080 ? 10 : ($tela['resolucao'] > 720 ? 8 : 6)) : ($tela['tamanho'] > 4 ? ($tela['resolucao'] > 480 ? 8 : 4) : 2)));
        return $review;
    }

    private function setResumoBateriaCortex($cortex) {
        $this->resumo['bateria'] = ($cortex > 100 ? -1 : ($cortex > 80 ? -0.7 : ($cortex > 60 ? -0.4 : ($cortex > 40 ? 0 : 0.5))));
    }

    private function setResumoBateriaNucleos($velocidade) {
        $this->resumo['bateria'] += ($velocidade > 8 ? ($velocidade > 12 ? ($velocidade > 15 ? -0.6 : -0.4) : -0.1) : ($velocidade < 5 ? 0.7 : 0.3));
    }

    private function setResumoBateria($bateria, $resolucao) {
        $this->resumo['info']['bateria'] = $bateria['capacidade'];

        $cap = $bateria['capacidade'];
        if ($resolucao > 1080):
            $review = ($cap > 4500 ? 10 : ($cap > 4000 ? 8 : ($cap > 3000 ? 6 : ($cap < 2500 ? 2 : 4))));
        elseif ($resolucao === 1080):
            $review = ($cap > 4000 ? 10 : ($cap > 3500 ? 8 : ($cap > 2500 ? 6 : ($cap < 2200 ? 2 : 4))));
        elseif ($resolucao === 720):
            $review = ($cap > 3500 ? 10 : ($cap > 2800 ? 8 : ($cap > 2400 ? 6 : ($cap < 2000 ? 2 : 4))));
        else:
            $review = ($cap > 3000 ? 10 : ($cap > 2500 ? 8 : ($cap > 2000 ? 6 : ($cap < 1500 ? 2 : 4))));
        endif;
        if (isset($this->resumo['bateria'])):
            $this->resumo['bateria'] += $review + ($bateria['quick_charge'] ? 0.5 : 0);
        endif;
    }

    private function setResumoCamera($id, $dados) {
        if ($id === "camera-Traseira"):
            $this->resumo['info']['camera_back'] = $dados['megapixel'];

            $dados['flash'] = strtolower($dados['flash']);
            $this->resumo['camera'] = ($dados['megapixel'] > 13 ? ($dados['megapixel'] > 16 ? 10 : 8) : ($dados['megapixel'] > 8 ? 6 : ($dados['megapixel'] < 8 ? 2 : 4)));
            if (!empty($dados['aperture_size'])):
                $dados['aperture_size'] = (float)str_replace(array("F", "f"), "", $dados['aperture_size']);
                $this->resumo['camera'] = (($dados['aperture_size'] < 2 ? ($dados['aperture_size'] < 1.8 ? 10 : 8) : ($dados['aperture_size'] < 2.3 ? 6 : ($dados['aperture_size'] > 2.5 ? 2 : 4))) + (isset($this->resumo['camera']) ? $this->resumo['camera'] * 2 : 0)) / (isset($this->resumo['camera']) ? 3 : 1);
            endif;
            $this->resumo['camera'] += (!empty($dados['zoom']) ? (preg_match('/[otico|optico|ótico|óptico]/i', $dados['zoom']) ? 1 : (preg_match('/[4x|8x|digital]/i', $dados['zoom']) ? 0.5 : 0)) : 0);
            $this->resumo['camera'] += (!empty($dados['flash']) && preg_match('/[dual-tone|dualtone]/i', $dados['flash']) ? 0.7 : (!empty($dados['flash']) && preg_match('/[dual-flash|dualflash]/i', $dados['flash']) ? 0.3 : 0));

        elseif ($id === "filma-Traseira"):
            $dados['resolucao'] = $this->getResolution($dados['resolucao']);
            $this->resumo['camera'] = (($dados['resolucao'] > 1080 ? ($dados['resolucao'] > 1440 ? 10 : 8) : ($dados['resolucao'] > 720 ? 6 : ($dados['resolucao'] < 720 ? 2 : 4))) + (isset($this->resumo['camera']) ? $this->resumo['camera'] : 0)) / (isset($this->resumo['camera']) ? 2 : 1);
            $this->resumo['camera'] = (($dados['fps'] > 60 ? ($dados['fps'] > 120 ? 10 : 8) : ($dados['fps'] > 30 ? 6 : ($dados['fps'] < 30 ? 2 : 4))) + (isset($this->resumo['camera']) ? $this->resumo['camera'] : 0)) / (isset($this->resumo['camera']) ? 2 : 1);
            $this->resumo['camera'] += (!empty($dados['camera_lenta']) ? 1 : 0);

        elseif ($id === "filma-Frontal"):
            $dados['resolucao'] = $this->getResolution($dados['resolucao']);
            $this->resumo['camera'] = (($dados['resolucao'] > 720 ? 10 : ($dados['resolucao'] > 480 ? 6 : 2)) + (isset($this->resumo['camera']) ? $this->resumo['camera'] : 0)) / (isset($this->resumo['camera']) ? 2 : 1);
            $this->resumo['camera'] = (($dados['fps'] > 30 ? ($dados['fps'] > 60 ? 10 : 8) : ($dados['fps'] == 30 ? 6 : 2)) + (isset($this->resumo['camera']) ? $this->resumo['camera'] : 0)) / (isset($this->resumo['camera']) ? 2 : 1);
            $this->resumo['camera'] += (!empty($dados['camera_lenta']) ? 4 : 0);

        else:
            $this->resumo['info']['camera_front'] = $dados['megapixel'];
            if (!empty($dados['aperture_size'])):
                $dados['aperture_size'] = (float)str_replace(array("F", "f"), "", $dados['aperture_size']);
                $this->resumo['camera'] = (($dados['aperture_size'] < 2 ? ($dados['aperture_size'] < 1.8 ? 10 : 8) : ($dados['aperture_size'] < 2.3 ? 6 : ($dados['aperture_size'] > 2.5 ? 2 : 4))) + (isset($this->resumo['camera']) ? $this->resumo['camera'] * 2 : 0)) / (isset($this->resumo['camera']) ? 3 : 1);
            endif;
            $this->resumo['camera'] = (($dados['megapixel'] > 3 ? ($dados['megapixel'] > 5 ? 10 : 8) : ($dados['megapixel'] > 2 ? 6 : ($dados['megapixel'] < 2 ? 2 : 4))) + (isset($this->resumo['camera']) ? $this->resumo['camera'] : 5)) / 2;
            $this->resumo['camera'] += (!empty($dados['flash']) ? 1 : 0);

        endif;
    }

    private function setProcessador() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_processador", "WHERE id=:h", "h={$this->smartphone['processador']}");
        if ($read->getResult()):
            $dados = $read->getResult()[0];
            $dados['nucleos_velocidade'] = $this->setResumoProcessador($dados);
            $view = new View();
            $this->especificacao .= $view->Retorna($dados, $view->Load("especificacao/processador"));
        endif;
    }

    private function setMemorias() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_memorias", "WHERE id=:h", "h={$this->smartphone['memorias']}");
        if ($read->getResult()):
            $hardware = $read->getResult()[0];
            $view = new View();

            $read->ExeRead(PRE . "smart_memoria", "WHERE id=:h", "h={$hardware['memoria_ram']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "memória ram")), $view->Load("especificacao/memoria"));
                $this->setResumoMemoria($read->getResult()[0]['capacidade']);
                $this->resumo['info']['memoria_ram'] = $read->getResult()[0]['capacidade'];
            else:
                $this->resumo['memoria'] = "Sem informação";
            endif;

            $read->setPlaces("h={$hardware['armazenamento']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "armazenamento")), $view->Load("especificacao/memoria"));
                $this->setResumoMemoriaArmazenamento($read->getResult()[0]['capacidade']);
                $this->resumo['info']['memoria_rom'] = $read->getResult()[0]['capacidade'];
            else:
                $this->resumo['memoria'] = "Sem informação";
            endif;

            $read->setPlaces("h={$hardware['armazenamento_expansivel']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "armazenamento expansível")), $view->Load("especificacao/memoria"));
            endif;
        else:
            $this->resumo['memoria'] = "Sem informação";
        endif;
    }

    private function setTela() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_tela", "WHERE id=:h", "h={$this->smartphone['tela']}");
        if ($read->getResult()):
            $view = new View();
            $this->resumo['tela'] = $this->setResumoTela($read->getResult()[0]);
            $this->resumo['resolucao'] = $this->getResolution($read->getResult()[0]['resolucao']);
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/tela"));
        endif;

    }

    private function setBateria() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_bateria", "WHERE id=:h", "h={$this->smartphone['bateria']}");
        if ($read->getResult()):
            $view = new View();
            if (isset($this->resumo['resolucao'])):
                $this->setResumoBateria($read->getResult()[0], $this->resumo['resolucao']);
            endif;
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/bateria"));
        endif;
    }

    private function setCamera($title, $id) {
        $read = new Read();
        $read->ExeRead(PRE . "smart_camera", "WHERE id=:h", "h={$id}");
        if ($read->getResult()):
            $this->setResumoCamera("camera-{$title}", $read->getResult()[0]);
            $view = new View();
            $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "Câmera {$title}")), $view->Load("especificacao/camera"));
        endif;
    }

    private function setFilmagem($title, $id) {
        $read = new Read();
        $read->ExeRead(PRE . "smart_filmadora", "WHERE id=:h", "h={$id}");
        if ($read->getResult()):
            $this->setResumoCamera("filma-{$title}", $read->getResult()[0]);
            $view = new View();
            $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "Filmagem {$title}")), $view->Load("especificacao/filmagem"));
        endif;
    }

    private function setConectividade() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_conectividade", "WHERE id=:h", "h={$this->smartphone['conectividade']}");
        if ($read->getResult()):
            $view = new View();
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/conectividade"));
        endif;
    }

    private function setHardware() {
        $this->setProcessador();
        $this->setMemorias();
        $this->setTela();
        $this->setBateria();

        if ($this->smartphone['camera_traseira']):
            $this->setCamera("Traseira", $this->smartphone['camera_traseira']);
        endif;
        if ($this->smartphone['filmagem_traseira']):
            $this->setFilmagem("Traseira", $this->smartphone['filmagem_traseira']);
        endif;
        if ($this->smartphone['camera_frontal']):
            $this->setCamera("Frontal", $this->smartphone['camera_frontal']);
        endif;
        if ($this->smartphone['filmagem_frontal']):
            $this->setFilmagem("Frontal", $this->smartphone['filmagem_frontal']);
        endif;
        $this->setConectividade();
    }

    private function getReview($value, $info) {
        $value = number_format($value, 1, '.', '');
        $value = $value > 10 ? 10 : $value;

        if ($value >= 8.5):
            $value = $value == "10.0" ? 10 : $value;
            return "<b class='color-terceary'>{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}</b>";
        elseif ($value >= 4.5):
            return "{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}";
        else:
            return "<span class='color-tomato'>{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}</span>";
        endif;
    }

    private function getResolution($resolucao) {
        $resolucao = strtolower($resolucao);
        if ($resolucao === "fullhd" || $resolucao === "full hd" || $resolucao === "1080p" || $resolucao === "1080"):
            return 1080;
        elseif ($resolucao === "hd" || $resolucao === "720p" || $resolucao === "720"):
            return 720;
        elseif ($resolucao === "quadhd" || $resolucao === "quad hd" || $resolucao === "2k" || $resolucao === "1440p" || $resolucao === "1440"):
            return 1440;
        elseif ($resolucao === "4k" || $resolucao === "2160p" || $resolucao === "2160"):
            return 2160;
        else:
            return 480;
        endif;
    }

}