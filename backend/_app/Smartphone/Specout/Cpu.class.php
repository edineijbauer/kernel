<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Cpu extends BoxValidation {

    private $htmlContent;
    private $processador;
    private $ram;
    private $nucleo;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("CPU & RAM", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
                $this->finalizaTratamento();
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getProcessador() {
        return $this->processador;
    }

    /**
     * @return mixed
     */
    public function getNucleo() {
        return $this->nucleo;
    }

    /**
     * @return mixed
     */
    public function getRam() {
        return $this->ram;
    }

    private function getInfo($site) {
        $this->getCpu();
        $this->getBaseClock();

        for ($i = 1; $i < 6; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function getCpu() {
        $site = parent::getSite();
        $site->getTagByClass("srp-table-div");
        $content = new Semantica($site->getConteudo());
        $content->getTagByName("td", 2);
        $this->processador['title'] = strip_tags($content->getConteudo());
        $content->getTagByName("td", 3);
        $this->processador['gpu'] = strip_tags($content->getConteudo());
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/CPU/i', $content)):
                $this->processador['title'] = $dados->getConteudo();

            elseif (preg_match('/Processor Cores/i', $content)):
                $this->nucleo['nucleo'] = Check::nucleoPadrao($dados->getConteudo());

            elseif (preg_match('/Graphics Processor/i', $content)):
                $this->processador['gpu'] = $dados->getConteudo();

            elseif (preg_match('/Ram Options/i', $content)):
                $this->getAllOptions($dados->getConteudo());

            endif;
        endif;
    }

    private function getBaseClock() {
        $site = parent::getSite();
        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' velocidade processador não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->nucleo['velocidade'] = (float)$site->getAttr("data-v");
        endif;

        $site->getTagByClass("js-vz-u", 2);
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' memória ram não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->ram['capacidade'][] = (int)round($site->getAttr("data-v") / 1000);
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->ram['capacidade']);

        if (preg_match('/<td/i', $dados)):
            $dado = new Semantica($dados);

            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->ram['capacidade'][] = (int)$dado->getConteudo();
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->ram['capacidade'][] = (int)$dados;
        endif;
    }

    private function finalizaTratamento() {
        if (isset($this->nucleo['velocidade']) && isset($this->nucleo['nucleo'])):
            $this->nucleo['title'] = $this->nucleo['velocidade'] . "Ghz " . Check::nucleoPadraoNome($this->nucleo['nucleo']);
        else:

        endif;
    }
}