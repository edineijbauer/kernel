<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Battery extends BoxValidation {

    private $htmlContent;
    private $capacidade;
    private $tecnologia;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Battery", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getCapacidade() {
        return $this->capacidade;
    }

    /**
     * @return mixed
     */
    public function getTecnologia() {
        return $this->tecnologia;
    }

    public function getDados() {
        return array('title' => $this->getCapacidade() . "mAh {$this->getTecnologia()}",'capacidade' => $this->getCapacidade(), 'tecnologia' => $this->getTecnologia());
    }

    private function getInfo($site) {
        $this->getBaseCapacidade();

        for ($i = 1; $i < 3; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Battery Technology/i', $content)):
                $this->tecnologia = $dados->getConteudo();
            endif;
        endif;
    }

    private function getBaseCapacidade() {
        $site = parent::getSite();

        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' capacidade da Bateria não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->capacidade = (int)$site->getAttr("data-v", 1);
        endif;
    }

}