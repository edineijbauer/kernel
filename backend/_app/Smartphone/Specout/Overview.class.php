<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Overview extends BoxValidation {

    private $htmlContent;
    private $price;
    private $release;
    private $status;
    private $os;
    private $rom;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Overview", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getRelease() {
        return $this->release;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getOs() {
        return $this->os;
    }

    /**
     * @return mixed
     */
    public function getRom() {
        return $this->rom;
    }

    private function getInfo($site) {
        for ($i = 1; $i < 6; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Price Without Contract/i', $content)):
                $this->price = (float)str_replace('$', '', $dados->getConteudo());

            elseif (preg_match('/Release Date/i', $content)):
                $this->release = $dados->getConteudo();

            elseif (preg_match('/Market Status/i', $content)):
                $this->status = $dados->getConteudo();

            elseif (preg_match('/Release Operating System/i', $content)):
                $this->os = $dados->getConteudo();

            elseif (preg_match('/User Interface/i', $content)):
                $this->rom = $dados->getConteudo();

            endif;
        endif;
    }

}