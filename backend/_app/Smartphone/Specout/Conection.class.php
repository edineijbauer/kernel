<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Conection extends BoxValidation {

    private $htmlContent;
    private $simType;
    private $simDual;
    private $usb;
    private $bluetooth;
    private $sensors;
    private $caracteristicasExtras;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Connectivity & Features", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getSimType() {
        return $this->simType;
    }

    /**
     * @return mixed
     */
    public function getSimDual() {
        return $this->simDual;
    }

    /**
     * @return mixed
     */
    public function getUsb() {
        return $this->usb;
    }

    /**
     * @return mixed
     */
    public function getBluetooth() {
        return $this->bluetooth;
    }

    /**
     * @return mixed
     */
    public function getSensors() {
        return $this->sensors;
    }

    /**
     * @return mixed
     */
    public function getCaracteristicasExtras() {
        return $this->caracteristicasExtras;
    }

    public function getDados() {
        return array('chip_modelo' => $this->getSimType(), 'chip_dual' => $this->getSimDual(), 'bluetooth' => $this->getBluetooth(), 'usb' => $this->getUsb(), 'sensores' => $this->getSensors(), 'caracteristicas_extras' => $this->getCaracteristicasExtras());
    }

    private function getInfo($site) {
        for ($i = 1; $i < 24; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Sensors/i', $content)):
                $this->getAllOptionsSensors($dados->getConteudo());

            elseif (preg_match('/SIM Card Size/i', $content)):
                $this->simType = Check::chipPadrao($dados->getConteudo());

            elseif (preg_match('/Dual Sim/i', $content)):
                $this->simDual = Check::chipDualPadrao($dados->getConteudo());

            elseif (preg_match('/Interfaces/i', $content)):
                $this->usb = $dados->getConteudo();

            elseif (preg_match('/Bluetooth Version/i', $content)):
                $this->bluetooth = (float)$dados->getConteudo();

            elseif (preg_match('/Wireless Connectivity/i', $content)):
                $this->getAllOptions($dados->getConteudo());

            endif;
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->caracteristicasExtras);

        if (preg_match('/<td/i', $dados)):
            $dado = new Semantica($dados);
            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->caracteristicasExtras[] = strip_tags($dado->getConteudo());
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->caracteristicasExtras[] = strip_tags($dados);
        endif;
    }

    private function getAllOptionsSensors($dados) {
        unset($this->sensors);

        if (preg_match('/<td/i', $dados)):
            $dado = new Semantica($dados);
            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->sensors[] = strip_tags($dado->getConteudo());
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->sensors[] = strip_tags($dados);
        endif;
    }

}