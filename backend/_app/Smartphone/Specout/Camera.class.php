<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 19:10
 */
class Camera extends BoxValidation {

    private $htmlContent;
    private $videoResolution;
    private $aperture;
    private $score;
    private $options;
    private $back;
    private $front;

    /**
     * Camera constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Camera", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getVideoResolution() {
        return $this->videoResolution;
    }

    /**
     * @return mixed
     */
    public function getAperture() {
        return $this->aperture;
    }

    /**
     * @return mixed
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * @return mixed
     */
    public function getBack() {
        return $this->back;
    }

    /**
     * @return mixed
     */
    public function getFront() {
        return $this->front;
    }

    /**
     * @return mixed
     */
    public function getOptions() {
        return $this->options;
    }

    public function getDadosFront() {
        return array('title' => $this->getFront() . "MP", 'megapixel' => $this->getFront());
    }

    public function getDadosBack() {
        return array('title' => $this->getBack() . "MP", 'megapixel' => $this->getBack(), 'aperture_size' => $this->getAperture(), 'score' => $this->getScore(), 'caracteristicas_extras' => $this->getOptions());
    }

    public function getDadosVideo() {
        return array('title' => "filma em " . $this->getVideoResolution(), 'resolucao' => $this->getVideoResolution());
    }

    private function getInfo($site) {
        $this->getBaseMegapixel();

        for ($i = 1; $i < 6; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Max Video Resolution/i', $content)):
                $this->videoResolution = $dados->getConteudo();

            elseif (preg_match('/Camera Lens Aperture/i', $content)):
                $this->aperture = $dados->getConteudo();

            elseif (preg_match('/DxOMark Mobile Score/i', $content)):
                $this->score = (int)$dados->getConteudo();
                $this->score = (float)number_format($this->score / 10, 1);

            elseif (preg_match('/Camera Options/i', $content)):
                $this->getAllOptions($dados->getConteudo());

            endif;
        endif;
    }

    private function getBaseMegapixel() {
        $site = parent::getSite();
        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' BACK camera não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->back = (float)$site->getAttr("data-v", 1);
        endif;

        $site->getTagByClass("js-vz-u", 2);
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' FRONT camera não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->front = (float)$site->getAttr("data-v", 2);
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->options);

        if (preg_match('/<td/i', $dados)):
            $dado = new Semantica($dados);
            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->options[] = $dado->getConteudo();
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->options[] = $dados;
        endif;
    }
}