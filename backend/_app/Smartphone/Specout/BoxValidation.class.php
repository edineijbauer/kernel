<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 19:13
 */
class BoxValidation {
    private $title;
    private $content;
    private $site;
    private $result;

    /**
     * BoxValidation constructor.
     * @param $title
     * @param $content
     */
    public function __construct($title, $content) {
        $this->title = $title;
        $this->content = $content;
        if($this->title && $this->content):
            $this->start();
        endif;
    }

    /**
     * @return mixed
     */
    protected function getSite() {
        return $this->site;
    }

    /**
     * @return mixed
     */
    protected function getTitle() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    protected function getResult() {
        return $this->result;
    }

    private function start() {
        $this->site = new Semantica($this->content);
        if (!empty($this->content)):

            $this->site->getTagByClass("card-sec-title-txt");
            if ($this->site->getConteudo() && $this->site->getConteudo() === $this->title):
                $this->site->getTagByName("tr");
                if ($this->site->getConteudo()):
                    $this->result = true;
                else:
                    Check::Notifica("SpecOut: tabela {$this->title}.", "tr não encontrada no bloco {$this->title}.");
                endif;
            else:
                Check::Notifica("SpecOut: Bloco {$this->title}", "bloco não identificado. Classe 'card-sec-title-txt' não encontrada ou titulo do bloco diferente de {$this->title}.");
            endif;
        else:
            Check::Notifica("SpecOut: Html Content em Branco.", "conteúdo html passado para {$this->title} esta vazio.");
        endif;
    }
}