<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Dimension extends BoxValidation {

    private $htmlContent;
    private $tamanho;
    private $peso;
    private $profundidade;
    private $features = "";

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Dimensions and Design", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getTamanho() {
        return $this->tamanho;
    }

    /**
     * @return mixed
     */
    public function getPeso() {
        return $this->peso;
    }

    /**
     * @return mixed
     */
    public function getProfundidade() {
        return $this->profundidade;
    }

    /**
     * @return mixed
     */
    public function getFeatures() {
        return $this->features;
    }

    public function getDados() {
        return array('peso' => $this->getPeso(), 'profundidade' => $this->getProfundidade(), 'tamanho' => $this->getTamanho(), "caracteristicas_extras" => $this->getFeatures());
    }

    private function getInfo($site) {
        $this->getBasePeso();
        for ($i = 1; $i < 4; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Dimensions/i', $content)):
                $tamanhos = explode('x', $dados->getConteudo());
                $tamanhos[0] = (float)$tamanhos[0];
                $tamanhos[1] = (float)$tamanhos[1];
                $tamanhos[2] = (float)$tamanhos[2];
                $this->tamanho = trim($tamanhos[0]) . " x " . trim($tamanhos[1]);
                $this->profundidade = (float)number_format($tamanhos[2], 2);

            elseif (preg_match('/Rugged Features/i', $content)):
                $this->getAllOptions($dados->getConteudo());

            endif;
        endif;
    }

    private function getBasePeso() {
        $site = parent::getSite();
        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' profundidade não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->profundidade = (float)number_format($site->getAttr("data-v"), 2);
        endif;

        $site->getTagByClass("js-vz-u", 2);
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' peso não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->peso = (int)$site->getAttr("data-v");
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->features);
        if (preg_match('/<td/i', $dados)):
            $dado = new Semantica($dados);
            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->features[] = $dado->getConteudo();
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->features[] = $dados;
        endif;
    }

}