<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Storage extends BoxValidation {

    private $htmlContent;
    private $storage;
    private $external;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Storage", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getStorage() {
        return $this->storage;
    }

    /**
     * @return mixed
     */
    public function getExternal() {
        return $this->external;
    }

    private function getInfo($site) {
        $this->getBaseStorage();

        for ($i = 1; $i < 6; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Storage Options/i', $content)):
                $this->getAllOptions($dados->getConteudo());
            endif;
        endif;
    }

    private function getBaseStorage() {
        $site = parent::getSite();
        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' internal storage não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->storage['capacidade'][] = (int)$site->getAttr("data-v");
        endif;

        $site->getTagByClass("js-vz-u", 2);
        if ($site->getConteudo()):
            if ($site->getAttr("data-v")):
                $this->external['capacidade'] = (int)$site->getAttr("data-v");
            endif;
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->storage['capacidade']);
        if (preg_match('/<td/i', $dados)):

            $dado = new Semantica($dados);

            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                $store = (int)$dado->getConteudo();
                if ($dado->getConteudo()):
                    $this->storage['capacidade'][] = (int)$store;
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->storage['capacidade'][] = (int)$dados;
        endif;
    }

}