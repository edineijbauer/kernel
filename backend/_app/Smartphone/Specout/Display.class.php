<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 10/04/2017
 * Time: 17:42
 */
class Display extends BoxValidation {

    private $htmlContent;
    private $size;
    private $resolution;
    private $tecnologia;
    private $protection;
    private $features;

    /**
     * Overview constructor.
     * @param $htmlContent
     */
    public function __construct($htmlContent) {
        $this->htmlContent = $htmlContent;

        if ($this->htmlContent):
            parent::__construct("Display", $this->htmlContent);

            if (parent::getResult()):
                $this->getInfo(parent::getSite());
            endif;
        endif;
    }

    /**
     * @return mixed
     */
    public function getSize() {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getResolution() {
        return $this->resolution;
    }

    /**
     * @return mixed
     */
    public function getTecnologia() {
        return $this->tecnologia;
    }

    /**
     * @return mixed
     */
    public function getProtection() {
        return $this->protection;
    }

    /**
     * @return mixed
     */
    public function getFeatures() {
        return $this->features;
    }

    public function getDados() {
        return array('title' => $this->getTecnologia() . " " . $this->getSize() . '" ' . strtoupper($this->getResolution()), 'tamanho' => $this->getSize(), 'resolucao' => $this->getResolution(), 'tecnologia' => $this->getTecnologia(), 'protecao' => $this->getProtection(), 'caracteristicas_extras' => $this->getFeatures());
    }

    /**
     * @return mixed
     */
    public function getRom() {
        return $this->rom;
    }

    private function getInfo($site) {
        $this->getBasePolegada();

        for ($i = 1; $i < 6; $i++):
            $site->getTagByName("tr", $i);
            if ($site->getConteudo()):
                if (preg_match('/td/i', $site->getConteudo())):
                    $this->filterDados($site->getConteudo());
                endif;
            endif;
        endfor;
    }

    private function filterDados($content) {
        $dados = new Semantica($content);
        $dados->getTagByName("td", 1);
        $content = $dados->getConteudo();

        if ($content):
            $dados->getTagByName("td", 2);

            if (preg_match('/Screen Resolution/i', $content)):
                $this->resolution = Check::resolucaoPadrao(trim(str_replace(array(',', 'pixels'), array('.', ''), $dados->getConteudo())));

            elseif (preg_match('/Display Technology/i', $content)):
                $this->tecnologia = $dados->getConteudo();

            elseif (preg_match('/Screen Material/i', $content)):
                $this->protection = $dados->getConteudo();

            elseif (preg_match('/Display Features/i', $content)):
                $this->getAllOptions($dados->getConteudo());

            endif;
        endif;
    }

    private function getBasePolegada() {

        $site = parent::getSite();
        $site->getTagByClass("js-vz-u");
        if (!$site->getConteudo()):
            Check::Notifica("SpecOut: " . parent::getTitle() . ".", "classe 'js-vz-u' screen size não encontrada no bloco " . parent::getTitle() . ".");
        endif;

        if ($site->getAttr("data-v")):
            $this->size = (float)$site->getAttr("data-v", 1);
        endif;
    }

    private function getAllOptions($dados) {
        unset($this->features);

        if (preg_match('/<td/i', $dados)):

            $dado = new Semantica($dados);
            for ($i = 1; $i < 25; $i++):
                $dado->getTagByName("td", $i);
                if ($dado->getConteudo()):
                    $this->features[] = $dado->getConteudo();
                else:
                    $i = 25;
                endif;
            endfor;
        else:
            $this->features[] = $dados;
        endif;
    }

}