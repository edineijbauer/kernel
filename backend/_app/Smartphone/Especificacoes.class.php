<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 13/03/2017
 * Time: 10:10
 */
class Especificacoes {

    private $id;
    private $smartphone;
    private $imagem;
    private $dados;
    private $especificacao = "";

    /**
     * @param mixed $id
     */
    public function setId($id) {
        if (!$this->smartphone):
            $read = new Read();
            $read->ExeRead(PRE . "smartphone", "WHERE id=:id", "id={$id}");
            if ($read->getResult()):
                $this->id = $id;
                $this->smartphone = $read->getResult()[0];
                $this->start();
            endif;
        endif;
    }

    /**
     * @param mixed $smartphone
     */
    public function setSmartphone($smartphone) {
        if (!$this->id):
            $this->smartphone = $smartphone;
            $this->id = $this->smartphone['id'];
            $this->start();
        endif;
    }

    /**
     * @return mixed
     */
    public function getSmartphone() {
        return $this->smartphone;
    }

    /**
     * @return mixed
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * @return string
     */
    public function getEspecificacao() {
        return $this->especificacao;
    }

    private function start() {
        if ($this->id && $this->smartphone):
            $this->setImagem($this->smartphone['imagem']);
            $this->setEspecificacao();
            $this->setHardware();
        endif;
    }

    private function setImagem($id) {
        $read = new Read();
        $read->ExeRead(PRE . "gallery", "WHERE id =:ji", "ji= {$id}");
        if ($read->getResult()):
            $this->imagem = $read->getResult()[0];
        endif;
    }
    private function setEspecificacao() {
        $smart['marca'] = Check::getBanco(PRE . "marca", (int)$this->smartphone['marca'], "title");
        $smart['sistema_operacional'] = $this->smartphone['sistema_operacional'];
        $smart['views'] = $this->smartphone['views'];
        $smart['caracteristicas_extras'] = $this->smartphone['caracteristicas_extras'];

        $view = new View();
        $this->especificacao .= $view->Retorna($smart, $view->Load("especificacao/smartphone"));
    }

    private function convertNucleoNome($nucleo) {
        switch ($nucleo):
            case 1:
                return "Single-Core";
                break;
            case 2:
                return "Dual-Core";
                break;
            case 4:
                return "Quad-Core";
                break;
            case 6:
                return "Six-Core";
                break;
            case 8:
                return "Octa-Core";
                break;
            case 10:
                return "Deca-Core";
                break;
        endswitch;

        return "Dual-Core";
    }

    private function setResumoProcessador($processador) {
        $nucleosVelocidade = "";
        $read = new Read();
        $read->ExeRead(PRE . "smart_nucleos", "WHERE processador = :p", "p={$processador['id']}");
        if ($read->getResult()):
            foreach ($read->getResult() as $n):
                $nucleosVelocidade .= (empty($nucleosVelocidade) ? "" : " + ") . "{$this->convertNucleoNome($n['nucleo'])} {$n['velocidade']} GHz";
            endforeach;
        endif;

        return $nucleosVelocidade;
    }

    private function setProcessador() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_processador", "WHERE id=:h", "h={$this->smartphone['processador']}");
        if ($read->getResult()):
            $dados = $read->getResult()[0];
            $dados['nucleos_velocidade'] = $this->setResumoProcessador($dados);
            $view = new View();
            $this->especificacao .= $view->Retorna($dados, $view->Load("especificacao/processador"));
        endif;
    }

    private function setMemorias() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_memorias", "WHERE id=:h", "h={$this->smartphone['memorias']}");
        if ($read->getResult()):
            $hardware = $read->getResult()[0];
            $view = new View();

            $read->ExeRead(PRE . "smart_memoria", "WHERE id=:h", "h={$hardware['memoria_ram']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "memória ram")), $view->Load("especificacao/memoria"));
            endif;

            $read->setPlaces("h={$hardware['armazenamento']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "armazenamento")), $view->Load("especificacao/memoria"));
            endif;

            $read->setPlaces("h={$hardware['armazenamento_expansivel']}");
            if ($read->getResult()):
                $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "armazenamento expansível")), $view->Load("especificacao/memoria"));
            endif;
        endif;
    }

    private function setTela() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_tela", "WHERE id=:h", "h={$this->smartphone['tela']}");
        if ($read->getResult()):
            $view = new View();
            $this->dados['resolucao'] = $this->getResolution($read->getResult()[0]['resolucao']);
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/tela"));
        endif;

    }

    private function setBateria() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_bateria", "WHERE id=:h", "h={$this->smartphone['bateria']}");
        if ($read->getResult()):
            $view = new View();
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/bateria"));
        endif;
    }

    private function setCamera($title, $id) {
        $read = new Read();
        $read->ExeRead(PRE . "smart_camera", "WHERE id=:h", "h={$id}");
        if ($read->getResult()):
            $view = new View();
            $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "Câmera {$title}")), $view->Load("especificacao/camera"));
        endif;
    }

    private function setFilmagem($title, $id) {
        $read = new Read();
        $read->ExeRead(PRE . "smart_filmadora", "WHERE id=:h", "h={$id}");
        if ($read->getResult()):
            $view = new View();
            $this->especificacao .= $view->Retorna(array_merge($read->getResult()[0], array("titulo" => "Filmagem {$title}")), $view->Load("especificacao/filmagem"));
        endif;
    }

    private function setConectividade() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_conectividade", "WHERE id=:h", "h={$this->smartphone['conectividade']}");
        if ($read->getResult()):
            $view = new View();
            $this->especificacao .= $view->Retorna($read->getResult()[0], $view->Load("especificacao/conectividade"));
        endif;
    }

    private function setHardware() {
        $this->setProcessador();
        $this->setMemorias();
        $this->setTela();
        $this->setBateria();

        if ($this->smartphone['camera_traseira']):
            $this->setCamera("Traseira", $this->smartphone['camera_traseira']);
        endif;
        if ($this->smartphone['filmagem_traseira']):
            $this->setFilmagem("Traseira", $this->smartphone['filmagem_traseira']);
        endif;
        if ($this->smartphone['camera_frontal']):
            $this->setCamera("Frontal", $this->smartphone['camera_frontal']);
        endif;
        if ($this->smartphone['filmagem_frontal']):
            $this->setFilmagem("Frontal", $this->smartphone['filmagem_frontal']);
        endif;
        $this->setConectividade();
    }

    private function getReview($value, $info) {
        $value = number_format($value, 1, '.', '');
        $value = $value > 10 ? 10 : $value;

        if ($value >= 8.5):
            return "<b class='color-terceary'>{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}</b>";
        elseif ($value >= 4.5):
            return "{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}";
        else:
            return "<span class='color-tomato'>{$value}&starf; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$info}</span>";
        endif;
    }

    private function getResolution($resolucao) {
        $resolucao = strtolower($resolucao);
        if ($resolucao === "fullhd" || $resolucao === "full hd" || $resolucao === "1080p" || $resolucao === "1080"):
            return 1080;
        elseif ($resolucao === "hd" || $resolucao === "720p" || $resolucao === "720"):
            return 720;
        elseif ($resolucao === "quadhd" || $resolucao === "quad hd" || $resolucao === "2k" || $resolucao === "1440p" || $resolucao === "1440"):
            return 1440;
        elseif ($resolucao === "4k" || $resolucao === "2160p" || $resolucao === "2160"):
            return 2160;
        else:
            return 480;
        endif;
    }

}