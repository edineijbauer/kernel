<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 15/03/2017
 * Time: 15:03
 */
class GearBestCraw {
    private $url;
    private $site;

    function __construct() {
        $this->start();
    }

    /**
     * @return mixed
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getSite() {
        return $this->site;
    }

    private function start() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_craw", "WHERE index = 1");
        if ($read->getResult()):
            $this->url = $read->getResult()[0]['link'];
            $del = new Delete();
            $del->ExeDelete(PRE . "smart_craw", "WHERE id = :i", "i={$read->getResult()[0]['id']}");

            $this->site = file_get_contents($this->url);
        else:
            $this->getLinks();
        endif;

        //$this->getLink();
    }

    private function getLinks() {

        $links = array();

        for ($i = 1; $i < 30; $i++):
            $site = file_get_contents("http://www.gearbest.com/cell-phones-c_11293/{$i}.html");
            foreach(explode('href="http://www.gearbest.com/cell-phones/', $site) as $e => $d):
                if($e > 0):
                    $b = explode('"', $d)[0];
                    $link = "http://www.gearbest.com/cell-phones/{$b}";
                    if(!in_array($link, $links) && !preg_match("/Reviews/i", $link)):
                        $links[] = $link;
                    endif;
                endif;
            endforeach;
        endfor;

        $craw = new Banco("smart_craw");
        foreach($links as $l):
            $craw->load("link", $l);
            if (!$craw->exist()):
                $craw->site = 1;
                $craw->save();
            endif;
        endforeach;
    }

    private function getLink() {
        $s = explode('<div class="recomPro_inner">', $this->site);

        for ($i = 3; $i < 6; $i++):
            if (isset($s[$i]) && !empty($s[$i])):
                $content = '<div class="recomPro_inner">' . explode('</section>', $s[$i])[0];
                $this->getList($content);
            endif;
        endfor;
    }

    private function getList($content) {
        $craw = new Banco("smart_craw");

        $href = explode('<a href="', $content);
        for ($i = 1; $i < 30; $i++):
            if ($i % 2 === 1 && isset($href[$i])):
                $link = explode('"', $href[$i])[0];
                if (!empty($link) && preg_match('/\/cell-phones\//i', $link)):
                    $craw->load("link", $this->site->getAttr("href"));
                    if (!$craw->exist()):
                        $craw->site = 1;
                        $craw->save();
                    endif;
                endif;
            endif;
        endfor;
    }
}