<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 16/03/2017
 * Time: 09:48
 */
abstract class GearBestGetInfo {

    private $smartphone;
    private $processador;
    private $memoria_ram;
    private $memoria_rom;
    private $memoria_ext;
    private $camera_back;
    private $camera_front;
    private $marca;
    private $tela;
    private $nucleos;
    private $bateria;
    private $conectividade;
    private $cover;
    private $galeria;
    private $result = false;

    /**
     * @return mixed
     */
    protected function getResult() {
        return $this->result;
    }

    /**
     * @return mixed
     */
    protected function getSmartphone() {
        return $this->smartphone;
    }

    /**
     * @return mixed
     */
    protected function getProcessador() {
        return $this->processador;
    }

    /**
     * @return mixed
     */
    protected function getTela() {
        return $this->tela;
    }

    /**
     * @return mixed
     */
    protected function getNucleos() {
        return $this->nucleos;
    }

    /**
     * @return mixed
     */
    protected function getBateria() {
        return $this->bateria;
    }

    /**
     * @return mixed
     */
    protected function getConectividade() {
        return $this->conectividade;
    }

    /**
     * @return mixed
     */
    public function getMemoriaRam() {
        return $this->memoria_ram;
    }

    /**
     * @return mixed
     */
    public function getMemoriaRom() {
        return $this->memoria_rom;
    }

    /**
     * @return mixed
     */
    public function getMemoriaExt() {
        return $this->memoria_ext;
    }

    /**
     * @return mixed
     */
    public function getCameraBack() {
        return $this->camera_back;
    }

    /**
     * @return mixed
     */
    public function getCameraFront() {
        return $this->camera_front;
    }

    /**
     * @return mixed
     */
    public function getMarca() {
        return $this->marca;
    }

    /**
     * @return mixed
     */
    public function getGaleria() {
        return $this->galeria;
    }

    /**
     * @return mixed
     */
    public function getCover() {
        return $this->cover;
    }

    protected function getInfo($s) {

        $s->getTagByName("h1");
        if ($s->getConteudo()):
            $this->getTitle($s->getConteudo());
        else:
            Check::Notifica("GearBest Title Not Found", "id 'h1' não encontrado na página de smartphone da GearBest.");
        endif;

        $s->getTagById("js_jqzoom");
        if ($s->getConteudo()):
            $this->getInfoCover($s->getConteudo());
        else:
            Check::Notifica("GearBest Cover Not Found", "id 'js_jgzoom' não encontrado na página de smartphone da GearBest.");
        endif;

        $s->getTagByClass("product_pz_style2");
        if ($s->getConteudo()):

            $this->getInfoValues($s->getConteudo(), 0);
            $this->getInfoGaleria($s);

        else:

            $s->getTagByClass("product_pz_style1");
            if ($s->getConteudo()):
                $this->getInfoValues($s->getConteudo(), 1);
                $this->getInfoGaleria($s);

            else:
                Check::Notifica("GearBest Corpo Not Found", "class 'product_pz_style1' não encontrado na página de smartphone da GearBest.");
            endif;

        endif;
    }

    private function getTitle($content) {
        $this->smartphone['title'] = $content;
        $this->smartphone['urlname'] = Check::Name($this->smartphone['title']);
    }

    private function getInfoCover($content) {
        $img = new Semantica($content);
        $img->getTagByName("img");
        $this->cover = $img->getAttr("src");
    }

    private function getInfoValues($s, $tipo) {
        $e = new Semantica($s);
        for ($i = 1; $i < 15; $i++):
            $e->getTagByName(($tipo === 0 ? "tr" : "td"), $i);
            $f = new Semantica($e->getConteudo());
            $f->getTagByName("p");
            $info = trim($f->getConteudo());
            $f->getTagByName(($tipo === 0 ? "td" : "p"), ($tipo === 0 ? 1 : 2));

            if ($f->getConteudo()):
                $dado = str_replace("<br>", "<br/>", $f->getConteudo());

                if ($info === "Package Contents"):
                else:

                    foreach (explode('<br/>', $dado) as $pc):
                        if (!empty($pc)):
                            $dado = explode(":", $pc);

                            if (isset($dado[1])):
                                $this->switchInfo($info, trim($dado[0]), trim($dado[1]));
                            endif;
                        endif;
                    endforeach;

                endif;
            else:
                break;
            endif;

        endfor;

        $this->tela['title'] = (isset($this->tela['title']) ? $this->tela['title'] . " " : "") . $this->tela['tamanho'] . '" ' . $this->tela['resolucao'];

        $this->result = true;
    }

    private function switchInfo($info, $name, $value) {

        switch ($info):
            case "Basic Information":
                $this->getBasico($name, $value);
                break;

            case "Hardware":
                $this->getHardware($name, $value);
                break;

            case "Network":
                $this->getConection($name, $value);
                break;

            case "Display":
                $this->getInfoTela($name, $value);
                break;

            case "Camera":
                $this->getCamera($name, $value);
                break;

            case "Battery":
                $this->getInfoBateria($name, $value);
                break;
        endswitch;
    }

    private function getBasico($name, $value) {
        switch ($name):
            case "Brand":
                $this->marca = strtolower($value);
                break;

            case "OS":
                $this->smartphone['sistema_operacional'] = $value;
                break;
        endswitch;
    }

    private function getHardware($name, $value) {
        switch ($name):
            case "CPU":
                $this->processador['title'] = (strtolower($value));
                break;

            case "Cores":
                $this->getInfoNucleos(strtolower($value));
                break;

            case "GPU":
                $this->processador['gpu'] = $value;
                break;

            case "RAM":
                $this->memoria_ram['title'] = $value;
                $this->memoria_ram['capacidade'] = (int)$this->memoria_ram['title'];
                if (!is_numeric($this->memoria_ram['capacidade'])):
                    unset($this->memoria_ram['capacidade']);
                endif;
                break;

            case "ROM":
                $this->memoria_rom['title'] = $value;
                $this->memoria_rom['capacidade'] = (int)$this->memoria_rom['title'];
                if (!is_numeric($this->memoria_rom['capacidade'])):
                    unset($this->memoria_rom['capacidade']);
                endif;
                break;

            case "Optional Version":
                //                Optional Version: 4GB RAM + 32GB ROM / 4GB RAM + 64GB ROM
                $this->memoria_ram['title'] = $value;
                $this->memoria_rom['title'] = $value;
                $memory = explode("+", trim(explode("/", $value)[0]));
                $this->memoria_ram['capacidade'] = (int)trim($memory[0]);
                $this->memoria_rom['capacidade'] = (int)trim($memory[1]);
                if (!is_numeric($this->memoria_ram['capacidade'])):
                    unset($this->memoria_ram['capacidade']);
                endif;
                if (!is_numeric($this->memoria_rom['capacidade'])):
                    unset($this->memoria_rom['capacidade']);
                endif;
                break;

            case "External Memory":
                $this->memoria_ext['title'] = $value;
                foreach (explode(" ", strtolower($this->memoria_ext['title'])) as $e):
                    if (preg_match('/gb/i', $e)):
                        $this->memoria_ext['capacidade'] = (int)$e;
                        break;
                    endif;
                endforeach;
                if (!isset($this->memoria_ext['capacidade']) || (isset($this->memoria_ext['capacidade']) && !is_numeric($this->memoria_ext['capacidade']))):
                    unset($this->memoria_ext['capacidade']);
                endif;
                break;

        endswitch;
    }

    private function getInfoNucleos($nucleo) {

        $this->nucleos['title'] = $nucleo;
        $dado = explode(",", $nucleo);

        foreach ($dado as $n):
            $n = strtolower($n);

            if (preg_match('/(ghz|gh|gz|gh)/i', $n)):

                $this->nucleos['velocidade'] = (float)$n;
                if (!is_numeric($this->nucleos['velocidade'])):
                    unset($this->nucleos['velocidade']);
                endif;

            else:
                $this->nucleos['nucleo'] = Check::nucleoPadrao($n);
            endif;
        endforeach;
    }

    private function getConection($name, $value) {
        switch ($name):
            case "WIFI":
                $this->conectividade = (str_replace(' wireless internet', '', $value));
                break;
        endswitch;
    }

    private function getInfoTela($name, $value) {
        switch ($name):
            case "Screen type":
                $this->tela['title'] = $value;
                break;
            case "Screen size":
                $this->tela['tamanho'] = (float)$value;
                if (!is_numeric($this->tela['tamanho'])):
                    unset($this->tela['tamanho']);
                endif;
                break;
            case "Screen resolution":
                $this->tela['resolucao'] = $this->convertResolucao(strtolower($value));
                break;
        endswitch;
    }

    private function getCamera($name, $value) {
        if ($name === "Back-camera" || $name === "Back camera"):
            $this->camera_back['title'] = $value;
            $this->camera_back['megapixel'] = (float)$this->camera_back['title'];
            if (!is_numeric($this->camera_back['megapixel'])):
                unset($this->camera_back['megapixel']);
            endif;
        elseif ($name === "Front-camera" || $name === "Front camera"):
            $this->camera_front['title'] = $value;
            $this->camera_front['megapixel'] = (float)$this->camera_front['title'];
            if (!is_numeric($this->camera_front['megapixel'])):
                unset($this->camera_front['megapixel']);
            endif;
        endif;
    }

    private function getInfoBateria($name, $value) {
        switch ($name):
            case "Battery Capacity (mAh)":
                $value = str_replace("1 x ", "", $value);
                $this->bateria['title'] = $value;
                $this->bateria['capacidade'] = (float)$this->bateria['title'];
                if (!is_numeric($this->bateria['capacidade'])):
                    unset($this->bateria['capacidade']);
                endif;
                break;
        endswitch;
    }

    protected function getInfoGaleria($s) {
        $cont = 0;
        $s->getTagByClass("detailShow");
        $gal = new Semantica($s->getConteudo());
        for ($e = 1; $e < 25; $e++):
            $gal->getTagByName("img", $e);
            if ($gal->getAttr("src") && $cont < 25 && !$gal->getAttr("height")):
                $cont++;
                $this->galeria[] = $gal->getAttr("src");
            endif;
        endfor;
    }

    private function convertResolucao($resolucao) {
        if (preg_match('/\(/i', $resolucao)):
            $resolucao = explode("(", $resolucao);
            $a = $this->convertResolucao(trim($resolucao[0]));
            if (!$a):
                return $this->convertResolucao(trim(explode(")", $resolucao[1])[0]));
            else:
                return $a;
            endif;
        else:
            if ($resolucao === "3840x2160" || $resolucao === "3840 x 2160" || $resolucao === "4k" || $resolucao === "2160p" || $resolucao === "2160" || $resolucao === "ultrahd" || $resolucao === "ultra hd"):
                return "4k";
            elseif ($resolucao === "2560x1440" || $resolucao === "2560 x 1440" || $resolucao === "2048 x 1080" || $resolucao === "2k" || $resolucao === "1440" || $resolucao === "1440p" || $resolucao === "quadhd" || $resolucao === "quad hd"):
                return "2k";
            elseif ($resolucao === "1920x1080" || $resolucao === "1920 x 1080" || $resolucao === "1080p" || $resolucao === "1080" || $resolucao === "fullhd" || $resolucao === "full hd"):
                return "fullhd";
            elseif ($resolucao === "1280x720" || $resolucao === "1280 x 720" || $resolucao === "1024x768" || $resolucao === "1024 x 768" || $resolucao === "1366x768" || $resolucao === "1366 x 768" || $resolucao === "720p" || $resolucao === "720" || $resolucao === "hd"):
                return "hd";
            elseif ($resolucao === "854 x 480" || $resolucao === "FWVGA"):
                return "FWVGA";
            else:
                return null;
            endif;
        endif;
    }
}