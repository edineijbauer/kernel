<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 05/04/2017
 * Time: 10:18
 */
class CreateSmartphone {
    private $dados;
    private $DIR = "../../";

    /**
     * @param mixed $this ->dados
     */
    public function setDados($dados) {
        $this->dados = $dados;
        $this->start();
    }

    private function start() {

        if (isset($this->dados->site->urlname)):
            $banco = new Banco("index");
            $banco->load("urlname", $this->dados->site->urlname);
            $banco->setDados($this->dados->site);
            $this->dados->preco->site = $banco->save();
        endif;

        if (isset($this->dados->smart_bateria)):
            $banco = new Banco("smart_bateria");
            $bateria['title'] = $this->dados->smart_bateria->title;
            $bateria['capacidade'] = $this->dados->smart_bateria->capacidade;
            $banco->loadArray($bateria);
            $banco->setDados($this->dados->smart_bateria);
            $this->dados->smartphone->bateria = $banco->save();
        endif;

        if (isset($this->dados->smart_camera_front)):
            $banco = new Banco("smart_camera");
            $front['title'] = $this->dados->smart_camera_front->title;
            $front['megapixel'] = $this->dados->smart_camera_front->megapixel;
            $banco->loadArray($front);
            $banco->setDados($this->dados->smart_camera_front);
            $this->dados->smartphone->camera_frontal = $banco->save();
        endif;

        if (isset($this->dados->smart_camera_back)):
            $banco = new Banco("smart_camera");
            $camera['title'] = $this->dados->smart_camera_back->title;
            $camera['megapixel'] = $this->dados->smart_camera_back->megapixel;
            $banco->loadArray($camera);
            $banco->setDados($this->dados->smart_camera_back);
            $this->dados->smartphone->camera_traseira = $banco->save();
        endif;

        if (isset($this->dados->smart_filmadora_back)):
            $banco = new Banco("smart_filmadora");
            $filma['title'] = $this->dados->smart_filmadora_back->title;
            $filma['resolucao'] = $this->dados->smart_filmadora_back->resolucao;
            $banco->loadArray($filma);
            $banco->setDados($this->dados->smart_filmadora_back);
            $this->dados->smartphone->filmagem_traseira = $banco->save();
        endif;

        if (isset($this->dados->smart_filmadora_front)):
            $banco = new Banco("smart_filmadora");
            $filmaSelfie['title'] = $this->dados->smart_filmadora_front->title;
            $filmaSelfie['resolucao'] = $this->dados->smart_filmadora_front->resolucao;
            $banco->loadArray($filmaSelfie);
            $banco->setDados($this->dados->smart_filmadora_front);
            $this->dados->smartphone->filmagem_frontal = $banco->save();
        endif;

        if (isset($this->dados->smart_conectividade)):
            $banco = new Banco("smart_conectividade");
            $banco->load("title", $this->dados->smart_conectividade->title);
            $banco->setDados($this->dados->smart_conectividade);
            $this->dados->smartphone->conectividade = $banco->save();
        endif;

        $this->createMemorias();
        $this->createProcessador();

        if (isset($this->dados->smart_tela)):
            $banco = new Banco("smart_tela");
            $tela['title'] = $this->dados->smart_tela->title;
            $tela['tamanho'] = $this->dados->smart_tela->tamanho;
            $tela['resolucao'] = $this->dados->smart_tela->resolucao;
            $banco->loadArray($tela);
            $banco->setDados($this->dados->smart_tela);
            $this->dados->smartphone->tela = $banco->save();
        endif;

        if (isset($this->dados->marca)):
            $banco = new Banco("marca");
            $banco->load("urlname", $this->dados->marca->urlname);
            $banco->title = $this->dados->marca->title;
            $this->dados->smartphone->marca = $banco->save();
        endif;

        if (isset($this->dados->smartphone->title)):
            $this->createCover();
            $this->createSmart();
            $this->createGallerySmartphone();
            $this->createPreco();
        endif;
    }

    private function createSmart() {
        $banco = new Banco("smartphone");
        $banco->load("title", $this->dados->smartphone->title);
        $banco->setDados($this->dados->smartphone);
        $this->dados->preco->smartphone = $banco->save();
    }

    private function createCover() {
        if (isset($this->dados->gallery) && Check::IsImage($this->dados->gallery)):

            $g['title'] = $this->dados->smartphone->title . '-cover';
            $g['urlname'] = Check::Name($g['title']);
            $g['cover'] = $this->folderImg($this->DIR) . $g['urlname'] . '.jpg';
            $g['folder'] = "images";

            copy($this->dados->gallery, $this->DIR . "uploads/" . $g['cover']);

            $banco = new Banco("gallery");
            $banco->load("cover", $g['cover']);
            $banco->setDados($g);
            $this->dados->smartphone->imagem = $banco->save();
        endif;
    }

    private function createProcessador() {
        if (isset($this->dados->smart_processador)):
            $banco = new Banco("smart_processador");
            $banco->load('title', $this->dados->smart_processador->title);
            $banco->setDados($this->dados->smart_processador);
            $this->dados->smartphone->processador = $banco->save();

            if (isset($this->dados->smart_nucleos)):
                $del = new Delete();
                $del->ExeDelete(PRE . "smart_nucleos", "WHERE processador = :p", "p={$this->dados->smartphone->processador}");
                foreach ($this->dados->smart_nucleos as $n):
                    $n->processador = $this->dados->smartphone->processador;
                    $banco = new Banco("smart_nucleos");
                    $banco->loadArray($n);
                    $banco->save();
                endforeach;
            endif;
        endif;
    }

    private function createMemorias() {
        if (isset($this->dados->smart_memoria_ram)):
            $banco = new Banco("smart_memoria");
            $ram['title'] = $this->dados->smart_memoria_ram->title;
            $ram['capacidade'] = $this->dados->smart_memoria_ram->capacidade;
            $banco->loadArray($ram);
            if (isset($this->dados->smart_memoria_ram->tecnologia) && !empty($this->dados->smart_memoria_ram->tecnologia)):
                $banco->tecnologia = $this->dados->smart_memoria_ram->tecnologia;
            endif;
            $memorias['memoria_ram'] = $banco->save();
            $memorias['title'] = $this->dados->smart_memoria_ram->capacidade;

            if (isset($this->dados->smart_memoria_ram)):
                $banco = new Banco("smart_memoria");
                $rom['title'] = $this->dados->smart_memoria_rom->title;
                $rom['capacidade'] = $this->dados->smart_memoria_rom->capacidade;
                $banco->loadArray($rom);
                if (isset($this->dados->smart_memoria_rom->tecnologia) && !empty($this->dados->smart_memoria_rom->tecnologia)):
                    $banco->tecnologia = $this->dados->smart_memoria_rom->tecnologia;
                endif;
                $memorias['armazenamento'] = $banco->save();

                $memorias['title'] = $this->dados->smart_memoria_ram->capacidade . '/' . $this->dados->smart_memoria_rom->capacidade;
            endif;

            if (isset($this->dados->smart_memoria_ext)):
                $banco = new Banco("smart_memoria");
                $ext['title'] = $this->dados->smart_memoria_ext->title;
                $ext['capacidade'] = $this->dados->smart_memoria_ext->capacidade;
                $banco->loadArray($ext);
                $banco->setDados($this->dados->smart_memoria_ext);
                $memorias['armazenamento_expansivel'] = $banco->save();
            endif;

            $banco = new Banco("smart_memorias");
            $banco->loadArray($memorias);
            $this->dados->smartphone->memorias = $banco->save();
        endif;
    }

    private function createGallerySmartphone() {
        if (isset($this->dados->smartphone_gallery) && is_array($this->dados->smartphone_gallery)):
            $del = new Delete();
            $del->ExeDelete(PRE . "smartphone_gallery", "WHERE smartphone = :st", "st={$this->dados->preco->smartphone}");

            foreach ($this->dados->smartphone_gallery as $i => $image):
                if ($i < 15 && Check::IsImage($image)):

                    $g['title'] = $this->dados->smartphone->title . ' ' . $i;
                    $g['urlname'] = Check::Name($g['title']);
                    $g['cover'] = $this->folderImg() . $g['urlname'] . '.jpg';
                    $g['folder'] = "images";
                    $g['smartphone'] = $this->dados->preco->smartphone;

                    copy($image, $this->DIR . "uploads/" . $g['cover']);

                    $create = new Create();
                    $create->ExeCreate(PRE . "smartphone_gallery", $g);
                endif;
            endforeach;
        endif;
    }

    private function createPreco() {
        if (isset($this->dados->preco)):
            $this->dados->preco->link = (preg_match('/\?/i', $this->dados->preco->link) ? explode('?', $this->dados->preco->link)[0] : $this->dados->preco->link);
            unset($this->dados->preco->link);

            $banco = new Banco("preco");
            $banco->loadArray(array("index" => 1, "smartphone" => $this->dados->preco->smartphone));
            $banco->setDados($this->dados->preco);
            $banco->save();

            $this->otimizaHistoricoDB();
            $this->createPrecoHistorico();
        endif;
    }

    private function createPrecoHistorico() {
        $banco = new Banco("preco_historico");
        $banco->load("dia", date("Y-m-d"));
        if (!$banco->exist() || ($banco->exist() && $banco->preco > $this->dados->preco->valor)):
            $banco->preco = $this->dados->preco->valor;
        endif;
        $banco->smartphone = $this->dados->preco->smartphone;
        $banco->save();

        $this->checkDesconto();
    }

    private function checkDesconto() {

        $read = new Read();
        $read->ExeRead(PRE . "preco_historico_mes", "WHERE smartphone = :s ORDER BY mes DESC LIMIT 1", "s={$this->dados->preco->smartphone}");
        if ($read->getResult()):

            if ($read->getResult()[0]['preco'] > ($this->dados->preco->valor * 1.05)):
                //preço atual + que 5% mais barato que a média do últimos 30 dias

                $preco_desconto['date'] = date("Y-m-d H:i:s");
                $preco_desconto['valor'] = $this->dados->preco->valor;
                $preco_desconto['antigo_valor'] = $read->getResult()[0]['preco'];
                $preco_desconto['desconto'] = (($preco_desconto['antigo_valor'] / $preco_desconto['valor']) - 1) * 100;

                $desconto = new Banco("preco_desconto");
                $desconto->load("smartphone", $this->dados->preco->smartphone);
                $desconto->setDados($preco_desconto);
                $desconto->save();

                $this->notificaEmails($preco_desconto);
            else:

                $del = new Delete();
                $del->ExeDelete(PRE . "preco_desconto", "WHERE smartphone = :ss", "ss={$this->dados->preco->smartphone}");
            endif;
        endif;

        $this->createResumo();
    }

    private function otimizaHistoricoDB() {
        $read = new Read();
        $read->ExeRead(PRE . "preco_historico", "WHERE smartphone = :s ORDER BY dia DESC", "s={$this->dados->preco->smartphone}");
        if ($read->getResult() && $read->getRowCount() > 30):
            $soma = 0;
            foreach ($read->getResult() as $precos):
                $soma += $precos['preco'];
            endforeach;

            $banco = new Banco("preco_historico_mes");
            $banco->loadArray(array("smartphone" => $this->dados->preco->smartphone, "mes" => date("m")));
            $banco->preco = number_format($soma / $read->getRowCount(), 2);
            $banco->save();

            $del = new Delete();
            $del->ExeDelete(PRE . "preco_historico", "WHERE smartphone = :s", "s={$this->dados->preco->smartphone}");

        endif;
    }

    private function createResumo() {
        $read = new Read();
        $read->ExeRead(PRE . 'smartphone', "WHERE id=:id", "id={$this->dados->preco->smartphone}");
        if ($read->getResult()):
            $smartphone = new ResumoSmartphone();
            $smartphone->setSmartphone($read->getResult()[0]);
        endif;
    }

    private function notificaEmails($preco_desconto) {
        $read = new Read();
        $read->ExeRead(PRE . "email", "LIMIT 9000");
        if ($read->getResult()):
            foreach ($read->getResult() as $item):
                $content = "aproveite essa queda de preço para comprar agora mesmo! {$preco_desconto['desconto']}% OFF -> <a href='{$this->dados->preco['link']}' target='_blank' >R${$preco_desconto['valor']} Comprar</a>";
                Check::EnviaEmail($preco_desconto['desconto'] . "% OFF em {$this->dados->smartphone->title}", $content, $item['email']);
            endforeach;
        endif;
    }

    private function createFolder($Folder) {
        if (!file_exists($this->DIR . 'uploads/' . $Folder) && !is_dir($this->DIR . 'uploads/' . $Folder)):
            mkdir($this->DIR . 'uploads/' . $Folder, 0755);
        endif;
    }

    private function folderImg() {
        list($y, $m) = explode('/', date('Y/m'));
        $this->createFolder("images");
        $this->createFolder("images/{$y}");
        $this->createFolder("images/{$y}/{$m}/");
        return "images/{$y}/{$m}/";
    }

}