<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 16/03/2017
 * Time: 18:27
 */
class GearBestSend {

    private $gallery;
    private $url;
    private $cover;

    /**
     * @param mixed $gallery
     */
    public function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover) {
        $this->cover = $cover;
    }

    public function send() {
        $read = new Read();
        $read->ExeRead(PRE . "smart_preco", "WHERE link = '{$this->url}'");
        if ($read->getResult()):
            $dados['preco'] = $read->getResult()[0];
            unset($dados['preco']['id']);

            $read->ExeRead(PRE . "smartphone", "WHERE id = :id", "id={$dados['preco']['smartphone']}");
            if ($read->getResult()):
                $smartphone = $read->getResult()[0];
            endif;

            $dados['gallery'] = $this->cover;

            $read->ExeRead(PRE . "smart_site", "WHERE id = :id", "id={$dados['preco']['index']}");
            if ($read->getResult()):
                $dados['index'] = $read->getResult()[0];
                unset($dados['index']['id']);
            endif;

            $dados['smartphone_gallery'] = $this->gallery;

            $read->ExeRead(PRE . "smart_bateria", "WHERE id = :id", "id={$smartphone['bateria']}");
            if ($read->getResult()):
                $dados['smart_bateria'] = $read->getResult()[0];
                unset($dados['smart_bateria']['id']);
            endif;

            $read->ExeRead(PRE . "smart_camera", "WHERE id = :id", "id={$smartphone['camera_frontal']}");
            if ($read->getResult()):
                $dados['smart_camera_front'] = $read->getResult()[0];
                unset($dados['smart_camera_front']['id']);
            endif;

            $read->ExeRead(PRE . "smart_camera", "WHERE id = :id", "id={$smartphone['camera_traseira']}");
            if ($read->getResult()):
                $dados['smart_camera_back'] = $read->getResult()[0];
                unset($dados['smart_camera_back']['id']);
            endif;

            $read->ExeRead(PRE . "smart_filmadora", "WHERE id = :id", "id={$smartphone['filmagem_frontal']}");
            if ($read->getResult()):
                $dados['smart_filmadora_front'] = $read->getResult()[0];
                unset($dados['smart_filmadora_front']['id']);
            endif;

            $read->ExeRead(PRE . "smart_filmadora", "WHERE id = :id", "id={$smartphone['filmagem_traseira']}");
            if ($read->getResult()):
                $dados['smart_filmadora_back'] = $read->getResult()[0];
                unset($dados['smart_filmadora_back']['id']);
            endif;

            $read->ExeRead(PRE . "smart_conectividade", "WHERE id = :id", "id={$smartphone['conectividade']}");
            if ($read->getResult()):
                $dados['smart_conectividade'] = $read->getResult()[0];
                unset($dados['smart_conectividade']['id']);
            endif;

            $read->ExeRead(PRE . "smart_gallery", "WHERE id = :id", "id={$smartphone['imagem']}");
            if ($read->getResult()):
                $dados['gallery'] = $read->getResult()[0];
                unset($dados['gallery']['id']);
            endif;

            $read->ExeRead(PRE . "smart_marca", "WHERE id = :id", "id={$smartphone['marca']}");
            if ($read->getResult()):
                $dados['marca'] = $read->getResult()[0];
                unset($dados['marca']['id']);
            endif;

            $read->ExeRead(PRE . "smart_processador", "WHERE id = :id", "id={$smartphone['processador']}");
            if ($read->getResult()):
                $dados['smart_processador'] = $read->getResult()[0];
                unset($dados['smart_processador']['id']);
            endif;

            $read->ExeRead(PRE . "smart_nucleos", "WHERE processador = :id", "id={$smartphone['processador']}");
            if ($read->getResult()):
                foreach ($read->getResult() as $n):
                    unset($n['id']);
                    $dados['smart_nucleos'][] = $n;
                endforeach;
            endif;

            $read->ExeRead(PRE . "smart_preco_historico", "WHERE smartphone = :id", "id={$smartphone['id']}");
            if ($read->getResult()):
                $dados['preco_historico'] = $read->getResult()[0];
                unset($dados['preco_historico']['id']);
                unset($dados['preco_historico']['smartphone']);
            endif;

            $read->ExeRead(PRE . "smart_tela", "WHERE id = :id", "id={$smartphone['tela']}");
            if ($read->getResult()):
                $dados['smart_tela'] = $read->getResult()[0];
                unset($dados['smart_tela']['id']);
            endif;

            $read->ExeRead(PRE . "smart_memorias", "WHERE id = :id", "id={$smartphone['memorias']}");
            if ($read->getResult()):
                $memorias = $read->getResult()[0];
                $read->ExeRead(PRE . "smart_memoria", "WHERE id = :id", "id={$memorias['memoria_ram']}");
                if ($read->getResult()):
                    $dados['smart_memoria_ram'] = $read->getResult()[0];
                    unset($dados['smart_memoria_ram']['id']);
                endif;
                $read->ExeRead(PRE . "smart_memoria", "WHERE id = :id", "id={$memorias['armazenamento']}");
                if ($read->getResult()):
                    $dados['smart_memoria_rom'] = $read->getResult()[0];
                    unset($dados['smart_memoria_rom']['id']);
                endif;
                $read->ExeRead(PRE . "smart_memoria", "WHERE id = :id", "id={$memorias['armazenamento_expansivel']}");
                if ($read->getResult()):
                    $dados['smart_memoria_ext'] = $read->getResult()[0];
                    unset($dados['smart_memoria_ext']['id']);
                endif;
            endif;

            unset($dados['preco']['smartphone'], $dados['preco']['index']);
            $dados['smartphone']['title'] = $smartphone['title'];
            $dados['smartphone']['urlname'] = $smartphone['urlname'];
            $dados['smartphone']['sistema_operacional'] = $smartphone['sistema_operacional'];
            $dados['smartphone']['caracteristicas_extras'] = $smartphone['caracteristicas_extras'];

            var_dump(Check::send_to("http://localhost/buscaphone/_cdn/receive/smartphones.php", $dados));

        endif;
    }
}