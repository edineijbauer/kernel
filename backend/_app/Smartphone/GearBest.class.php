<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 14/03/2017
 * Time: 11:18
 */
class GearBest extends GearBestGetInfo {

    private $url;
    private $domain;
    private $smartphone;
    private $memorias;
    private $preco;

    /**
     * GearBest constructor.
     * @param $url
     */
    public function __construct() {
        $this->getDomain();
        $this->start();
    }

    /**
     * @return mixed
     */
    public function getUrl() {
        return $this->url;
    }

    private function getDomain() {
        if (file_exists('../uploads/')):
            $this->domain = '../';
        elseif (file_exists('../../uploads/')):
            $this->domain = '../../';
        elseif (file_exists('../../../uploads/')):
            $this->domain = '../../../';
        elseif (file_exists('../../../../uploads/')):
            $this->domain = '../../../../';
        elseif (file_exists('./uploads/')):
            $this->domain = './';
        endif;
    }

    private function start() {
        $craw = new GearBestCraw();
        $this->url = $craw->getUrl();

        $s = new Semantica($craw->getSite());

        $s->getTagByClass("last-li");
        $tipo = new Semantica($s->getConteudo());
        $tipo->getTagByName("span");
        if ($tipo->getConteudo() && trim($tipo->getConteudo()) === "Cell phones"):

            $this->getPreco($s);
            $this->preco['link'] = $this->url;

            parent::getInfo($s);

            if (parent::getResult()):
                $this->createBaseSmartphone();
                $this->createSmartPhone();
                $this->createSite();
                $this->createPreco();

                $this->sendSmartphone();
            endif;
        endif;
    }

    private function sendSmartphone() {
        $gear = new GearBestSend();
        $gear->setUrl($this->url);
        $gear->setCover(parent::getCover());
        $gear->setGallery(parent::getGaleria());
        $gear->send();
    }

    private function getPreco($s) {
        $s->getTagById("unit_price");
        if (!$s->getAttr("data-orgp")):

            Check::Notifica("GearBest Preço Not Found", "attr 'data-orgp' ou id 'unit_price' não encontrado na página de smartphone da GearBest.");

        else:
            $this->preco['valor'] = (double)$s->getAttr("data-orgp");
            $s->getTagById("my_shop_price");
            $this->preco['frete'] = (double)($s->getConteudo() ? $s->getAttr("orgp") : 0);
        endif;
    }

    private function createBaseSmartphone() {

        $this->smartphone = parent::getSmartphone();

        $this->createMarca(parent::getMarca());
        $this->createProcessador(parent::getProcessador());
        $this->createNucleo(parent::getNucleos());
        $this->createMemoriaRam(parent::getMemoriaRam());
        $this->createMemoriaRom(parent::getMemoriaRom());
        $this->createMemoriaExt(parent::getMemoriaExt());
        $this->createMemorias();
        $this->createConectividade(parent::getConectividade());
        $this->createTela(parent::getTela());
        $this->createCameraBack(parent::getCameraBack());
        $this->createCameraFront(parent::getCameraFront());
        $this->createBateria(parent::getBateria());
    }

    private function createMarca($marca) {
        if ($marca):
            $read = new Read();
            $read->ExeRead(PRE . "smart_marca", "WHERE title = '{$marca}'");
            if (!$read->getResult()):
                $dados['title'] = $marca;
                $dados['urlname'] = Check::Name($marca);

                $create = new Create();
                $create->ExeCreate(PRE . "smart_marca", $dados);
                $this->smartphone['marca'] = $create->getResult();
            else:
                $this->smartphone['marca'] = $read->getResult()[0]['id'];
            endif;
        endif;
    }

    private function createProcessador($processador) {
        if (isset($processador['title'])):
            $p = new Banco("smart_processador");
            $p->load("title", $processador['title']);
            $p->setDados($processador);
            $this->smartphone['processador'] = $p->save();
        endif;
    }

    private function createNucleo($nucleos) {
        if (isset($this->smartphone['processador'])):
            $del = new Delete();
            $del->ExeDelete(PRE . "smart_nucleos", "WHERE processador = :p", "p={$this->smartphone['processador']}");

            if (isset($nucleos['nucleo']) && isset($nucleos['title']) && isset($this->smartphone['processador'])):
                $p = new Banco("smart_nucleos");
                $p->loadArray(array("title" => $nucleos['title'], "processador" => $this->smartphone['processador']));
                $p->setDados($nucleos);
                $p->save();
            endif;
        endif;
    }

    private function createMemoriaRam($ram) {
        if (isset($ram['title']) && isset($ram['capacidade'])):
            $p = new Banco("smart_memoria");
            $p->loadArray($ram);
            $this->memorias['title'] = $ram['capacidade'] . '/';
            $this->memorias['memoria_ram'] = $p->save();
        endif;
    }

    private function createMemoriaRom($rom) {
        if (isset($rom['title']) && isset($rom['capacidade'])):
            $p = new Banco("smart_memoria");
            $p->loadArray($rom);
            $this->memorias['title'] = (isset($this->memorias['title']) ? $this->memorias['title'] : "ROM: ") . $rom['capacidade'];
            $this->memorias['armazenamento'] = $p->save();
        endif;
    }

    private function createMemoriaExt($ext) {
        if (isset($ext['title'])):
            $p = new Banco("smart_memoria");
            $p->loadArray($ext);
            $this->memorias['armazenamento_expansivel'] = $p->save();
        endif;
    }

    private function createMemorias() {
        if (isset($this->memorias['title'])):
            $p = new Banco("smart_memorias");
            $p->loadArray($this->memorias);
            $this->smartphone['memorias'] = $p->save();
        endif;
    }

    private function createConectividade($wifi) {
        $p = new Banco("smart_conectividade");
        $p->loadArray(array("wifi" => $wifi, "bluetooth" => NULL, "usb" => NULL, "nfc" => 0, "infra_vermelho" => 0));
        if (!$p->exist()):
            $p->title = $wifi;
            $this->smartphone['conectividade'] = $p->save();
        else:
            $this->smartphone['conectividade'] = $p->id;
        endif;
    }

    private function createTela($tela) {
        if (isset($tela['title']) && isset($tela['tamanho']) && isset($tela['resolucao'])):
            $p = new Banco("smart_tela");
            $p->load("title", $tela['title']);
            if (!$p->exist()):
                $p->tamanho = $tela['tamanho'];
                $p->resolucao = $tela['resolucao'];
                $this->smartphone['tela'] = $p->save();
            else:
                $this->smartphone['tela'] = $p->id;
            endif;
        endif;
    }

    private function createCameraBack($camera) {
        if (isset($camera['title']) && isset($camera['megapixel'])):
            $p = new Banco("smart_camera");
            $p->loadArray($camera);
            $this->smartphone['camera_traseira'] = $p->save();
        endif;
    }

    private function createCameraFront($camera) {
        if (isset($camera['title']) && isset($camera['megapixel'])):
            $p = new Banco("smart_camera");
            $p->loadArray($camera);
            $this->smartphone['camera_frontal'] = $p->save();
        endif;
    }

    private function createBateria($bateria) {
        if (isset($bateria['title']) && isset($bateria['capacidade'])):
            $p = new Banco("smart_bateria");
            $p->loadArray($bateria);
            $this->smartphone['bateria'] = $p->save();
        endif;
    }

    private function createSmartPhone() {
        if (isset($this->smartphone['title'])):
            $smart = new Banco("smartphone");
            $smart->load("title", $this->smartphone['title']);
            $smart->setDados($this->smartphone);
            $this->preco['smartphone'] = $smart->save();
        endif;
    }

    private function createPreco() {

        if (isset($this->preco['title']) && isset($this->preco['valor'])):
            $preco = new Banco("smart_preco");
            $preco->load("link", $this->preco['link']);
            $preco->setDados($this->preco);
            $preco->save();

            $historico = new Banco("smart_preco_historico");
            $historico->loadArray(array("smartphone" => $preco->smartphone, "dia" => date("Y-m-d")));
            if (!$historico->exist() || ($historico->exist() && $historico->preco > $this->preco['valor'])):
                $historico->preco = $this->preco['valor'];
                $historico->save();
            endif;
        endif;
    }

    private function createSite() {
        $site = Check::getUrlPai($this->url);

        $s = new Banco("smart_site");
        $s->load("index", $site);
        if (!$s->exist()):
            $s->title = explode(".", $site)[0];
            $s->urlname = Check::Name($s->title);
            $this->preco['index'] = $s->save();
        else:
            $this->preco['index'] = $s->id;
        endif;

        $this->preco['title'] = $s->title . ' - $' . $this->preco['valor'];
    }
}