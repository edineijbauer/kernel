<?php

function deslogar() {
    unset($_SESSION['userlogin']);
    setcookie("pmail", 0, time() - 1, "/");
    setcookie("ppass", 0, time() - 1, "/");
    header("Location: " . HOME . "/ADM");
}

//Verifica se falta informações na session
if (!isset($_SESSION['userlogin']['email'])):
    if (isset($_SESSION['userlogin'])):
        unset($_SESSION['userlogin']);
    endif;
endif;

//Verifica se a Sessão é igual ao cookie
if (isset($_SESSION['userlogin']['email']) && isset($_COOKIE['pmail'])):
    if ($_SESSION['userlogin']['email'] != base64_decode($_COOKIE['pmail'])):
        deslogar();
    endif;
endif;

//Verifica se não tem Sessão e tem cookie
if (!isset($_SESSION['userlogin'])):
    if (isset($_COOKIE['pmail'])):
        //Faz login com o cookie
        $up_banco['email'] = base64_decode($_COOKIE['pmail']);
        $up_banco['password'] = $_COOKIE['ppass'];

        $login = new Login(0, PRE);
        $login->ExeLogin($up_banco);
        if (!$login->getResult()):
            //Não deu para logar com os cookies, remove cookie
            setcookie("pmail", 0, time() - 1, "/");
            setcookie("ppass", 0, time() - 1, "/");
        endif;
        unset($up_banco);
    endif;
endif;

//Obtem informações de privilégio deste usuário [LEVEL]
if (isset($_SESSION['userlogin'])):
    if ($_SESSION['userlogin']['status'] === "0"):
        deslogar();
    endif;

    switch ($_SESSION['userlogin']['level']):
        case 3:
            define('ISADMIN', TRUE);
            define('ISCLIENTE', TRUE);
            define('ISHELPER', TRUE);
            define('ISGERENTE', TRUE);
            define('ISWEBMASTER', FALSE);
            break;
        case 2:
            define('ISGERENTE', TRUE);
            define('ISHELPER', TRUE);
            define('ISCLIENTE', TRUE);
            define('ISADMIN', FALSE);
            define('ISWEBMASTER', FALSE);
            break;
        case 1:
            define('ISHELPER', TRUE);
            define('ISCLIENTE', TRUE);
            define('ISADMIN', FALSE);
            define('ISGERENTE', FALSE);
            define('ISWEBMASTER', FALSE);
            break;
        default:
            define('ISCLIENTE', TRUE);
            define('ISADMIN', FALSE);
            define('ISHELPER', FALSE);
            define('ISGERENTE', FALSE);
            define('ISWEBMASTER', FALSE);
            break;
    endswitch;
    define('ISV', FALSE);
else:
    //Visitante
    define('ISV', TRUE);
    define('ISCLIENTE', FALSE);
    define('ISADMIN', FALSE);
    define('ISHELPER', FALSE);
    define('ISGERENTE', FALSE);
endif;
?>