<?php

/**
 * Lista.class [ HELPER ]
 * Realização a lista e paginação de um banco de dados
 * 
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class Lista {

    /** DEFINE O PAGER */
    private $Blog = '';
    private $Page;
    private $Limit;
    private $Offset;

    /** REALIZA A LEITURA */
    private $Banco;
    private $Where;
    private $key;

    /** DEFINE O PAGINATOR */
    private $HavePage;
    private $Rows;
    private $Content;
    private $Tpl;
    private $Colunas;
    private $Font;
    private $MaxLinks;
    private $First;
    private $Last;

    /** RENDERIZA O PAGINATOR */
    private $Paginator = '';
    private $Cont = 0;

    /**
     * <b>Iniciar Paginação:</b> Defina o link onde a paginação será recuperada. Você ainda pode mudar os textos
     * do primeiro e último link de navegação e a quantidade de links exibidos (opcional)
     * @param STRING $Content = ID da div onde receberá os resultados da navegação
     * @param STRING $Max = Ex: máximo de resultados por página
     * @param STRING $Banco = banco a se conectar
     * @param STRING $Where = restrições
     * @param STRING $key = querys das restrições
     */
    function __construct($Banco, $Where, $Paginacao, $Tpl = null, $Col = null, $Font = null, $Max = null, $Pag = null, $Offset = null) {
        $this->Banco = (string) $Banco;
        $this->Where = ( (string) $Where ? $Where : '');
        $this->HavePage = ( (bool) $Paginacao == 0 ? false : true);
        $this->Tpl = ( (string) $Tpl ? $Tpl : 'post_blog');
        $this->Limit = ( (int) $Max ? $Max : 10);
        $this->Offset = ( (int) $Offset ? $Offset : 0);
        $this->Colunas = ( (int) $Col ? $Col : 1);
        $this->Page = ( (int) $Pag ? $Pag : 1);
        $this->Font = ($Font ? $Font : '');
    }

    public function getRowCount() {
        return $this->Cont;
    }

    public function ExePager() {
        if ($this->HavePage):
            $this->First = '<<';
            $this->Last = '>>';
            $this->MaxLinks = 5;
            $this->getSyntax();
        endif;
    }

    /**
     * <b>Retornar:</b> Caso informado uma page com número maior que os resultados, este método navega a paginação
     * em retorno até a página com resultados!
     * @return LOCATION = Retorna a página
     */
    public function getPosts() {

        $this->getPostagens();
        echo $this->Blog . $this->Paginator;
    }

    /**
     * <b>Retornar:</b> Caso informado uma page com número maior que os resultados, este método navega a paginação
     * em retorno até a página com resultados!
     * @return LOCATION = Retorna a página
     */
    public function getReturnPosts() {

        $this->getPostagens();
        return $this->Blog . $this->Paginator;
    }

    /*
     * <b>Insere um post:</b> Insere post pré-determinado no blog list
     * @return NULL = Não retorna
     */

    public function setPost($id) {
        $read = new Read();
        $read->ExeRead($this->Banco, "WHERE id=:mi" . " LIMIT 1", "mi={$id}");
        if ($read->getResult()):
            $this->Cont ++;

            $listEmpty = ['title', 'urlname', '_content', 'gallery_id', 'date', 'nota', 'views', 'produto_views', 'category_id'];
            foreach ($listEmpty as $e):
                $content[$e] = '';
            endforeach;

            $content = $this->FormatInputsValues($read->getResult()[0], $content);

            $this->InsertListPost($content);

        endif;
    }

    public function getPostagens() {
        $read = new Read();
        $read->ExeRead($this->Banco, $this->Where . " LIMIT {$this->Offset},{$this->Limit}");
        if ($read->getResult()):

            foreach ($read->getResult() as $r):

                if ($this->Cont < $this->Limit):
                    $this->setPost($r['id']);
                endif;

            endforeach;

            $this->ExePager();

        endif;
    }

    /**
     * <b>Obter Página:</b> Retorna o número da página atualmente em foco pela URL. Pode ser usada para validar
     * a navegação da paginação!
     * @return INT = Retorna a página atual
     */
    public function getPage() {
        return $this->Page;
    }

    /**
     * <b>Limite por Página:</b> Retorna o limite de resultados por página da paginação. Deve ser usada na SQL que obtém
     * os resultados. Ex: LIMIT = getLimit();
     * @return INT = Limite de resultados
     */
    public function getLimit() {
        return $this->Limit;
    }

    /**
     * <b>Offset por Página:</b> Retorna o offset de resultados por página da paginação. Deve ser usada na SQL que obtém
     * os resultado. Ex: OFFSET = getLimit();
     * @return INT = Offset de resultados
     */
    public function getOffset() {
        return $this->Offset;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    private function InsertListPost($content) {
        if (isset($content['title']) && !empty($content['title'])):
            if (isset($content['_content'])):
                $content['_content'] = Check::Words($content['_content'], 30);
            endif;

            $content['c'] = $this->Cont;

            $content['colunas'] = $this->getColunas() . ' ' . $this->Font;

            $content['style'] = "bg-secondary border border-bottom box box-1 mg-content10";

            $View = new View();
            $this->Blog .= $View->Retorna($content, $View->Load($this->Tpl));
        endif;
    }

    private function FormatInputsValues($dados, $content) {
        foreach ($dados as $k => $value):
            if (preg_match('/title/i', $k)):
                $content['title'] = $value;

            elseif (preg_match('/name/i', $k)):
                $content['urlname'] = str_replace(PRE, '', $this->Banco) . '/' . $value;

            elseif (preg_match('/(_content|conteudo|descr)/i', $k)):
                $content['_content'] = $value;

            elseif (preg_match('/cover/i', $k)):
                $content['gallery_id'] = HOME . '/uploads/' . $value;

            elseif (preg_match('/date/i', $k)):
                $content['date'] = $value;

            elseif (preg_match('/(nota|review|avali)/i', $k)):
                $content['nota'] = $value;

            elseif (preg_match('/view/i', $k)):
                $content['views'] = $value;
                $content['produto_views'] = $value;

            elseif (preg_match('/categor/i', $k)):
                $content['category_id'] = $value;

            endif;
        endforeach;

        return $content;
    }

    private function getColunas() {
        switch ($this->Colunas):
            case 1: $a = 'container';
                break;
            case 2: $a = 'box box-2 s7box-1';
                break;
            case 3: $a = 'box box-3 s11box-2 s7box-1';
                break;
            case 4: $a = 'box box-4 s11box-3 s9box-2 s6box-1';
                break;
            case 5: $a = 'box box-5 s11box-4 s9box-3 s7box-2 s5box-1';
                break;
            default: $a = 'box box-6 s11box-5 s9box-4 s7box-3 s6box-2 s4box-1';
        endswitch;
        return $a;
    }

    //Cria a paginação de resultados
    private function getSyntax() {
        $classes = 'fl-left pointer bg-white boxshadow hovershadow-heavy mg-small transition-easy pd-mediumb';

        //Tem mais que uma página, exibe o navegador
        $MaxLinks = $this->MaxLinks;
        
        $ant = $this->Page - 1;

        $this->Paginator .= "<ul class=\"container pd-content30 al-center paginator\">"
                            . "<a href='1' class='{$classes}'>{$this->First}</a>"
                            . "<a href='{$ant}' class='{$classes}'>anterior</a>"
                            . "<span class='fl-left pd-mediumb mg-small'>{$this->Page}</span>";

        for ($dPag = $this->Page + 1; $dPag < $this->Page + 4; $dPag ++):
            if (!isset($keyprox)):
                $keyprox = 1;
                $this->Paginator .= "<a href='{$dPag}' class='{$classes}'>próximo</a>";
            else:
                $this->Paginator .= "<a href='{$dPag}' class='{$classes}'>{$dPag}</a>";
            endif;
        endfor;

        $maximo = $dPag +20;
        $this->Paginator .= "<a href='{$maximo}' class='{$classes}'>{$this->Last}</a>"
            . "</ul>";
    }

    private function checkPage() {
        $int = (int) $this->File;
        $int = (string) $int;
        if ($int == $this->File):
            $this->File = "index";
            $this->Link = (int) $int;
        endif;

        if ($this->Link):
            $int = (int) $this->Link;
            $int = (string) $int;
            if ($int == $this->Link):
                $this->Link = (int) $int;
            endif;
        endif;

        if ($this->Redir):
            $int = (int) $this->Redir;
            $int = (string) $int;
            if ($int == $this->Redir):
                $this->Redir = (int) $int;
            endif;
        endif;
    }

}
