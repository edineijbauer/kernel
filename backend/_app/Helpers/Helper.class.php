<?php

/**
 * Check.class [ HELPER ]
 * Classe responável por manipular e validade dados do sistema!
 * 
 * @copyright (c) 2015, Edinei J. Bauer
 */
class Helper {

    private static $Data;
    private static $Format;

    /*
     * retorna cor auxiliar inversa em hexadecimal
     */

    public static function corAuxiliar($name) {
        $style = str_replace("#", "", $name);
        $style = strlen($style) === 3 ? $style[0] . $style[0] . $style[1] . $style[1] . $style[2] . $style[2] : $style ;

        $todo = hexdec($style[0] . $style[1]) + hexdec($style[2] . $style[3]) + hexdec($style[4] . $style[5]) - 550;

        if($todo > 0): //cor clara
            $base = round($todo/60);
            return "#{$base}{$base}{$base}";
        else: //cor escura
            return "#FFF";
        endif;
    }

}
