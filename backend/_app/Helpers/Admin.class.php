<?php

/**
 * Check.class [ HELPER ]
 * Classe responável por manipular e validade dados do sistema!
 * 
 * @copyright (c) 2015, Edinei J. Bauer
 */
class Admin {

    private static $Data;
    private static $Format;

    /**
     * <b>Verifica Estilo:</b> Faz uma busca para encontrar o estilo do elemento para uma determinada resolução. Caso não exista esta resolução, tenta encontrar um pai
     */
    public static function getlastStyle($alvo, $resolution) {
        $read = new Read();
        $read->ExeRead(PRE. "padrao", "WHERE name=:g ORDER BY resolution DESC", "g={$alvo}");
        if($read->getResult()):
            foreach ($read->getResult() as $r):
                if($r['resolution'] >= $resolution):
                    $a[0]=$r['padrao'];
                    $a[1] = $r['resolution'];
                endif;
            endforeach;
            if(isset($a)):
                return $a;
            else:
                return 0;
            endif;
        else:
            return 0;
        endif;
    }

    public static function getUrlUpdateSistem($user_id){
        $read = new Read();
        $read->ExeRead(PRE . "user_site", "WHERE user_id=:ui", "ui={$user_id}");
        if($read->getResult()):
            return $read->getResult()[0]['site_name'] . RECEIVE_PATH . "receive_sistem_update.php";
        endif;
    }
    
    public static function CheckToken($token) {
        $read = new Read();
        $read->ExeRead(PRE . "user", "WHERE user_token='{$token}'");
        if ($read->getResult()):
            //existe um usuário com este token ativo no momento
            return true;
        else:
            return false;
        endif;
    }

    public static function getToken($email) {
        $read = new Read();
        $read->ExeRead(PRE . "token", "WHERE user_email='{$email}' ORDER BY user_date DESC LIMIT 1");
        if ($read->getResult()):
            //existe um usuário com este token ativo no momento
            return $read->getResult()[0]['user_token'];
        else:
            return "";
        endif;
    }
    
}
