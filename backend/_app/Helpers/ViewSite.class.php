<?php

/**
 * View.class [ HELPER MVC ]
 * Reponsável por carregar o template, povoar e exibir a view, povoar e incluir arquivos PHP no sistem.
 * Arquitetura MVC!
 * 
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class ViewSite {

    private $Data;
    private $Keys;
    private $Values;
    private $Template;

    /**
     * <b>Exibir Template View:</b> Execute um foreach com um getResult() do seu model e informe o envelope
     * neste método para configurar a view. Não esqueça de carregar a view acima do foreach com o método Load.
     * @param array $Data = Array com dados obtidos
     * @param View $View = Template carregado pelo método Load()
     */
    public function Show(array $Data, $View) {
        $this->setKeys($Data);
        $this->setValues();
        echo $this->ShowView($View);
    }
    
    public function Retorno(array $Data, $View) {
        $this->setKeys($Data);
        $this->setValues();
        return $this->ShowView($View);
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    //Executa o tratamento dos campos para substituição de chaves na view.
    private function setKeys($Data) {
        $this->Data = $Data;
        $this->Data['HOME'] = HOME;
        
        $this->Keys = explode('&', '@#' . implode("#@&@#", array_keys($this->Data)) . '#@');
    }

    //Obtém os valores a serem inseridos nas chaves da view.
    private function setValues() {
        $this->Values = array_values($this->Data);
        foreach ($this->Values as $i => $a):
            while(preg_match('/@#/i', $a)):
                $a = $this->ShowView($a);
                if(preg_match('/@#/i', $a)):
                    $a = str_replace(array('@#', '#@'), array('',''), $a);
                endif;
            endwhile;
            $this->Values[$i] = $a;
        endforeach;
    }

    //Exibe o template view com echo!
    private function ShowView($View) {
        return str_replace($this->Keys, $this->Values, $View);
    }

}
