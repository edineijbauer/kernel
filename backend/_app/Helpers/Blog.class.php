<?php

/**
 * Pager.class [ HELPER ]
 * Realização a gestão e a paginação de resultados do sistema!
 * 
 * @copyright (c) 2014, Edinei J. Bauer NENA PRO
 */
class Blog {

    /** DEFINE O PAGER */
    private $Blog = '';
    private $Page;
    private $Limit;
    private $Offset;

    /** REALIZA A LEITURA */
    private $Banco;
    private $Where;
    private $key;
    private $Type;

    /** DEFINE O PAGINATOR */
    private $HavePage;
    private $Rows;
    private $Content;
    private $Tpl;
    private $Colunas;
    private $Font;
    private $MaxLinks;
    private $First;
    private $Last;

    /** RENDERIZA O PAGINATOR */
    private $Paginator = '';
    private $Cont = 0;

    /**
     * <b>Iniciar Paginação:</b> Defina o link onde a paginação será recuperada. Você ainda pode mudar os textos
     * do primeiro e último link de navegação e a quantidade de links exibidos (opcional)
     * @param STRING $Content = ID da div onde receberá os resultados da navegação
     * @param STRING $Max = Ex: máximo de resultados por página
     * @param STRING $Banco = banco a se conectar
     * @param STRING $Where = restrições
     * @param STRING $key = querys das restrições
     */
    function __construct($Banco, $Where, $Paginacao, $Tpl = null, $Col = null, $Font = null, $Max = null, $Offset = null, $Pag = null) {
        $this->Banco = (string) $Banco;
        $this->Where = ( (string) $Where ? $Where : '');
        $this->HavePage = ( (bool) $Paginacao == 0 ? false : true);
        $this->Tpl = ( (string) $Tpl ? $Tpl : 'post_blog');
        $this->Limit = ( (int) $Max ? $Max : 10);
        $this->Offset = ( (int) $Offset ? $Offset : 0);
        $this->Colunas = ( (int) $Col ? $Col : 1);
        $this->Page = ( (int) $Pag ? $Pag : 1);
        $this->Font = ($Font ? $Font : '');
        $this->Type = 0;
    }

    public function getRowCount() {
        return $this->Cont;
    }

    public function ExePager() {
        if ($this->HavePage):
            $this->First = '<<';
            $this->Last = '>>';
            $this->MaxLinks = 3;
            $this->getSyntax();
        endif;
    }

    /**
     * <b>Retornar:</b> Caso informado uma page com número maior que os resultados, este método navega a paginação
     * em retorno até a página com resultados!
     * @return LOCATION = exibe a página
     */
    public function getPosts() {

        if ($this->Where == 'getPatternsformPost'):
            $this->getRelateds();
        else:
            $this->getPostagens();
        endif;

        echo $this->Blog . $this->Paginator;
    }

    /**
     * <b>Retornar:</b> Caso informado uma page com número maior que os resultados, este método navega a paginação
     * em retorno até a página com resultados!
     * @return LOCATION = Retorna a página
     */
    public function getReturnPosts() {

        if ($this->Where == 'getPatternsformPost'):
            $this->getRelateds();
        else:
            $this->getPostagens();
        endif;

        return $this->Blog . $this->Paginator;
    }

    /*
     * <b>Insere um post:</b> Insere post pré-determinado no blog list
     * @return NULL = Não retorna
     */

    public function setPost($id, $m = null) {
        if (!$m):
            $id = ($this->Type === 1 ? Check::getBanco(PRE . "produto", (int) $id, "extends_posts") : $id);
        endif;

        $this->Cont++;

        if ($this->Type === 1):
            $page = new TemplateModelBox('produto', $id);

        else:
            if ($this->Banco === PRE . "posts_backup"):
                $page = new TemplateModelBox('posts_backup', $id);

            elseif ($this->Banco === PRE . "post"):
                $page = new TemplateModelBox('post', $id);

            endif;
        endif;

        $page->tratarDados();
        $content = $page->getResult();

        $this->InsertBlogPost($content);
    }

    public function getPostagens() {
        $read = new Read();
        $read->ExeRead($this->Banco, $this->Where . " LIMIT {$this->Offset},{$this->Limit}");
        if ($read->getResult()):

            foreach ($read->getResult() as $r):
                if ($this->Cont < $this->Limit):
                    $this->setPost($r['id'], 1);
                endif;
            endforeach;
            $this->ExePager();

        endif;
    }

    /**
     * <b>Obter Página:</b> Retorna o número da página atualmente em foco pela URL. Pode ser usada para validar
     * a navegação da paginação!
     * @return INT = Retorna a página atual
     */
    public function getPage() {
        return $this->Page;
    }

    /**
     * <b>Limite por Página:</b> Retorna o limite de resultados por página da paginação. Deve ser usada na SQL que obtém
     * os resultados. Ex: LIMIT = getLimit();
     * @return INT = Limite de resultados
     */
    public function getLimit() {
        return $this->Limit;
    }

    /**
     * <b>Offset por Página:</b> Retorna o offset de resultados por página da paginação. Deve ser usada na SQL que obtém
     * os resultado. Ex: OFFSET = getLimit();
     * @return INT = Offset de resultados
     */
    public function getOffset() {
        return $this->Offset;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    private function InsertBlogPost($content) {
        if (isset($content['title']) && !empty($content['title'])):
            if (isset($content['_content'])):
                $content['_content'] = Check::Words($content['_content'], 30);
            endif;

            $content['c'] = $this->Cont;

            $content['colunas'] = $this->getColunas() . ' ' . $this->Font;

            $content['style'] = "bg-secondary border border-bottom box box-1 mg-content10";
            $View = new View();
            $this->Blog .= $View->Retorna($content, $View->Load($this->Tpl));
        endif;
    }

    /**
     * <b>Retornar:</b> Caso informado uma page com número maior que os resultados, este método navega a paginação
     * em retorno até a página com resultados!
     * @return LOCATION = Retorna a página
     */
    private function getRelateds() {
        $View = new View();
        $t = $View->Load($this->Tpl);
        $this->Blog .= "<section class='container'><header class='font-zero'><h1>Você poderá gostar destes também:</h1></header>";
        for ($i = 0; $i < $this->Limit; $i++):
            $r['title'] = "<?=\$title_{$i}?>";
            $r['nota'] = "<?=\$nota_{$i}?>";
            $r['urlname'] = "<?=\$urlname_{$i}?>";
            $r['_content'] = "<?=\$content_{$i}?>";
            $r['produto_views'] = "<?=\$produto_views_{$i}?>";
            $r['category_id'] = "<?=\$category_id_{$i}?>";
            $r['produto_preco'] = "<?=\$produto_preco_{$i}?>";
            $r['gallery_id'] = HOME . "/uploads/<?=\$gallery_id_{$i}?>";
            $r['colunas'] = $this->getColunas() . ' ' . $this->Font;
            $r['style'] = "bg-secondary border border-bottom box box-1 mg-content10";
            $this->Blog .= $View->Retorna($r, $t);
            $this->Cont++;
        endfor;
        $this->Blog .= "</section>";
        $this->ExePager();
    }

    private function getColunas() {
        switch ($this->Colunas):
            case 1: $a = 'container';
                break;
            case 2: $a = 'box box-2 s7box-1';
                break;
            case 3: $a = 'box box-3 s11box-2 s7box-1';
                break;
            case 4: $a = 'box box-4 s11box-3 s7box-2';
                break;
            case 5: $a = 'box box-5 s11box-4 s9box-3 s6box-2';
                break;
            default: $a = 'box box-6 s11box-5 s9box-4 s7box-3 s6box-2 s4box-1';
        endswitch;
        return $a;
    }

    //Cria a paginação de resultados
    private function getSyntax() {
        $classes = 'fl-left pointer bg-white boxshadow hovershadow-heavy mg-small transition-easy pd-mediumb';

        //Tem mais que uma página, exibe o navegador
        $MaxLinks = $this->MaxLinks;
        
        $ant = $this->Page - 1;

        $this->Paginator .= "<ul class=\"container pd-content30 al-center paginator\">"
                            . "<a href='1' class='{$classes}'>{$this->First}</a>"
                            . "<a href='{$ant}' class='{$classes}'>anterior</a>"
                            . "<span class='fl-left pd-mediumb mg-small'>{$this->Page}</span>";

        for ($dPag = $this->Page + 1; $dPag < $this->Page + 4; $dPag ++):
            if (!isset($keyprox)):
                $keyprox = 1;
                $this->Paginator .= "<a href='{$dPag}' class='{$classes}'>próximo</a>";
            else:
                $this->Paginator .= "<a href='{$dPag}' class='{$classes}'>{$dPag}</a>";
            endif;
        endfor;

        $maximo = $dPag +20;
        $this->Paginator .= "<a href='{$maximo}' class='{$classes}'>{$this->Last}</a>"
            . "</ul>";
    }

}
