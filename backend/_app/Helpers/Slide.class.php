<?php

/**
 * Slide [ MODEL ]
 * Classe de apoio para o modelo slide. Cria slides responsivos
 *
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class Slide {

    private $Slide = '';
    private $Images;
    private $Titles;
    private $Href;
    private $Contents;
    private $Modelo = 0;
    private $Limite;
    private $Atual = 1;
    private $Pagination = true;
    private $Paginas;
    private $Mostra = false;
    private $Auto = false;
    private $Seta = 1;
    private $EstiloPage;
    private $PaginationPosition = 'top_left';
    private $Tipos;

    function __construct($e = NULL) {
        $this->Limite = ($e ? $e : 5);
        $this->Tipos['article'] = 'article';
        $this->Tipos['header'] = 'header';
        $this->Tipos['h1'] = 'h1';
    }

    /*
     * SETA IMAGENS NO SLIDE
     */

    public function setSlide($img, $title = NULL, $href = NULL, $desc = NULL) {
        if ($this->Atual <= $this->Limite):
            $this->Images[] = ($img ? $img : "");
            $this->Titles[] = ($title ? $title : "");
            $this->Href[] = ($href ? $href : "");
            $this->Contents[] = ($desc ? $desc : "");
            $this->Atual++;
        endif;
    }

    public function setEmbed() {
        $this->Slide .= "<input type='hidden' id='<?=\$gallery_embed?>' />";
    }

    /*
     * DEFINE SLIDE SEM PAGINAÇÃO
     */

    public function noPagination() {
        $this->Pagination = false;
    }

    /*
     * DEFINE SLIDE COM IMAGENS A AMOSTRA
     */

    public function galleryMode() {
        $this->Mostra = true;
    }

    /*
     * DEFINE SLIDE AUTO
     */

    public function galleryAuto() {
        $this->Auto = true;
    }

    /*
     * DEFINE SETA DO SLIDE
     */

    public function setaMode($estilo) {
        $this->Seta = (int)$estilo;
    }

    /*
     * DEFINE ESTILO DA NAVEGAÇÃO
     */

    public function setaPageStyle($estilo) {
        $this->EstiloPage = $estilo;
    }

    /*
     * Seta a posição da navegação, caso haja
     */

    public function setaPagePosition($position) {
        $this->PaginationPosition = ($position ? $position : $this->PaginationPosition);
    }

    /*
     * MOSTRA SLIDE
     */

    public function Show() {
        $this->startSlide();
        echo $this->Slide;
    }

    /*
     * RETORNA SLIDE
     */

    public function Retorno() {
        $this->startSlide();
        return $this->Slide;
    }

    /*
     * DEFINE MODELO A UTILIZAR
     */

    public function setModel($e) {
        $this->Modelo = (int)$e;
    }

    /*
     * ////////////////////////////
     * PRIVATES FUNCTIONS
     * ////////////////////////////
     */

    private function getInitial() {
        if ($this->Mostra):
            $gg = "sGallery sGallery_" . $this->Modelo;
        else:
            $gg = "slide slide_" . $this->Modelo;
        endif;

        if ($this->Auto):
            $gg .= " slideauto";
        endif;

        $this->Slide .= '<div class="container ps-relative"><section id="sGallery" class="container ps-relative ' . $gg . '">'
            . '<header class="font-zero"><h1>Slide ' . SITENAME . '</h1></header>'
            . '<div class="slide_nav container">';

        $this->getSeta() . "</div>";

        if ($this->Mostra):
            $this->Slide .= "<div class='slide_gallery slide_gallery_{$this->Modelo}' >";
        endif;
    }

    private function getFinal() {
        if ($this->Mostra):
            $this->Slide .= '</div>';
        endif;

        $this->getPagination();

        $this->Slide .= '</section>';
        $this->getModel();
        $this->Slide .= '</div>';
    }

    private function getModel() {
        if ($this->Modelo > 0):
            $this->Slide .= '<span class="container ps-relative shadow-bottom" id="shadowSlide"></span>';
        endif;
    }

    /*
     * Seta a paginação dos slides, revisando o ESTILO aplicado
     */

    private function setPaginacao($active, $i) {
        switch ($this->EstiloPage):
            case 'squad' :
                $classe = 'border-2 bg-gray';
                $content = '';
                break;
            case 'number' :
                $classe = 'bg-white pd-smallb font-size12';
                $content = $i + 1;
                break;
            case 'preview' :
                $active = $active . "' style='padding:34px 2px 0px;'";
                $classe = '';
                $content = "<img src='" . HOME . '/tim.php?src=' . $this->Images[$i] . "&w=200' width='200' style='float:initial' />";
                break;
            default :
                $classe = 'radius-circle bg-gray border-2';
                $content = '';
        endswitch;
        $this->Paginas[$i] = "<span class='pointer boxshadow {$classe} markpage_{$this->Modelo} inhover {$active}' id='{$i}'>{$content}</span>";
    }

    private function getImages() {
        if ($this->Atual <= $this->Limite):
            $this->Limite = $this->Atual - 1;
        endif;

        $f = ($this->Mostra ? 3 : 1);

        for ($e = 0; $e < $f; $e++):
            for ($i = 0; $i < $this->Limite; $i++):
                $active = ($i == 0 ? ' active' : '');
                $this->setPaginacao($active, $i);

                if ($this->Mostra) {
                    $this->SlideGallery($i, $e);
                } else {
                    $this->SlideShow($i, $e);
                }
            endfor;
            $this->Tipos['article'] = 'div';
            $this->Tipos['header'] = 'div';
            $this->Tipos['h1'] = 'div';
        endfor;
    }

    private function SlideGallery($i) {
        $marked = ($i == 2 ? '' : ' marked');

        $this->Slide .= '<' . $this->Tipos['article'] . ' rel="' . $this->Titles[$i] . '" class="fl-left slide_item_gallery gallery_' . $i . ' transition-easy slide_item_gallery_' . $this->Modelo . $marked . ' animated ps-relative" id="' . $i . '" >';

        if (isset($this->Href[$i]) && !empty($this->Href[$i])):
            $this->Slide .= '<a href="' . HOME . '/' . $this->Href[$i] . '/" class="container">';
        endif;

        $this->Slide .= '<img src="' . HOME . '/tim.php?src=' . $this->Images[$i] . '&h=400&w=905" alt="[SLIDE ' . $this->Titles[$i] . ']" title="Slide ' . $this->Titles[$i] . ' ' . SITENAME . '">'
            . '<' . $this->Tipos['header'] . ' class="slide_item_desc container pd-big">'
            . '<' . $this->Tipos['h1'] . ' class="slideh1">' . $this->Titles[$i] . '</' . $this->Tipos['h1'] . '>'
            . '<p class="tagline">' . $this->Contents[$i] . '</p>'
            . '</' . $this->Tipos['header'] . '><span class="efects_' . $this->Modelo . '"></span>';

        if (isset($this->Href[$i]) && !empty($this->Href[$i])):
            $this->Slide .= '</a>';
        endif;

        $this->Slide .= '</' . $this->Tipos['article'] . '>';
    }

    private function SlideShow($i) {
        $first = ($i == 0 ? ' first' : '');
        $this->Slide .= '<' . $this->Tipos['article'] . ' class="container slide_item slide_item_' . $this->Modelo . ' animated' . $first . '">';
        if (isset($this->Href[$i]) && !empty($this->Href[$i])):
            $this->Slide .= '<a href="' . HOME . '/' . $this->Href[$i] . '/">';
        endif;
        $this->Slide .= '<img src="' . HOME . '/tim.php?src=' . $this->Images[$i] . '&h=600&w=905" alt="[SLIDE ' . $this->Titles[$i] . ']" title="Slide ' . $this->Titles[$i] . ' ' . SITENAME . '">'
            . '<' . $this->Tipos['header'] . ' class="slide_item_desc container pd-big">'
            . '<' . $this->Tipos['h1'] . ' class="slideh1">' . $this->Titles[$i] . '</' . $this->Tipos['h1'] . '>'
            . '<p class="tagline">' . $this->Contents[$i] . '</p>'
            . '</' . $this->Tipos['header'] . '><span class="efects_' . $this->Modelo . '"></span>';
        if (isset($this->Href[$i]) && !empty($this->Href[$i])):
            $this->Slide .= '</a>';
        endif;
        $this->Slide .= '</' . $this->Tipos['article'] . '>';
    }

    private function getPagination() {
        if (isset($this->Paginas) && !empty($this->Paginas)):
            if ($this->Mostra):
                if ($this->Pagination):
                    $this->Slide .= '<div class="slide_pager_gallery ' . $this->PaginationPosition . ' slide_pager_gallery_' . $this->Modelo . '">' . implode(' ', $this->Paginas) . '</div>';
                else:
                    $this->Slide .= '<div class="ds-none slide_pager_gallery slide_pager_gallery_' . $this->Modelo . '">' . implode(' ', $this->Paginas) . '</div>';
                endif;
            else:
                if ($this->Pagination):
                    $this->Slide .= '<div class="slide_pager ' . $this->PaginationPosition . ' slide_pager_' . $this->Modelo . '">' . implode(' ', $this->Paginas) . '</div>';
                else:
                    $this->Slide .= '<div class="ds-none slide_pager slide_pager_' . $this->Modelo . '">' . implode(' ', $this->Paginas) . '</div>';
                endif;
            endif;
        endif;
    }

    private function getSeta() {
        $stylei2 = '';
        $sylei = '';
        $style = '';
        switch ($this->Seta):
            case 1:
                $class = 'ps-absolute left top barraVertical pointer';
                $stylei2 = 'margin-left:-2px;';
                break;
            case 2:
                $class = 'ps-absolute left radius-circle boxshadow bg-shadow mg-small hovershadow-heavy transition-easy pointer';
                $style = 'top:40%;padding: 12px 23px!important;height:50px;width:50px';
                $sylei = 'margin-left: -8px;';
                $stylei2 = 'margin-left:-9px;';
                break;
            case 3:
                $class = 'ps-absolute left pd-big smart';
                $style = 'top:40%;background-color:transparent!important';
                $stylei2 = 'left: 0;';
                break;
            case 4:
                $class = 'ps-absolute left radius pointer bg-shadow pd-big';
                $style = 'top:40%;height:56px;width:50px;';
                $stylei2 = 'margin-left:-9px;';
                $sylei = 'margin-left:-8px;';
                break;
            default:
                $class = 'ps-absolute left top barraVertical pointer';
        endswitch;

        if ($this->Mostra):
            $slideItem = 'slide_nav_item_gallery';
        else:
            $slideItem = 'slide_nav_item';
        endif;

        $this->Slide .= "<div class='{$slideItem} b font-size15 z-plus transition-fast {$class}' style='{$style}'><i class='shoticon shoticon-button shoticon-arrow-pure shoticon-reverse' style='{$sylei}'></i></div>";
        $class = str_replace(array(' left '), array(' right '), $class);
        $this->Slide .= "<div class='{$slideItem} g font-size15 z-plus transition-fast {$class}' style='{$style}'><i class='shoticon shoticon-button shoticon-arrow-pure' style='{$stylei2} position: inherit; height: 26px; width: 19px;'></i></div>";
    }

    private function startSlide() {
        $this->getInitial();
        $this->getImages();
        $this->getFinal();
    }

}
