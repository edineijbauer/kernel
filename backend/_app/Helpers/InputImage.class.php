<?php

/**
 * FormFile.class [ HELPER MVC ]
 * Reponsável por carregar imagens para o sistema e banco de dados por ajax.
 * Arquitetura MVC!
 * 
 * @copyright (c) 2015, Edinei J. Bauer NENA PRO
 */
class InputImage {

    private $Banco;
    private $Title;
    private $Name;
    private $Nome;
    private $Id;
    private $Html;
    private $Js;

	function __construct($name, $banco=null, $id=null, $title=null) {
        $this->Name = (string) $name;
        $this->Banco = ($banco? (string) $banco : "");
        $this->Id = ($id? (int) $id : "");
        $this->Title = ($title? (string) $title : "");
    }
	
	public function getName(){
		return $this->Nome;
	}
	
    /**
     * <b>Envia imagem e retorna o link da imagem:</b> =
     */
    public function getImage() {
		$this->setScript();
		$this->setHtml();
		echo $this->Html;
		echo $this->Js;
    }
	
	private function setHtml(){
		if(isset($this->Id) && !empty($this->Id)):
			$this->Html = '<div id="visualizar" class="container pd-small"></div><div class="ds-none" id="formfile-' .$this->Name. '"></div><form method="POST" id="formfile" enctype="multipart/form-data" action="'.HOME.'/_include/formfile/fileup.php"><input type="file" class="font-size12 font-light pd-smallb" id="filename-'. $this->Name .'" name="'. $this->Name .'" /><input type="hidden" name="campo" value="' . $this->Name . '"><input type="hidden" name="id" value="' . $this->Id . '" /><input type="hidden" name="banco" value="' . $this->Banco . '" /></form>';
		else:
			$this->Html = '<div id="visualizar" class="container pd-small"></div><div class="ds-none" id="formfile-' .$this->Name. '"></div><form method="POST" id="formfile" enctype="multipart/form-data" action="'.HOME.'/_include/formfile/fileup.php"><input type="file" class="font-size12 font-light pd-smallb" id="filename-'. $this->Name .'" name="'. $this->Name .'" /><input type="hidden" name="campo" value="' . $this->Name . '"></form>';
		endif;
	}
	
	private function setScript(){
		$this->Js = "<script type='text/javascript' src='" . HOME . "/js/jquery.form.js'></script> 
				<script type='text/javascript'> 
				$('#filename-{$this->Name}').change(function(){
					$('#visualizar').html('Enviando...'); 
					$('#formfile').ajaxForm({
						target:'#formfile-".$this->Name."'
					}).submit(); 
					$('#visualizar').html(''); 
				}); 
				</script>";
	}
}
