<?php

class Gallery {

    private $id;
    private $post_id;
    private $cover;
    private $gallery;
    private $table;

    /**
     * @param mixed $post_id
     */
    public function setPostId($post_id) {
        $this->post_id = $post_id;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
        if (!$this->cover):
            $this->checkCover();
        endif;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover) {
        $this->cover = $cover;
        $this->checkId();
        $this->checkCover();
    }

    /**
     * @return mixed
     */
    public function getGallery() {
        $this->makeGallery();
        return $this->gallery;
    }

    private function checkCover() {
        $read = new Read();
        $read->ExeRead($this->table, "WHERE id = :id", "id={$this->id}");
        if ($read->getResult()):
            $this->cover = $read->getResult()[0]["cover"] . ($read->getResult()[0]['version'] ? "?v=" . $read->getResult()[0]['version'] : "");
        endif;
    }

    private function checkId() {
        if (!$this->id):
            $read = new Read();
            $read->ExeRead($this->table, "WHERE cover = '{$this->cover}'");
            if ($read->getResult()):
                $this->id = (int)$read->getResult()[0]['id'];
            endif;
        endif;
    }


    private function makeGallery() {
        if ($this->cover):

            $dados["cover"] = "../uploads/" . $this->cover;
            $dados['id'] = $this->id;
            $dados['width'] = (int)Check::getBanco(PRE . "padrao", "width-cover", "padrao", "name");
            $dados['width'] = $dados['width'] < 30 ? 700 : $dados['width'];
            $dados['post_id'] = $this->post_id;
            $dados['banco'] = $this->table;

            $tpl = new View();
            $tpl->setBase("_imageUpload/assets/tpl");
            $this->gallery = $tpl->Retorna($dados, $tpl->Load("galleryImageControl"));

        endif;
    }
}