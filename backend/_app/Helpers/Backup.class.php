<?php

/**
 * Esta classe tem como objetivo criar backups organizados dos seus scripts no servidor
 * a fim de manter um formato recente não alterado em caso de modificações indesejadas
 *
 * @author Edinei 2016
 */
class Backup {

    private $Arquivo;
    private $Destino;

    function __construct($arquivo, $destination) {
        $this->Arquivo = $arquivo;
        $this->Destino = $destination;

        // Make sure the script can handle large folders/files
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');
        $this->setZip();
    }

    private function setZip() {
        if (extension_loaded('zip') === true) {
            if (file_exists($this->Arquivo) === true) {
                $zip = new ZipArchive();

                if ($zip->open($this->Destino, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE ) === true) {
                    $this->Arquivo = realpath($this->Arquivo);

                    if (is_dir($this->Arquivo) === true) {
                        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->Arquivo), RecursiveIteratorIterator::SELF_FIRST);

                        foreach ($files as $file) {
                            $file = realpath($file);

                            if (is_dir($file) === true) {
                                $zip->addEmptyDir(str_replace($this->Arquivo . '/', '', $file . '/'));
                            } else if (is_file($file) === true) {
                                $zip->addFromString(str_replace($this->Arquivo . '/', '', $file), file_get_contents($file));
                            }
                        }
                    } else if (is_file($this->Arquivo) === true) {
                        $zip->addFromString(basename($this->Arquivo), file_get_contents($this->Arquivo));
                    }
                }
                return $zip->close();
            }
        }

        return false;
    }

}
