<?php

/**
 * Check.class [ HELPER ]
 * Classe responável por manipular e validade dados do sistema!
 * 
 * @copyright (c) 2015, Edinei J. Bauer
 */
class Android {

    private static $Data;
    private static $Format;

    public static function removeCharactersUnrelevantAndroid($titulo) {
        $titulo_back = $titulo;
        $titulo = strtolower(trim($titulo));
        if (preg_match('/v\d/i', $titulo)):  
            if (preg_match('/v0/i', $titulo)):
                $t = explode('v0', $titulo);
            elseif (preg_match('/v1/i', $titulo)):
                $t = explode('v1', $titulo);
            elseif (preg_match('/v2/i', $titulo)):
                $t = explode('v2', $titulo);
            elseif (preg_match('/v3/i', $titulo)):
                $t = explode('v3', $titulo);
            elseif (preg_match('/v4/i', $titulo)):
                $t = explode('v4', $titulo);
            elseif (preg_match('/v5/i', $titulo)):
                $t = explode('v5', $titulo);
            elseif (preg_match('/v6/i', $titulo)):
                $t = explode('v6', $titulo);
            elseif (preg_match('/v7/i', $titulo)):
                $t = explode('v7', $titulo);
            elseif (preg_match('/v8/i', $titulo)):
                $t = explode('v8', $titulo);
            elseif (preg_match('/v9/i', $titulo)):
                $t = explode('v9', $titulo);
            endif;
            if (isset($t[0])):
                $titulo = trim($t[0]);
            endif;
        endif;

        if (preg_match('/\(/i', $titulo)):
            $t = explode('(', $titulo);
            $titulo = trim($t[0]);
        endif;

        $tituloA = [" + ", "build", "unlocked", "unlock", "torrent", "donwload", "download", "apk", "full", "obb", "mod", "money", "data", "hack", "    ", "   ", "  ", "&amp;"];
        $tituloB = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", " ", " ", " ", "&"];
        $titulo = trim(utf8_decode(str_replace($tituloA, $tituloB, $titulo)));
        
        return (empty($titulo) ? $titulo_back: $titulo);
    }

    public static function getUrlBackupApp($user_id){
        $read = new Read();
        $read->ExeRead(PRE . "user_site", "WHERE user_id=:ui", "ui={$user_id}");
        if($read->getResult()):
            return $read->getResult()[0]['site_name'] . RECEIVE_PATH . "receive_backup_app.php";
        endif;
    }

    public static function getUrlProdutoApp($user_id){
        $read = new Read();
        $read->ExeRead(PRE . "user_site", "WHERE user_id=:ui", "ui={$user_id}");
        if($read->getResult()):
            return $read->getResult()[0]['site_name'] . RECEIVE_PATH . "receive_produto_app.php";
        endif;
    }

    public static function getUrlProdutoAppRascunho($user_id){
        $read = new Read();
        $read->ExeRead(PRE . "user_site", "WHERE user_id=:ui", "ui={$user_id}");
        if($read->getResult()):
            return $read->getResult()[0]['site_name'] . RECEIVE_PATH . "receive_produto_app_rascunho.php";
        endif;
    }
    
    /*
     * Passe um nome de aplicativo, e retorna um link de app relativo a busca
     */

    public static function PdaBusca($nomeapp) {
        $read = new Read;
        $read->ExeRead(PRE . "produto", "WHERE produto_title='{$nomeapp}' ORDER BY id DESC LIMIT 1");
        if ($read->getResult()):
            if (!empty($read->getResult()[0]['produto_pda'])):
                $key_pass = 1;
                $this->Data = $read->getResult()[0]['produto_pda'];
            endif;
        endif;

        if (!isset($key_pass)):
            $post_title = $nomeapp;
            $nomeapp = strtolower($nomeapp);
            $nomeapp = trim(str_replace(array('pro', '50', 'off', 'adw', 'theme', 'premium', '   ', '  '), array('', '', '', '', '', '', ' ', ' '), $nomeapp));
            $busca = str_replace(' ', '+', $nomeapp);
            $motor = file_get_contents('http://pdalife.ru/search/?os=2&s=' . $busca);
            $motor3 = explode('<div class="b-application b-application_size_biggest b-application_type_in-catalog">', $motor);
            $i = 0;
            $em = 0;
            $conta = 100;
            foreach ($motor3 as $result):
                $i++;
                if ($i > 1 && $i < 5):
                    $title1 = explode('alt="', $result);
                    $title2 = explode('"', $title1[1]);

                    $title = trim(str_replace(array(' - PDAlife.ru', ' для андроид', 'android', 'для', '&#1072;&#1085;&#1076;&#1088;&#1086;&#1080;&#1076;', ' &#1076;&#1083;&#1103;', '   ', '  '), array('', '', '', '', '', '', '', ''), $title2[0]));

                    // Verifica qual o link mais qualificado
                    $titlev = strtolower($title);
                    $keytitle = explode($nomeapp, $titlev);
                    $nomeapp = Check::Name($nomeapp);
                    $keytitle[0] = Check::Name($keytitle[0]);

                    if (isset($keytitle[1]) || $keytitle[0] == $nomeapp):
                        $em1 = explode('<b>', $title);
                        if (count($em1) >= $em):
                            $em = count($em1);
                            $conta1 = explode(' ', $title);
                            if (count($conta1) < $conta):
                                $link1 = explode('<a href="', $result);
                                $link2 = explode('"', $link1[1]);
                                $link = trim($link2[0]);
                                $conta = count($conta1);
                                $this->Data = $link;
                            endif;
                        endif;
                    endif;
                endif;
            endforeach;

            if ($conta != 100):
                $key_pass = 1;
            endif;

        endif;
        if (isset($key_pass)):
            return $this->Data;
        else:
            return false;
        endif;
    }

    /*
     * Passe um url (link de aplicativo) da pda e tenha um retorno dos links de download
     */

    public static function PdaDownload($busca, $url = NULL) {
        if (!isset($url) || empty($url)):
            $searchpda = new SearchPDA($busca);
            $url = $searchpda->getUrl();
        endif;
        if (strlen($url) > 10):
            $site = Check::file_get($url);
            if ($site[1]['http_code'] === '200'):
                $site = $site[0];

                $body1 = explode('<div class="b-application__links">', $site);
                $body2 = explode('<!--noindex-->', $body1[1]);

                $body = '<div><div class="links_place">' . $body2[0];
                $body = str_replace(array('много', 'Много ', 'денег', 'установить', 'запустить', 'игру', 'Free', 'Скачать', 'Мб', 'много денег', 'Кэш к игре', 'папку из архива распаковать в', 'должно получится так', 'размер распакованного кэша', 'кэш', 'через торрент', 'все открыто', 'Гб', 'кеш', 'Кэш к игре', 'разместить в ту же папку что и первый, можно начать играть и без него', 'папку их архива распаковать', 'если у вас установлена версия', 'то вместо всего dataа можно скачать только патч', 'патч', 'бесконечные патроны', 'графика', 'для', 'или', 'все разблокировано', 'мод облегчения игры', 'свободные покупки', 'много жизней', 'мод', 'ключ', 'открыты', 'все', 'тачки'), array('um monte', 'um monte ', 'de dinheiro', 'instalar', 'executar o', 'jogo', 'Gratis', 'Download', 'MB', 'Mod dinheiro', 'Data do jogo', 'descompacte a pasta de arquivos em', 'deve ficar assim', 'o tamanho final da data deve ficar', 'data', 'por torrent', ' tudo desbloqueado', 'Gb', 'data', 'Data do jogo', 'colocado na mesma pasta que o primeiro, você pode começar a jogar sem ele', 'descompactar a pasta de dados em', 'se você tiver uma versão', 'pode baixar somente o patch (não precisa baixar a data)', 'patch', 'munição infinita', 'gráficos', 'para', 'ou', 'tudo desbloqueado', 'modos de facilitar o jogo', 'compras grátis', 'muitas vidas', 'mod', 'chave', 'aberto', 'todos os', 'carros'), $body);
                $body = str_replace(array('b-download-button', 'b-application__links-tag', 'b-application__additional-link', ' popupTrigger', 'b-button_size_big', 'b-button_size_middle', ' js-mirror-dwn-btn', 'b-button', ' btn btn-green mg-5'), array('btn btn-green', 'taglink', 'downloadlink', '', 'btn btn-green mg-5', 'btn btn-green mg-5', '', '', 'btn btn-green mg-5'), $body);

                $body = str_replace('<a ', '<a rel="nofollow" ', $body);
                if (!empty($body)):
                    return $body;
                endif;
            endif;
        endif;
    }

    public static function getInside($div, $content) {
        $q = explode(' ', $div);
        $r = $q[0];
        $q = str_replace('<', '</', $q[0]) . '>';
        $a = explode($div, $content);
        $z = explode($q, $a[1]);

        $i = 0;
        $div = '';
        foreach ($z as $f):
            $v = explode($r, $div);
            if (count($v) >= $i):
                $div .= $f . $q;
            endif;
            $i++;
        endforeach;

        return $r . '>' . $r . '>' . $div;
    }

    public static function MobSearch($nome) {
        $nomeapp = trim(strtolower($nome));
        $nomeapp = str_replace(array('pro', '50', 'off', '   ', '  ', ' '), array('', '', '', ' ', ' ', '_'), $nomeapp) . '.html';

        // Inicia o cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://play.mob.org.pt/xrequest/search/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "razdel=android&search_word={$nome}&limit=10;&offset=0");
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $store = curl_exec($ch);
        curl_close($ch);

        return $store;
    }

    public static function MobDownload($store) {

        if ($store && preg_match('/\"url\":\"/i', $store)):
            $a = explode('"url":"', $store);
            $a = explode('"', $a[1]);
            $url = str_replace(array('\/', '\.'), array('/', '.'), $a[0]);

            $motor = Check::file_get($url);
            if ($motor[0]):
                if (!preg_match('/<title>Error 404<\/title>/i', $motor[0])):
                    $div = Android::getInside('<div class="links">', $motor[0]);
                    $a = ['http://help.mob.org.pt/post/910/', 'class="link"', 'class="button-block"', 'btn btn-green', 'class="info"', 'class="text"', 'class="caption"', 'class="cache-info"'];
                    $b = [HOME . '/page/como-instalar-o-jogo-com-cache/', 'class="container"', 'class="pd-mediumb"', 'btn btn-green pd-mediumb font-size13 font-light transition-fast', 'class="al-center container font-size12 font-light pd-medium"', 'class="textDownload"', 'class="container font-bold font-size12 al-center pd-mediumb"', 'class="container al-center pd-mediumb"'];
                    return str_replace($a, $b, $div);
                endif;
            endif;
        endif;
    }

    /*
     * Busca aplicativo na google play
     */

    public static function PlaySearch($busca) {
        $busca2 = str_replace(array(' ', 'ç', 'ó', 'á', 'ã', 'í', 'ú', 'é', 'õ', "'", '"', ':', '´'), array('%20', '%C3%A7', '%C3%B3', '%C3%A1', '%C3%A3', '%C3%AD', '%C3%BA', '%C3%A9', '%C3%B5', '%27', '%22', '%3A', '%C2%B4'), $busca);
        if (preg_match('/(^http|\w+\.\w+)/i', $busca)):

            if (preg_match('/\&hl=/i', $busca)):
                $tplay = explode('&hl=', $busca);
                $busca = $tplay[0];
                unset($tplay);
            endif;
            $busca = str_replace('https://play.google.com/store/apps/details?id=', '', $busca);
            return "https://play.google.com/store/apps/details?id={$busca}&hl=pt-BR";
        else:
            $a = file_get_contents("https://play.google.com/store/search?q={$busca2}&c=apps");
        endif;

        if (preg_match('/id-card-list card-list two-cards/i', $a)):
            $corpo = explode('id-card-list card-list two-cards', $a);
            $cards = explode('square-cover', $corpo[1]);
            $i = 0;
            foreach ($cards as $card):
                if (preg_match('/class=\"title\"/i', $card)):
                    $e = explode('class="title"', $card);
                    $e = explode('>', $e[1]);
                    $e = explode('<', $e[1]);
                    $title = trim($e[0]);

                    $i++;
                    if ($i < 15):
                        //máximo, os primeiros 15 resultados
                        $title = trim(str_replace(array(' - PDAlife.ru', ' для андроид', 'android', 'для', '&#1072;&#1085;&#1076;&#1088;&#1086;&#1080;&#1076;', ' &#1076;&#1083;&#1103;', '   ', '  '), array('', '', '', '', '', '', '', ''), $title));

                        // Verifica qual o link mais qualificado
                        $title = strtolower($title);
                        $busca = strtolower($busca);
                        $tem_no_titulo = explode($busca, $title);

                        $key_busca = Check::Name($busca);
                        $key_title = Check::Name($title);

                        //se existir o titulo buscado no titulo do aplicativo em questão OU se os titulos forem iguais
                        //####  debug   ######
                        //echo $key_title . ' ___ ' . $key_busca . '<hr>';

                        if (isset($tem_no_titulo[1]) || $key_title == $key_busca):
                            //caso passe, busca o link do play
                            $u = explode('class="card-click-target"', $card);
                            $u = explode('href="', $u[1]);
                            $u = explode('"', $u[1]);
                            $u = 'https://play.google.com' . $u[0];

                            break;
                        else:
                            //se ainda não tiver um link secundario
                            if (!isset($u2)):
                                //verificação detalhada do titulo
                                if (preg_match('/-/i', $key_busca)):
                                    $key_itens = explode('-', $key_busca);
                                    $erro = 0;
                                    $conta = count($key_itens);
                                    foreach ($key_itens as $item):
                                        if (!preg_match("/{$item}/i", $key_title)):
                                            $erro++;
                                        endif;
                                    endforeach;

                                    if ($erro === 0):
                                        //todas as palavras coincediram com o titulo
                                        $u2 = explode('class="card-click-target"', $card);
                                        $u2 = explode('href="', $u2[1]);
                                        $u2 = explode('"', $u2[1]);
                                        $u2 = 'https://play.google.com' . $u2[0];
                                    else:
                                        if ((($erro * 100) / $conta > 66) && $conta > 2):
                                            //mais que 75% das palavras coincediram, sendo que no mínimo deve ter 3 palavras
                                            $u2 = explode('class="card-click-target"', $card);
                                            $u2 = explode('href="', $u2[1]);
                                            $u2 = explode('"', $u2[1]);
                                            $u2 = 'https://play.google.com' . $u2[0];
                                        endif;
                                    endif;
                                endif;
                            endif;
                        endif;
                    endif;
                endif;
            endforeach;
        endif;

        if (isset($u)):
            return $u;
        else:
            if (isset($u2)):
                return $u2;
            else:
                return false;
            endif;
        endif;
    }

    /*
     * Através de um link da googleplay, envia o app para o backup
     */

    public static function sendToBackup($play) {
        $add = new AdminCreate($play);
        $Data = $add->getData();

        if (isset($Data['post_title']) && !empty($Data['post_title']) && isset($Data['post_cover']) && !empty($Data['post_cover'])):

            $Data['categories'] = implode(',', $Data['categories']);
            $Data['gallery'] = implode(',', $Data['gallery']);
            $Data['date'] = date('Y-m-d H:i:s');
            $Data['status'] = 1;
            if (!mb_check_encoding($Data['post_content'], "UTF-7")) {
                $ar = ['+ADw-', '+AD4-', '/p+AD4APA', '/b+AD4APA-', '+APU-', '+AOE-', '+AOcA4w-', '+AOM-', '+AO0-', '+AOo', '+AOk-', '+AOc-', '+AOE', '+IAsgCw-', '+ACo', '+AOI-', '+ACE', '+AOk'];
                $ar2 = ['<', '>', '', '', 'õ', 'á', 'çã', 'ã', 'í', 'ê', 'é', 'ç', 'á', '', 'x', 'â', '', 'é'];
                $Data['post_content'] = str_replace($ar, $ar2, mb_convert_encoding($Data['post_content'], "UTF-7"));
            }

            if (preg_match('/(и|н|к|в|а|я|ф|ж|д|г|у|б|п|л|з)/i', $Data['post_content']) || preg_match('/\?\?\?\?\?\?\?/i', $Data['post_content'])):
                $Data['status'] = 0;
                $Data['post_content'] = '';
            endif;

            if (empty($Data['post_title']) || empty($Data['post_name'])):
                $Data['status'] = 0;
            endif;

            $read = new Read();
            $read->ExeRead(PRE . "posts_backup", "WHERE produto_play='{$Data['produto_play']}'");
            if ($read->getResult()):
                $up = new Update();
                $up->ExeUpdate(PRE . "posts_backup", $Data, "WHERE id=:p", "p={$read->getResult()[0]['id']}");

                return $read->getResult()[0]['id'];

            else:

                $create = new Create();
                $create->ExeCreate(PRE . "posts_backup", $Data);
                if ($create->getResult()):
                    return $create->getResult();
                else:
                    return false;
                endif;

            endif;
        else:
            return false;
        endif;
    }

}
