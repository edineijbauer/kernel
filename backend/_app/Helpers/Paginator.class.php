<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 30/03/2017
 * Time: 19:25
 */
class Paginator {
    private $table;
    private $offset;
    private $limite;
    private $where;
    private $order;
    private $result;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $offset
     */
    public function setOffset($offset) {
        $this->offset = $offset;
    }

    /**
     * @param mixed $limite
     */
    public function setLimite($limite) {
        $this->limite = $limite;
    }

    /**
     * @param mixed $where
     */
    public function setWhere($where) {
        $this->where = $where;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order) {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        $this->exePager();
        return $this->result;
    }

    private function exePager() {
        $this->where = ($this->where ? $this->where : "id > 0");

        $read = new Read();
        $read->ExeRead(PRE . $this->table, "WHERE {$this->where} ORDER BY {$this->order} LIMIT :offset, :limit", "offset={$this->offset}&limit={$this->limite}");
        if ($read->getResult() && (($this->offset > 1) || ($this->offset < 2 && $read->getRowCount() >= $this->limite))):
            $this->getSyntax('<<', '>>');
        endif;
    }

    //Cria a paginação de resultados
    private function getSyntax($first, $last, $links = 4) {
        $classes = 'fl-left pointer radius inhover bg-secondary mg-small transition-easy pd-mediumb';
        $this->result .= "<ul class=\"container pd-content30 al-center paginator\">";

        $atual = ($this->offset >= $this->limite ? (($this->offset / $this->limite) + 1) : 1);

        $ant = $atual - 1;

        if ($ant > 0):
            if ($ant > 1):
                $this->result .= "<a href='1' class='{$classes}'>{$first}</a>" . "<a href='{$ant}' class='{$classes}'>{$ant}</a>";
            else:
                $this->result .= "<a href='1' class='{$classes}'>{$first}</a>";
            endif;
        endif;

        $this->result .= "<span class='fl-left pd-mediumb mg-small'>{$atual}</span>";

        $read = new Read();
        $read->ExeRead(PRE . $this->table, "WHERE {$this->where} ORDER BY {$this->order}");
        if ($read->getResult()):
            $paginas = ceil($read->getRowCount() / $this->limite);
        endif;

        for ($dPag = $atual + 1; $dPag < $atual + $links; $dPag++):
            if ($dPag < $paginas):
                $this->result .= "<a href='{$dPag}' class='{$classes}'>{$dPag}</a>";
            endif;
        endfor;

        if ($atual < $paginas):
            $this->result .= (isset($paginas) ? "<a href='{$paginas}' class='{$classes}'>{$last}</a>" : "");
        endif;

        $this->result .= "</ul>";
    }
}