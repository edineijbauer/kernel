<?php

/**
 * Check.class [ HELPER ]
 * Classe responável por manipular e validade dados do sistema!
 *
 * @copyright (c) 2015, Edinei J. Bauer
 */
class Check {

    private static $Data;
    private static $Format;

    public static function replace_string($from, $to, $subject, $limit = 100000) {
        $from = '/' . preg_quote($from, '/') . '/';
        return preg_replace($from, $to, $subject, $limit);
    }

    public static function listDirectory($dir) {
        $directory = scandir($dir);
        foreach ($directory as $b):
            if ($b !== "." && $b !== ".."):
                $directory[] = $b;
            endif;
        endforeach;

        return $directory;
    }
	
    public static function getNameBanco($banco) {
        $banco = str_replace(array(PRE, ' ', '_'), array("", '-', '-'), $banco);
        $array1 = ['colecao', 'category', 'gallery', 'review'];
        $array2 = ['colecão', 'categoria', 'galeria', 'avaliação'];
        $banco = str_replace($array1, $array2, $banco);
        if (preg_match('/^-/i', $banco)):
            $banco = substr($banco, 1);
        endif;
        if (preg_match('/-$/i', $banco)):
            $banco = substr($banco, 0 - 1);
        endif;

        return ucwords(trim(str_replace(array('-', '_'), " ", $banco)));
    }
	

    /**
     * <b>recupera a resposta de um padrão:</b> passa um nome de padrão e obtem seu valor ou um else.
     * @param STRING $name = nome do padrão
     * @param STRING $else = caso não tenha este padrão
     */
    public static function getPattern($name, $else = null) {
        $else = (isset($else) ? $else : "");
        $read = new Read();
        $read->ExeRead(PRE . "padrao", "WHERE name=:tt && user_id=:ff", "tt={$name}&ff={$_SESSION['userlogin']['id']}");
        if ($read->getResult()):
            return $read->getResult()[0]['padrao'];
        else:
            $Dados['padrao'] = $else;
            $Dados['name'] = $name;
            $Dados['user_id'] = $_SESSION['userlogin']['id'];
            $create = new Create();
            $create->ExeCreate(PRE . "padrao", $Dados);
            return $else;
        endif;
    }

    public static function getUrlPai($url) {
        $url = str_replace(array("https://", "http://", "www.", ".html", ".asp", ".php"), "", $url);
        return explode('/', $url)[0];
    }

    /**
     * <b>Verifica E-mail:</b> Executa validação de formato de e-mail. Se for um email válido retorna true, ou retorna false.
     * @param STRING $Email = Uma conta de e-mail
     * @return BOOL = True para um email válido, ou false
     */
    public static function Email($Email) {
        self::$Data = (string)$Email;
        self::$Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

        if (preg_match(self::$Format, self::$Data)):
            return true;
        else:
            return false;
        endif;
    }

    public static function Codificacao($term) {
        $term = Check::convert_utf8($term);
        $content_back = $term;
        $i = -1;
        while (preg_match('/(\&amp;lt;|\&amp;|\&lt;|\&gt;)/i', $term) && $i < 8):
            $term = $content_back;
            $i++;

            if ($i === 0):
                $term = htmlspecialchars($term);
            elseif ($i === 1):
                $term = htmlentities($term);
            elseif ($i === 2):
                $term = htmlspecialchars(htmlentities($term));
            elseif ($i === 3):
                $term = html_entity_decode($term);
            elseif ($i === 4):
                $term = htmlspecialchars_decode($term);
            elseif ($i === 5):
                $term = htmlspecialchars_decode(html_entity_decode($term));
            elseif ($i === 6):
                $term = htmlentities(htmlspecialchars($term));
            elseif ($i === 7):
                $term = html_entity_decode(htmlspecialchars_decode($term));
            endif;

            $term = (strlen($term) < 3 ? $content_back : $term);
        endwhile;

        if ($i === 8):
            return false;
        endif;

        return strip_tags($term);
    }

    public static function convert_utf8($source) {
        // detect the character encoding of the incoming file
        $encoding = mb_detect_encoding($source, "auto");

        // escape all of the question marks so we can remove artifacts from
        // the unicode conversion process
        $target = str_replace("?", "[question_mark]", $source);

        // convert the string to the target encoding
        $target = mb_convert_encoding($target, "UTF-8", $encoding);

        // remove any question marks that have been introduced because of illegal characters
        $target = str_replace("?", "", $target);

        // replace the token string "[question_mark]" with the symbol "?"
        $target = str_replace("[question_mark]", "?", $target);

        return $target;
    }

    /**
     * <b>recupera a resposta de um padrão:</b> passa um nome de padrão e obtem seu valor ou um else.
     * @param STRING $name = nome do padrão
     * @param STRING $else = caso não tenha este padrão
     */
    public static function Notifica($title, $content, $level = 1, $user = null) {
        $user = ($user ? $user : 1);
        $idnot = md5($content) . $user;
        $read = new Read();
        $read->ExeRead(PRE . "notificacoes", 'WHERE id_not=:idd', "idd={$idnot}");
        if (!$read->getResult()):

            $Not['not_title'] = $title;
            $Not['not_content'] = $content;
            $Not['not_date'] = date('Y-m-d H:i:s');
            $Not['not_level'] = $level;
            $Not['user_id'] = $user;
            $Not['id_not'] = $idnot;

            $create = new Create;
            $create->ExeCreate(PRE . "notificacoes", $Not);
        else:
            //já existe esta notificação, realça o level
            if ($title == 'Busca não Encontada!'):
                $Not['not_level'] = $read->getResult()[0]['not_level'] + 1;
                $up = new Update();
                $up->ExeUpdate(PRE . "notificacoes", $Not, 'WHERE not_title=:tt && id_not=:id', "tt={$title}&id={$idnot}");
            endif;
        endif;
    }

    public static function nucleoPadrao($n) {
        if (preg_match('/(dual|2 nu|2 co)/i', $n)):
            return 2;
        elseif (preg_match('/(quad|4 nu|4 co)/i', $n)):
            return 4;
        elseif (preg_match('/(six|6 nu|6 co|hex)/i', $n)):
            return 6;
        elseif (preg_match('/(octa|8 nu|8 co)/i', $n)):
            return 8;
        elseif (preg_match('/(deca|10 nu|10 co)/i', $n)):
            return 10;
        else:
            return 1;
        endif;
    }

    public static function nucleoPadraoNome($n) {
        if ($n === 2 || preg_match('/(dual|2 nu|2 co)/i', $n)):
            return "Dual Core";
        elseif ($n === 4 || preg_match('/(quad|4 nu|4 co)/i', $n)):
            return "Quad Core";
        elseif ($n === 6 || preg_match('/(six|6 nu|6 co|hex)/i', $n)):
            return "Hexa Core";
        elseif ($n === 8 || preg_match('/(octa|8 nu|8 co)/i', $n)):
            return "Octa Core";
        elseif ($n === 10 || preg_match('/(deca|10 nu|10 co)/i', $n)):
            return "Deca Core";
        else:
            return "Single Core";
        endif;
    }

    public static function chipPadrao($chip) {
        if (preg_match('/nano/i', $chip)):
            return "nano";
        elseif (preg_match('/micro/i', $chip)):
            return "micro";
        else:
            return $chip;
        endif;
    }

    public static function chipDualPadrao($chip) {
        if (preg_match('/(not|no )/i', $chip)):
            return 0;
        elseif (preg_match('/(supported|yes)/i', $chip)):
            return 1;
        else:
            return 0;
        endif;
    }

    public static function statusPadrao($status) {
        if (preg_match('/(release|lançado|dispon)/i', $status)):
            return "lançado";
        elseif (preg_match('/(announced|anunciado)/i', $status)):
            return "anunciado";
        elseif (preg_match('/rumor/i', $status)):
            return "rumor";
        else:
            return $status;
        endif;
    }

    public static function resolucaoPadrao($resolucao) {
        $resolucao = str_replace(array('.', ','), '', $resolucao);
        if (preg_match('/(2160|3840|4k|ultra)/i', $resolucao)):
            return "4k";
        elseif (preg_match('/(2560|1440|quad|2k)/i', $resolucao)):
            return "2k";
        elseif (preg_match('/(1920|1080|full)/i', $resolucao)):
            return "fullhd";
        elseif (preg_match('/(1280|720|1366|768|hd)/i', $resolucao)):
            return "hd";
        elseif (preg_match('/(854|480|960|vga)/i', $resolucao)):
            return "fwvga";
        else:
            return null;
        endif;
    }

    /**
     * <b>Obtem IP real:</b> obtem o IP real do usuário que esta acessando
     */
    public static function GetIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])):
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])):
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else:
            $ip = $_SERVER['REMOTE_ADDR'];
        endif;
        return $ip;
        unset($ip);
    }


    /**
     * <b>Tranforma URL:</b> Tranforma uma string no formato de URL amigável e retorna o a string convertida!
     * @param STRING $Name = Uma string qualquer
     */
    public static function Name($Name) {
        self::$Format = array();
        self::$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr|"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª¹²³£¢¬™®★’`§☆●•…”“’‘♥♡■◎≈◉';
        self::$Format['b'] = "aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                                            ";

        self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), self::$Format['b']);
        self::$Data = strip_tags(trim(self::$Data));
        self::$Data = str_replace(' ', '-', self::$Data);
        self::$Data = str_replace(array('-----', '----', '---', '--'), '-', self::$Data);

        return str_replace('?', '-', utf8_decode(strtolower(utf8_encode(self::$Data))));
    }

    public static function Cpf($cpf = false) {

        $cpf = preg_replace('/[^0-9]/', '', (string)$cpf);
        if (strlen($cpf) !== 11 || $cpf === '00000000000' || $cpf === '11111111111' || $cpf === '22222222222' || $cpf === '33333333333' || $cpf === '44444444444' || $cpf === '55555555555' || $cpf === '66666666666' || $cpf === '77777777777' || $cpf === '88888888888' || $cpf === '99999999999'):
            return false;
        endif;

        for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--):
            $soma += $cpf{$i} * $j;
        endfor;

        $resto = $soma % 11;
        if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto)):
            return false;
        endif;

        for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--):
            $soma += $cpf{$i} * $j;
        endfor;

        $resto = $soma % 11;
        return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);

    }

    /**
     * <b>Arruma Strings:</b> Tranforma uma string no formato correto quando não reconhecido
     * @param STRING $Name = Uma string qualquer
     */
    public static function Strings($Name) {
        $Name = str_replace(array("<![CDATA[", "]]>", "&lt;", "&gt;", "&quot;"), array("", "", "<", ">", '"'), $Name);
        self::$Format = array();
        self::$Format['a'] = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr'$*{[}];\\\'°ºª¹²³£¢¬™®★’`§☆●•…”“’‘♥♡■◎≈◉–’";
        self::$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr"                             .""""       "';

        self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), self::$Format['b']);
        $ad = array("???", "amp", "Amp", " 124", "8210", "8211", "8212", "8213", "8214", "8215", "8216", "8217", "8218", "8219", "8220", "8221", "8222", "8223", "8224", "8225", "&#39;");
        $ab = array("&star;", "e", "e", ":", "", "", "", "", "", "", "", "", "", "", '"', '"', "", "", "", "", "'");
        self::$Data = trim(str_replace($ad, $ab, self::$Data));
        return utf8_encode(self::$Data);
    }

    /**
     * <b>Obtem uma avaliação em estrelas:</b>
     * @idretorno STRING $idretorno = envia o id para retorno da avaliação
     */
    public static function Star($idretorno, $nota = NULL, $max = NULL) {
        $max = (int)($max ? $max : 5);
        $content = '<ul class="al-center font-size11 starbox">';

        for ($i = 0; $i < $max; $i++):
            if ($nota >= $i):
                $content .= '<li rel="' . $idretorno . '" alt="' . $i . '" class="fl-left pointer smart transition-easy star font-size15 font-bold">&starf;</li>';
            else:
                $content .= '<li rel="' . $idretorno . '" alt="' . $i . '" class="fl-left pointer smart transition-easy star font-size15 font-bold">&star;</li>';
            endif;
        endfor;

        $content .= '<div class="fl-left default pd-smallb font-size13" id="exibi-' . $idretorno . '">' . $nota . '</div><input type="hidden" id="' . $idretorno . '" class="avalStar" value="' . $nota . '" /></ul>';
        return $content;
    }

    /**
     * <b>Obtem dados do link:</b> obtem os dados do link informado
     * @param STRING $url = url do site requisitado
     */
    public static function send_to($url, $postinfo) {
        if (is_array($postinfo)):
            $postinfo = json_encode($postinfo);
        endif;

        $endereco = str_replace("&amp;", "&", urldecode(trim($url)));

        $cookie = tempnam("/tmp", "CURLCOOKIE");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endereco);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "smartphone={$postinfo}");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $store = curl_exec($ch);
        curl_close($ch);
        return $store;
    }

    /**
     * <b>Obtem dados do link:</b> obtem os dados do link informado
     * @param STRING $url = url do site requisitado
     */
    public static function file_get($url, $javascript_loop = 0, $timeout = 15) {
        $url = str_replace("&amp;", "&", urldecode(trim($url)));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");
        curl_setopt($ch, CURLOPT_REFERER, $url);

        curl_setopt($ch, CURLOPT_COOKIEJAR, tempnam("/tmp", "CURLCOOKIE"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate, br");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);

        if ($response['http_code'] == 301 || $response['http_code'] == 302) {
            ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

            if ($headers = get_headers($response['url'])) {
                foreach ($headers as $value) {
                    if (substr(strtolower($value), 0, 9) == "location:")
                        return get_url(trim(substr($value, 9, strlen($value))));
                }
            }
        }

        if ((preg_match("/>[[:space:]]+window\.location\.replace\('(.*)'\)/i", $content, $value) || preg_match("/>[[:space:]]+window\.location\=\"(.*)\"/i", $content, $value)) && $javascript_loop < 5) {
            return get_url($value[1], $javascript_loop + 1);
        } else {
            return array($content, $response);
        }
    }

    public static function PostRequest($url, $data) {
        $url = str_replace("&amp;", "&", urldecode(trim($url)));

        $teste_conexao = Check::file_get($url);
        if ($teste_conexao[1]['http_code'] === 200):

            $options = array('http' => array('header' => "Content-type: application/x-www-form-urlencoded\r\n", 'method' => 'POST', '_content' => http_build_query($data)));
            $context = stream_context_create($options);

            return file_get_contents($url, false, $context);

        else:

            return false;

        endif;
    }

    /**
     * <b>Verifica se é imagem:</b> verifica se a url passado é uma imagem
     * @param STRING $img = url da imagem
     */
    public static function IsImage($img) {
        $result = Check::file_get($img);
        if (!preg_match('/image/i', $result[1]['content_type']) || empty($result[0]) || $result[0] == ''):
            return false;
        else:
            return true;
        endif;
    }

    /**
     * <b>Arruma o link:</b> os links muitas vezes são passados de diferentes formas, padroniza-las!
     * @param STRING $url = a url a padronizar
     */
    public static function FilterUrl($url, $domain = null) {

        if ($domain):
            $domain = (preg_match('/\/$/i', $domain) ? substr($domain, 0, -1) : $domain);
            $domain = str_replace(array("https://", "http://"), "", $domain);
            $domains = explode("/", $domain);
            $domain = "http://" . $domains[0];
            unset($domains[count($domains) - 1], $domains[0]);
            $count = count($domains);
        endif;

        if (preg_match('/^\/\//', $url) || preg_match('/^www\./', $url)):
            $url = 'http:' . (preg_match('/^www\./', $url) ? "//" : "") . $url;

        elseif ((preg_match('/^\//', $url) || preg_match('/^(\w|-)/i', $url)) && !preg_match('/^http:/i', $url) && $domain):
            $url = $domain . (!preg_match('/^\//', $url) ? "/" : "") . $url;

        elseif (preg_match('/^\.\./', $url) && $domain):
            if (preg_match_all('/\.\.\//i', $url, $matches) > 0):
                foreach ($matches[0] as $i => $t):
                    unset($domains[$count - $i]);
                endforeach;
            endif;

            $url = $domain . (!empty($domains) ? '/' . implode("/", $domains) : "") . '/' . str_replace('../', '', $url);

        elseif (preg_match('/^\./', $url) && $domain):
            $url = $domain . (isset($domains[1]) ? "/" . $domains[1] : "") . (isset($domains[2]) ? "/" . $domains[2] : "") . (isset($domains[3]) ? "/" . $domains[3] : "") . str_replace('./', "/", $url);
        endif;

        $testeHttp = explode("http", $url);
        if (isset($testeHttp[2])):
            $url = "http" . $testeHttp[count($testeHttp) - 1];
        endif;
        return $url;
    }

    /**
     * <b>Online:</b> Verifica se a url passada esta online e funcionando
     * @param STRING $url = Url a ser verifica o status online
     */
    public static function Online($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($code > 104 && $code != 404 && $code != 402 && $code != 401 && $code != 400):
            return true;
        endif;
        return false;
    }

    /**
     * <b>getBanco:</b> Obtem um valor do banco passando o id ou um determinado valor
     * @param STRING $banco = O banco no qual buscar
     * @param INT $id = o ID da tabela a ser requisitado
     * @param STRING $opt = o campo do valor a ser requisitado
     * @param STRING $tipo = Caso o valor passado não seja o ID, informe qual é
     */
    public static function getBanco($banco, $id, $opt, $tipo = NULL) {
        $read = new Read;
        if (!isset($tipo) || empty($tipo)):
            $read->ExeRead($banco, "WHERE id=:id", "id={$id}");
        else:
            $read->ExeRead($banco, "WHERE $tipo=:id", "id={$id}");
        endif;

        if ($read->getResult() && isset($read->getResult()[0][$opt])):
            return $read->getResult()[0][$opt];
        endif;

        return "";
    }

    /**
     * <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
     * @param STRING $Name = Data em (d/m/Y) ou (d/m/Y H:i:s)
     * @return STRING = $Data = Data no formato timestamp!
     */
    public static function Data($Data) {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('/', self::$Format[0]);

        if (empty(self::$Format[1])):
            self::$Format[1] = date('H:i:s');
        endif;

        self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
        return self::$Data;
    }

    /**
     * <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
     * @param STRING $Name = Data em (d/m/Y) ou (d/m/Y H:i:s)
     * @return STRING = $Data = Data no formato timestamp!
     */
    public static function getData($Data, $mode = NULL) {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('-', self::$Format[0]);
        $a = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $b = array("jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez");

        if (!empty(self::$Format[1])):
            $d = explode(':', self::$Format[1]);
        else:
            $d[0] = 0;
        endif;

        if (isset(self::$Data[2])):
            if ($mode && $mode === 2):
                return self::$Data[2] . ' ' . str_replace($a, $b, self::$Data[1]);
            else:
                return self::$Data[2] . ' ' . str_replace($a, $b, self::$Data[1]) . ' ' . self::$Data[0];
            endif;
        endif;
    }

    /**
     * <b>Limita os Palavras:</b> Limita a quantidade de palavras a serem exibidas em uma string!
     * @param STRING $String = Uma string qualquer
     * @return INT = $Limite = String limitada pelo $Limite
     */
    public static function Words($String, $Limite, $Pointer = null) {
        self::$Data = strip_tags(trim($String));
        self::$Format = (int)$Limite;

        $ArrWords = explode(' ', self::$Data);
        $NumWords = count($ArrWords);
        $NewWords = implode(' ', array_slice($ArrWords, 0, self::$Format));

        $Pointer = (empty($Pointer) ? '...' : ' ' . $Pointer);
        $Result = (self::$Format < $NumWords ? $NewWords . $Pointer : self::$Data);
        return $Result;
    }

    /**
     * <b>Usuários Online:</b> Ao executar este HELPER, ele automaticamente deleta os usuários expirados. Logo depois
     * executa um READ para obter quantos usuários estão realmente online no momento!
     * @return INT = Qtd de usuários online
     */
    public static function UserOnline() {
        $now = date('Y-m-d H:i:s');
        $deleteUserOnline = new Delete;
        $deleteUserOnline->ExeDelete('ws_siteviews_online', "WHERE online_endview < :now", "now={$now}");

        $readUserOnline = new Read;
        $readUserOnline->ExeRead('ws_siteviews_online');
        return $readUserOnline->getRowCount();
    }

    /**
     * <b>Imagem Upload:</b> Ao executar este HELPER, ele automaticamente verifica a existencia da imagem na pasta
     * uploads. Se existir retorna a imagem redimensionada!
     * @return HTML = imagem redimencionada!
     */
    public static function Image($ImageUrl, $ImageDesc, $ImageW = null, $ImageH = null) {

        self::$Data = $ImageUrl;

        if (file_exists(self::$Data) && !is_dir(self::$Data)):
            $patch = HOME;
            $imagem = self::$Data;
            return "<img src=\"{$patch}/tim.php?src={$patch}/{$imagem}&w={$ImageW}&h={$ImageH}\" alt=\"{$ImageDesc}\" title=\"{$ImageDesc}\"/>";
        elseif (file_exists('../' . self::$Data) && !is_dir('../' . self::$Data)):
            $patch = HOME;
            $imagem = self::$Data;
            return "<img src=\"{$patch}/tim.php?src={$patch}/{$imagem}&w={$ImageW}&h={$ImageH}\" alt=\"{$ImageDesc}\" title=\"{$ImageDesc}\" class='imgreturn' />";
        else:
            return false;
        endif;
    }

    /**
     * <b>Criptografar:</b> Criptografa, sem volta, somente pode ser feito a criptografia novamente para comparar valores idênticos
     * @param STRING $e = string a ser criptografada
     */
    public static function Encrypt($e) {
        $e = (string)md5(base64_encode(strip_tags(trim($e))));
        $key1 = array('1', 'c', 's', '2', 'r', 'o', 'n', 'l', 'f', 'x', '0', 'k', 'v', '5', 'y');
        $key2 = array('b', '4', '9', '6', 'w', 'a', 'd', '3', 'z', '7', 'j', 'm', '8', 'h', 't');
        return md5(str_replace($key1, $key2, $e));
    }

    public static function EncryptMiddle($e) {
        return (string)md5(base64_encode(strip_tags(trim($e))));
    }

    /**
     * <b>Criptografar:</b> Criptografa dados em uma determinada ordem, de modo a ser recuperada
     */
    public static function Criptografar($e) {
        return base64_encode(base64_encode($e));
        $key1 = array('r', 'g', 's', 'x', 'k', 'b', 'f', 'q', 'a', 'e', '5', 'i', 'u', 'o', 'p', 'h', '2', '0');
        $key2 = array('4', '9', '6', 'w', 'd', '3', 'z', '7', 'j', 'm', '8', 't', 'l', 'c', '1', 'n', 'v', 'y');
        return (string)str_replace($key1, $key2, $e);
    }

    /**
     * <b>Descriptografar:</b> Descriptografa dados que foram criptografados
     */
    public static function Descriptografar($e) {
        $key1 = array('r', 'g', 's', 'x', 'k', 'b', 'f', 'q', 'a', 'e', '5', 'i', 'u', 'o', 'p', 'h', '2', '0');
        $key2 = array('4', '9', '6', 'w', 'd', '3', 'z', '7', 'j', 'm', '8', 't', 'l', 'c', '1', 'n', 'v', 'y');
        return base64_decode(base64_decode($e));
    }

}
