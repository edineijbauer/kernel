<?php

require('Connection.inc.php');

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {

    $cDir = ['Conn', 'Helpers', 'Models'];
    $iDir = null;
	
	if($Class == 'check'):
		$Class = 'Check';
	endif;

    foreach ($cDir as $dirName):
        if (!$iDir && file_exists(__DIR__ . '/' . $Class . '.class.php') && !is_dir(__DIR__ . '/' . $Class . '.class.php')):
            include_once (__DIR__ . '/' . $Class . '.class.php');
            $iDir = true;
        endif;
    endforeach;

    if (!$iDir):
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    endif;
}
$read = new Read;
$View = new View;
$read->ExeRead(PRE. "configs");
if($read->getResult()):
	// DEFINE IDENTIDADE DO SITE ################
	define('SITENAME', $read->getResult()[0]['title']);
	define('SITESUB', $read->getResult()[0]['sub']);
	define('SITEDESC', $read->getResult()[0]['desc']);

	// DEFINE A BASE DO SITE ####################
	define('THEME', $read->getResult()[0]['theme']);
	define('HTTP', $read->getResult()[0]['http']);

	// DEFINE SERVIDOR DE E-MAIL ################
	/*
	define('MAILUSER', $read->getResult()[0]['mail']);
	define('MAILPASS', $read->getResult()[0]['mailpass']);
	define('MAILPORT', $read->getResult()[0]['mailport']);
	define('MAILHOST', $read->getResult()[0]['mailhost']);
	*/

	define('HOME', HTTP . DOMINIO);
	define('RSS', HTTP . DOMINIO . '/feed'); // padrão: HOME/feed

	define('INCLUDE_PATH', HOME . '/themes/' . THEME);
	define('REQUIRE_PATH', 'themes/' . THEME);
else:
	header('Location: Start/StartSite.inc.php');
endif;


// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');

//WSErro :: Exibe erros lançados :: Front
function WSErro($ErrMsg, $ErrNo, $ErrDie = null) {
	$CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
	echo "<p class=\"trigger shoticon shoticon-{$CssClass} shoticon-sectiontitle {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

	if ($ErrDie):
		die;
	endif;
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
	$CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
	echo "<p class=\"trigger {$CssClass}\">";
	echo "<b>Erro na Linha: #{$ErrLine} ::</b> {$ErrMsg}<br>";
	echo "<small>{$ErrFile}</small>";
	echo "<span class=\"ajax_close\"></span></p>";

	if ($ErrNo == E_USER_ERROR):
		die;
	endif;
}

set_error_handler('PHPErro');