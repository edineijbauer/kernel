<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 17/04/2017
 * Time: 10:15
 */
abstract class MinerBase {
    private $table;
    private $infoBanco;
    private $site;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getSite($url) {
        $url = $this->ajusteUrl($url);
        //        $this->getBaseSite($url);
        //        $this->delimitaArea();
        $this->infoBanco = $this->getBancoDados();
        $this->infoBanco[$this->table] = $this->getResultsFromInfoBanco($this->table, $this->infoBanco[$this->table]);

        return $this->site;
    }

    /**
     * @return mixed
     */
    public function getInfoBanco() {
        return $this->infoBanco;
    }

    private function ajusteUrl($url) {
        $url = 'select%20*%20from%20html%20where%20url=%22' . str_replace(array('"', ' '), array('%22', '%20'), $url) . '%22';
        $url = "http://query.yahooapis.com/v1/public/yql?q={$url}&diagnostics=true";
        return $url;
    }

    private function getBaseSite($url) {
        $filter = html_entity_decode(file_get_contents($url));

        if (preg_match('/<header/i', $filter)):
            $filter = str_replace('<header' . explode('</header>', explode('<header', $filter)[1])[0] . '</header>', '', $filter);
        endif;
        if (preg_match('/<footer/i', $filter)):
            $foo = explode('<footer', $filter);
            $filter = str_replace('<footer' . explode('</footer>', explode('<footer', $filter)[count($foo) - 1])[0] . '</footer>', '', $filter);
        endif;
        if (preg_match('/<body/i', $filter)):
            $filter = explode('</body', explode('<body', $filter)[1])[0];
            $filter = Check::replace_string(explode('>', $filter)[0] . '>', '', $filter, 1);
        endif;

        $tagsDelete = ["script", "noscript", "style", "!--", "head"];
        foreach ($tagsDelete as $tag):
            if (preg_match("/<{$tag}/i", $filter)):
                foreach (explode("<{$tag}", $filter) as $i => $script):
                    if ($i > 0):
                        $endTag = ($tag === "!--" ? "-->" : "</{$tag}>");
                        $teste = explode('>', $script)[0] . '>';
                        if (preg_match('/\/>/i', $teste)):
                            $filter = str_replace("<{$tag}" . $teste, '', $filter);
                        else:
                            $filter = str_replace("<{$tag}" . explode($endTag, $script)[0] . $endTag, '', $filter);
                        endif;
                    endif;
                endforeach;
            endif;
        endforeach;

        $this->site = trim($filter);
    }

    private function delimitaArea() {
        $total = strlen($this->site) * 0.6;

        $semantica = new Semantica($this->site);
        foreach ($semantica->getTags() as $tag):
            if (isset($tag['conteudo'])):
                $comp = strlen($tag['conteudo']);
                if ($comp > $total && (!isset($newString) || strlen($newString) > $comp)):
                    $newString = $tag['conteudo'];
                endif;
            endif;
        endforeach;

        $this->site = (isset($newString) ? new Semantica($newString) : $semantica);
    }

    private function getBancoDados() {
        $bancoInfo = new tableStruct();
        $bancoInfo->setTable($this->table);
        return $bancoInfo->getResult();
    }

    private function getResultsFromInfoBanco($table, $dados) {
        $read = new Read();
        $read->ExeRead($table, "ORDER BY id DESC LIMIT 50");
        if ($read->getResult()):
            foreach ($read->getResult() as $item):
                foreach ($item as $column => $value):

                    if ($dados[$column]['chave'] !== "pk" && !empty($value)):
                        $dados[$column]['results'][] = $value;
                    endif;

                    if (isset($dados[$column]['table']) && !isset($allread[$column])):
                        $dados[$column][$dados[$column]['table']] = $this->getResultsFromInfoBanco($dados[$column]['table'], $dados[$column][$dados[$column]['table']]);
                        $allread[$column] = 1;
                    endif;

                endforeach;
            endforeach;
        endif;

        if (isset($dados['dados_value'])):
            if (isset($dados['dados_value']['oneToMany'])):
                foreach ($dados['dados_value']['oneToMany'] as $tableOneToMany => $valores):
                    $dados['dados_value']['oneToMany'][$tableOneToMany] = $this->getResultsFromInfoBanco($tableOneToMany, $dados['dados_value']['oneToMany'][$tableOneToMany]);
                endforeach;
            endif;
            if (isset($dados['dados_value']['manyToMany'])):
                foreach ($dados['dados_value']['manyToMany'] as $tableManyToMany => $valores):
                    $dados['dados_value']['manyToMany'][$tableManyToMany] = $this->getResultsFromInfoBanco($tableManyToMany, $dados['dados_value']['manyToMany'][$tableManyToMany]);
                endforeach;
            endif;
        endif;

        return $dados;
    }
}