<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 20/04/2017
 * Time: 19:08
 */
class PadronizaString {

    private $strings;
    private $content;

    /**
     * @param mixed $strings
     */
    public function setStrings($strings) {
        $this->strings = $strings;
    }

    /**
     * @return mixed
     */
    public function getContent() {
        $this->start();
        return $this->content;
    }

    private function start() {
        foreach ($this->strings as $i => $string):
            $this->content[$i] = $this->checkSpacesAcentoNumber($string);
        endforeach;
    }

    private function checkSpacesAcentoNumber($string) {
        $pattern[] = '/(.*)\s*(\d+)([^A-Za-z0-9\s\.,])(\d+)(.*)/i';
        $pattern[] = '/(.*)\s*(\d+)([^A-Za-z0-9\s\.,])\s(\d+)(.*)/i';
        $pattern[] = '/(.*)\s*(\d+)\s([^A-Za-z0-9\s\.,])(\d+)(.*)/i';

        foreach ($pattern as $p):
            while (preg_match($p, $string)):
                $string = trim(preg_replace($p, '$1$2 $3 $4$5', $string));
            endwhile;
        endforeach;

        return $string;
    }


}