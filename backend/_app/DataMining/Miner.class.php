<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 16/04/2017
 * Time: 15:20
 */
class Miner extends MinerBase {

    private $table;
    private $url;
    private $site;
    private $infoBanco;
    private $regex;
    private $data;
    private $erro;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = (preg_match("/" . PRE . "/i", $table) ? "" : PRE) . $table;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getErro() {
        return $this->erro;
    }

    public function minerar() {
        if ($this->url && $this->table):
            $this->startMining();
        else:
            $this->erro = ($this->url ? "não sou adivinho, qual a tabela para mineração?" : "informe a url que deseja minerar.");
        endif;

        if ($this->erro):
            echo $this->erro;
        else:
            $this->saveData();
        endif;
    }

    private function startMining() {
        parent::setTable($this->table);
        $this->site = parent::getSite($this->url);

        $this->infoBanco = parent::getInfoBanco();
        $this->infoBanco[$this->table] = $this->setInfoBancoEstrutura($this->infoBanco[$this->table]);

        var_dump($this->infoBanco);
        die;

        var_dump(parent::getInfoBanco()[$this->table]);
        die;

        $semantica = new Semantica($this->site);

        foreach ($semantica->getTags() as $tag):
            if (!empty($tag['conteudo'])):
                $this->findField($tag['conteudo']);
            endif;
        endforeach;

        var_dump($this->site);
        die;
    }

    private function setInfoBancoEstrutura($infoTable) {
        foreach ($infoTable as $column => $valor):
            if (isset($valor['results'])):

                $this->regex[$column] = $this->getStringFormat($valor['results']);
                $infoTable[$column]['regex'] = $this->regex[$column];

            endif;

            if (isset($valor['table'])):
                $infoTable[$column][$valor['table']] = $this->setInfoBancoEstrutura($infoTable[$column][$valor['table']]);
            endif;
        endforeach;

        if (isset($infoTable['dados_value'])):
            if (isset($infoTable['dados_value']['oneToMany'])):
                foreach ($infoTable['dados_value']['oneToMany'] as $tableOneToMany => $valores):
                    $infoTable['dados_value']['oneToMany'][$tableOneToMany] = $this->setInfoBancoEstrutura($infoTable['dados_value']['oneToMany'][$tableOneToMany]);
                endforeach;
            endif;
            if (isset($infoTable['dados_value']['manyToMany'])):
                foreach ($infoTable['dados_value']['manyToMany'] as $tableManyToMany => $valores):
                    $infoTable['dados_value']['manyToMany'][$tableManyToMany] = $this->setInfoBancoEstrutura($infoTable['dados_value']['manyToMany'][$tableManyToMany]);
                endforeach;
            endif;
        endif;

        return $infoTable;
    }

    private function findField($cont) {
        foreach (parent::getInfoBanco()[$this->table] as $item):
            if ($item['chave'] !== ""):
                $string = $this->getStringFormat($item['results']);
                var_dump($string);
                die;
            elseif ($item['chave'] === "fk"):

            endif;
        endforeach;
    }

    private function getStringFormat($exe) {
        $match = new PregMatch();
        $match->setString($exe);
        return $match->getRegex();
    }

    /**
     * Salva a mineração obtida no banco
     */
    private function saveData() {

    }
}