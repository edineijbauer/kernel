<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 19/04/2017
 * Time: 10:39
 */
class PregMatch {
    private $string;
    private $rigorInicial = -1;
    private $rigorFinal = -1;
    private $pregTemp;
    private $regex;
    private $pregAtual;
    private $pregPosicao;
    private $pregBettwen;
    private $preg;

    /**
     * @param int $rigorInicial
     */
    public function setRigorInicial($rigorInicial) {
        $this->rigorInicial = $this->setRigor($rigorInicial);
    }

    /**
     * @param int $rigorFinal
     */
    public function setRigorFinal($rigorFinal) {
        $this->rigorFinal = $this->setRigor($rigorFinal);
    }

    /**
     * @param mixed $string
     */
    public function setString($string) {
        $pattern = new PadronizaString();
        if (!is_array($string)):
            $pattern->setStrings(array(0 => $string));
        else:
            $pattern->setStrings($string);
        endif;

        $this->string = $pattern->getContent();
    }

    /**
     * @return mixed
     */
    public function getPreg() {
        return $this->preg;
    }

    /**
     * @return mixed
     */
    public function getRegex() {
        $this->generatePreg();
        $this->checkFinalPreg();
        $this->convertInitialPreg();
        $this->setFinalPregMatch();

        $this->pregBettwen = $this->findBettwen();
        $this->regex = (isset($this->regex[0]) ? $this->regex[0] : "") . ($this->pregBettwen ? '.*' : "") . (isset($this->regex[1]) ? $this->regex[1] : "");

        return $this->regex;
    }

    private function findBettwen() {
        if ($this->pregBettwen):
            return true;
        else:
            $cont = $this->pregPosicao[0];
            rsort($cont);
            $cont = $cont[0] - (isset($this->preg[1]) ? count($this->preg[1]) : 0);

            return ($cont > 15 ? true : false);
        endif;
    }

    private function setRigor($rigor) {
        if (is_numeric($rigor)):
            return round($rigor < 0 ? 0 : ($rigor < 3 ? $rigor : ($rigor > 10 ? ($rigor > 100 ? 3 : ($rigor * 3) / 100) : ($rigor > 5 ? ($rigor * 3) / 10 : ($rigor / 5) * 3))));
        elseif (!$rigor):
            return 0;
        else:
            return 1;
        endif;
    }

    private function checkSame($num, $letra, $reverso, $e) {
        if ($this->pregAtual > -1 && $this->pregAtual !== $num):
            if (($reverso === 0 && $this->pregPosicao[$reverso][$e] < 25) || ($reverso === 1 && $this->pregPosicao[$reverso][$e] < 3)):
                if (($reverso === 0 && $this->pregPosicao[$reverso][$e] < 15) || $reverso === 1):
                    if (!isset($this->preg[$reverso][$this->pregPosicao[$reverso][$e]][$this->pregAtual]) || !in_array($this->pregTemp, $this->preg[$reverso][$this->pregPosicao[$reverso][$e]][$this->pregAtual])):

                        if ($reverso === 1):
                            if (count($this->preg[0]) > 6):
                                $this->preg[$reverso][$this->pregPosicao[$reverso][$e]][$this->pregAtual][] = $this->pregTemp;
                            endif;
                        else:
                            $this->preg[$reverso][$this->pregPosicao[$reverso][$e]][$this->pregAtual][] = $this->pregTemp;
                        endif;
                    endif;
                endif;

                $this->pregBettwen = ($reverso === 0 && $this->pregPosicao[$reverso][$e] > 18 ? true : false);

                $this->pregPosicao[$reverso][$e]++;
                $this->pregTemp = "";
                $this->pregAtual = -1;
            endif;
        endif;

        $this->pregTemp .= ($this->pregAtual === -1 || $this->pregAtual === $num ? $letra : "");
        $this->pregAtual = $num;
    }

    private function generatePreg($reverso = 0) {
        foreach ($this->string as $e => $ex):
            $ex = strtolower(($reverso === 1 ? strrev($ex) : $ex));
            $this->pregTemp = "";
            $this->pregPosicao[$reverso][$e] = 0;
            $this->pregAtual = -1;

            for ($i = 0; $i <= strlen($ex); $i++):
                if (isset($ex[$i])):
                    $this->findNumberPreg($ex[$i], $reverso, $e);
                else:
                    if (!empty($this->pregTemp)):
                        $this->checkSame(9, " ", $reverso, $e);
                    endif;
                endif;
            endfor;
        endforeach;

        if ($reverso === 0):
            $this->generatePreg(1);
            $this->reverteStringsFinal();
        endif;
    }

    private function reverteStringsFinal() {
        if (isset($this->preg[1])):
            foreach ($this->preg[1] as $t => $v):
                foreach ($v as $i => $p):
                    if (is_array($p)):
                        foreach ($p as $e => $d):
                            $this->preg[1][$t][$i][$e] = strrev($d);
                        endforeach;
                    else:
                        $this->preg[1][$t][$i] = strrev($p);
                    endif;
                endforeach;
            endforeach;
        endif;
    }

    private function findNumberPreg($letra, $reverso, $e) {
        if (preg_match('/[0-9]+/i', $letra)):
            $this->checkSame(1, $letra, $reverso, $e);
        elseif (preg_match('/[A-Za-zÀ-ú]+/i', $letra)):
            $this->checkSame(10, $letra, $reverso, $e);
        elseif (preg_match('/[^A-Za-z0-9\s]+/i', $letra)):
            $this->checkSame(100, $letra, $reverso, $e);
        elseif (preg_match('/\s+/i', $letra)):
            $this->checkSame(1000, $letra, $reverso, $e);
        endif;
    }

    private function checkFinalPreg() {
        if (isset($this->preg[1])):
            $tolerancia = round(count($this->string) * 0.1);
            $tolerancia = (int)($tolerancia < 1 ? 1 : $tolerancia);
            $numMax = (int)round(count($this->preg[0]) * 0.6);
            $this->preg[1] = array_reverse($this->preg[1]);

            foreach ($this->preg[1] as $e => $d):
                if ($e < $numMax):
                    $soma = 0;
                    foreach ($d as $i => $p):
                        $soma += count($p);
                    endforeach;

                    if ($tolerancia >= $soma):
                        $menor = $e;
                        break;
                    endif;
                endif;
            endforeach;

            if (!isset($menor)):
                unset($this->preg[1]);

            elseif ($menor > 0):
                foreach ($this->preg[1] as $e => $d):
                    if ($e < $menor):
                        unset($this->preg[1][$e]);
                    endif;
                endforeach;
            endif;
        endif;
    }

    private function setFinalPregMatch() {
        if (isset($this->preg[1])):
            foreach ($this->preg[1] as $cont => $data):
                $tipos = 0;
                foreach ($data as $tipo => $value):
                    $tipos = $this->mergeTipo($tipos, $tipo);
                    $valores = (isset($valores) ? array_merge($valores, $value) : $value);
                endforeach;

                if (isset($tipos) && isset($valores)):
                    $total = count($this->string);
                    $toleranciaMin = ($total < 7 ? ($total < 5 ? 50 : 40) : ($total < 13 ? 30 : ($total < 18 ? 25 : ($total > 30 ? 15 : 21))));
                    $this->regex[1] = (isset($this->regex[1]) ? $this->regex[1] : "") . $this->convertPregMatch($cont, $tipos, $valores, $this->rigorInicial, $total, $toleranciaMin);
                    unset($tipos, $valores);
                endif;
            endforeach;

            $this->regex[1] = (isset($this->regex[1]) ? $this->regex[1] : "") . "$";
        else:
            $this->regex[1] = "$";
        endif;
    }

    private function convertInitialPreg() {
        if (isset($this->preg[0])):
            $maximo = count($this->preg[0]) - (isset($this->preg[1]) ? count($this->preg[1]) : 0);

            foreach ($this->preg[0] as $cont => $data):
                if ($cont < $maximo):
                    $tipos = 0;
                    foreach ($data as $tipo => $value):
                        $tipos = $this->mergeTipo($tipos, $tipo);
                        $valores = (isset($valores) ? array_merge($valores, $value) : $value);
                    endforeach;

                    if (isset($tipos) && isset($valores)):
                        $total = count($this->string);
                        $toleranciaMin = ($total < 7 ? ($total < 5 ? 50 : 40) : ($total < 13 ? 30 : ($total < 18 ? 25 : ($total > 30 ? 15 : 21))));
                        $this->regex[0] = (isset($this->regex[0]) ? $this->regex[0] : "^") . $this->convertPregMatch($cont, $tipos, $valores, $this->rigorInicial, $total, $toleranciaMin);

                        unset($tipos, $valores);
                    endif;
                else:
                    break;
                endif;
            endforeach;
        endif;
    }

    private function mergeTipo($tipos, $tipo) {
        if ($tipo === 1 && !preg_match('/1$/', $tipos)):
            return $tipos + $tipo;
        elseif ($tipo === 10 && !preg_match('/(11|10)$/', $tipos)):
            return $tipos + $tipo;
        elseif ($tipo === 100 && !preg_match('/(111|110|100)$/', $tipos)):
            return $tipos + $tipo;
        elseif ($tipo === 1000 && $tipos < 112):
            return $tipos + $tipo;
        endif;

        return $tipos;
    }

    private function convertPregMatch($cont, $tipo, $value, $rigor, $total, $toleranciaMin) {
        $tolerancia = round((count($value) * 100) / $total);
        $rigor = ($rigor < 0 ? ($tolerancia > $toleranciaMin ? ($tolerancia > 77 ? 0 : 1) : 2) : $rigor);
        $convert = "";

        if ($rigor == 2):
            $convert = (count($value) > 1 ? str_replace('\|&&\|', '|', "(" . preg_quote(implode("|&&|", $value), '/') . ")") : str_replace(' ', '\s*', preg_quote($value[0], '/')));
        else:

            $tamanho = (count($value) > 1 ? $this->getMaiorMenor($value) : null);
            if ($rigor != 1 && $tamanho):
                $tamanho['min'] = round($tamanho['min'] * 0.33);
                $tamanho['max'] = round($tamanho['max'] * 1.66);
                $tamanho['min'] = ($tamanho['min'] < 1 ? 1 : $tamanho['min']);
            endif;

            $caracter = $this->findCaracter($tipo);

            if ($caracter === '\s'):
                $convert = '\s+';
            elseif ($tamanho && count($value) > 1):

                $tamanho['max'] = (preg_match('/' . preg_quote('A-Za-zÀ-ú') . '/i', $caracter) && !preg_match('/' . preg_quote('^A-Za-zÀ-ú') . '/i', $caracter) && $tamanho['max'] < 2 ? 2 : $tamanho['max']);

                if ($tamanho['max'] >= $tamanho['min']):
                    $convert = $caracter . ($tamanho['max'] === $tamanho['min'] ? ($tamanho['max'] === 1 ? "" : '{' . $tamanho['max'] . '}') : '{' . $tamanho['min'] . ',' . $tamanho['max'] . '}');
                endif;
            else:
                $tamanho['max'] = (preg_match('/' . preg_quote('A-Za-zÀ-ú') . '/i', $caracter) && strlen($value[0]) < 2 ? 2 : strlen($value[0]));
                $convert = $caracter . '{' . $tamanho['max'] . '}';
            endif;

            if ($caracter === '[^A-Za-z0-9\s]'):
                $convert = '\s*(' . $convert . ')\s*';
            endif;

        endif;

        $menor = $this->pregPosicao[0];
        sort($menor);
        $menor = $menor[0] - (isset($this->preg[1]) ? count($this->preg[1]) : 0);

        $convert = ($cont >= $menor ? "({$convert})*" : $convert);

        return $convert;
    }

    /**
     * @param $tipo
     * @return string
     */
    private function findCaracter($tipo) {
        if ($tipo === 1):
            return '\d'; // inteiro
        elseif ($tipo === 10):
            return '[A-Za-zÀ-ú]'; // string
        elseif ($tipo === 100):
            return '[^A-Za-zÀ-ú0-9\s]'; //  acentos
        elseif ($tipo === 1000):
            return '\s';     //  espaço

        elseif ($tipo === 11):
            return '[A-Za-zÀ-ú0-9]';
        elseif ($tipo === 101):
            return '[^A-Za-zÀ-ú\s]';
        elseif ($tipo === 1001):
            return '(\d|\s)';
        elseif ($tipo === 110):
            return '[^0-9\s]';
        elseif ($tipo === 1010):
            return '[A-Za-zÀ-ú\s]';
        elseif ($tipo === 1100):
            return '[^A-Za-zÀ-ú0-9]';

        elseif ($tipo === 111):
            return '\S';
        elseif ($tipo === 1011):
            return '[A-Za-zÀ-ú\d\s]';
        elseif ($tipo === 1101):
            return '[^A-Za-zÀ-ú]';
        elseif ($tipo === 1110):
            return '\D';

        else:
            return '.'; // 1111 - tudo
        endif;
    }

    private function getMaiorMenor($value) {
        if (is_array($value) && count($value) > 1):
            $min = 99999999999;
            $max = -1;
            foreach ($value as $v):
                if (strlen($v) < $min):
                    $min = strlen($v);
                endif;
                if (strlen($v) > $max):
                    $max = strlen($v);
                endif;
            endforeach;

            return array("min" => $min, "max" => $max);
        endif;

        return null;
    }
}