<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 03/12/2016
 * Time: 19:59
 */
class Banco extends BancoOO {

    private $_data;

    public function __construct($table) {
        parent::__construct($table);
    }

    public function __set($property, $value) {
        if (is_array($this->getColunas()) && in_array($property, $this->getColunas())):
            $value = (is_float($value) ? (float)$value : (is_numeric($value) ? (int)$value : (empty($value)? NULL : (string)$value)));
            $this->_data[$property] = $value;
        endif;
    }

    public function __get($property) {
        if (is_array($this->getColunas()) && in_array($property, $this->getColunas())):
            return array_key_exists($property, $this->_data) ? $this->_data[$property] : null;
        endif;
    }

    public function exist() {
        return isset($this->_data['id']) && $this->_data['id'] > 0;
    }

    public function getDados() {
        $dados = array();
        if ($this->_data):
            foreach ($this->_data as $key => $value):
                $dados[$key] = $value;
            endforeach;
        endif;

        return $dados;
    }


}