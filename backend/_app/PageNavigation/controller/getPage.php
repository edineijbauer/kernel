<?php

$homebase = "../../../";

ob_start();
require("{$homebase}_app/Config.inc.php");
$Session = new Session;
require("{$homebase}_app/Login.inc.php");
$read = new Read();

if (ISHELPER):

    $id = filter_input(INPUT_POST, "id", FILTER_VALIDATE_INT);
    $banco = filter_input(INPUT_POST, "banco", FILTER_DEFAULT);
    $retorno = filter_input(INPUT_POST, "retorno", FILTER_DEFAULT);
    $bancoUso = filter_input(INPUT_POST, "bancoUso", FILTER_DEFAULT);
    $copy = filter_input(INPUT_POST, "copy", FILTER_VALIDATE_INT);

    if (!preg_match("/^" . PRE . "/i", $banco)):
        require("{$homebase}ADM/" . REQUIRE_PATH . DIRECTORY_SEPARATOR . $banco . ".php");
        echo "<script src='../ADM/" . REQUIRE_PATH . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR . $banco . ".js'></script>";

    elseif (isset($id) && !is_null($id) && $id >= 0):

        $crud = new formCrud();
        $crud->setTable($banco);
        $crud->setRetorno($retorno);
        $crud->setId($id);
        echo  $crud->getActionButtons();
        echo  $crud->getInputs();

    elseif ($banco):

        $title = Check::getNameBanco($banco);

        echo "<div class='container postBody'>";

        switch ($banco):
            case PRE . "category":
                $page = new Table($banco, $title, 1);
                $page->btnView("urlname", "categoria/");
                $page->setTable("date", "miniboxTable mg-small fl-left s5box-0");
                $page->setTable("title", "font-bold font-size12");
                $page->setTable("views", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-statics font-size09'></i>#views#</div>");
                $page->setTable("post_hidden", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-post-pure font-size09'></i>#post_hidden#</div>");
                $page->setTable("category_id", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-tree-pure font-size09'></i>#title#</div>");
                $page->ExeCreate();
                break;

            case PRE . "post":
                $page = new Table($banco, $title, 1);
                $page->btnView("urlname");
                $page->setTable("imagem", "miniboxTable mg-small fl-left", "<img src='" . HOME . "/tim.php?src=" . HOME . "/uploads/#cover#&h=35&w=35' height='35' width='35' style='height:35px;width:35px' />");
                $page->setTable("title", "font-bold font-size12");
                $page->setTable("user_id", "mg-small fl-left", "<div class='font-size08' style='width:100px'>#urlname#</div>");
                $page->setTable("views", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-statics font-size09'></i>#views#</div>");
                $page->ExeCreate();
                break;

            case PRE . "tag":
                $page = new Table($banco, $title, 1);
                $page->btnView("urlname", "tag/");
                $page->setTable("date", "miniboxTable mg-small fl-left s5box-0");
                $page->setTable("title", "font-bold font-size12");
                $page->setTable("views", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-statics font-size09'></i>#views#</div>");
                $page->setTable("post_hidden", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-post-pure font-size09'></i>#post_hidden#</div>");
                $page->ExeCreate();
                break;

            case PRE . "user":
                $page = new Table($banco, $title, 1);
                $page->setTable("gallery_id", "miniboxTable mg-small fl-left", "<img src='" . HOME . "/tim.php?src=" . HOME . "/uploads/#gallery_id#&h=35&w=35' height='35' width='35' style='height:35px;width:35px' />");
                $page->setTable("title", "font-bold font-size12");
                $page->setTable("email", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-mail font-size09'></i>#email#</div>");
                $page->ExeCreate();
                break;

            case PRE . "pagina":
                $page = new Table($banco, $title, 1);
                $page->btnView("urlname", "pagina/");
                $page->setTable("date", "miniboxTable mg-small fl-left s5box-0");
                $page->setTable("title", "font-bold font-size12");
                $page->setTable("views", "miniboxTable mg-small fl-left", "<div class='font-size08'><i class='shoticon shoticon-statics font-size09'></i>#views#</div>");
                $page->ExeCreate();

                break;

            default:

                $page = new Table($banco, $title, 1);

                $info = new tableStruct();
                $info->setTable($banco);

                foreach ($info->getResult()[$banco] as $coluna => $dado):
                    if ($coluna !== "dados_value" && $dado["chave"] === "fk" && $dado['comentario'] === "image"):
                        $page->setTable($coluna, "miniboxTable mg-small fl-left", "<img src='" . HOME . "/tim.php?src=" . HOME . "/uploads/#cover#&h=35&w=35' height='35' width='35' style='height:35px;width:35px' />");
                    endif;
                endforeach;

                foreach ($info->getResult()[$banco] as $coluna => $dado):
                    if ($coluna === "urlname"):
                        $page->btnView("urlname", str_replace(array(PRE, "index"), array("", "loja"), $banco) . "/");

                    elseif ($coluna === "date"):
                        $page->setTable("date", "miniboxTable mg-small fl-left s5box-0");

                    elseif ($coluna === "title"):
                        $page->setTable("title", "font-bold font-size12 pd-medium");

                    elseif ($coluna === "views"):
                        $page->setTable("views", "font-bold font-size12", "<div class='fl-left pd-smallb font-size07'><i class='shoticon shoticon-eyesb'></i> #views#</div>");
                    endif;
                endforeach;
                $page->ExeCreate();
        endswitch;

        ?>
        <script>
            $(".numrowsTable").change(function () {
                var b = $(this).siblings("input[type=text]:eq(0)").attr("rel");
                var a = $(this).attr("rel");

                $.post('../requests/back/addPattern.php', {a: 'rowTable_' + a, b: $(this).val(), c: 0, d: 0},
                    function () {
                        $.post('../_app/PageNavigation/controller/tableGetRows.php', {a: a, b: b, c: $("#procurarVars-" + b).val()},
                            function (f) {
                                $("#subcontent-" + a).find("table, ul").remove();
                                $("#subcontent-" + a).find(".postBody").append(f);
                            });
                    });
            });
            $(".searchPost").focus();
        </script>
        <?php

        if (ISGERENTE):
            $options = '../ADM/' . REQUIRE_PATH . "/inc/{$title}.php";
            if (file_exists($options)):
                require_once $options;
            endif;
        endif;

        echo "</div>";
    endif;
endif;