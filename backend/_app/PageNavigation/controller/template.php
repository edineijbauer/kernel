<?php

ob_start();
require("../../../_app/Config.inc.php");
$Session = new Session;
require("../../../_app/Login.inc.php");

$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$dados['title'] = (isset($dados['banco']) ? Check::getNameBanco($dados['banco']) : "");

$view = new View();
$view->setBase("_app/PageNavigation/tpl");
$view->Show($dados, $view->Load("space"));