<?php

ob_start();
require('../../../_app/Config.inc.php');
$Session = new Session;
require('../../../_app/Login.inc.php');

if (isset($_SESSION['userlogin']['id'])):
    $title = filter_input(INPUT_POST, 'a', FILTER_DEFAULT); //title
    $banco = filter_input(INPUT_POST, 'b', FILTER_DEFAULT); //banco
    $a = filter_input(INPUT_POST, 'c', FILTER_DEFAULT); //tables
    $offset = filter_input(INPUT_POST, 'd', FILTER_VALIDATE_INT); //offset
    $where = filter_input(INPUT_POST, 'e', FILTER_DEFAULT); //where
    $places = filter_input(INPUT_POST, 'f', FILTER_DEFAULT); //places

    if($title === "Posts" && $banco === PRE . "post"):
        $where = ($where? $where : 'WHERE status=1');
		if(!$places || empty($places)):
			$places = 'pt=0';
		endif;
    elseif($title === "Produtos" && $banco === PRE . "post"):
        $where = ($where? $where : 'WHERE status=1');
		if(!$places || empty($places)):
			$places = 'pt=1';
		endif;
    endif;

    $tabela = new Table($banco, $title, 1);
    $tabela->notHeader();
    $tabela->notGetHidden();

    if(isset($offset) && !empty($offset)):
        $tabela->setOffset($offset);
    endif;

    $b = explode("..", $a);
    foreach ($b as $c):
        $d = explode(',,', $c);
        if(isset($d[2])):
            $tabela->setTable($d[0], $d[1], $d[2]);
        elseif(isset($d[1])):
            $tabela->setTable($d[0], $d[1]);
        else:
            $tabela->setTable($d[0]);
        endif;
    endforeach;

    if(!$places || empty($places)):
		$tabela->ExeCreate($where);
	else:
		$tabela->ExeCreate($where, $places);
	endif;
endif;