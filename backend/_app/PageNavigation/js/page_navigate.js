$(function () {
    sessionStorage.boxheight = $(document).height();
});

function clearNav(e) {
    sessionStorage.removeItem('editing-' + e);
    sessionStorage.removeItem('editing-column-' + e);
}

function closeWindow(e) {
    $("#" + e).nextAll(".work").remove();
    $("#" + e).remove();

    var tabs = parseInt($("#workspace_" + sessionStorage.space).find(".work").length);
    if (tabs > 0) {
        $("#workspace_" + sessionStorage.space).find(".work").eq(tabs - 1).removeClass("miniSpace");
    }

    clearNav(e);
    $("#favfor-" + e).removeClass("opacidade bg-opaco");
    blurnotificacoes();
}

function closeWindowAll() {
    $("#workspace").html("");
    $("#painotificacoes").removeClass("ds-none");
    $(".favfolder").removeClass("opacidade");
}

function blurnotificacoes() {
    setTimeout(function () {
        if ($(".boxcontent-effect").length) {
            setTimeout(function () {
                $("#painotificacoes").addClass("ds-none");
            }, 150);
        } else {
            $("#painotificacoes").removeClass("ds-none");
        }
    }, 1);
}

function updateIconesOpacity(banco) {
    $(".favfolder").removeClass("opacidade");
    $("#favfor-" + banco).addClass("opacidade bg-opaco");
}

function createWorkSpace(banco) {
    $(".workspace").addClass("ds-none");
    if (!$("#workspace_" + banco).length) {
        updateIconesOpacity(banco);
        $("#workspace").prepend("<div class='container ps-relative workspace' style='height: " + sessionStorage.boxheight + "px' id='workspace_" + banco + "'></div>");
    } else {
        $("#workspace_" + banco).removeClass("ds-none");
    }
}

function createSpace(id) {
    if(!$("#" + id).length) {
        $.post("../_app/PageNavigation/controller/template.php", {
            banco: sessionStorage.space, id: id, height: sessionStorage.boxheight
        }, function (g) {
            var tabs = $("#workspace_" + sessionStorage.space).find(".work").length;
            if (tabs > 0) {
                $("#workspace_" + sessionStorage.space).find(".work").eq(tabs - 1).addClass("miniSpace");
                $("#workspace_" + sessionStorage.space).append(g);
            } else {
                $("#workspace_" + sessionStorage.space).append(g);
            }
        });
    }
}

function setContentToSpace(identificador, banco, id, idRetorno) {
    $("#content_" + identificador).load('../_app/PageNavigation/controller/getPage.php', {
        banco: banco, id: id, retorno: idRetorno, bancoUso: $("#bancoUso").val()
    },function () {
        var btn = $(".btnSavePost").last().clone();
        btn.css("margin", "3px 0 0 74px").addClass("ps-fixed z-plus");
        $(".btnSavePost").last().remove();
        $(".headerSpace").last().append(btn);
    });
}

function openWorkSpace(banco) {
    sessionStorage.space = banco;
    createWorkSpace(banco);
    getPage(banco);
    blurnotificacoes();
}

function getPage(banco, id, idRetorno) {
    clearNav(banco);
    var identificador = getIdentificador(banco, id);
    createSpace(identificador);
    setTimeout(function () {
        setContentToSpace(identificador, banco, id, idRetorno);
    }, 200);
}

function getIdentificador(banco, id) {
    return banco + (id && id > -1 ? '_' + id.toString().replace(".", "") : "");
}

function getPageCopy(banco, id) {
    sessionStorage.action = 'update';
    $.post('../_app/PageNavigation/controller/getPage.php', {banco: banco, id: id, copy: 1},
        function (g) {
            $("#subcontent-" + banco).html(g);
        });
}