<?php

/*PAGINATOR*/
//Retorna resultados do banco de dados através de ajax

ob_start();
require('../../_app/Config.inc.php');
$Session = new Session;
require('../../_app/Login.inc.php');

//Recebe informações do JS
/*
page -> página solicitada
banco -> no banco tal
where -> Restrições
key -> Chaves para as restrições
limit -> Limite da página solicitada
*/
$page = filter_input(INPUT_POST, "page", FILTER_VALIDATE_INT);
$banco = Check::Descriptografar(filter_input(INPUT_POST, "banco", FILTER_DEFAULT));
$where = Check::Descriptografar(filter_input(INPUT_POST, "where", FILTER_DEFAULT));
$key = Check::Descriptografar(filter_input(INPUT_POST, "key", FILTER_DEFAULT));
$tpl = filter_input(INPUT_POST, "tpl", FILTER_DEFAULT);
$limit = filter_input(INPUT_POST, "limit", FILTER_VALIDATE_INT);

$limite = $limit * $page;
$off = $limite - $limit;

$read = new Read();
$read->ExeRead($banco, $where . " LIMIT {$limite} OFFSET {$off}", $key);
if($read->getResult()):
	$View = new View();
	$t = $View->Load($tpl);
	$c=0;
	foreach($read->getResult() as $r):
		$c++;
		if($c <= $limit):
			if($r['slide'] == 1):
				$r['star'] = '&starf;';
				$r['ff'] = '';
			else:
				$r['star'] = '&star;';
				$r['ff'] = 'ff';
			endif;
			$r['c'] = $c;
			$View->Show($r, $t);
		endif;
	endforeach;
endif;

?>