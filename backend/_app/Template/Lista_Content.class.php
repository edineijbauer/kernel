<?php

/**
 * Lista_Content.class [ MODEL ADMIN ]
 * Responsável por organizar a listagem de conteúdos tanto fixa quanto dinamicamente na página php
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class Lista_Content {

    private $Id;
    private $Banco;
    private $Tipo;
    private $TipoDinamico;
    private $TempContent;
    private $Incorp;
    private $Where;
    private $Result;

    function __construct($id, $incorp) {
        $this->Id = (int) $id;
        $this->Incorp = $incorp;
        $this->Banco = $this->getBancoList();

        //obtem o tipo de template a utilizar
        $this->Tipo = $this->getTemplateList();

        //obtem o tipo desta incorporação
        $this->TipoDinamico = ($this->Incorp['incorporar_' . $this->Id] == -1 ? 0 : 1);

        $this->setInfosPadrao();
        $this->initial();
    }

    public function getResult() {

        return $this->Result;
    }

    /*
     * Obtem o banco da struct id
     */

    private function getBancoList() {
        if ($this->Incorp['incorporar_' . $this->Id] === '-1'):
            return "post";

        elseif ($this->Incorp['incorporar_' . $this->Id] === '-2'):
            return "post";

        elseif ($this->Incorp['incorporar_' . $this->Id] === '-3'):
            return "colecao";

        elseif ($this->Incorp['incorporar_' . $this->Id] === '-4'):
            return "fabricante";

        elseif ($this->Incorp['incorporar_' . $this->Id] === '-5'):
            return "category";

        elseif ($this->Incorp['incorporar_' . $this->Id] === '-6'):
            return "tag";
        endif;
    }

    /*
     * Obtem o template a utilizar na struct da id passada
     */

    private function getTemplateList() {
        return (isset($this->Incorp['post_slide_gallery_' . $this->Id]) && $this->Incorp['post_slide_gallery_' . $this->Id] === '3' ? 'blog' : 'slide');
    }

    /*
     * Obtem uma lista de uma tabela
     */

    private function initial() {
        if (isset($this->Incorp['paginacao_chose_' . $this->Id])):
            $this->Incorp['paginacao_chose_' . $this->Id] = (String) $this->Incorp['paginacao_chose_' . $this->Id];
        endif;

        if (isset($this->Incorp['paginacao_chose_' . $this->Id]) && $this->Incorp['paginacao_chose_' . $this->Id] === '1'):

            if ($this->Tipo === 'blog'):
                //obtem informações dinâmicas, pois tem paginação nos resultados que devem ser controlados pela classe Link
                $this->getBlogListDinamico();

            else:
                //não obtem as informações dinâmicamente, mas exibe a paginação no slide
                $this->exeFixoPaginado();

            endif;

        else:

            //obtem uma lista fixa, que não irá sofrer alterações
            if ($this->Tipo === 'blog'):

                $this->getBlogListFixo();

            else:

                $this->getSlideListFixo();

            endif;

        endif;
    }

    /*
     * ===============================================
     *                      DINÂMICO
     * ===============================================
     */

    /*
     * Obtem inicialização e configurações do blog list
     */


    /*
     * Retorna um código para obter uma lista no estilo blog dinamicamente com paginação
     */

    private function getBlogListDinamico() {

        //inicializa a variável offsete, where dinâmico na página
        $this->Result = "<?php ";

        //obtem a pagina atual atravél da classe LINK em tempo real
        $this->Result .= "\$pag = \$Link->getOffset();";

        $this->Result .= "\$offset = {$this->Incorp['post_offset_' . $this->Id]} + (({$this->Incorp['post_quant_' . $this->Id]} * \$pag) - {$this->Incorp['post_quant_' . $this->Id]});";

        if ($this->Banco === 'post'):
            $this->Result .= "\$ids = \$Link->getIds({$this->Incorp['post_quant_' . $this->Id]}, \$offset);";
            $this->Result .= "if (\$pag > 1):
                        \$blog = new Snippet({$this->TipoDinamico}, \$ids, 'ORDER BY {$this->Incorp['post_ordem_' . $this->Id]}', {$this->Incorp['paginacao_chose_' . $this->Id]}, '{$this->Incorp['post_template_' . $this->Id]}', {$this->Incorp['post_col_' . $this->Id]}, '{$this->Incorp['post_font_' . $this->Id]}', {$this->Incorp['post_quant_' . $this->Id]}, \$pag);
                    else:
                        \$blog = new Snippet({$this->TipoDinamico}, \$ids, 'ORDER BY {$this->Incorp['post_ordem_' . $this->Id]}', {$this->Incorp['paginacao_chose_' . $this->Id]}, '{$this->Incorp['post_template_' . $this->Id]}', {$this->Incorp['post_col_' . $this->Id]}, '{$this->Incorp['post_font_' . $this->Id]}', {$this->Incorp['post_quant_' . $this->Id]});
                    endif;";
        else:
            $this->Result .= "\$where = \$Link->getWhere({$this->Incorp['post_quant_' . $this->Id]}, \$offset);";
            $this->Result .= "if (\$pag > 1):
                        \$blog = new Lista('" . PRE . $this->Banco . "', 'WHERE (' . \$where . ') ORDER BY id DESC', {$this->Incorp['paginacao_chose_' . $this->Id]}, '{$this->Incorp['post_template_' . $this->Id]}', {$this->Incorp['post_col_' . $this->Id]}, '{$this->Incorp['post_font_' . $this->Id]}', {$this->Incorp['post_quant_' . $this->Id]}, \$pag, \$offset);
                    else:
                        \$blog = new Lista('" . PRE . $this->Banco . "', 'WHERE (' . \$where . ') ORDER BY id DESC', {$this->Incorp['paginacao_chose_' . $this->Id]}, '{$this->Incorp['post_template_' . $this->Id]}', {$this->Incorp['post_col_' . $this->Id]}, '{$this->Incorp['post_font_' . $this->Id]}', {$this->Incorp['post_quant_' . $this->Id]}, \$offset);
                    endif;";
        endif;

        $this->setPostPreDefinidosDinamico();

        $this->Result .= "\$blog->getPosts(); ?>";
    }

    private function setPostPreDefinidosDinamico() {
        for ($i = 0; $i < 20; $i++):
            if (isset($this->Incorp["post_chose_{$i}_" . $this->Id]) && !empty($this->Incorp["post_chose_{$i}_" . $this->Id])):
                $this->Result .= "\$blog->setPost({$this->Incorp["post_chose_{$i}_" . $this->Id]});";
            endif;
        endfor;
    }

    /*
     * ===============================================
     *                      FIXO
     * ===============================================
     */

    /*
     * Obtem uma lista de slide fixa
     */

    private function getSlideListFixo() {
        //inicializa slide
        $this->getOptionsSlideFixo();

        $this->getWhere();

        $tag = str_replace('post', 'post', $this->Banco);

        $this->setSlideListFixo($tag);
    }

    /*
     * Seta os slides fixos no slideshow
     */

    private function setSlideListFixo($tag) {

        $this->Result = "<div class='container {$this->Incorp['post_font_' . $this->Id]}'>";
        
        $this->setSlidePreDefinidosFixo($tag);

        $read = new Read();
        $read->ExeRead(PRE . $this->Banco, "WHERE {$this->Where} ORDER BY {$this->Incorp['post_ordem_' . $this->Id]} LIMIT {$this->Incorp['post_offset_' . $this->Id]},{$this->Incorp['post_quant_' . $this->Id]}");
        if ($read->getResult()):
            foreach ($read->getResult() as $gal):
            
                $this->setSlide($gal, $tag);
        
            endforeach;

            $this->Result .= $this->TempContent->Retorno();
        endif;
        
        $this->Result .= "</div>";
    }

    private function setSlidePreDefinidosFixo($tag) {
        $read = new Read();
        for ($i = 0; $i < 20; $i++):
            if (isset($this->Incorp["post_chose_{$i}_" . $this->Id]) && !empty($this->Incorp["post_chose_{$i}_" . $this->Id])):
                $read->ExeRead(PRE . $this->Banco, "WHERE id=:mii LIMIT 1", "mii={$this->Incorp["post_chose_{$i}_" . $this->Id]}");
                if ($read->getResult()):
                    
                    $this->setSlide($read->getResult()[0], $tag);
                
                endif;
            endif;
        endfor;
    }

    private function setSlide($gal, $tag) {
        if (isset($gal[$tag . '_title'])):
            if (!isset($gal[$tag . '_cover']) || empty($gal[$tag . '_cover']) || !file_exists("../../uploads/{$gal[$tag . '_cover']}")):
                $gal[$tag . '_cover'] = 'demo.jpg';
            endif;

            if ($tag === 'colecao' || $tag === 'tag' || $tag === 'category' || $tag === 'fabricante'):
                $gal[$tag . '_name'] = $tag . '/' . $gal[$tag . '_name'];
            endif;

            $desc = (isset($this->Incorp["post_slide_desc_" . $this->Id]) && $this->Incorp["post_slide_desc_" . $this->Id] == '1' && isset($gal[$tag . '_content']) && !empty($gal[$tag . '_content']) ? $gal[$tag . '_content'] : false);
            $link = (isset($gal[$tag . '_name']) && !empty($gal[$tag . '_name']) ? $gal[$tag . '_name'] : false);

            $this->TempContent->setSlide(HOME . "/uploads/{$gal[$tag . '_cover']}", $gal[$tag . '_title'], $link, $desc);
        endif;
    }

    /*
     * Obtem inicialização e opções do slide fixo
     */

    private function getOptionsSlideFixo($tipo = null) {

        //inicializa o slide fixo
        if (isset($this->Incorp['post_quant_' . $this->Id]) && !empty($this->Incorp['post_quant_' . $this->Id])):
            $this->TempContent = new Slide($this->Incorp['post_quant_' . $this->Id]);
        else:
            $this->TempContent = new Slide();
        endif;

        //Slide Modelo
        if (isset($this->Incorp['post_slide_' . $this->Id]) && !empty($this->Incorp['post_slide_' . $this->Id])):
            $this->TempContent->setModel($this->Incorp['post_slide_' . $this->Id]);
        else:
            $this->TempContent->setModel(0);
        endif;

        //SETA PERSONALIZADA
        if (isset($this->Incorp['post_seta_slide_' . $this->Id]) && !empty($this->Incorp['post_seta_slide_' . $this->Id])):
            $this->TempContent->setaMode($this->Incorp['post_seta_slide_' . $this->Id]);
        endif;
        
        //PAGINAÇÃO SIM OU NÃO
        if (!isset($this->Incorp['paginacao_chose_' . $this->Id]) || empty($this->Incorp['paginacao_chose_' . $this->Id])):
            $this->TempContent->noPagination();
        else:
            if (isset($this->Incorp['paginacao_style_' . $this->Id]) && !empty($this->Incorp['paginacao_style_' . $this->Id])):
                $this->TempContent->setaPageStyle($this->Incorp['paginacao_style_' . $this->Id]);
            endif;

            if (isset($this->Incorp['paginacao_position_' . $this->Id]) && !empty($this->Incorp['paginacao_position_' . $this->Id])):
                $this->TempContent->setaPagePosition($this->Incorp['paginacao_position_' . $this->Id]);
            endif;
        endif;

        //SLIDE OU GALERIA
        if (isset($this->Incorp['post_slide_gallery_' . $this->Id]) && $this->Incorp['post_slide_gallery_' . $this->Id] === '2'):
            $this->TempContent->galleryMode();
        endif;

        //SLIDE AUTO
        if (isset($this->Incorp['post_slide_auto_' . $this->Id]) && !empty($this->Incorp['post_slide_auto_' . $this->Id])):
            $this->TempContent->galleryAuto();
        endif;
    }

    private function getBlogListFixo() {

        $this->getWhere();

        if ($this->Banco === 'post'):
            
            $ids = "";
            $read = new Read();
            $read->ExeRead(PRE . "post", "WHERE {$this->Where} ORDER BY {$this->Incorp['post_ordem_' . $this->Id]} LIMIT {$this->Incorp['post_offset_' . $this->Id]}, {$this->Incorp['post_quant_' . $this->Id]}");
            if($read->getResult()):
                foreach ($read->getResult() as $p):
                    $ids .= "{$p['id']}||";
                endforeach;
            endif;
            
            $blog = new Snippet($this->TipoDinamico, $ids, "ORDER BY {$this->Incorp['post_ordem_' . $this->Id]}", $this->Incorp['paginacao_chose_' . $this->Id], $this->Incorp['post_template_' . $this->Id], $this->Incorp['post_col_' . $this->Id], $this->Incorp['post_font_' . $this->Id], $this->Incorp['post_quant_' . $this->Id]);
        
        else:
            $blog = new Lista(PRE . $this->Banco, "WHERE {$this->Where} ORDER BY {$this->Incorp['post_ordem_' . $this->Id]}", $this->Incorp['paginacao_chose_' . $this->Id], $this->Incorp['post_template_' . $this->Id], $this->Incorp['post_col_' . $this->Id], $this->Incorp['post_font_' . $this->Id], $this->Incorp['post_quant_' . $this->Id], $this->Incorp['post_offset_' . $this->Id]);
        endif;

        //setPostPreDefinidosFixo
        for ($i = 0; $i < 20; $i++):
            if (isset($this->Incorp["post_chose_{$i}_" . $this->Id]) && !empty($this->Incorp["post_chose_{$i}_" . $this->Id])):
                $blog->setPost($this->Incorp["post_chose_{$i}_" . $this->Id]);
            endif;
        endfor;

        $this->Result = $blog->getReturnPosts();
    }

    /*
     * Busca um executável fixo, e insere o resultado na página
     */

    private function exeFixoPaginado() {
        //inicializa slide
        $this->getOptionsSlideFixo(1);

        $this->getWhere();

        $tag = str_replace('post', 'post', $this->Banco);

        $this->setSlideListFixo($tag);
    }

    /*
     * Retorna um where válido
     */

    private function getWhere() {
        if ($this->Banco === 'post'):
            $this->Where = 'status=1';
        else:
            $this->Where = 'id > 0';
        endif;

        if ($this->Banco === 'post' || $this->Banco === "produto"):
            $this->Banco = 'post';
        endif;
    }

    /*
     * Aplica as informações do Padrao aos elementos, garantindo que não fiquem NULL
     */

    private function setInfosPadrao() {

        //PAGINAÇÃO, TEMPLATE, COLUNAS, FONT, ORDEM, QUANT, OFFSET
        $this->Incorp['paginacao_chose_' . $this->Id] = (isset($this->Incorp['paginacao_chose_' . $this->Id]) && !empty($this->Incorp['paginacao_chose_' . $this->Id]) ? 1 : 0);
        $this->Incorp['post_template_' . $this->Id] = ( isset($this->Incorp['post_template_' . $this->Id]) && !empty($this->Incorp['post_template_' . $this->Id]) ? $this->Incorp['post_template_' . $this->Id] : 'post_blog');
        $this->Incorp['post_col_' . $this->Id] = (int) ( isset($this->Incorp['post_col_' . $this->Id]) && !empty($this->Incorp['post_col_' . $this->Id]) ? $this->Incorp['post_col_' . $this->Id] : 3);
        $this->Incorp['post_font_' . $this->Id] = (isset($this->Incorp['post_font_' . $this->Id]) && !empty($this->Incorp['post_font_' . $this->Id]) ? $this->Incorp['post_font_' . $this->Id] : "font-size10");
        $this->Incorp['post_ordem_' . $this->Id] = (isset($this->Incorp['post_ordem_' . $this->Id]) && !empty($this->Incorp['post_ordem_' . $this->Id]) ? $this->Incorp['post_ordem_' . $this->Id] : "id DESC");
        $this->Incorp['post_quant_' . $this->Id] = (int) (isset($this->Incorp['post_quant_' . $this->Id]) && !empty($this->Incorp['post_quant_' . $this->Id]) ? $this->Incorp['post_quant_' . $this->Id] : 10);
        $this->Incorp['post_offset_' . $this->Id] = (int) (isset($this->Incorp['post_offset_' . $this->Id]) && !empty($this->Incorp['post_offset_' . $this->Id]) ? $this->Incorp['post_offset_' . $this->Id] : 0);

        if ($this->Banco !== "post"):
            $a = ["date", "views", "title"];
            $b = ["id", $this->Banco . "_views", $this->Banco . "_title"];
            $this->Incorp['post_ordem_' . $this->Id] = str_replace($a, $b, $this->Incorp['post_ordem_' . $this->Id]);
        endif;
    }

}
