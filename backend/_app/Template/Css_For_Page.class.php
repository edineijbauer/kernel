<?php

/**
 * Css_For_Page.class [ TEMPLATE ADMIN ]
 * Esta classe irá varrer um arquivo de css e simplificá-lo, atribuir somente um style para o elemento
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class Css_For_Page {

    private $Classes;
    private $Css;
    private $Result;

    function __construct($css) {
        $this->Css = $css;

        $this->ClassesPre();
        
        $this->Inicio();
    }

    public function getResult() {
        return $this->Classes;
    }
    
    private function ClassesPre() {
        $this->Classes['.shoticon-lapis'] = '.shoticon-lapis';
        $this->Classes['.shoticon-denveloper-pure'] = '.shoticon-denveloper-pure';
        $this->Classes['.shoticon-trash'] = '.shoticon-trash';
        $this->Classes['.shoticon-certob'] = '.shoticon-certob';
    }

    private function Inicio() {
        $read = new Read();
        $read->ExeRead(PRE . "site_page", "ORDER BY id");
        if ($read->getResult()):
            foreach ($read->getResult() as $page):
                $this->PageSearch($page['page_name'], $page['page_table']);
            endforeach;
        endif;

        //busca agora por styles no scrip
        $this->OpenScript();

        //busca agora por tpls do site
        $this->OpenTpl();
    }

    private function PageSearch($name, $table) {
        if ($table === '0'):
            if ($name === 'index'):
                $link = HOME;
            else:
                $link = HOME . '/' . $name . '/';
            endif;
        else:
            $read = new Read();
            $read->ExeRead(PRE . $table, "ORDER BY id DESC LIMIT 1");
            if ($read->getResult() && isset($read->getResult()[0]["{$table}_name"])):
                $link = HOME . '/' . $name . '/' . $read->getResult()[0]["{$table}_name"];
            else:
                $link = HOME . '/' . $name . '/';
            endif;
        endif;

        $this->getFile($link);
    }

    private function getFile($link) {
        $file = Check::file_get($link);
        if ($file[1]['http_code'] === 200):
            $file = str_replace('"', "'", $file[0]);
            $c = explode("class='", $file);

            foreach ($c as $i => $d):
                if ($i > 0):
                    $ef = explode("'", $d);
                    $e = explode(' ', $ef[0]);
                    foreach ($e as $a):
                        $this->Classes['.' . $a] = '.' . $a;
                    endforeach;
                endif;
            endforeach;

            $this->getIds($file);

        endif;
    }

    private function getIds($file) {

        $c = explode("id='", $file);

        foreach ($c as $i => $d):
            if ($i > 0):
                $e = explode("'", $d);
                $this->Classes['#' . $e[0]] = '#' . $e[0];
            endif;
        endforeach;
    }

    private function OpenScript() {
        $file = Check::file_get(HOME . "/js/scriptP.js");
        if ($file[1]['http_code'] === 200):
            $file = $file[0];

            $this->getClassesScript($file);
            $this->getIdsScript($file);
            $this->getClassesHtml($file);
            $this->getIdsHtml($file);

        endif;
    }

    private function getClassesScript($file) {

        //verifica para aspas duplas
        $a = explode('$(".', $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode('"', $c);
                $this->Classes['.' . $b[0]] = '.' . $b[0];
            endif;
        endforeach;

        //verifica para aspas simples
        $a = explode("$('.", $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode("'", $c);
                $this->Classes['.' . $b[0]] = '.' . $b[0];
            endif;
        endforeach;
    }

    private function getIdsScript($file) {

        //verifica para aspas duplas
        $a = explode('$("#', $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode('"', $c);
                $this->Classes['#' . $b[0]] = '#' . $b[0];
            endif;
        endforeach;

        //verifica para aspas simples
        $a = explode("$('#", $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode("'", $c);
                $this->Classes['#' . $b[0]] = '#' . $b[0];
            endif;
        endforeach;
    }

    private function getClassesHtml($file) {

        //verifica para aspas duplas
        $a = explode('class="', $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode('"', $c);
                $g = explode(' ', $b[0]);
                foreach ($g as $cl):
                    if ($cl !== "'" && $cl != '+'):
                        $this->Classes['.' . $cl] = '.' . $cl;
                    endif;
                endforeach;
            endif;
        endforeach;

        //verifica para aspas simples
        $a = explode("class='", $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode("'", $c);
                $g = explode(' ', $b[0]);
                foreach ($g as $cl):
                    if ($cl !== '"' && $cl != '+'):
                        $this->Classes['.' . $cl] = '.' . $cl;
                    endif;
                endforeach;
            endif;
        endforeach;
    }

    private function getIdsHtml($file) {

        //verifica para aspas duplas
        $a = explode('id="', $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode('"', $c);
                if (!preg_match('/\+/i', $b[0])):
                    $this->Classes['#' . $b[0]] = '#' . $b[0];
                endif;
            endif;
        endforeach;

        //verifica para aspas simples
        $a = explode("id='", $file);
        foreach ($a as $i => $c):
            if ($i > 0):
                $b = explode("'", $c);
                if (!preg_match('/\+/i', $b[0])):
                    $this->Classes['#' . $b[0]] = '#' . $b[0];
                endif;
            endif;
        endforeach;
    }

    private function OpenTpl() {
        $file = Check::file_get(HOME . "/" . REQUIRE_PATH . "/tpl/");
        if ($file[1]['http_code'] === 200):
            $file = $file[0];

            $this->getClassesScript($file);
            $this->getIdsScript($file);
            $this->getClassesHtml($file);
            $this->getIdsHtml($file);

        endif;
    }

}
