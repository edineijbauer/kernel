<?php

/**
 * SessaoSave.class [ MODEL ADMIN ]
 * Responável por salvar a estrutura e sessões de um site!
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class SessaoSave {

    private $Padrao;
    private $Incorp;
    private $StyleFile;
    private $Resolution;
    private $LastResolution = 0;
    private $ValoresStyle;
    private $LastTop;
    private $Menus;
    private $Hover;

    function __construct() {
        $this->Hover = false;
        $this->getMenus();
        $this->setPadrao();
        $this->setStilos();
        $this->CreateFileStyle();
    }

    protected function getPadrao() {
        return $this->Padrao;
    }

    protected function getIncorp() {
        return $this->Incorp;
    }

    protected function getResolution() {
        return $this->LastResolution;
    }

    /*
     * getMenus - Obtem uma lista de blocos que são menus
     * para poder aplicar os valores nos subitens ao invés do bloco
     */

    private function getMenus() {
        $read = new Read();
        $read->ExeRead(PRE . "menu");
        if ($read->getResult()):
            foreach ($read->getResult() as $m):
                $mp = '9999' . $m['id'];
                $read->ExeRead(PRE . "padrao", "WHERE name LIKE 'incorporar_%' && padrao = :mp ORDER BY resolution DESC", "mp={$mp}");
                if ($read->getResult()):
                    foreach ($read->getResult() as $p):
                        $this->Menus[] = (int) str_replace('incorporar_', '', $p['name']);
                    endforeach;
                endif;
            endforeach;
        endif;
    }

    /*
     * setPadrao
     * Método para obter os padrões dos elementos
     */

    private function setPadrao() {
        $read = new Read();
        $read->ExeRead(PRE . "padrao", "WHERE side > 0 ORDER BY resolution DESC");
        if ($read->getResult()):

            foreach ($read->getResult() as $p):

                //Verifica se é um hover style
                if (preg_match('/_hover/i', $p['name'])):
                    $this->Hover = true;
                    $p['name'] = str_replace('_hover', '', $p['name']);
                endif;

                $this->setResolutionsInitial($p['resolution']);
                $this->ColheEstilo($p);

                $this->Hover = false;

            endforeach;

            krsort($this->Resolution);
        endif;
    }

    /*
     * Configura as informações de resoluções iniciais para a aplicação
     */

    private function setResolutionsInitial($r) {
        if (empty($this->Resolution) || (!in_array($r, $this->Resolution) && $r > 1)):
            $this->Resolution[] = $r;

            //obtem a resolução mais alta
            if ($this->LastResolution == 0):
                $this->LastResolution = $r;
            endif;
        endif;
    }

    /*
     * Colhe Estilo
     * Separa as informações de padrão para aplicar em diferentes áreas da sessão
     */

    private function ColheEstilo($p) {
        $file = file_get_contents(HOME . '/css/boot.css');
        $listClass = ["shadow_", "position_", "post_font_", "radius_", "post_font_align_", "post_font_weight_", "post_font_size_", "border_", "shadow_", "padding_", "margin_", "cursor_", "transition_", "cor_", "post_font_color_", "dimension_"];
        $listaClass = "/( {" . implode("|", $listClass) . ")/i";

        if ($p['side'] == 3):
            //VALOR NUMÉRICO de estilo, necessita verificar se este valor é px ou %
            $this->ValoresStyle[$p['resolution']][$p['name']] = $p['padrao'];

        elseif ($p['side'] == 1):
            //CLASSES e a unidade de medida para acompanhas com o valor numérico
            $this->Padrao[$p['resolution']][$p['name']] = $p['padrao'];

            if (preg_match($listaClass, $p['name']) && !empty($p['padrao'])):
                $a = explode('.' . $p['padrao'] . '{', $file);
                if (isset($a[1])):
                    $a = explode('}', $a[1]);
                    $n = explode('_', $p['name']);
                    $idb = $n[count($n) - 1];
                    if (in_array($idb, $this->Menus) && !preg_match('/^position_/i', $p['name'])):

                        //menu
                        $this->sendToStyle('.bloco_' . $idb . ' ul li{' . $a[0] . '}', $p['resolution']);
                    else:

                        //normal
                        $this->sendToStyle('.bloco_' . $idb . '{' . $a[0] . '}', $p['resolution']);
                    endif;
                endif;

            endif;

        elseif ($p['side'] == 2):
            //STYLE PURE 

            $a = explode('{', $p['padrao']);
            $idb = (int) str_replace('.bloco_', '', $a[0]);
            if (in_array($idb, $this->Menus) && preg_match('/^(cor_|post_font_color_)/i', $p['name'])):

                //menu
                if ('post_font_color_' . $idb === $p['name']):
                    $this->sendToStyle(str_replace('{', ' ul li a{', $p['padrao']), $p['resolution']);
                else:
                    $this->sendToStyle(str_replace('{', ' ul li{', $p['padrao']), $p['resolution']);
                endif;
            else:

                //normal
                $this->sendToStyle($p['padrao'], $p['resolution']);
            endif;

        elseif ($p['side'] > 5):
            //informações sobre INCORPORAÇÕES de conteúdo
            $this->Incorp[$p['name']] = $p['padrao'];
        endif;
    }

    private function sendHeightToValores($p) {
        if (preg_match('/height_/i', $p['name'])):
            $e = explode('height:', $p['padrao']);
            $e = explode('px}', $e[1]);
            $this->ValoresStyle[$p['resolution']][$p['name']] = (int) $e[0];
        endif;
    }

    /*
     * Verificas se deve-se usar o media query ou não
     */

    private function sendToStyle($estilo, $resolution) {
        if (!empty($estilo)):

            //$estilo = checkToLeft($estilo);

            if ($resolution == 0 || $resolution == $this->LastResolution):
                //Aplica direto
                $this->aplicaEstiloPuro($estilo);

            else:
                //Aplica com media query
                $this->aplicaEstiloPuro($estilo, $resolution);

            endif;
        endif;
    }

    /*
     * Obtem um estilo puro do padrão e transforma para a variável StyleFile
     */

    private function aplicaEstiloPuro($estilo, $media = null) {
        $media = ($media == 350 ? 420 : $media);
        $media = (string) ($media ? "@media screen and (max-width: " . ($media * 1.2) . "px){" : '' );
        $a = explode('{', $estilo);
        $estilo = str_replace('}', '', $a[1]);
        if (!preg_match('/;$/i', $estilo)):
            $estilo .= ';';
        endif;
        if ($this->Hover):
            $a[0] .= ":hover";
        endif;
        $this->StyleFile[$media][$a[0]] = (isset($this->StyleFile[$media][$a[0]]) ? $this->StyleFile[$media][$a[0]] . ' ' . $estilo : $estilo );
    }

    private function setStilos() {
        if (!empty($this->ValoresStyle)):
            foreach ($this->ValoresStyle as $AResolution => $d):
                foreach ($d as $name => $v):

                    //separa name de id
                    $name2 = explode('_', $name);
                    $id = $name2[count($name2) - 1];
                    $name = str_replace('_' . $id, '', $name);
                    if ($name === 'left'):
                        $name = 'margin-left';
                        $read = new Read();
                        $read->ExeRead(PRE . "temp", "WHERE struct_id=:st && resolution=:rr", "st={$id}&rr={$AResolution}");
                        if ($read->getResult()):
                            $v = $read->getResult()[0]['struct_left'];
                        endif;
                    elseif ($name === 'top'):
                        $name = 'margin-top';
                        $read = new Read();
                        $read->ExeRead(PRE . "temp", "WHERE struct_id=:st && resolution=:rr", "st={$id}&rr={$AResolution}");
                        if ($read->getResult()):
                            $v = $read->getResult()[0]['struct_top'];
                        endif;
                    endif;

                    $this->setUnidadePadrao($id, $AResolution);
                    $this->aplySplitStyle($AResolution, $id, $name, $v);

                endforeach;
            endforeach;
        endif;
    }

    private function setUnidadePadrao($id, $AResolution) {
        $temp = Admin::getlastStyle('unidade_' . $id, $AResolution);
        $this->Padrao[$AResolution]['unidade_' . $id] = $temp[0];
    }

    /*
     * Apply Split Style = Aplica estilos separados
     * Classe lógica para a aplicação dos estilos nos elementos
     */

    private function aplySplitStyle($AResolution, $id, $name, $v) {

        //Verifica porcentagem, se for diferente de false e diferente de poecento ou se name for igual a top = pixel
        if ((($this->Padrao[$AResolution]['unidade_' . $id] && $this->Padrao[$AResolution]['unidade_' . $id] != "porcento") || $name == "margin-top") && $name !== 'margin-left'):

            //caso seja top, verifica se é incorporado, pois incorporado não se executa o top, e sim o margin-top em relação a outos incorporados
            if ($name == 'margin-top'):

                $this->sendToStyle(".bloco_{$id}{{$name}:" . $v . "px;}", $AResolution);

            else:
                $this->sendToStyle(".bloco_{$id}{{$name}:" . $v . "px;}", $AResolution);
            endif;

        else:
            $value = ($v == 0 ? 0 : round(($v * 100) / $AResolution, 1));
            $this->sendToStyle(".bloco_{$id}{{$name}:" . $value . "%;}", $AResolution);
        endif;
    }

    private function CreateFileStyle() {
        $cssFiles = array(
            "css/boot.css",
            "css/shoticon/material.css",
            "css/theme/smart.css",
            "_cdn/Slide/Slide_Style.css",
            "themes/smart/css/style.css"
        );

        $fp = fopen("../css/styleP.css", "w");

        $buffer = "";
        foreach ($cssFiles as $cssFile) {
            if ($cssFile === 'css/shoticon/material.css'):
                $buffer .= str_replace(array("../boot/", "pure/", "material/"), array("boot/", "shoticon/pure/", "shoticon/material/"), file_get_contents('../' . $cssFile));
            else:
                $buffer .= file_get_contents('../' . $cssFile);
            endif;
        }
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
        $buffer = str_replace(': ', ':', $buffer);
        $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
        $escreve = fwrite($fp, $buffer);

        foreach ($this->StyleFile as $media => $a):
            if (!empty($media)):
                $escreve .= fwrite($fp, $media);
            endif;

            foreach ($a as $classe => $estilo):
                $escreve .= fwrite($fp, $classe . '{' . $estilo . '}');
            endforeach;

            if (!empty($media)):
                $escreve .= fwrite($fp, '}');
            endif;

        endforeach;

        fclose($fp);

        $this->CreateFileScript();
    }

    private function CreateFileScript() {

        $jsFiles = array(
            "js/jquery.min.js",
            "themes/smart/js/script.js",
            "_cdn/Social/sharebox.js",
            "js/boot_site.js",
            "_cdn/Slide/Slide_Script.js",
            "_cdn/Slide/Slide_Gallery_Script.js"
        );

        $fpt = fopen("../js/scriptP.js", "w");
        $buffer = '';

        foreach ($jsFiles as $jsFile) {
            $buffer .= file_get_contents('../' . $jsFile);
        }
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
        $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

        fwrite($fpt, $buffer);

        fclose($fpt);
    }

}
