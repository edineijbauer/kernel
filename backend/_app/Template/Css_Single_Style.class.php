<?php

/**
 * Css_Single_Style.class [ TEMPLATE ADMIN ]
 * Esta classe irá varrer um arquivo de css e simplificá-lo, atribuir somente um style para o elemento
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class Css_Single_Style {

    private $File;
    private $Tipo;
    private $Styles;
    private $Class;
    private $ValorMedia;
    private $Css;
    private $Ocss;
    private $Rcss;
    private $Conta = 0;
    private $Media;
    private $Result;

    function __construct($file, $tipo, $styles = null) {
        $this->File = $file;
        $this->Tipo = (bool) $tipo;
        $this->Styles = $styles;

        $this->OpenMedias();
        $this->OpenStyles();

        arsort($this->ValorMedia);

        if($this->Tipo):
            $page = new Css_For_Page($this->Css);
            $this->Class = $page->getResult();
        endif;

        $this->makeFile();
    }

    public function getResult() {
        return $this->Result;
    }

    private function OpenStyles() {
        foreach ($this->Styles as $classe => $estilo):
            $this->Media = 0;
            $elementos[0] = $classe;

            if (preg_match('/;/i', $estilo)):

                $d = explode(';', $estilo);
                foreach ($d as $e):
                    if (!empty($e)):
                        $this->makeCssArray($elementos, trim($e));
                    endif;
                endforeach;

            else:
                $this->makeCssArray($elementos, $estilo);
            endif;

        endforeach;
    }

    /*
     * Retorna o número de elementos removidos inúteis no código
     * Visualiza a real efetividade deste código
     */

    public function getCount() {
        return $this->Conta;
    }

    private function OpenMedias() {
        if (preg_match('/@media screen and/i', $this->File)):
            $a = explode('@media screen and', $this->File);
            foreach ($a as $medias):
                $media = trim($medias);

                if (preg_match('/^\(/i', $media)):
                    //é um media query

                    $b = explode(')', $media);
                    $c = explode('(', $b[0]);

                    $this->Media = str_replace(' ', '', trim($c[1]));

                    $v = explode(':', $this->Media);
                    $v = explode('px', $v[1]);
                    $this->ValorMedia[$this->Media] = (int) trim($v[0]);

                    $file = str_replace(array('(' . $c[1] . ') {', '(' . $c[1] . '){'), array('', ''), $media);

                    $f = explode('}}', $file);

                    $this->OpenFile($f[0]);

                    if (!empty($f[1])):
                        $file = str_replace($f[0] . '}}', '', $file);
                        $this->Media = 0;
                        $this->OpenFile($file);
                    endif;

                else:
                    //não é um media query, ou seja, todo o resto
                    $this->Media = 0;

                    $this->OpenFile($media);

                endif;

            endforeach;
        else:

            $this->Media = 0;
            $this->OpenFile($this->File);

        endif;
    }

    private function openFile($file) {
        $a = explode('}', $file);
        foreach ($a as $b):
            if (!empty($b) && preg_match('/{/i', $b)):

                $c = explode('{', $b);

                if (isset($c[2])):
                    $c[0] = $c[count($c) - 2];
                    $c[1] = $c[count($c) - 1];
                endif;

                $elementos[0] = $c[0];

                if (preg_match('/;/i', $c[1])):

                    $d = explode(';', $c[1]);
                    foreach ($d as $e):
                        if (!empty($e)):
                            $this->makeCssArray($elementos, trim($e));
                        endif;
                    endforeach;

                else:
                    $this->makeCssArray($elementos, $c[1]);
                endif;

                unset($elementos, $c, $b, $d, $e);

            endif;

        endforeach;
    }

    private function makeCssArray($elementos, $e) {

        if (preg_match('/:/i', $e)):
            $f = explode(':', $e);

            //para cada elemento sendo atribuido estes style
            foreach ($elementos as $elemento):
                $elemento = trim($elemento);
                $style = trim($f[0]);
                $value = trim($f[1]);

                if (preg_match('/!important/i', $value)):
                    //css important

                    if (isset($this->Css[$this->Media][$elemento][$style][0])):

                        //somente conta mais um elemento removido
                        $this->Conta++;
                        unset($this->Css[$this->Media][$elemento][$style][0]);

                    endif;

                    //somente para contar mais um elemento removido
                    if (isset($this->Css[$this->Media][$elemento][$style][1])):
                        $this->Conta++;
                    endif;

                    $this->Css[$this->Media][$elemento][$style][1] = $value;

                else:

                    if (!isset($this->Css[$this->Media][$elemento][$style][1])):

                        //somente para contar mais um elemento removido
                        if (isset($this->Css[$this->Media][$elemento][$style][0])):
                            $this->Conta++;
                        endif;

                        $this->Css[$this->Media][$elemento][$style][0] = $value;
                    else:

                        //somente conta mais um elemento removido
                        $this->Conta++;

                    endif;


                endif;

            endforeach;

        endif;
    }

    /*
     * Contrói o arquivo css novamente 
     * agora sem repetições de style
     */

    private function makeFile() {
        $this->Result = '';

        foreach ($this->Css[0] as $elemento => $a):

            $this->aplyStyle($elemento, $a);

        endforeach;

        foreach ($this->ValorMedia as $media => $valor):

            $this->Result .= "@media screen and ({$media}){";

            foreach ($this->Css[$media] as $elemento => $a):

                $this->aplyStyle($elemento, $a);

            endforeach;

            $this->Result .= "}";

        endforeach;
    }

    private function aplyStyle($elemento, $a) {

        if (preg_match('/,/i', $elemento)):
            $g = explode(',', $elemento);
            foreach ($g as $b):
                
                if($this->ValidateStyle($b)):
                    $this->sendToResult($elemento, $a);
                    break;
                endif;
            
            endforeach;
        else:
                
            if($this->ValidateStyle($elemento)):
                $this->sendToResult($elemento, $a);
            endif;

        endif;
    }

    private function ValidateStyle($element) {
        
        $elemento = trim($element);
        
        $f = explode(' ', $elemento);
        $f = explode(':', $f[0]);
        $f = explode('.', $f[0]);
        if (preg_match('/^\./', $elemento)):
            $elemento = '.' . $f[1];
        else:
            $elemento = $f[0];
        endif;

        if (((isset($this->Class) && in_array($elemento, $this->Class)) || !preg_match('/^(#|\.)/i', $elemento)) || !isset($this->Class)):
            
            return true;
            
        else:
            
            return false;
        
        endif;
    }

    private function sendToResult($elemento, $a) {
        $this->Result .= $elemento . '{';

        foreach ($a as $style => $b):
            foreach ($b as $value):

                $this->Result .= $style . ':' . $value . ';';

            endforeach;
        endforeach;

        $this->Result .= '}';
    }

}
