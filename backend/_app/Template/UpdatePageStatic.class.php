<?php

/*
 * Este Software esta licenciado pela ontab.com.br e disponibilizado gratuitamente na web para uso pessoal e profissional
 * Você tem permissão para usá-la como bem entender, mas não comercializá-la. * 
 * A sua distribuição esta proibida sob pena jurídica, a ontab.com.br é o único site com permissão para distribuição.  * 
 */

/**
 * Description of UpdatePageStatic
 *
 * @author nenab
 */
class UpdatePageStatic {

    //put your code here

    private $Tabela;
    private $Id;

    function __construct($tabela, $id = null) {
        /*$this->Tabela = $tabela;

        if ($this->Tabela === PRE . "sessoes"):
            $this->Id = ( $id ? $id : 0);
            $this->AtualizacaodeSessaoContent();
        else:

            $inc = $this->getIncCache();
            if ($inc < 0):

                $this->AtualizacaodeProdutos($inc);

            endif;
        endif;*/
    }

    private function AtualizacaodeProdutos($inc) {
        $read = new Read();
        $read->ExeRead(PRE . "cache_content", "WHERE include=:inc && dinamico=0 && status=0", "inc={$inc}");
        if ($read->getResult()):
            $up = new Update();
            foreach ($read->getResult() as $c):
                $Dados['status'] = 1;
                $Dados2['content_status'] = 1;
                $List[] = (int) $c['page_id'];
                $up->ExeUpdate(PRE . "cache_content", $Dados, "WHERE id=:linc", "linc={$c['id']}");
                $up->ExeUpdate(PRE . "cache_sessao", $Dados2, "WHERE sessao_id=:sinc && content_status=0", "sinc={$c['sessao_id']}");
            endforeach;

            foreach ($List as $id):
                new Save_Page($id, 1);
            endforeach;

        endif;
    }

    private function getIncCache() {

        if ($this->Tabela === PRE . "produto"):
            $inc = -2;
        elseif ($this->Tabela === PRE . "post"):
            $inc = -1;
        elseif ($this->Tabela === PRE . "colecao"):
            $inc = -3;
        elseif ($this->Tabela === PRE . "fabricante"):
            $inc = -4;
        elseif ($this->Tabela === PRE . "category"):
            $inc = -5;
        elseif ($this->Tabela === PRE . "tag"):
            $inc = -6;
        else:
            $inc = 0;
        endif;

        return $inc;
    }

    private function AtualizacaodeSessaoContent() {
        if (isset($this->Id) && !empty($this->Id)):
            $id = $this->getIdPage($this->Id);
            if ($id > 0):
                new Save_Page($id, 1);
            endif;
        endif;
    }

    private function getIdPage($id) {
        $read = new Read();
        $read->ExeRead(PRE . "sessoes", "WHERE id=:i", "i={$id}");
        if ($read->getResult()):
            if ($read->getResult()[0]['site_page_id'] === '0'):

                $read->ExeRead(PRE . "padrao", "WHERE name LIKE 'incorporar_%' && padrao=:p", "p={$read->getResult()[0]['id']}");
                if ($read->getResult()):
                    $name = (int) str_replace('incorporar_', '', $read->getResult()[0]['name']);
                    $read->ExeRead(PRE . "struct", "WHERE id=:ij", "ij={$name}");
                    if ($read->getResult()):

                        return $this->getIdPage($read->getResult()[0]['sessao_id']);

                    endif;
                endif;

            else:

                return (int) $read->getResult()[0]["site_page_id"];

            endif;
        endif;

        return 0;
    }

}
