<?php

/**
 * Save_Page.class [ TEMPLATE ADMIN ]
 * Esta classe irá ser responsável por organizar as sessões e struturas de uma determinada página e salvar em um arquivo
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class Save_Page extends SaveStyleScript {

    private $Id;
    private $SessaoId;
    private $PageId;
    private $FileName;
    private $Padrao;
    private $Incorp;
    private $Slide;
    private $SiteContent;
    private $Tag;
    private $Attr;
    private $Classes;
    private $Content;

    function __construct($id, $style = null) {
        $this->FileName = Check::Name(Check::getBanco(PRE . "site_page", (int) $id, 'page_name'));
        $this->PageId = (int) $id;
        $this->Id = (int) Check::getBanco(PRE . "sessoes", $this->PageId, "id", "site_page_id");
        
        parent::__construct($style);
        $this->Incorp = parent::getIncorp();
        $this->Padrao = parent::getPadrao();

        $this->initial();
        $this->CreateFile();
    }

    /*
     * Inicializa as funções desta classe
     */

    private function initial() {
        $IDS = $this->Id . $this->getId($this->Id);
        $temp = explode("||", $IDS);
        foreach ($temp as $id):
            if ($id > 0 && $id < 99990):
                $this->SessaoId = $id;

                $read = new Read();
                $read->ExeRead(PRE . "cache_struct_info", "WHERE sessao_id=:si ORDER BY position", "si={$id}");
                if ($read->getResult()):

                    //Verifica Cache, caso esteja intácto, pega do cache, senão, cria conteúdo e salva em cache
                    //adiciona conteúdo no _content atual
                    $this->CreateContent($read->getResult(), $id);

                endif;

            endif;
        endforeach;
    }

    private function CreateContent($cache, $id) {
        $read = new Read();
        $site_content = '';

        $r = $this->CheckCache();

        foreach ($cache as $struct):
            if ($r === 1):

                $this->SaveContent($struct);
                $site_content .= $this->SiteContent;

            else:

                $read->ExeRead(PRE . "cache_content", "WHERE struct_id=:st && status=0", "st={$struct['struct_id']}");
                if ($read->getResult()): 

                    $site_content .= $read->getResult()[0]['_content'];

                else:

                    $this->SaveContent($struct);
                    $site_content .= $this->SiteContent;
                    
                endif;
            endif;
        endforeach;

        //Obtem tipo de tag a implementar
        $this->getTag();

        //obtem os atributos para este tipo de tag
        $this->getAttr();

        $this->Content[$id] = "<{$this->Tag} class='container ps-relative' {$this->Attr}>" . $site_content . "</{$this->Tag}>";
    }

    /*
     * inicia o conteúdo da struct, caso tenha um break point, separa da próxima linha
     */

    private function SaveContent($struct) {
        $this->SiteContent = ($struct['break_point'] === '1' ? '<div class="container clear"></div>' : '');

        $this->CreateBloco($struct['struct_id']);

        $this->CreateCacheContent($struct['struct_id'], $struct['sessao_id']);
    }

    private function CheckCache() {

        $read = new Read();
        $read->ExeRead(PRE . "cache_sessao", "WHERE sessao_id=:si && content_status=1", "si={$this->Id}");
        if ($read->getResult()):

            //remove cache de conteúdos
            $Dados['status'] = 1;
            $Dados2['content_status'] = 0;
            $up = new Update();
            $up->ExeUpdate(PRE . "cache_content", $Dados, "WHERE sessao_id=:st && status=0", "st={$this->Id}");
            $up->ExeUpdate(PRE . "cache_sessao", $Dados2, "WHERE sessao_id=:si && content_status=1", "si={$this->Id}");

            return 1;
        endif;
        return 0;
    }

    private function CreateCacheContent($id, $sessao) {
        $read = new Read();
        $read->ExeRead(PRE . "cache_content", "WHERE struct_id=:st", "st={$id}");
        if ($read->getResult()):
            //atualiza cache

            $Dados['_content'] = $this->SiteContent;
            $Dados['include'] = (isset($this->Incorp['incorporar_' . $id]) ? $this->Incorp['incorporar_' . $id] : '');
            if (isset($this->Incorp['post_slide_gallery_' . $id]) && $this->Incorp['post_slide_gallery_' . $id] === '3'):
                $Dados['dinamico'] = (isset($this->Incorp['paginacao_chose_' . $id]) && !empty($this->Incorp['paginacao_chose_' . $id]) ? $this->Incorp['paginacao_chose_' . $id] : 0);
            else:
                $Dados['dinamico'] = 0;
            endif;
            $Dados['sessao_id'] = $sessao;
            $Dados['page_id'] = $this->PageId;
            $Dados['status'] = 0;
            $up = new Update();
            $up->ExeUpdate(PRE . "cache_content", $Dados, "WHERE struct_id=:st", "st={$id}");
            
            $Dados2['content_status'] = 0;
            $up->ExeUpdate(PRE . "cache_sessao", $Dados2, "WHERE sessao_id=:si && content_status=1", "si={$sessao}");
            
        else:
            //cria cache

            $Dados['_content'] = $this->SiteContent;
            $Dados['include'] = (isset($this->Incorp['incorporar_' . $id]) ? $this->Incorp['incorporar_' . $id] : '');
            if (isset($this->Incorp['post_slide_gallery_' . $id]) && $this->Incorp['post_slide_gallery_' . $id] === '3'):
                $Dados['dinamico'] = (isset($this->Incorp['paginacao_chose_' . $id]) && !empty($this->Incorp['paginacao_chose_' . $id]) ? $this->Incorp['paginacao_chose_' . $id] : 0);
            else:
                $Dados['dinamico'] = 0;
            endif;
            
            $Dados['sessao_id'] = $sessao;
            $Dados['struct_id'] = $id;
            $Dados['page_id'] = $this->PageId;
            $create = new Create();
            $create->ExeCreate(PRE . "cache_content", $Dados);
        endif;
    }

    private function CreateFile() {
        $ViewSite = new ViewSite();
        $e = "<?php extract(\$Link->getData()); ?>" . $ViewSite->Retorno($this->Content, $this->Content[$this->Id]);

        $this->FileName = Check::Name(Check::getBanco(PRE . "site_page", (int) $this->PageId, 'page_name'));
        if (file_exists("../../themes/" . THEME)):
            $fp = fopen("../../themes/" . THEME . "/{$this->FileName}.php", "w");
        elseif ("../themes/" . THEME):
            $fp = fopen("../themes/" . THEME . "/{$this->FileName}.php", "w");
        endif;
        fwrite($fp, $e);
        fclose($fp);
    }

    /*
     * Cria um modelo padrão de slide
     */

    private function setNewSlide($id, $quant = NULL) {
        if (isset($quant) && !empty($quant) && $quant > 0):
            $this->Slide = new Slide($quant);
        else:
            $this->Slide = new Slide();
        endif;

        //Slide
        if (isset($this->Incorp['post_slide_' . $id]) && !empty($this->Incorp['post_slide_' . $id])):
            $this->Slide->setModel($this->Incorp['post_slide_' . $id]);
        else:
            $this->Slide->setModel(0);
        endif;

        //SETA PERSONALIZADA
        if (isset($this->Incorp['post_seta_slide_' . $id]) && !empty($this->Incorp['post_seta_slide_' . $id])):
            $this->Slide->setaMode($this->Incorp['post_seta_slide_' . $id]);
        endif;

        //SLIDE OU GALERIA
        if (isset($this->Incorp['post_slide_gallery_' . $id]) && $this->Incorp['post_slide_gallery_' . $id] == '2'):
            $this->Slide->galleryMode();
        endif;

        //PAGINAÇÃO OU NÃO
        
        if (!isset($this->Incorp['paginacao_chose_' . $id]) || empty($this->Incorp['paginacao_chose_' . $id])):
            $this->Slide->noPagination();
        else:
            if (isset($this->Incorp['paginacao_style_' . $id]) && !empty($this->Incorp['paginacao_style_' . $id])):
                $this->Slide->setaPageStyle($this->Incorp['paginacao_style_' . $id]);
            endif;

            if (isset($this->Incorp['paginacao_position_' . $id]) && !empty($this->Incorp['paginacao_position_' . $id])):
                $this->Slide->setaPagePosition($this->Incorp['paginacao_position_' . $id]);
            endif;
        endif;

        //SLIDE AUTO
        if (isset($this->Incorp['post_slide_auto_' . $id]) && !empty($this->Incorp['post_slide_auto_' . $id])):
            $this->Slide->galleryAuto();
        endif;
    }

    /*
     * trabalha com as sessões para separa-las em blocos e buscar informações para tratar das margins
     */

    private function CreateBloco($id) {

        //obtem as classes e controle de posição do bloco
        $this->getClasses($id);

        //cria o inicio do bloco
        $this->SiteContent .= "<div class='bloco_{$id} {$this->Classes}' rel='" . Check::getBanco(PRE . "struct", (int) $id, "struct_item") . "' id='{$id}'>";

        $this->setContent($id);

        $this->SiteContent .= "</div>";
    }

    /*
     * Seta conteúdo para o bloco em processo, seja escrito ou incorporado
     */

    private function setContent($id) {
        //Se tiver conteúdo escrito no bloco, adiciona
        if (isset($this->Incorp['content_' . $id]) && !empty($this->Incorp['content_' . $id])):
            $this->SiteContent .= $this->Incorp['content_' . $id];
        endif;

        if (isset($this->Incorp['incorporar_' . $id]) && !empty($this->Incorp['incorporar_' . $id])):

            if (!is_numeric($this->Incorp['incorporar_' . $id])):
                //tem uma atribuição para este elemento, aplica
                $this->setInfosPadrao($id);
                $this->getInfoProduto($id);

            else:
                //INCORPORAÇÃO
                //3 tipos diferente

                if ($this->Incorp['incorporar_' . $id] > 99990):

                    //#######   Incorpora MENU   #######
                    $this->getMenu($id);

                elseif ($this->Incorp['incorporar_' . $id] < 0 && $this->Incorp['incorporar_' . $id] > -7):

                    //#######   Incorporação do tipo lista
                    $this->getList($id);

                else:

                    //INCORPORA UMA OUTRA SESSÃO
                    $this->SiteContent .= "@#" . $this->Incorp['incorporar_' . $id] . "#@";
                endif;

            endif;

        else:

        //#######   NÃO TEM Incorporação   #######

        endif;
    }

    private function getList($id) {
        $lista = new Lista_Content($id, $this->Incorp);
        $this->SiteContent .= $lista->getResult();
    }

    /*
     * Obtem as Classes do bloco e também as classes que controlam a posição
     */

    private function getClasses($id) {
        if (( isset($this->Incorp['incorporar_' . $id]) && !empty($this->Incorp['incorporar_' . $id]) ) && (!is_int($this->Incorp['incorporar_' . $id]) || ($this->Incorp['incorporar_' . $id] > -7 || ($this->Incorp['incorporar_' . $id] == '-8' && isset($this->Incorp['post_slide_gallery_' . $id]) && !empty($this->Incorp['post_slide_gallery_' . $id])) ) )):
            $this->Classes = 'ps-relative fl-left auto-height';
        else:
            $this->Classes = 'fl-left';
        endif;

        if (isset($this->Padrao['position_' . $id]) && $this->Padrao['position_' . $id] == 'ps-fixed'):
            $this->Classes = 'ps-fixed';
        endif;
    }

    /*
     * Obtem os atributos deste bloco dependete da tag tipo
     */

    private function getAttr() {
        $this->Attr = '';

        //Caso a tag seja do tipo link, adiciona seus atributos especiais de link
//        if ($this->Tag === "a" && isset($this->Incorp['a_' . $id]) && !empty($this->Incorp['a_' . $id])):
//            $this->Attr .= " href='{$this->Incorp['a_' . $id]}'";
//            $this->Attr .= (isset($this->Incorp['nfollow_' . $id]) && !empty($this->Incorp['nfollow_' . $id]) ? " rel='nofollow'" : "");
//            $this->Attr .= (isset($this->Incorp['targetb_' . $id]) && !empty($this->Incorp['targetb_' . $id]) ? " target='_blank'" : "");
//        endif;

        //le outros atributos para este elemento (title, alt, rel)
        $atributos = ["title", "alt", "rel"];
        foreach ($atributos as $attr):
            if (isset($this->Incorp["{$attr}_{$this->SessaoId}"]) && !empty($this->Incorp["{$attr}_{$this->SessaoId}"])):
                $this->Attr .= " {$attr}='{$this->Incorp["{$attr}_{$this->SessaoId}"]}'";
            endif;
        endforeach;
    }

    /*
     * Obtem o tipo de tag que é este bloco
     */

    private function getTag() {
        $read = new Read();
        $read->ExeRead(PRE . "sessoes", "WHERE id=:ii", "ii={$this->SessaoId}");
        if ($read->getResult()):
            $read->ExeRead(PRE . "tipo", "WHERE id=:i", "i={$read->getResult()[0]['tipo_id']}");
            if ($read->getResult()):
                $this->Tag = $read->getResult()[0]['tipo_title'];
            else:
                $this->Tag = 'div';
            endif;
        else:
            $this->Tag = 'div';
        endif;
    }

    /*
     * Obtem inforproduto
     */

    private function getInfoProduto($id) {
        $elemento = $this->Incorp['incorporar_' . $id];

        if (preg_match('/cover/i', $elemento)):
            //COVER
            $this->SiteContent .= "<img src='<?php if(!preg_match('/http/i', \${$elemento})): echo HOME . '/uploads/'; endif; echo \${$elemento}; ?>' />";

        elseif ($elemento == "post_social"):
            //social
            $this->SiteContent .= file_get_contents('../../_cdn/Social/social.inc.php');

        elseif ($elemento == "post_related"):
            //relacionados
            if (!isset($this->Incorp['post_col_' . $id])):
                $this->Incorp['post_col_' . $id] = 3;
            endif;

            $this->SiteContent .= "<?php \$ids = '';";
            $this->SiteContent .= "\$id = (isset(\$extends_posts)? \$extends_posts : \$id);";
            $this->SiteContent .= "\$cat = Check::getBanco(PRE . 'snippet', (int) \$id, 'category', 'post_id');";
            $this->SiteContent .= "\$read->ExeRead(PRE . 'snippet', 'WHERE status = 1 && category=\''.\$cat.'\' && post_id!= '. \$id .' ORDER BY RAND() LIMIT {$this->Incorp['post_offset_' . $id]}, {$this->Incorp['post_quant_' . $id]}');";

            $this->SiteContent .= "if(\$read->getResult()): foreach (\$read->getResult() as \$p): \$ids .= \$p['post_id'] .'||'; endforeach; else:";

            $this->SiteContent .= "\$read->ExeRead(PRE . 'snippet', 'WHERE status = 1 && post_id!= '. \$id .' ORDER BY RAND() LIMIT {$this->Incorp['post_offset_' . $id]}, {$this->Incorp['post_quant_' . $id]}');";
            $this->SiteContent .= "if(\$read->getResult()): foreach (\$read->getResult() as \$p): \$ids .= \$p['post_id'] .'||'; endforeach; endif; endif;";

            $this->SiteContent .= "\$blog = new Snippet(0, \$ids, 'ORDER BY {$this->Incorp['post_ordem_' . $id]}', {$this->Incorp['paginacao_chose_' . $id]}, '{$this->Incorp['post_template_' . $id]}', {$this->Incorp['post_col_' . $id]}, '{$this->Incorp['post_font_' . $id]}', {$this->Incorp['post_quant_' . $id]});
                  \$blog->getPosts(); ?>";

        elseif ($elemento == "post_related_fabricante"):
            //relacionados
            if (!isset($this->Incorp['post_col_' . $id])):
                $this->Incorp['post_col_' . $id] = 3;
            endif;

            $this->SiteContent .= "<?php \$ids = '';";
            $this->SiteContent .= "\$id = (isset(\$extends_posts)? \$extends_posts : \$id);";
            $this->SiteContent .= "\$fab = Check::getBanco(PRE . 'snippet', (int) \$id, 'fabricante', 'post_id');";
            $this->SiteContent .= "\$read->ExeRead(PRE . 'snippet', 'WHERE status = 1 && fabricante=\''.\$fab.'\' ORDER BY views DESC LIMIT {$this->Incorp['post_offset_' . $id]}, {$this->Incorp['post_quant_' . $id]}');";

            $this->SiteContent .= "if(\$read->getResult()): foreach (\$read->getResult() as \$p): if(\$p['post_id'] != \$id): \$ids .= \$p['post_id'] .'||'; endif; endforeach; endif;";

            $this->SiteContent .= "\$blog = new Snippet(0, \$ids, 'ORDER BY {$this->Incorp['post_ordem_' . $id]}', {$this->Incorp['paginacao_chose_' . $id]}, '{$this->Incorp['post_template_' . $id]}', {$this->Incorp['post_col_' . $id]}, '{$this->Incorp['post_font_' . $id]}', {$this->Incorp['post_quant_' . $id]});
                  \$blog->getPosts(); ?>";

        elseif ($elemento == "gallery_id"):
            //GALERIA

            if (!isset($this->Incorp['post_slide_' . $id]) || empty($this->Incorp['post_slide_' . $id])):
                $this->Incorp['post_slide_' . $id] = 1;
            endif;

            /* ########  SLIDE GALERIA  ########## */
            $this->setNewSlide($id, $this->Incorp['post_quant_' . $id]);

            $this->Slide->galleryMode();

            //INSERE CADA SLIDE
            $read = new Read();
            $read->ExeRead(PRE . "padrao", "WHERE name='gallery_number'");
            $galNumber = (int) ($read->getResult() ? $read->getResult()[0]['padrao'] : 15);

            for ($i = 0; $i < $galNumber; $i++):
                $this->Slide->setSlide("<?=\$post_gallery_{$i}?>", "<?=\$title?> {$i}");
            endfor;

            $this->Slide->setEmbed();

            $this->SiteContent .= "<div class='container {$this->Incorp['post_font_' . $id]}'>" . $this->Slide->Retorno() . "</div>";

        else:
            //OUTROS
            $this->SiteContent .= "<?= \${$elemento}; ?>";
        endif;
    }

    /*
     * Obtem um modelo de menu para aplicar á página
     */

    private function getMenu($id) {

        $read = new Read();
        $id = (int) str_replace('9999', '', $this->Incorp['incorporar_' . $id]);
        $read->ExeRead(PRE . "menu_select", "WHERE menu_id=:mi", "mi={$id}");
        if ($read->getResult()):
            $this->SiteContent .= "<header class='font-zero'><h1>" . Check::getBanco(PRE . "menu", (int) $id, "menu_title") . "</h1></header><ul class='container menu'>";
            foreach ($read->getResult() as $m):
                if ($m['menu_banco'] == 'produto'):
                    $pref = '';
                else:
                    if ($m['menu_banco'] == 'pagina'):
                        $pref = 'pagina/';
                    else:
                        $pref = $m['menu_banco'] . '/';
                    endif;
                endif;
                $read->ExeRead(PRE . $m['menu_banco'], "WHERE id=:ji", "ji={$m['menu_select']}");
                if ($read->getResult()):
                    if ($m['menu_banco'] == 'produto'):
                        $name = Check::getBanco(PRE . "post", (int) $read->getResult()[0]['extends_posts'], "urlname");
                    else:
                        $name = $read->getResult()[0][$m['menu_banco'] . '_name'];
                    endif;

                    $titulomenu = $read->getResult()[0][$m['menu_banco'] . '_title'];

                    //busca por category filhas para implementar
                    if ($m['menu_banco'] === 'category'):
                        $read->ExeRead(PRE . "category", "WHERE category_id=:cp", "cp={$m['menu_select']}");
                        if ($read->getResult()):
                            $this->SiteContent .= "<li class='fl-left pd-medium menuparent pointer menulist'>{$titulomenu} <span class='fl-right arrowmenu'><i class='shoticon shoticon-button shoticon-arrow-pure'></i></span></li>";
                            $this->SiteContent .= "<ul class='container menuchild'>";
                            foreach ($read->getResult() as $mm):
                                $this->SiteContent .= "<a href='" . HOME . "/category/{$mm['urlname']}/' class='menuparentchild' ><li class='fl-left pd-medium'>{$mm['title']}</li></a>";
                            endforeach;
                            $this->SiteContent .= "</ul>";
                        else:
                            $this->SiteContent .= "<a href='" . HOME . "/{$pref}{$name}/' class='menuparent' ><li class='fl-left pd-medium'>{$titulomenu}</li></a>";
                        endif;
                    else:
                        $this->SiteContent .= "<li class='fl-left pd-medium'><a href='" . HOME . "/{$pref}{$name}/' >{$titulomenu}</a></li>";
                    endif;
                endif;
            endforeach;
            $this->SiteContent .= "</ul>";
        endif;
    }

    /*
     * Aplica as informações do Padrao aos elementos, garantindo que não fiquem NULL
     */

    private function setInfosPadrao($id) {

        //PAGINAÇÃO, TEMPLATE, COLUNAS, FONT, ORDEM, QUANT, OFFSET
        $this->Incorp['paginacao_chose_' . $id] = (isset($this->Incorp['paginacao_chose_' . $id]) && !empty($this->Incorp['paginacao_chose_' . $id]) ? 1 : 0);
        $this->Incorp['post_template_' . $id] = ( isset($this->Incorp['post_template_' . $id]) && !empty($this->Incorp['post_template_' . $id]) ? $this->Incorp['post_template_' . $id] : 'post_blog');
        $this->Incorp['post_col_' . $id] = (int) ( isset($this->Incorp['post_col_' . $id]) && !empty($this->Incorp['post_col_' . $id]) ? $this->Incorp['post_col_' . $id] : 3);
        $this->Incorp['post_font_' . $id] = (isset($this->Incorp['post_font_' . $id]) && !empty($this->Incorp['post_font_' . $id]) ? $this->Incorp['post_font_' . $id] : "");
        $this->Incorp['post_ordem_' . $id] = (isset($this->Incorp['post_ordem_' . $id]) && !empty($this->Incorp['post_ordem_' . $id]) ? $this->Incorp['post_ordem_' . $id] : "id DESC");
        $this->Incorp['post_quant_' . $id] = (int) (isset($this->Incorp['post_quant_' . $id]) && !empty($this->Incorp['post_quant_' . $id]) ? $this->Incorp['post_quant_' . $id] : 10);
        $this->Incorp['post_offset_' . $id] = (int) (isset($this->Incorp['post_offset_' . $id]) && !empty($this->Incorp['post_offset_' . $id]) ? $this->Incorp['post_offset_' . $id] : 0);
    }

    /*
     * Obtem id de todas as sessões de uma página
     * pois o id das sessões de uma página esta vinculado através de valores inseridos na tabela padrão, name = incorporar_%
     */

    private function getId($id) {
        $ids_sessao = '';
        $read = new Read();
        $read->ExeRead(PRE . "struct", "WHERE sessao_id=:ss", "ss={$id}");
        if ($read->getResult()):
            foreach ($read->getResult() as $ta):
                $tt = 'incorporar_' . $ta['id'];
                $read->ExeRead(PRE . "padrao", "WHERE name=:l", "l={$tt}");
                if ($read->getResult() && !empty($read->getResult()[0]['padrao']) && is_numeric($read->getResult()[0]['padrao'])):
                    $ids_sessao .= '||' . $read->getResult()[0]['padrao'] . $this->getId($read->getResult()[0]['padrao']);
                endif;
            endforeach;

            return $ids_sessao;
        endif;
        return '';
    }

}
