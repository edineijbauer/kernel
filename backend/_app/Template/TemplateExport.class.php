<?php

/**
 * Respnsável por criar os arqivos de exportação do template
 * 
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class TemplateExport extends Template {

    private $Dados;

    function __construct($id, $tipo) {
        parent::__construct($id, $tipo);
        $this->EscreverExport();
    }

    public function getResult() {
        return $this->Dados;
    }

    /*
     * Escreve padrão de exportação
     * para cada informação, Página, Sessão, Padrão
     * Ele divide por [<>], separa cada row da table por |##|
     */

    private function EscreverExport() {
        if (parent::Pagina()):
            $this->Dados = '1[<>]' . $this->ruleTable(parent::getPagina()) . '[<>]';
        else:
            $this->Dados = '0[<>]';
        endif;

        if (parent::Sessao()):
            foreach (parent::getSessao() as $s):
                $this->Dados .= $this->ruleTable($s) . '|##|';
            endforeach;

            $this->Dados .= '[<>]';

            if (parent::Struct()):
                foreach (parent::getStruct() as $s):
                    $this->Dados .= $this->ruleTable($s) . '|##|';
                endforeach;

                $this->Dados .= '[<>]';

                if (parent::Padrao()):
                    foreach (parent::getPadrao() as $p):
                        $this->Dados .= $this->ruleTable($p) . '|##|';
                    endforeach;
                endif;

            endif;
        endif;
    }

    /*
     * Passa uma table por parametro
     * retorna uma string com o array convertido, separado por || cada coluna, 
     * e atribuindo valores para os campos com :=
     */

    private function ruleTable($dados) {
        foreach ($dados as $campo => $value):
            if (!isset($rule)):
                $rule = $campo . ':=' . $value;
            else:
                $rule .= "||" . $campo . ':=' . $value;
            endif;
        endforeach;

        return $rule;
    }

}
