<?php

/**
 * SessaoStructSave.class [ MODEL ADMIN ]
 * Responável por salvar a estrutura e sessões de um site!
 * 
 * @copyright (c) 2016, Edinei J. Bauer
 */
class SessaoStructSave extends SessaoSave {

    private $Padrao;
    private $Incorp;
    private $LastResolution;
    private $Temps;
    private $Data;
    private $SessaoAtual;
    private $Bloco;
    private $Slide;
    private $StructType;

    function __construct() {
        parent::__construct();
        $this->Incorp = parent::getIncorp();
        $this->Padrao = parent::getPadrao();
        $this->LastResolution = parent::getResolution();
        $this->initial();
    }

    /*
     * Obtem o tipo pai da sessão, para identificar atribuição de valores
     * Se é do tipo POST, ou TAG, ou PAGINA ...
     * $page_id = site_page_id -> encontrada em sessao
     * $id = id de uma sessao, para buscar nos padrões, quem esta incorporando esta sessao
     * return = retorna o tipo da página ao qual a sessão pertence -> PRE . site_page
     */

    private function getPageID($page_idq, $idq) {
        $page_id = (int) $page_idq;
        $id = (int) $idq;
        if ($page_id === 8):
            $read = new Read();
            $read->ExeRead(PRE . "padrao", "WHERE name LIKE 'incorporar_%' && padrao=:pd", "pd={$id}");
            if ($read->getResult()):
                $a = (int) str_replace('incorporar_', '', $read->getResult()[0]['name']);
                $read->ExeRead(PRE . "struct", "WHERE id=:si", "si={$a}");
                if ($read->getResult()):
                    $read->ExeRead(PRE . "sessoes", "WHERE id=:sei", "sei={$read->getResult()[0]['sessao_id']}");
                    if ($read->getResult()):
                        return $this->getPageID($read->getResult()[0]['site_page_id'], $read->getResult()[0]['id']);
                    endif;
                endif;
            endif;
        else:
            return $page_id;
        endif;
    }

    /*
     * Inicializa as funções desta classe
     */

    private function initial() {
        //busca todas as temps
        $read = new Read();
        $read->ExeRead(PRE . "temp");
        if ($read->getResult()):
            foreach ($read->getResult() as $tempor):
                if (!isset($this->Temps[$tempor['struct_id']])):
                    $this->Temps[$tempor['struct_id']] = $tempor;
                endif;
            endforeach;
        endif;

        $read->ExeRead(PRE . "sessoes");
        foreach ($read->getResult() as $s):

            $this->Data['site_content'] = '';

            if (isset($this->Temps)):
                if (isset($struct_container)):
                    unset($struct_container);
                endif;
                foreach ($this->Temps as $struct):
                    if ($struct['parent_struct_id'] == 0 && $struct['sessao_id'] == $s['id']):

                        if (isset($struct_container) && $struct_container != $struct['struct_container']):
                            $this->Data['site_content'] .= '<div class="container"></div>';
                            $struct_container = $struct['struct_container'];
                        endif;

                        $this->StructType = '';

                        $struct_type = $this->getPageID($s['site_page_id'], $s['id']);

                        switch ($struct_type):
                            case 2: $this->StructType = 'category';
                                break;
                            case 4: $this->StructType = 'pagina';
                                break;
                            case 5: $this->StructType = 'post';
                                break;
                            case 6: $this->StructType = 'produto';
                                break;
                            case 7: $this->StructType = 'tag';
                                break;
                            case 11: $this->StructType = 'fabricante';
                                break;
                        endswitch;

                        $struct_container = (!isset($struct_container) ? $struct['struct_container'] : $struct_container);
                        $this->Data['site_content'] .= $this->setContent($struct);

                    endif;
                endforeach;

            endif;

            $this->workContent($s);
            $this->CreateTemp();

        endforeach;
    }

    /*
     * Trabalha com os conteúdos das sessões, ou seja, finaliza colocando dentro de uma div, ou setando titles e headers
     */

    private function workContent($s) {
        if ($s['tipo_id'] == '7'):

            $this->Data['site_content'] = "<div class='container ps-relative'>" . $this->Data['site_content'] . "</div>";
        else:

            //elemento incorporado e semântico, por isso não implementa divs no início e fim, e adiciona header
            if ($s['site_page_id'] == 8 && $s['tipo_id'] != 7 && isset($this->Incorp['title_sessao_' . $s['id']]) && !empty($this->Incorp['title_sessao_' . $s['id']])):
                if ($s['tipo_id'] == 1):
                    $this->Data['site_content'] = "<h1 class='font-zero'>{$this->Incorp['title_sessao_' . $s['id']]}</h1>" . $this->Data['site_content'];
                else:
                    $this->Data['site_content'] = "<header class='font-zero'><h1>{$this->Incorp['title_sessao_' . $s['id']]}</h1></header>" . $this->Data['site_content'];
                endif;
            endif;
        endif;

        $this->Data['site_content'] .= "<div class='clear'></div>";
        $this->Data['sessoes_id'] = $s['id'];
    }

    /*
     * Cria a temp no banco de dados, ou atualiza
     */

    private function CreateTemp() {
        $read = new Read();
        $read->ExeRead(PRE . "site_content", "WHERE sessoes_id=:ji", "ji={$this->Data['sessoes_id']}");
        if ($read->getResult()):
            $up = new Update();
            $up->ExeUpdate(PRE . "site_content", $this->Data, "WHERE sessoes_id=:ji", "ji={$this->Data['sessoes_id']}");
        else:
            $create = new Create();
            $create->ExeCreate(PRE . "site_content", $this->Data);
        endif;
    }

    /*
     * Cria um modelo padrão de slide para a aplicação no setContent()
     */

    private function setNewSlide($id, $quant = NULL) {
        if (isset($quant) && !empty($quant) && $quant > 0):
            $this->Slide = new Slide($quant);
        else:
            $this->Slide = new Slide();
        endif;

        //Slide
        if (isset($this->Incorp['post_slide_' . $id]) && !empty($this->Incorp['post_slide_' . $id])):
            $this->Slide->setModel($this->Incorp['post_slide_' . $id]);
        else:
            $this->Slide->setModel(0);
        endif;

        //SETA PERSONALIZADA
        if (isset($this->Incorp['post_seta_slide_' . $id]) && !empty($this->Incorp['post_seta_slide_' . $id])):
            $this->Slide->setaMode($this->Incorp['post_seta_slide_' . $id]);
        endif;

        //SLIDE OU GALERIA
        if (isset($this->Incorp['post_slide_gallery_' . $id]) && $this->Incorp['post_slide_gallery_' . $id] == '2'):
            $this->Slide->galleryMode();
        endif;

        //PAGINAÇÃO OU NÃO
        if (!isset($this->Incorp['paginacao_' . $id]) || empty($this->Incorp['paginacao_' . $id])):
            $this->Slide->noPagination();
        else:
            if (isset($this->Incorp['paginacao_style_' . $id]) && !empty($this->Incorp['paginacao_style_' . $id])):
                $this->Slide->setaPageStyle($this->Incorp['paginacao_style_' . $id]);
            endif;

            if (isset($this->Incorp['paginacao_position_' . $id]) && !empty($this->Incorp['paginacao_position_' . $id])):
                $this->Slide->setaPagePosition($this->Incorp['paginacao_position_' . $id]);
            endif;
        endif;

        //SLIDE AUTO
        if (isset($this->Incorp['post_slide_auto_' . $id]) && !empty($this->Incorp['post_slide_auto_' . $id])):
            $this->Slide->galleryAuto();
        endif;
    }

    private function getPaiSessao($pai) {
        //check e verifica quem é o pai deste elemento
        $read = new Read();
        $read->ExeRead(PRE . "sessoes", "WHERE id=:ii", "ii={$pai}");
        if ($read->getResult()):
            if ($read->getResult()[0]['site_page_id'] == 8):
                $read->ExeRead(PRE . "padrao", "WHERE name LIKE 'incorporar_%' && padrao=:pp", "pp={$pai}");
                if ($read->getResult()):
                    $a = (int) str_replace('incorporar_', '', $read->getResult()[0]['name']);
                    $read->ExeRead(PRE . "struct", "WHERE id=:ll", "ll={$a}");
                    if ($read->getResult()):
                        return $this->getPaiSessao($read->getResult()[0]['sessao_id']);
                    endif;
                endif;
            else:
                return (int) $read->getResult()[0]['site_page_id'];
            endif;
        else:
            return false;
        endif;
    }

    /*
     * trabalha com as sessões para separa-las em blocos e buscar informações para tratar das margins
     */

    private function setContent($struct, $div = NULL) {
        $read = new Read();

        $id = $struct['struct_id'];
        $siteContent = '';
        $attr = '';

        $psposition = 'fl-left';
        $optstyle = "";

        if (( isset($this->Incorp['incorporar_' . $id]) && !empty($this->Incorp['incorporar_' . $id]) ) && ( (!empty($this->StructType) && !is_int($this->Incorp['incorporar_' . $id])) || ($this->Incorp['incorporar_' . $id] > -11 || ($this->Incorp['incorporar_' . $id] == '-8' && isset($this->Incorp['post_slide_gallery_' . $id]) && !empty($this->Incorp['post_slide_gallery_' . $id])) ) )):
            $psposition = 'ps-relative fl-left';
            $optstyle = "height:auto;";
        endif;

        if (isset($this->Incorp['incorporar_' . $id])):
            if (!is_numeric($this->Incorp['incorporar_' . $id])):
                if (isset($this->Incorp['titleh1_' . $id]) && !empty($this->Incorp['titleh1_' . $id])):
                    $stipo = "header class='font-size10'><" . $this->Incorp['titleh1_' . $id];
                    $stipo_end = $this->Incorp['titleh1_' . $id] . "></header";
                else:
                    $stipo = 'div';
                    $stipo_end = 'div';
                endif;
            elseif ($this->Incorp['incorporar_' . $id] > 99990):
                if (isset($stipo) && $stipo !== "nav"):
                    $stipo = "nav";
                else:
                    $stipo = 'div';
                endif;
                $stipo_end = $stipo;
            elseif ($this->Incorp['incorporar_' . $id] > 0):
                $read->ExeRead(PRE . "sessoes", "WHERE id=:sii", "sii={$this->Incorp['incorporar_' . $id]}");
                if ($read->getResult()):
                    $attr = $read->getResult()[0]['sessao_atributos'];
                    $stipo = strtolower(Check::getBanco(PRE . "tipo", (int) $read->getResult()[0]['tipo_id'], 'tipo_title'));
                else:
                    $stipo = 'div';
                endif;

                if (isset($stipo_end) && $stipo === $stipo_end):
                    $stipo = 'div';
                endif;
                $stipo_end = $stipo;
            else:
                $stipo = 'div';
                $stipo_end = $stipo;
            endif;
        else:
            $stipo = 'div';
            $stipo_end = $stipo;
        endif;

        if (isset($this->Padrao[$this->LastResolution]['position_' . $id]) && $this->Padrao[$this->LastResolution]['position_' . $id] == 'ps-fixed'):
            $psposition = 'ps-fixed';
            $optstyle = "left:0;top:0;";
        endif;

        $classe = 'bloco_' . $id . " {$psposition}";

        //adiciona title para o bloco
        if (isset($this->Incorp['title_' . $id]) && !empty($this->Incorp['title_' . $id])):
            $title = " title='" . $this->Incorp['title_' . $id] . "'";
        else:
            $title = '';
        endif;

        //Adiciona links para o bloco
        if (isset($this->Incorp['a_' . $id]) && !empty($this->Incorp['a_' . $id])):
            $target = '';
            $nofollow = '';
            if (isset($this->Incorp['targetb_' . $id]) && !empty($this->Incorp['targetb_' . $id])):
                $target = "target='_blank'";
            endif;
            if (isset($this->Incorp['nfollow_' . $id]) && !empty($this->Incorp['nfollow_' . $id])):
                $nofollow = "rel='nofollow'";
            endif;
            $siteContent .= "<a href='{$this->Incorp['a_' . $id]}' {$target} {$nofollow} >";
        endif;

        if (!empty($struct['schemaorg'])):
        //$attr = $this->TrataScrema($struct['schemaorg']);
        endif;

        //cria o inicio do bloco
        $struu = Check::getBanco(PRE . "struct", (int) $id, "struct_item");
        $siteContent .= "<{$stipo} class='{$classe}'{$title} rel='{$struu}' style='{$optstyle}' id='{$id}' {$attr}>";

        //Se tiver _content no bloco, adiciona
        if (isset($this->Incorp['content_' . $id]) && !empty($this->Incorp['content_' . $id])):
            $siteContent .= $this->Incorp['content_' . $id];
        endif;

        if (isset($this->Incorp['incorporar_' . $id]) && !empty($this->Incorp['incorporar_' . $id])):

            $this->setIncorpPadrao($id);

            if (!is_numeric($this->Incorp['incorporar_' . $id])):
                //tem uma atribuição para este elemento, aplica
                unset($aux_int);

                $elemento = $this->Incorp['incorporar_' . $id];

                if (preg_match('/cover/i', $elemento)):
                    //COVER
                    $siteContent .= "<img src='#" . $elemento . "#' />";

                elseif ($elemento == "post_social"):
                    //social
                    $siteContent .= file_get_contents('../_cdn/Social/social.inc.php');

                elseif ($elemento == "post_related"):
                    //relacionados
                    if (!isset($this->Incorp['post_col_' . $id])):
                        $this->Incorp['post_col_' . $id] = 3;
                    endif;

                    $blog = new Blog(PRE . "post", "getPatternsformPost", 0, $this->Incorp['post_template_' . $id], $this->Incorp['post_col_' . $id], $this->Incorp['post_font_' . $id], $this->Incorp['post_col_' . $id], 0);
                    $siteContent .= $blog->getPosts();

                elseif ($elemento == "gallery_id"):
                    //GALERIA

                    if (!isset($this->Incorp['post_slide_' . $id]) || empty($this->Incorp['post_slide_' . $id])):
                        $this->Incorp['post_slide_' . $id] = 1;
                    endif;

                    /* ########  SLIDE GALERIA  ########## */
                    $this->setNewSlide($id, $this->Incorp['post_quant_' . $id]);

                    $this->Slide->galleryMode();

                    //INSERE CADA SLIDE
                    $read->ExeRead(PRE . "padrao", "WHERE name='gallery_number'");
                    $galNumber = (int) ($read->getResult() ? $read->getResult()[0]['padrao'] : 15);

                    for ($i = 0; $i < $galNumber; $i++):
                        $this->Slide->setSlide("#post_gallery_{$i}#", "#title# {$i}");
                    endfor;

                    $this->Slide->setEmbed();

                    $siteContent .= "<div class='container {$this->Incorp['post_font_' . $id]}'>" . $this->Slide->Retorno() . "</div>";

                else:
                    //OUTROS
                    $siteContent .= "#" . $elemento . "#";
                endif;

            else:
                //Tem um elemento incorporado, verifica que tipo de incorporação é e aplica

                if ($this->Incorp['incorporar_' . $id] > 99990):

                    //#######   Incorpora MENU   #######
                    //==================================

                    $id = (int) str_replace('9999', '', $this->Incorp['incorporar_' . $id]);
                    $read->ExeRead(PRE . "menu_select", "WHERE menu_id=:mi", "mi={$id}");
                    if ($read->getResult()):
                        $siteContent .= "<header class='font-zero'><h1>" . Check::getBanco(PRE . "menu", (int) $id, "menu_title") . "</h1></header><ul class='container menu'>";
                        foreach ($read->getResult() as $m):
                            if ($m['menu_banco'] == 'produto'):
                                $pref = '';
                            else:
                                if ($m['menu_banco'] == 'pagina'):
                                    $pref = 'pagina/';
                                else:
                                    $pref = $m['menu_banco'] . '/';
                                endif;
                            endif;
                            $read->ExeRead(PRE . $m['menu_banco'], "WHERE id=:ji", "ji={$m['menu_select']}");
                            if ($read->getResult()):
                                if ($m['menu_banco'] == 'produto'):
                                    $name = Check::getBanco(PRE . "post", (int) $read->getResult()[0]['extends_posts'], "urlname");
                                else:
                                    $name = $read->getResult()[0][$m['menu_banco'] . '_name'];
                                endif;

                                $titulomenu = $read->getResult()[0][$m['menu_banco'] . '_title'];

                                //busca por category filhas para implementar
                                if ($m['menu_banco'] === 'category'):
                                    $read->ExeRead(PRE . "category", "WHERE category_id=:cp", "cp={$m['menu_select']}");
                                    if ($read->getResult()):
                                        $siteContent .= "<li class='fl-left pd-medium menuparent pointer' onclick='opensubmenu({$m['menu_select']})'>{$titulomenu} <span class='fl-right arrowmenu'><i class='shoticon shoticon-button shoticon-arrowblack-pure'></i></span></li>";
                                        $siteContent .= "<ul class='container ds-none menuchild' id='menu-child-{$m['menu_select']}'>";
                                        $siteContent .= "<li class='fl-left pd-medium marked font-bold menuback pointer' onclick='comebackmenu({$m['menu_select']});' >< {$titulomenu}</li>";
                                        foreach ($read->getResult() as $mm):
                                            $siteContent .= "<a href='" . HOME . "/category/{$mm['urlname']}/' ><li class='fl-left pd-medium'>{$mm['title']}</li></a>";
                                        endforeach;
                                        $siteContent .= "</ul>";
                                    else:
                                        $siteContent .= "<a href='" . HOME . "/{$pref}{$name}/' class='menuparent' ><li class='fl-left pd-medium'>{$titulomenu}</li></a>";
                                    endif;
                                else:
                                    $siteContent .= "<li class='fl-left pd-medium'><a href='" . HOME . "/{$pref}{$name}/' >{$titulomenu}</a></li>";
                                endif;
                            endif;
                        endforeach;
                        $siteContent .= "</ul>";
                    endif;


                elseif ($this->Incorp['incorporar_' . $id] < 0 && $this->Incorp['incorporar_' . $id] > -8):

                    //#######   lista de PRODUTOS ou lista de POSTS
                    //=============================================

                    $idp = $this->getPaiSessao($struct['sessao_id']);

                    //se for tag, categoria ou fabricante então seta informações dinamicas
                    if ($idp && ($idp === 2 || $idp === 12 || $idp === 13 || $idp === 3 || $idp === 11)):

                        if ($this->Incorp['post_slide_gallery_' . $id] == 3):
                            $tipo = 'blog';
                            $vbanco = "post";
                        else:
                            $tipo = 'slide';
                            $vbanco = "post";
                        endif;

                        if ($this->Incorp['incorporar_' . $id] == '-4'):
                            $vbanco = "fabricante";
                        elseif ($this->Incorp['incorporar_' . $id] == '-5'):
                            $vbanco = "category";
                        elseif ($this->Incorp['incorporar_' . $id] == '-6'):
                            $vbanco = "tag";
                        elseif ($this->Incorp['incorporar_' . $id] == '-7'):
                            $vbanco = "colecao";
                        endif;


                        $siteContent .= "#{$tipo}_{$id}#";

                        //cadastra EXE
                        $Exec['site_page_id'] = $idp;
                        $Exec['replacement'] = "{$tipo}_{$id}";
                        $tipob = ($this->Incorp['incorporar_' . $id] == -1 ? 0 : 1);
                        $Exec['comando'] = "{$tipo}," . PRE . "{$vbanco},{$tipob},{$this->Incorp['post_ordem_' . $id]},{$this->Incorp['paginacao_' . $id]},{$this->Incorp['post_template_' . $id]},{$this->Incorp['post_col_' . $id]},{$this->Incorp['post_font_' . $id]},{$this->Incorp['post_quant_' . $id]},{$this->Incorp['post_offset_' . $id]}";

                        $read->ExeRead(PRE . "exec", "WHERE replacement=:rr && site_page_id=:spi", "rr={$Exec['replacement']}&spi={$Exec['site_page_id']}");
                        if ($read->getResult()):
                            $up = new Update();
                            $up->ExeUpdate(PRE . "exec", $Exec, "WHERE replacement=:rr && site_page_id=:spi", "rr={$Exec['replacement']}&spi={$Exec['site_page_id']}");
                        else:
                            $create = new Create();
                            $create->ExeCreate(PRE . "exec", $Exec);
                        endif;
                    else:

                        //#######   Incorpora POSTS e CONTEÚDOS   #######
                        //===============================================
                        //informações da tabela
                        if ($this->Incorp['incorporar_' . $id] == '-3'):
                            $banco = PRE . 'tag';
                            $title = "tag";
                        elseif ($this->Incorp['incorporar_' . $id] == '-5'):
                            $banco = PRE . 'version_control';
                            $title = "versao";
                        elseif ($this->Incorp['incorporar_' . $id] == '-2' && (!isset($this->Incorp['post_cat_' . $id]) || $this->Incorp['post_cat_' . $id] < 0)):
                            $banco = PRE . 'produto';
                            $title = "produto";
                        else:
                            $banco = PRE . "post";
                            $title = "post";
                        endif;

                        if (isset($this->Incorp['post_chose_' . $id]) && !empty($this->Incorp['post_chose_' . $id])):
                            /*
                             * É um post escolhido a dedo
                             */

                            if ($this->Incorp['incorporar_' . $id] == '-4'):
                                //galeria
                                $read->ExeRead(PRE . "gallery_post", "WHERE post_id=:pp", "pp={$this->Incorp['post_chose_' . $id]}");
                                if ($read->getResult()):

                                    $this->setNewSlide($id);

                                    foreach ($read->getResult() as $ga):
                                        $read->ExeRead(PRE . "gallery", "WHERE id=:hhi", "hhi={$ga['gallery_id']}");
                                        if ($read->getResult()):
                                            $this->Slide->setSlide('uploads/' . $read->getResult()[0]["cover"], $read->getResult()[0]["title"]);
                                        endif;
                                    endforeach;

                                    //retorna slide já montado
                                    $siteContent .= $this->Slide->Retorno();

                                endif;
                            else:
                                $blog = new Blog($banco, "WHERE id='{$this->Incorp['post_chose_' . $id]}' ORDER BY id DESC", 0, $this->Incorp['post_template_' . $id], $this->Incorp['post_col_' . $id], $this->Incorp['post_font_' . $id], 1, 0);
                                $siteContent .= $blog->getPosts();
                            endif;
                        else:

                            if ($this->Incorp['post_slide_gallery_' . $id] == 3):
                                $tipo = 'blog';
                                $vbanco = "post";
                            else:
                                $tipo = 'slide';
                                $vbanco = "post";
                            endif;

                            if ($title == "tag"):
                                $vbanco = "colecao";
                            endif;

                            if ($this->Incorp['incorporar_' . $id] == '-4'):
                                $vbanco = "fabricante";
                            elseif ($this->Incorp['incorporar_' . $id] == '-5'):
                                $vbanco = "category";
                            elseif ($this->Incorp['incorporar_' . $id] == '-6'):
                                $vbanco = "tag";
                            elseif ($this->Incorp['incorporar_' . $id] == '-7'):
                                $vbanco = "colecao";
                            endif;

                            $siteContent .= "#{$tipo}_{$id}#";

                            if ($idp):
                                //cadastra EXE
                                $Exec['site_page_id'] = $idp;
                                $Exec['replacement'] = "{$tipo}_{$id}";
                                $tipob = ($this->Incorp['incorporar_' . $id] == -1 ? 0 : 1);
                                $Exec['comando'] = "{$tipo}," . PRE . "{$vbanco},{$tipob},{$this->Incorp['post_ordem_' . $id]},{$this->Incorp['paginacao_' . $id]},{$this->Incorp['post_template_' . $id]},{$this->Incorp['post_col_' . $id]},{$this->Incorp['post_font_' . $id]},{$this->Incorp['post_quant_' . $id]},{$this->Incorp['post_offset_' . $id]}";
                                $read->ExeRead(PRE . "exec", "WHERE replacement=:rr && site_page_id=:spi", "rr={$Exec['replacement']}&spi={$Exec['site_page_id']}");
                                if ($read->getResult()):
                                    $up = new Update();
                                    $up->ExeUpdate(PRE . "exec", $Exec, "WHERE replacement=:rr && site_page_id=:spi", "rr={$Exec['replacement']}&spi={$Exec['site_page_id']}");
                                else:
                                    $create = new Create();
                                    $create->ExeCreate(PRE . "exec", $Exec);
                                endif;
                            endif;

                        endif;

                    endif;

                else:

                    //INCORPORA SESSÃO
                    $siteContent .= "@#" . $this->Incorp['incorporar_' . $id] . "#@";
                endif;

            endif;

        else:

            //#######   NÃO TEM Incorporação   #######
            //========================================
            //Verifica se tem elementos dentro deste para considerar um bloco

            foreach ($this->Temps as $t):
                if ($t['struct_id'] != $struct['struct_id'] && $t['parent_struct_id'] == $struct['struct_id'] && $t['parent_struct_id'] > 0):
                    $siteContent .= $this->setContent($t, 1);
                endif;
            endforeach;
        endif;

        $siteContent .= "</{$stipo_end}>";

        if (isset($this->Incorp['a_' . $id])):
            $siteContent .= "</a>";
        endif;

        return $siteContent;
    }

    /*
     * Aplica as informações do Padrao aos elementos, garantindo que não fiquem NULL
     */

    private function setIncorpPadrao($id) {

        //PAGINAÇÃO, TEMPLATE, COLUNAS, FONT, ORDEM, QUANT, OFFSET
        $this->Incorp['paginacao_' . $id] = (isset($this->Incorp['paginacao_' . $id]) && !empty($this->Incorp['paginacao_' . $id]) ? 1 : 0);
        $this->Incorp['post_template_' . $id] = ( isset($this->Incorp['post_template_' . $id]) && !empty($this->Incorp['post_template_' . $id]) ? $this->Incorp['post_template_' . $id] : 'post_blog');
        $this->Incorp['post_col_' . $id] = (int) ( isset($this->Incorp['post_col_' . $id]) && !empty($this->Incorp['post_col_' . $id]) ? $this->Incorp['post_col_' . $id] : 3);
        $this->Incorp['post_font_' . $id] = (isset($this->Incorp['post_font_' . $id]) && !empty($this->Incorp['post_font_' . $id]) ? $this->Incorp['post_font_' . $id] : "");
        $this->Incorp['post_ordem_' . $id] = (isset($this->Incorp['post_ordem_' . $id]) && !empty($this->Incorp['post_ordem_' . $id]) ? $this->Incorp['post_ordem_' . $id] : "id DESC");
        $this->Incorp['post_quant_' . $id] = (int) (isset($this->Incorp['post_quant_' . $id]) && !empty($this->Incorp['post_quant_' . $id]) ? $this->Incorp['post_quant_' . $id] : 10);
        $this->Incorp['post_offset_' . $id] = (int) (isset($this->Incorp['post_offset_' . $id]) && !empty($this->Incorp['post_offset_' . $id]) ? $this->Incorp['post_offset_' . $id] : 0);
    }

    private function TrataScrema($struct) {
        switch ($struct):
            case 'title' : return $this->SchemaTitle();
                break;
            case '_content' : return $this->SchemaContent();
                break;
            case 'date' : return $this->SchemaDate();
                break;
            case 'gallery_id' : return $this->SchemaCover();
                break;
            case 'user_id' : return $this->SchemaAutor();
                break;
            case 'gallery_id' : return $this->SchemaCover();
                break;
            case 'gallery_id' : return $this->SchemaCover();
                break;
        endswitch;
    }

    private function SchemaTitle() {
        $tipo = "BlogPosting";
        if ($tipo === 'BlogPosting'):
            return "itemprop='name'";

        elseif ($tipo === 'Article' || $tipo === 'NewsArticle'):
            return "itemprop='headline'";

        endif;
    }

    private function SchemaContent() {
        $tipo = "BlogPosting";
        if ($tipo === 'BlogPosting'):
            return "itemprop='articleBody'";

        elseif ($tipo === 'Article' || $tipo === 'NewsArticle'):
            return "itemprop='description'";

        endif;
    }

    private function SchemaDate() {
        $tipo = "BlogPosting";
        if ($tipo === 'BlogPosting'):
            return "itemprop='name'";

        elseif ($tipo === 'Article' || $tipo === 'NewsArticle'):
            return "itemprop='headline'";

        endif;
    }

    private function SchemaCover() {
        $tipo = "BlogPosting";
        if ($tipo === 'BlogPosting'):
            return "itemprop='name'";

        elseif ($tipo === 'Article' || $tipo === 'NewsArticle'):
            return "itemprop='headline'";

        endif;
    }

}
