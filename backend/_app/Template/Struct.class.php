<?php

/**
 * Struct [ MODEL ]
 * Classe para Estruturar o post
 * 
 * @copyright (c) 2015, Edinei J. Bauer NENA PRO
 */
class Struct {
	private $Images;
	private $Titles;
	private $Contents;
	private $Modelo=0;
	private $Limite;
	private $Atual=1;
	
	function __construct($e=NULL) {
        $this->Limite = ($e? $e : 5);
    }
	
	public function setSlide($img, $title = NULL, $desc = NULL){
		if($this->Atual <= $this->Limite):
			$this->Images[] = ($img? $img : "");
			$this->Titles[] = ($title? $title : "");
			$this->Contents[] = ($desc? $desc : "");
			$this->Atual++;
		endif;
	}
	
	public function Show(){
		echo '<link rel="stylesheet" href="'.CDN.'Slide/Slide_Style_' . $this->Modelo . '.css"/>';
		
		echo '<div class="ps-relative container"><section class="container slide ps-relative">
				<header class="font-zero"><h1>Slide Show '.SITENAME.'</h1></header>
            <div class="slide_nav container">
                <div class="slide_nav_item b radius-circle font-size15 font-bold bg-white pointer fl-left z-plus hovershadow inhover"><</div>
                <div class="slide_nav_item g radius-circle font-size15 font-bold bg-white pointer fl-right z-plus hovershadow inhover">></div>
            </div>';
			
			if($this->Atual <= $this->Limite):
				$this->Limite = $this->Atual - 1;
			endif;
			
            for ($i = 0; $i < $this->Limite; $i++):
                $slide = str_pad($i, 2, 0, STR_PAD_LEFT);
                $first = ($i == 0 ? ' first' : '');
                $active = ($i == 0 ? ' active' : '');
				$pager[$i] = "<span class='pointer radius-circle inhover {$active}' id='{$i}'></span>";

                echo '<article class="slide_item animated'. $first .'">
                    <img src="'.$this->Images[$i].'" alt="[SLIDE '.$this->Titles[$i].']" title="Slide '.$this->Titles[$i].' '.SITENAME.'">
                    <header class="slide_item_desc transition-easy container pd-big">
                        <h1>'.$this->Titles[$i].'</h1>
                        <p class="tagline">'.$this->Contents[$i].'</p>
                    </header>
                </article>';
            endfor;
			
        echo '<div class="slide_pager">' . implode(' ' , $pager) . '</div></section>';
		
		switch($this->Modelo):
			case 0: echo '<span class="container shadow-bottom"></span><script src="'.CDN.'Slide/Slide_Script_0.js" async></script>'; break;
			case 1: echo '<div class="slide_cover" style="background-image:url(' . $this->Images[0] . ')"><img src="' . $this->Images[0] . '" id="slide_cover_img" /></div><div class="layer"></div><script src="'.CDN.'Slide/Slide_Script_1.js" async></script>'; break;
			case 2: echo '<span class="container shadow-bottom"></span><script src="'.CDN.'Slide/Slide_Script_2.js" async></script>'; break;
			default: echo '<span class="container shadow-bottom"></span><script src="'.CDN.'Slide/Slide_Script_0.js" async></script>';
		endswitch;
		
		echo '</div>';
	}
	
	public function setModel($e){
		$this->Modelo = $e;
	}
	
}
