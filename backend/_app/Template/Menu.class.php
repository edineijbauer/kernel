<?php

/**
 * Menu [ MODEL ]
 * Classe de apoio para o modelo Menu. Cria Menus responsivos e distintos
 * 
 * @copyright (c) 2015, Edinei J. Bauer NENA PRO
 */
class Menu {

    private $Menu;
    private $Title = "Menu de Navegação";
    private $Nav = "li-horizontal";
    private $Modelo;
    private $Position;
    private $Style;

    public function setMenu($nome, $link, $icone = NULL) {
        $this->Menu['title'][] = ($nome ? $nome : "");
        $this->Menu['link'][] = ($link ? $link : "");
        $this->Menu['Icones'][] = ($icone && !empty($icone) ? $icone : "");
    }

    public function Show($Mode = NULL) {
        $this->Modelo = ($Mode ? $Mode : 0);
        switch ($this->Modelo):
            case 0:
                $this->Style["nav"] = "";
                $this->Style["ul"] = "";
                $this->Style["li"] = "";
                $this->Style["a"] = "fl-left";
                $this->Style["shoticon"] = "";
                $this->Style["header"] = "";
                break;
            default:
                if (isset($this->Style['nav'])): $this->Style["nav"] .= "";
                else: $this->Style["nav"] = "";
                endif;
                if (isset($this->Style['ul'])): $this->Style["ul"] .= "fl-left";
                else: $this->Style["ul"] = "fl-left";
                endif;
                if (isset($this->Style['li'])): $this->Style["li"] .= "fl-left hover s11box-0";
                else: $this->Style["li"] = "fl-left hover";
                endif;
                if (isset($this->Style['a'])): $this->Style["a"] .= "pd-small fl-left";
                else: $this->Style["a"] = "pd-small fl-left";
                endif;
                if (isset($this->Style['shoticon'])): $this->Style["shoticon"] .= "font-size15 shoticon-padding";
                else: $this->Style["shoticon"] = "font-size15 shoticon-padding";
                endif;
                if (isset($this->Style['header'])): $this->Style["header"] .= "font-zero";
                else: $this->Style["header"] = "font-zero";
            endif;
        endswitch;

        if (!isset($this->Position['tipo'])): 
            $this->setPosition("initial");
        endif;

        echo "<nav class='{$this->Style['nav']} {$this->Position['tipo']} {$this->Position['height']} {$this->Position['width']}'>"
        . "<header class='{$this->Style['header']}'><h1>{$this->Title}</h1></header>"
        . "<ul class='{$this->Style['ul']} {$this->Nav}'>";

        for ($i = 0; $i < count($this->Menu['title']); $i++):

            if ($this->Menu['Icones'][$i]):
                echo "<li class='{$this->Style['li']}'><a class='{$this->Style['a']}' href='" . HOME . "/{$this->Menu['link'][$i]}'><i class='shoticon shoticon-{$this->Menu['Icones'][$i]} {$this->Style['shoticon']}'></i>{$this->Menu['title'][$i]}</a></li>";
            else:
                echo "<li class='{$this->Style['li']}'><a class='{$this->Style['a']}' href='" . HOME . "/{$this->Menu['link'][$i]}'>{$this->Menu['title'][$i]}</a></li>";
            endif;
        endfor;

        echo '</ul>'
        . '</nav>';
    }

    public function setModel($e) {
        $this->Modelo = $e;
    }

    public function setNav($e) {
        $this->Nav = $e;
    }

    public function setTitle($e) {
        $this->Title = $e;
    }

    public function setStyle($m, $s) {
        if (isset($this->Style[(String) $m])): $this->Style[(String) $m] .= " " . $s;
        else: $this->Style[(String) $m] = $s . " ";
        endif;
    }

    public function setPosition($tipo, $height = NULL, $width = NULL) {
        $this->Position['tipo'] = 'ps-' . $tipo;
        if (isset($height) && $height == "center"): $height = "height-center";
        endif;
        $this->Position['height'] = ($height ? $height : "top");
        if (isset($width) && $width == "center"): $width = "width-center";
        endif;
        $this->Position['width'] = ($width ? $width : "left");
    }

    public function Inverter() {
        $this->Menu['title'] = array_reverse($this->Menu['title']);
        $this->Menu['link'] = array_reverse($this->Menu['link']);
        $this->Menu['Icones'] = array_reverse($this->Menu['Icones']);
    }

}
