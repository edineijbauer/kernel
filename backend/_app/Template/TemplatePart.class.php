<?php

/**
 * AdminPost.class [ MODEL ADMIN ]
 * Respnsável por crair novos apps e post para o index!
 * 
 * @copyright (c) 2014, Edinei J. Bauer NENA PRO
 */
class TemplatePart {

    private $Id;
    private $Title;
    private $Data;
    private $DBSA;
    private $Banco;
    private $Result;
    private $NotAllow;
    private $Recursivo;

    function __construct($banco, $id = NULL, $rec = NULL) {
        $this->Id = (int) ($id ? $id : 0);
        $this->Recursivo = ( $rec ? false : true);
        $this->DBSA = DBSA;
        $this->Title = $banco;
        $this->Banco = PRE . $banco;
    }

    public function getSelect() {
        $this->notAllow();
        $this->InitialSelect();
        $this->SlavesDatas();
    }

    public function getSeo() {
        if ($this->Id > 0):
            $this->notAllowSeo();
            $this->InitialSeo();
            $this->SlavesDatasRecover();
        endif;
    }

    public function getResult() {
        return $this->Result;
    }

    public function getData() {
        return $this->Data;
    }

    private function notAllow() {
        switch ($this->Title):
            case "post" :
                $this->NotAllow = ['', 'id', 'status', 'urlname'];
                break;
            case "pagina" :
                $this->NotAllow = ['', 'id', 'status', 'urlname'];
                break;
            case "produto" :
                $this->NotAllow = ['', 'id', 'produto_pda'];
                break;
            case "user" :
                $this->NotAllow = ['', 'id', 'email', 'password', 'level', 'urlname', 'status', 'code'];
                break;
            case "fabricante":
                $this->NotAllow = ['', 'id', 'fabricante_date'];
            case "category":
                $this->NotAllow = ['', 'id', 'urlname', 'date', 'category_id'];
            case "tag":
                $this->NotAllow = ['', 'id', 'urlname', 'date'];
            case "colecao":
                $this->NotAllow = ['', 'id', 'urlname', 'date'];
            default:
                $this->NotAllow[] = '';
        endswitch;
    }

    private function trataInfo($name) {
        $arrayOrigem = ['title', '_content', 'gallery_id', 'views', 'post_hidden','title', '_content', 'tag_cover', 'post_hidden', 'views', 'title', '_content', 'gallery_id', 'views', 'post_hidden', 'title', '_content', 'date', 'views', 'fabricante_name', 'fabricante_title', 'fabricante_cover', 'title', 'gallery_id', 'views', '_content', 'date', 'gallery_id', 'title', 'produto_title', 'produto_tamanho', 'produto_preco', 'produto_requer', 'produto_faixa_etaria', 'produto_limite', 'produto_views', 'produto_play'];
        $arrayDestino = ['Titulo', 'Descrição', 'Image', 'Visualizações', 'n° de post', 'Título', 'Descrição', 'Image', 'num de post', 'visualizações', 'Título', 'Descrição', 'image', 'visualizações', 'num de post', 'Titulo', 'Conteúdo', 'data', 'Visualizações', 'Link do Fabricante', 'Nome do Fabricante', 'Imagem do Fabricante', 'Titulo', 'Imagem', 'Visualizações', 'Descrição', 'Data de pub.', 'Autor Imagem', 'Autor Nome', 'Nome do Produto', 'Tamanho', 'Preço', 'Requer', 'Faixa Etária', 'Lim. Disponível', 'n° Ações', 'Play link'];

        return str_replace($arrayOrigem, $arrayDestino, $name);
    }

    private function InitialSelect() {
        $readI = new ReadInfo();
        $readI->ExeRead("COLUMNS", "WHERE TABLE_SCHEMA = :nb && TABLE_NAME = :nt", "nb={$this->DBSA}&nt={$this->Banco}");
        if ($readI->getResult()):
            $postpart[''] = '';
            foreach ($readI->getResult() as $gg):
                if (!in_array($gg['COLUMN_NAME'], $this->NotAllow)):
                    $this->setInfo($gg['COLUMN_NAME']);
                endif;
            endforeach;
        endif;
    }

    private function setInfo($name) {
        if (preg_match('/(_id|extends_)/i', $name)):
            $b = str_replace(array('_id', 'extends_', 'post', 'postss'), array('', '', 'post', 'post'), $name);
            $comp = new TemplatePart($b);
            $comp->getSelect();
            $narray = $comp->getData();
            if (is_array($narray)):
                foreach ($narray as $r => $rr):
                    $this->Data[$r] = $rr;
                endforeach;
            endif;
        else:
            $this->Data[$name] = ucwords($this->trataInfo($name));
        endif;
    }

    private function setManual() {
        if ($this->Title === 'post' || $this->Title === 'produto'):
            $this->Data['post_social'] = "Social";
        endif;
    }

    private function SlavesDatas() {
        /*$read = new Read();
        $read->ExeRead(PRE . "slave", "WHERE banco=:b", "b={$this->Title}");
        if ($read->getResult()):
            foreach ($read->getResult() as $slave):
                $this->setSlave($slave['slave']);
            endforeach;
        endif;*/

        $this->setManual();

        $this->Result = "<select class='pd-small font-size11 font-light incContent' id='{$this->Title}_part'>"
                . "<option class='font-light' id='{$this->Title}_part_'></option>";

        $option = Check::getBanco(PRE . "padrao", "incorporar_{$this->Id}", "padrao", "name");
                
        if (isset($this->Data)):
            foreach ($this->Data as $pp => $name):
                if($option === $pp):
                    $this->Result .= "<option class='font-light' selected id='{$this->Title}_part_{$pp}' value='{$pp}'>{$name}</option>";
                else:
                    $this->Result .= "<option class='font-light' id='{$this->Title}_part_{$pp}' value='{$pp}'>{$name}</option>";
                endif;
            endforeach;
        endif;

        $this->Result .= "</select>";
    }

    private function setSlave($slave) {
        switch ($slave):
            case 'category_post':
                $this->Data['category_id'] = 'Categoria';
                $this->Data['post_related'] = 'Relacionados';
                $this->Data['post_related_fabricante'] = 'Relacionados ao Fabricante';
                break;
            case 'tag_post':
                $this->Data['tag_id'] = 'Tags';
                $this->Data['post_related'] = 'Relacionados';
                $this->Data['post_related_fabricante'] = 'Relacionados ao Fabricante';
                break;
            case 'gallery_post':
                $this->Data['gallery_id'] = 'Galeria';
                break;
            case 'download':
                $this->Data['download_content'] = 'Download';
                break;
            case 'review':
                $this->Data['nota'] = 'Nota';
                $this->Data['_content'] = 'Review';
                $this->Data['content_nota'] = 'Review c/ Notas';
                break;
            case 'versao':
                $this->Data['versao_title'] = 'Última Versão';
                $this->Data['versao_content'] = 'Novidades';
                $this->Data['versao_date'] = 'Atualizado em..';
                break;
        endswitch;
    }

    /*
     * #############################################
     * #########    SEO     ########################
     * ---------------------------------------------
     */

    public function trataSeoDados() {
        $read = new Read();
        $home = str_replace(array('/', '.'), array('\/', '\.'), HOME);
        
        //determina valores a serem criados forçadamente
        $seo = ['category_id','tag_id', 'category_id', 'content_nota'];
        foreach ($seo as $s):
            if (!isset($this->Result[$s])):
                $this->Result[$s] = '';
            endif;
        endforeach;
        
        foreach ($this->Result as $i => $a):
            if ((preg_match('/_cover/i', $i) || preg_match('/(\.png|\.jpg)$/i', $a)) && !preg_match("/^{$home}/i", $a) && !preg_match('/^http/i', $a)):
                $this->Result[$i] = HOME . '/tim.php?src=' . HOME . '/uploads/' . $a;

            elseif ($i === 'produto_tamanho'):
                $this->Result[$i] = $a . 'MB';

            elseif ($i === 'produto_faixa_etaria'):
                if ($a > 0):
                    $this->Result[$i] = $a . ' anos';
                else:
                    $this->Result[$i] = 'Livre';
                endif;

            elseif ($i === 'produto_limite' && $a == -1):
                $this->Result[$i] = '-';

            elseif ($i === 'nota' && $a > 0):
                $this->Result[$i] = Check::getStar($a);

            elseif ($i === '_content' && $a == 'nota'):
                $this->Result[$i] = '';

            elseif ($i === 'embed'):
                if (isset($this->Result['extends_posts'])):
                    $read->ExeRead(PRE . "embed", "WHERE post_id=:pp", "pp={$this->Result['extends_posts']}");
                    if ($read->getResult()):
                        if (!preg_match('/<embed/i', $read->getResult()[0]['embed'])):
                            $this->Result[$i] = "<embed style='width:100%;height:300px' src='{$read->getResult()[0]['embed']}' id='videot' />";
                        else:
                            $this->Result[$i] = str_replace('<embed', "<embed style='width:100%;height:300px'", $read->getResult()[0]['embed']);
                        endif;
                    else:
                        $this->Result[$i] = '';
                    endif;
                endif;

            elseif (preg_match('/preco/i', $i)):
                if ($a > 0):
                    $this->Result[$i] = 'R$' . $a;
                else:
                    $this->Result[$i] = 'grátis';
                endif;

            elseif ($i === 'produto_requer'):
                $this->Result[$i] = 'Android ' . $a . ' ou superior';

            elseif ($i === 'produto_play'):
                $this->Result[$i] = "<a href='{$a}' class='fl-left pd-smallb' style='color: inherit;' target='_blank' rel='nofollow'>>> googleplay</a>";

            elseif ((preg_match('/_date/i', $i) || $i === 'date') && $i !== 'versao_date'):
                $this->Result[$i] = Check::getData($a);

            elseif ($i === '_content'):
                $this->Result[$i] = trim(str_replace('<p><br></p>', '', $a));
            elseif ($i === 'category_id'):
                $read->ExeRead(PRE . "category", "WHERE id=:ci", "ci={$a}");
                if ($read->getResult()):
                    $this->Result[$i] = "<a class='fl-left' href='" . HOME . "/category/{$read->getResult()[0]['urlname']}/'><i class='shoticon shoticon-category-pure shoticon-button'></i>" . ucwords($read->getResult()[0]['title']) . "</a>";
                else:
                    $this->Result[$i] = "";
                endif;

                $this->setRelatedPosts($a);

            elseif ($i === 'download_content' && isset($this->Id)):
                $this->Result[$i] = "<span class='fl-left pd-smallb' onclick='opendownload({$this->Id});'><i class='shoticon shoticon-download-white-pure font-size08'></i>download</span>";

            elseif ($i === 'tag_id'):
                $read->ExeRead(PRE . "tag", "WHERE id=:ci", "ci={$a}");
                if ($read->getResult()):
                    $this->Result[$i] = "<a class='fl-left' href='" . HOME . "/tag/{$read->getResult()[0]['urlname']}/'><i class='shoticon shoticon-tag-pure'></i>" . ucwords($read->getResult()[0]['title']) . "</a>";
                else:
                    $this->Result[$i] = "";
                endif;

            elseif ($i === 'fabricante_name'):
                $this->Result[$i] = "<a class='fl-left' href='" . HOME . "/fabricante/{$a}/'><i class='shoticon shoticon-denveloper-pure'></i>" . ucwords($this->Result['fabricante_title']) . "</a>";
            endif;
        endforeach;

        $this->getGallery();
        $this->getReviewNota();
        if($this->Banco === PRE . "posts_backup"):
            $this->posts_backup();
        endif;
    }

    private function getReviewNota() {
        if(isset($this->Result['extends_posts'])):
            $read = new Read();
            $read->ExeRead(PRE . "review", "WHERE post_id=:ji", "ji={$this->Result['extends_posts']}");
            if ($read->getResult()):
                foreach ($read->getResult() as $r):
                    if ($r['_content'] !== 'nota'):
                        if ($r['type'] == 0):
                            $reviews[$r['_content']] = $r['nota'];
                        else:
                            $content = $r['_content'];
                        endif;
                    endif;
                endforeach;
            endif;
            
            if (isset($read->getResult()[0]['nota']) && (isset($reviews) || isset($content))):
                $this->Result['content_nota'] = "<div class='container bg-light radius border'>";

                if(isset($this->Result['gallery_id'])):
                    $this->Result['content_nota'] .= "<span class='fl-left pd-medium'><img src='" . HOME . "/tim.php?src={$this->Result['gallery_id']}&w=50&h=50' class='radius' /></span><span class='fl-left pd-small'>{$this->Result['title']}</span>";
                endif;

                if (isset($reviews)):
                    if(isset($read->getResult()[0]['nota'])):
                        $this->Result['content_nota'] .= "<div style='margin-left:20px' class='fl-left'>" . Check::getStar($read->getResult()[0]['nota']) . "</div><div class='container pd-small'></div>";
                    endif;
                    
                    foreach ($reviews as $f => $g):
                        $this->Result['content_nota'] .= "<div class='container pd-smallb'><div class='fl-left'>{$f}</div><div class='fl-right'>" . Check::getStar($g) . "</div></div>";
                    endforeach;
                    
                else:
                    $this->Result['content_nota'] .= "<div style='margin-left:20px' class='fl-left'>" . Check::getStar($read->getResult()[0]['nota']) . "</div>";
                endif;

                if (isset($content)):
                    $this->Result['content_nota'] .= "<div class='container pd-bigb bg-gray color-white pd-small'>{$content}</div>";
                endif;

                $this->Result['content_nota'] .= "</div>";
            endif;

        endif;
    }

    private function getGallery() {
        if(isset($this->Result['extends_posts'])):
            $read = new Read();
            $read->ExeRead(PRE . "gallery_post", "WHERE post_id=:ci", "ci={$this->Result['extends_posts']}");
            if ($read->getResult()):
                foreach ($read->getResult() as $ga):
                    $read->ExeRead(PRE . "gallery", "WHERE id=:hh", "hh={$ga['gallery_id']}");
                    if ($read->getResult()):
                        $gal[] = $read->getResult()[0]['cover'];
                    endif;
                endforeach;
            endif;

            if (isset($gal)):
                $read->ExeRead(PRE . "padrao", "WHERE name='gallery_number'");
                if ($read->getResult()):
                    $galNumber = $read->getResult()[0]['padrao'];
                else:
                    $galNumber = 20;
                endif;

                $nimg = 0;
                for ($i = 0; $i < $galNumber; $i++):
                    if (isset($gal[$i]) && !empty($gal[$i])):
                        if (!preg_match('/(^\/\/|^http)/i', $gal[$i])):
                            $gal[$i] = HOME . '/uploads/' . $gal[$i];
                        endif;
                        $this->Result['post_gallery_' . $i] = $gal[$i];
                    else:
                        $this->Result['post_gallery_' . $i] = $this->Result['post_gallery_' . $nimg];
                        $nimg++;
                    endif;
                endfor;

                if (isset($this->Result['embed']) && !empty($this->Result['embed'])):
                    $this->Result['gallery_embed'] = "embedvideo";
                else:
                    $this->Result['gallery_embed'] = "";
                endif;
                $this->Result['gallery_display'] = '';
            else:
                $this->Result['gallery_display'] = 'ds-none';
            endif;
        endif;
    }

    private function notAllowSeo() {
        switch ($this->Title):
            case "post" :
                $this->NotAllow = ['', 'id', 'status'];
                break;
            case "pagina" :
                $this->NotAllow = ['', 'id', 'status', 'urlname'];
                break;
            case "user" :
                $this->NotAllow = ['', 'id', 'email', 'password', 'level', 'status', 'code'];
                break;
            case "fabricante":
                $this->NotAllow = ['', 'id', 'fabricante_date'];
            case "gallery_post":
                $this->NotAllow = ['', 'id', 'gallery_date', 'gallery_id'];
            default:
                $this->NotAllow[] = '';
        endswitch;
    }

    private function removeNotAllow() {
        if (isset($this->NotAllow)):
            foreach ($this->NotAllow as $not):
                unset($this->Result[$not]);
            endforeach;
        endif;

        $requer = ['embed', 'download_content'];
        foreach ($requer as $e):
            if (!isset($this->Result[$e])):
                $this->Result[$e] = '';
            endif;
        endforeach;
    }

    private function InitialSeo() {
        $read = new Read();
        $read->ExeRead($this->Banco, "WHERE id=:ij", "ij={$this->Id}");
        if ($read->getResult()):
            $this->Result = $read->getResult()[0];
            $this->removeNotAllow();

            if ($this->Recursivo):
                foreach ($read->getResult()[0] as $name => $a):
                    //Seo, busca informações do produto
                    if (preg_match('/(_id|extends_)/i', $name)):
                        $b = str_replace(array('_id', 'extends_', 'post', 'postss'), array('', '', 'post', 'post'), $name);
                        $comp = new TemplatePart($b, $this->Result[$name]);
                        $comp->getSeo();
                        if (is_array($comp->getResult())):
                            foreach ($comp->getResult() as $r => $rr):
                                $this->Result[$r] = $rr;
                            endforeach;
                        endif;
                    endif;
                endforeach;
            endif;
        endif;
    }

    private function setManualSeo() {
        if (($this->Title === 'post' || $this->Title === 'produto') && !isset($this->Result['post_social'])):

            $this->Result['post_social'] = '<ul class="sharebox">'
                    . '<li class="like facebook"><span class="count">0</span> <a href="' . HOME . '/' . $this->Result['urlname'] . '/&app_id=732873586765179" title="Compartilhe no Facebook">Facebook</a></li>'
                    . '<li class="like google"><span class="count">0</span> <a href="' . HOME . '/' . $this->Result['urlname'] . '/" title="Recomende no Google+">Google+</a></li>'
                    . '<li class="like twitter"><span class="count">0</span> <a href="' . HOME . '/' . $this->Result['urlname'] . '/" rel="&text=' . $this->Result['urlname'] . ' @UebsterSite" title="Conte Isto no Twitter">Twitter</a></li>'
                    . '</ul>'
                    . '<script src="' . HOME . '/_cdn/Social/sharebox.js"  />';
        endif;
    }

    private function SlavesDatasRecover() {
        /*$read = new Read();
        $read->ExeRead(PRE . "slave", "WHERE banco=:b", "b={$this->Title}");
        if ($read->getResult()):
            foreach ($read->getResult() as $slave):
                $slave_id = Check::getBanco(PRE . $slave['slave'], (int) $this->Id, "id", str_replace('post', 'post', $this->Title) . '_id');
                $slaveData = new TemplatePart($slave['slave'], $slave_id, 1);
                $slaveData->getSeo();
                if (is_array($slaveData->getResult())):
                    foreach ($slaveData->getResult() as $i => $a):
                        $this->Result[$i] = $a;
                    endforeach;
                endif;
            endforeach;
        endif;*/

        $this->setManualSeo();
    }

    /*
     * Seta post relacionados ao post de acordo com a categoria do mesmo
     * $id -> ID da categoria a buscar relação
     */

    private function setRelatedPosts($id = null) {
        $read = new Read();
        if ($id):
            if (isset($this->Result['extends_posts'])):
                $idp = $this->Result['extends_posts'];
            else:
                $idp = $this->Result['id'];
            endif;

            $read->ExeRead(PRE . "category_post", "WHERE category_id=:pps && post_id!=:ppi ORDER BY RAND() LIMIT 15", "pps={$id}&ppi={$idp}");
            if ($read->getResult()):
                foreach ($read->getResult() as $rel):
                    $ids = (isset($ids) ? 'id=' . $rel['post_id'] . ' || ' . $ids : 'id=' . $rel['post_id']);
                endforeach;

                $read->ExeRead(PRE . "post", "WHERE {$ids} && status = 1");
                if ($read->getResult()):
                    foreach ($read->getResult() as $i => $post):
                        $this->setInfoRelated($i, $post);
                    endforeach;
                endif;
            endif;
        endif;

        $read->ExeRead(PRE . "post", "WHERE status = 1 ORDER BY RAND() LIMIT 10"); //preenchemos as lacunas que sobraram com valores
        if ($read->getResult()):
            foreach ($read->getResult() as $i => $post):
                $this->setInfoRelated($i, $post);
            endforeach;
        endif;
        $this->Result['post_related'] = '';
        $this->Result['post_related_fabricante'] = '';
    }

    /*
     * Seta as informações sobre os post relacionados, de forma trabalhada a funcionar com o template
     * $i -> indice do post relacionado
     * $post -> informações do post relacionado
     */

    private function setInfoRelated($i, $post) {
        if (!isset($this->Result['title_' . $i])):
            $this->Result['title_' . $i] = $post['title'];
            $this->Result['urlname_' . $i] = $post['urlname'];
            $this->Result['gallery_id_' . $i] = $post['gallery_id'];
            $this->Result['content_' . $i] = Check::Words($post['_content'],25);

            $this->Result['category_id_' . $i] = Check::getCategory($post['id']);
            $this->Result['produto_views_' . $i] = $post['views'];

            $read = new Read();
            $read->ExeRead(PRE . "review", "WHERE post_id=:pp", "pp={$post['id']}");
            if ($read->getResult()):
                $this->Result['nota_' . $i] = '&starf; ' . $read->getResult()[0]['nota'];
            else:
                $this->Result['nota_' . $i] = '&starf; 2.7';
            endif;
        endif;
    }
    
    private function posts_backup() {
        $this->Result['content_nota'] = "<div class='container radius bg-terceary pd-bigb font-size13 font-light'>Este App esta sendo postado no momento. <b>Isto é uma prévia!</b> <span class='container pd-small'></span><small class='container color-white'>Ajude o desenvolvedor com o seu trabalho! Basta compartilhar abaixo!</small></div>";
        $gal = explode(',', $this->Result['gallery']);
        foreach ($gal as $i => $g):
            $this->Result['post_gallery_' . $i] = $g;
        endforeach;
    }

}
