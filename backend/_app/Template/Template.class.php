<?php

/**
 * Respnsável por buscar as informações no banco de dados relacionadas a pagina e ou a sessão
 * 
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class Template {

    private $Id;
    private $Tipo;
    private $Pagina;
    private $Sessoes;
    private $Structs;
    private $Padrao;
    private $Dados;

    function __construct($id, $tipo) {
        $this->Id = (int) $id;
        $this->Tipo = (int) $tipo;

        if ($this->Tipo === 0):
            $read = new Read();
            $read->ExeRead(PRE . "sessoes", "WHERE id=:spi", "spi={$this->Id}");
            if ($read->getResult()):
                $this->getSessoes($read->getResult()[0]);
                $this->getStyles();
            endif;
        else:
            $this->initalPagina();
        endif;
    }

    public function getResult() {
        return $this->Dados;
    }
    
    public function Pagina(){
        if(isset($this->Pagina) && !empty($this->Pagina)):
            return true;
        endif;
        return false;
    }
    
    public function Sessao(){
        if(isset($this->Sessoes) && !empty($this->Sessoes)):
            return true;
        endif;
        return false;
    }
    
    public function Struct(){
        if(isset($this->Structs) && !empty($this->Structs)):
            return true;
        endif;
        return false;
    }
    
    public function Padrao(){
        if(isset($this->Padrao) && !empty($this->Padrao)):
            return true;
        endif;
        return false;
    }
    
    public function getPagina(){
        return $this->Pagina;
    }
	
	public function getId(){
        return $this->Id;
    }
    
    public function getSessao(){
        return $this->Sessoes;
    }
    
    public function getStruct(){
        return $this->Structs;
    }
    public function getPadrao(){
        return $this->Padrao;
    }

    /*
     * Obtem toda a tabela da Pagina, da sessão e da struct
     */

    private function initalPagina() {
        $read = new Read();
        $read->ExeRead(PRE . "site_page", "WHERE id=:oo", "oo={$this->Id}");
        if ($read->getResult()):
            $this->Pagina = $read->getResult()[0];

            $read->ExeRead(PRE . "sessoes", "WHERE site_page_id=:spi", "spi={$this->Id}");
            if ($read->getResult()):
                $this->getSessoes($read->getResult()[0]);
            endif;
        endif;

        $this->getStyles();
    }

    /*
     * Obtem os estilos e padrões de casa estrutura envolvida nesta página
     */

    private function getStyles() {
        $read = new Read();
        if (isset($this->Structs)):
            foreach ($this->Structs as $st):
                $read->ExeRead(PRE . "padrao", "WHERE name LIKE '%_{$st['id']}%'");
                if ($read->getResult()):
                    foreach ($read->getResult() as $s):
                        $this->Padrao[] = $s;
                    endforeach;
                endif;
            endforeach;
        endif;
    }

    /*
     * Obtem as Sessões e Structs da página
     */

    private function getSessoes($sessao) {
        $this->Sessoes[] = $sessao;
        $read = new Read();
        $read->ExeRead(PRE . "struct", "WHERE sessao_id=:si", "si={$sessao['id']}");
        if ($read->getResult()):
            foreach ($read->getResult() as $struct):
                $this->Structs[] = $struct;
                $incorporar_struct = 'incorporar_' . $struct['id'];
                $read->ExeRead(PRE . "padrao", "WHERE name=:l", "l={$incorporar_struct}");
                if ($read->getResult() && !empty($read->getResult()[0]['padrao']) && is_numeric($read->getResult()[0]['padrao'])):
                    $read->ExeRead(PRE . "sessoes", "WHERE id=:ss", "ss={$read->getResult()[0]['padrao']}");
                    if ($read->getResult()):
                        $this->getSessoes($read->getResult()[0]);
                    endif;
                endif;
            endforeach;
        endif;
    }

}
