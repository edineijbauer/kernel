<?php

/**
 * Respnsável por criar os arqivos de importação do template design
 * 
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class TemplateImport extends Template {

    private $Arquivo;
    private $Tipo;
    private $Pagina;
    private $Sessoes;
    private $Structs;
    private $Padrao;
    private $Dados;
    private $Reference;
    private $idDestino;

    function __construct($Arquivo, $tipo, $idDestino = NULL) {
        $this->Arquivo = $Arquivo;
        $this->Tipo = (int) $tipo;
        $this->idDestino = (int) ($idDestino ? $idDestino : 0);

        $this->extrairInfo();

        if ($this->Tipo === 1):
            parent::__construct($this->idDestino, 1);
        else:
            parent::__construct($this->idDestino, 0);
        endif;

        $this->DeleteInfo();

        $this->aplicaImport();
    }

    public function getResult() {
        var_dump($this->Sessoes);
        var_dump($this->Structs);
        var_dump($this->Padrao);
    }

    /*
     * Deleta informações da sessão atual, para incluir a da importação
     */

    private function DeleteInfo() {
        $read = new Read();
        $del = new Delete();
        if (parent::Sessao()):
            foreach (parent::getSessao() as $s):

                $read->ExeRead(PRE . "padrao", "WHERE name LIKE 'incorporar_%' && padrao=:pd", "pd={$s['id']}");
                if (!$read->getResult() || $read->getRowCount() == 1):
                    //lê as incorporações, se esta sessão não esta sendo chamada por nenhum bloco ou se estiver sendo chamado somente por este bloco
                    //somente deleta se não for uma sessão home
                    $del->ExeDelete(PRE . "sessoes", "WHERE id=:ik && site_page_id=0", "ik={$s['id']}");
                endif;

            endforeach;
        endif;

        //após deletar aquelas sessões anteriores

        if (parent::Struct()):
            foreach (parent::getStruct() as $s):

                //verifica se existe uma struct que esta vinculada a uma sessão que já foi excluida acima e exclui
                $read->ExeRead(PRE . "sessoes", "WHERE id=:li", "li={$s['sessao_id']}");
                if (!$read->getResult()):
                    $del->ExeDelete(PRE . "struct", "WHERE id=:ika", "ika={$s['id']}");
                endif;

            endforeach;
        endif;

        if (parent::Padrao()):
            foreach (parent::getPadrao() as $s):
                if (preg_match('/_/i', $s['name'])):
                    $a = explode('_', $s['name']);
                    $pid = $a[count($a) - 1];

                    //se for um número o ultimo valor e existir uma referencia para este elemento no nome do padrao
                    if (is_numeric($pid)):
                        $read->ExeRead(PRE . "struct", "WHERE id=:pi", "pi={$pid}");
                        if (!$read->getResult()):
                            $del->ExeDelete(PRE . "padrao", "WHERE name LIKE '%_:pid'", "pid={$s['id']}");
                        endif;
                    endif;
                endif;
            endforeach;
        endif;
    }

    /*
     * Extrai as informações
     */

    private function extrairInfo() {
        $a = explode('[<>]', $this->Arquivo);
        $tipo = (int) $a[0];
        if ($this->Tipo === $tipo):
            if ($this->Tipo === 1):
                $this->Dados[1] = $a[1];
                $this->Dados[2] = $a[2];
                $this->Dados[3] = $a[3];
                $this->Dados[4] = $a[4];
            else:
                $this->Dados[2] = $a[1];
                $this->Dados[3] = $a[2];
                $this->Dados[4] = $a[3];
            endif;

            $this->rebuildInfo();

        else:
            WSErro("Opps! Tipos incompatíveis de importação. Sessão e Página não podem ser alternados", WS_ERROR);
        endif;
    }

    /*
     * Reconstroi as informações
     * Transforma a string em array, para futuros trabalhos
     */

    private function rebuildInfo() {
        if ($this->Tipo === 1):
            $this->Pagina = $this->setRule($this->Dados[1]);
        endif;

        if (isset($this->Dados[2])):
            $e = explode('|##|', $this->Dados[2]);
            foreach ($e as $s):
                if (!empty($s)):
                    $this->Sessoes[] = $this->setRule($s);
                endif;
            endforeach;

            if (isset($this->Dados[3])):
                $e = explode('|##|', $this->Dados[3]);
                foreach ($e as $s):
                    if (!empty($s)):
                        $this->Structs[] = $this->setRule($s);
                    endif;
                endforeach;

                if (isset($this->Dados[4])):
                    $e = explode('|##|', $this->Dados[4]);
                    foreach ($e as $s):
                        if (!empty($s)):
                            $this->Padrao[] = $this->setRule($s);
                        endif;
                    endforeach;
                endif;

            endif;
        endif;
    }

    private function setRule($p) {
        $a = explode('||', $p);

        foreach ($a as $info):
            $info = explode(':=', $info);
            $dados[$info[0]] = $info[1];
        endforeach;

        return $dados;
    }

    private function aplicaImport() {
        $create = new Create();
        $up = new Update();
        $read = new Read();
        
        //Importa os valores passados, criando os valores
        if (isset($this->Sessoes)):
            foreach ($this->Sessoes as $s):
                if (!empty($s) && $s['site_page_id'] === '0'):
                    $tempid = $s['id'];
                    unset($s['id']);

                    $read->ExeRead(PRE . "sessoes", "WHERE id=:tt", "tt={$tempid}");
                    if (!$read->getResult()):
                        $create->ExeCreate(PRE . "sessoes", $s);

                        //se foi criado com sucesso no banco, entao referencia o id passado com o id do banco
                        $this->Reference['sessao'][$tempid] = (int) $create->getResult();
                    
                    else:
                        
                        $this->Reference['sessao'][$tempid] = (int) $tempid;
                    endif;
                
                elseif(!empty($s) && $s['site_page_id'] !== '0'):
                    
                    $read->ExeRead(PRE . "sessoes", "WHERE site_page_id=:si", "si={$this->idDestino}");
                    if($read->getResult()):
                        $this->Reference['sessao'][$s['id']] = (int) $read->getResult()[0]['id'];
                    endif;
                    
                endif;
            endforeach;

            if (isset($this->Structs)):
                foreach ($this->Structs as $s):
                    if (!empty($s)):
                        $tempid = $s['id'];
                        unset($s['id']);

                        //se existir essa struct vinculada a ID da SESSAO (válida em Reference), então substitui e continua
                        if (isset($this->Reference['sessao'][$s['sessao_id']])):
                            
                            $s['sessao_id'] = $this->Reference['sessao'][$s['sessao_id']];
                            $create->ExeCreate(PRE . "struct", $s);
                            
                            if ($create->getResult()):
                                
                                //se foi criado com sucesso no banco, entao referencia o id passado com o id do banco
                                $this->Reference['struct'][$tempid] = (int) $create->getResult();
                            
                            endif;
                        endif;

                    endif;
                endforeach;

                if (isset($this->Padrao)):
                    foreach ($this->Padrao as $s):
                        if (!empty($s) && !empty($s['padrao'])):

                            if (preg_match('/_/i', $s['name'])):
                                
                                $a = explode('_', $s['name']);
                                $pid = $a[count($a) - 1];

                                if (isset($this->Reference['struct'][$pid])):

                                    //se for um número o ultimo valor e existir uma referencia para este elemento no nome do padrao
                                    if (is_numeric($pid)):

                                        //troca o id da exportação pelo da importação do name e o padrao
                                        $s['name'] = str_replace('_' . $pid, '_' . $this->Reference['struct'][$pid], $s['name']);
                                        $s['padrao'] = str_replace('_' . $pid, '_' . $this->Reference['struct'][$pid], $s['padrao']);
                                    endif;

                                    //caso seja de incorporação, substitui o valor de importação pelo novo id
                                    if (preg_match('/^incorporar_/i', $s['name']) && isset($this->Reference['sessao'][$s['padrao']])):
                                        $s['padrao'] = $this->Reference['sessao'][$s['padrao']];
                                    endif;
                                endif;
                                
                            endif;

                            unset($s['id']);
                            $create->ExeCreate(PRE . "padrao", $s);

                        endif;
                    endforeach;
                endif;
            endif;
        endif;
    }

}
