<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 15/12/2016
 * Time: 17:30
 */
class Token {

    private $old;
    private $new;

    public function getToken() {
        return $this->connect();
    }

    /**
     * @return mixed
     */
    public function getNewToken() {
        return ($this->new ? $this->new : "");
    }

    public function existToken($myToken) {
        if ($this->brokenToken($myToken)):
            $token = new Banco("token");
            $token->load("token", $this->old);

            if ($token->exist()):
                $token->token = $this->new;
                $token->save();
                return true;
            endif;
        endif;

        return false;
    }

    private function brokenToken($myToken) {
        unset($this->new, $this->old);
        $broken = explode("(::<&H&>::)", base64_decode($myToken));
        if (isset($broken[1]) && !empty($broken[1])):
            $this->old = $broken[0];
            $this->new = $broken[1];

            return true;
        endif;

        return false;
    }

    private function connect() {
            $token = new Banco("token");
            $token->load("id", 1);
            if ($token->exist()):
                return base64_encode($token->token . '(::<&H&>::)' . $this->generateNewToken());
            endif;

        return false;
    }

    private function generateNewToken() {
        return md5($_SESSION['userlogin']['user_email']) . md5(rand(0, 1000000)) . md5(date("Y-m-d H:i:s"));
    }


}