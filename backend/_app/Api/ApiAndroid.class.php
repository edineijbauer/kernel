<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 15/12/2016
 * Time: 16:34
 */
class ApiAndroid {

    private $link;
    private $nome;
    private $result;
    private $erro;

    /**
     * ApiOntab constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function getApp() {
        if ($this->link || $this->nome):
            if ($this->connect()):
                return $this->result;
            endif;
        endif;

    }

    /**
     * @param mixed $link
     */
    public function setLink($link) {
        $this->link = (string)$link;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getErro() {
        return $this->erro;
    }

    private function connect() {
        $url = 'http://crawling.ontab.com.br/API/android/' . ($this->link? 'requisita_app_by_link' : 'requisita_app_by_name');

        if ($this->get_http_response_code($url) === "200" && ($this->nome || $this->link)):

            return $this->sendRequest($url);

        else:
            return false;
        endif;
    }

    private function sendRequest($url) {
        $auth = new Token();
        $data = array('auth' => $auth->getToken(), 'nome' => $this->nome, 'link' => $this->link);

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                '_content' => http_build_query($data)
            )
        );

        try {
            $this->result = file_get_contents($url, false, stream_context_create($options));
        } catch (Exception $e) {
            $this->erro = $e->getMessage();
            return false;
        }
        return true;
    }

    private function get_http_response_code($url) {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }

}