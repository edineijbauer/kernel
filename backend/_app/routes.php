<?php
/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 18/01/2017
 * Time: 13:45
 *
 * Este arquivo de rota é gerada automaticamente pelo sistema,
 * NÃO EDITAR ESTE ARQUIVO
 */

$routes = ["404", "index", "pagina", "marca", "smartphone", "categoria", "tag", "loja", "marcas", "smartphones"];
$routesTable = array("categoria" => "category", "pagina" => "pagina", "marca" => "marca", "smartphone" => "smartphone", "tag" => "tag", "loja" => "site");
$routesTitle = array("categoria" => "Categorias do site " . SITENAME);
$routesDescricao = array("categoria" => "Todas as Categorias do site");
$defaultFile = "post";