<?php


class InputChoice {

    private $table;
    private $tableParent;
    private $dados;
    private $title;
    private $id;
    private $result;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $tableParent
     */
    public function setTableParent($tableParent) {
        $this->tableParent = $tableParent;
    }

    /**
     * @param mixed $dados
     */
    public function setDados($dados) {
        $this->dados = $dados;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        $this->start();
        return $this->result;
    }

    private function start() {
        if ($this->table && $this->dados && $this->id):
            $this->beginStruct();
        endif;
    }

    private function beginStruct() {
        $button = ($this->table === $this->tableParent ? "" : "<button class='fl-right btn btn-white pd-medium radius addOneToOne'style='margin-top:15px' onclick=\"getPage('{$this->table}', 25, " . (!$this->dados['id']['value'] ? 0 : $this->dados['id']['value']) . ", '{$this->id}')\" > " . ($this->dados['id']['value'] > 0 ? "editar" : "adicionar") . "</button>");

        $image = (isset($this->dados['cover']) ? "<div class='fl-left pd-medium'><img src='" . HOME . "/tim.php?src=" . HOME . "/uploads/{$this->dados['cover']['value']}&w=40&h=40' width='40' height='40' style='width:40px' class='fl-left' title='{$this->dados['title']['value']}' alt='{$this->dados['title']['value']}' /></div>" : "");

        $titulo = str_replace(array('-', "_"), array(" ", " "), $this->title);
        if (isset($this->dados['title']['value'])):
            $this->result = "{$image}<label style='width:65%;padding-top:1px' class='fl-left'><input type='text' class='font-size12 container pd-small choiceOneToOne' value='{$this->dados['title']['value']}' rel='{$this->table}' placeholder='selecione...' /><span class='font-light'>{$titulo}</span></label>{$button}<div class='container ps-relative divoptionsOneToOne' rel='{$this->id}'><div class='container pd-medium bg-white ps-absolute optionsOneToOne' style='margin-top: -6px;'></div></div>";
        else:
            $this->result = "<div class='container pd-small'><span style='margin-top:20px' class='fl-left font-size15 font-light'>" . ucfirst(str_replace(PRE,  '', $this->table)) . "</span>{$button}</div>";
        endif;
        $this->result .= "<div class='container pd-medium'></div>";
    }
}