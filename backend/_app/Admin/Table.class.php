<?php

/**
 * Table [ MODEL ]
 * Cria Tabelas para exibição de dados do banco
 *
 * @copyright (c) 2015, Edinei J. Bauer [NENA PRO]
 */
class Table {
    /* Banco information */

    private $Title;
    private $Banco;
    private $Where;
    private $Places;

    /* Table information */
    private $Edit = false; //verifica se a tabela é editável ou só visualizável
    private $Table; //pega os nomes das tabelas
    private $Style; //Style de cada table
    private $Size = 0;
    private $HTML;
    private $Css; //Controla o corsim e cornão da table
    private $Result; //Resultados do banco de dados na busca
    private $Placeholders; //placeholders para cada TD
    private $PrevTable; //previne que uma td adjacente com a mesma requisição busque novamente
    private $HaveSearch = true; //verifica se deseja input hiddens para requisições ajax
    private $Search; //reúne as informações para usá-las em um search nesta mesma table (array javascript)
    private $ButtonTableView; //botão de visualização com a tabela para linkar ao valor
    private $ButtonTablePreView; //link prefixo do botão
    private $Limit; //limite de resultados da tabela
    private $Offset = 1; //offset inicial
    private $OffsetLimit = 0; //offset a iniciar a tabela
    private $Count; //conta quantos resultados a tabela resultou
    private $Page; //gera a paginação
    private $Modelo = 0; //controla os modelos diferentes de tabela disponibilizados
    private $ModeloSelect = 0; //seleciona o modelo desejado para exibição

    /* Adjustes information */
    private $Boxes;

    function __construct($banco, $title = null, $edit = null) {
        $this->Banco = (string)$banco;
        $this->Title = (string)($title ? $title : ucfirst(str_replace(PRE, '', $this->Banco)));
        $this->Edit = ($edit ? true : false);
        $this->Limit = (int)Check::getPattern("rowTable_" . $this->Title, 20);
        $a = [15, 25, 50, 100, 300];
        $this->HTML = '<div class="container pd-small"><div class="fl-left pd-small"><input type="text" style="width:150px;" class="fl-left mg-small searchPost" id="searchTable_' . $this->Title . '" onkeyup="searchinTable(\'' . $this->Title . '\', event);" title="' . $this->Title . '" rel="' . $this->Banco . '" placeholder="procurar..." />' . '<select class="numrowsTable mg-small pd-small fl-left" id="rowNum-' . $this->Title . '" rel="' . $this->Title . '">';

        foreach ($a as $option):
            if ($option == $this->Limit):
                $this->HTML .= '<option selected="selected" value="' . $option . '">' . $option . '</option>';
            else:
                $this->HTML .= '<option value="' . $option . '">' . $option . '</option>';
            endif;
        endforeach;

        $this->Title = str_replace(array("_", "-", "smt", "smart", "Smart", "-mini"), array(" ", " ", "", "", "", ""), $this->Title);

        $tituloNovo = str_replace('Sessoes', 'Sessao', $this->Title);
        $tituloNovo = (preg_match('/s$/i', $tituloNovo) ? substr($tituloNovo, 0, -1) : $tituloNovo);
        $tituloNovo = str_replace('Colecoe', 'Colecao', $tituloNovo);
        $tituloNovo = (preg_match('/a$/i', $tituloNovo) || preg_match('/a\w$/i', $tituloNovo) ? "Nova" : "Novo") . " " . $tituloNovo;

        $this->HTML .= '</select></div>' . '<div class="fl-right pd-small"><button class="btn btn-staticgray border-redhover transition-fast inhover pd-smallb font-size13" onclick="getPage(\'' . $this->Banco . '\', 50, 0);">' . $tituloNovo . '</button></div><div class="container pd-small" style="padding: 15px 5px 5px 10px;" id="cont_table"></div></div>' . '<table class="container">';
    }

    public function finalizar() {
        $this->Modelo++;
    }

    /*
     *   Informa quais os campos devem ser exibidos, caso nada seja passado, todos são selecionados
      @info $style = Estilo da TD or false
      @info $placeholders = html para ser usado junto ao valor requerido (## substitui o valor)
     */

    public function setTable($table, $style = null, $placeholders = null) {
        if ($table):

            $placeholders = ($placeholders ? $placeholders : false);
            $style = ($style ? $style : false);

            if ($table == ">>header"):
                $this->notHeader();
            elseif ($table == ">>view"):
                $this->btnView($style, $placeholders);
            else:

                $this->Table[$this->Modelo][] = $table; //indexa os placeholders
                $this->Placeholders[$this->Modelo][] = $placeholders; //indexa os placeholders
                $this->Style[$this->Modelo][] = $style; //indexa os estilos
                $this->Size++; //conta mais uma tabela
            endif;

            $this->addToOutput($table, $style, $placeholders);

        endif;
    }

    //Exclui cabeçalho padrão de busca e criação de novas tabelas (efetivo para buscas)
    public function notHeader() {
        $this->HTML = '<table class="container">';
        $this->addToOutput(">>header");
    }

    //Exclui as input hiddens para futuras buscas ajax, possívelmente usada quando já estiver sendo chamada via ajax
    public function notGetHidden() {
        $this->HaveSearch = false;
    }

    //Seta botão de visualização do conteúdo
    public function btnView($link, $pre = null, $id = null) {
        $pre = ($pre ? $pre : false);
        $this->ButtonTablePreView = (string)($pre ? $pre : "");
        $this->ButtonTableView = (string)$link;
        if ($id):
            $read = new Read();
            $read->ExeRead($this->Banco, "WHERE id=:dd ORDER BY id DESC LIMIT 1", "dd={$id}");
            if ($read->getResult()):
                $btn = $read->getResult()[0]['urlname'];

                if (isset($btn)):
                    $this->addToOutput(">>view", $link, $pre);
                    return '<a href="' . HOME . '/' . $this->ButtonTablePreView . $btn . '/" target="_blank" class="btn btn-transparent fl-right color-black font-light pd-big"><i class="shoticon shoticon-eyesb font-size07" style="padding-left: 15px;"></i></a>';
                else:
                    return "";
                endif;
            else:
                return "";
            endif;
        endif;
    }

    //seta offset
    public function setOffset($offset) {
        $this->Offset = (int)($offset < 1 ? 1 : $offset);
        $this->OffsetLimit = (int)$this->Offset * $this->Limit - $this->Limit;
    }

    /* Cria a Tabela com as informações passadas e retorna o html */

    public function ExeCreate($WHERE = null, $PLACES = null) {
        $this->Where = ($WHERE ? $WHERE : "ORDER BY id DESC");
        $this->Places = ($PLACES ? $PLACES : false);

        if (!preg_match('/ORDER /i', $this->Where)):
            $this->Where .= " ORDER BY id DESC";
        endif;

        $read = new Read();
        if ($this->Places):
            $read->ExeRead($this->Banco, $this->Where . " LIMIT {$this->Limit} OFFSET {$this->OffsetLimit}", $this->Places);
        else:
            $read->ExeRead($this->Banco, $this->Where . " LIMIT {$this->Limit} OFFSET {$this->OffsetLimit}");
        endif;
        if ($read->getResult()):
            $this->Css = "bg-light";
            foreach ($read->getResult() as $result):
                $this->Result = $result;
                $this->getInfo();
            endforeach;
            $this->HTML .= '</table>';
        endif;

        $this->setChangeModel();

        $this->Paginator();

        echo $this->HTML . $this->Page;

        if ($this->HaveSearch):
            echo $this->Search[$this->ModeloSelect] . ($this->Search[$this->ModeloSelect] ? '" />' : "");
        endif;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    private function setChangeModel() {
        if ($this->HaveSearch && $this->Modelo > 0):
            $content = '';
            for ($i = 1; $i < $this->Modelo + 1; $i++):
                $e = $i - 1;
                $content .= '<div rel=\"' . $this->Title . '\" title=\"' . $e . '\" class=\"changemodelTable pd-big border smart border-greenhover bg-light border-bottom transition-fast font-bold font-size12 al-center box box-6 s11box-5 s9box-3 s7box-2 s5box-1\">modelo de tabela ' . $i . '</div>';
            endfor;
            echo "<script>$('#btnoption_{$this->Title}').removeClass('ds-none');$('#divoption_{$this->Title}').html('{$content}'); $('.changemodelTable').click(function(){addPattern('modeloSelecionado_' + $(this).attr('rel'), $(this).attr('title'));});</script>";
        endif;
    }

    //Adiciona valores para um input HIDDEN para depois recuperar a estrutura desta tabela quando solicitado via ajax
    private function addToOutput($table, $style = null, $placeholders = null) {
        $table = (string)$table;
        $style = (string)($style ? $style : "");
        $placeholders = (string)($placeholders ? $placeholders : "");
        if (empty($this->Search[$this->Modelo])):
            $this->Search[$this->Modelo] = '<input type="hidden" id="procurarVars-' . $this->Banco . '" value="' . $table . ',,' . $style . ',,' . $placeholders . '..';
        else:
            $this->Search[$this->Modelo] .= $table . ',,' . $style . ',,' . $placeholders . '..';
        endif;
    }

    //Obtem as informações das Tables no banco de dados
    private function getInfo() {
        //Para cada resultado

        //cor da linha
        $this->Css = (isset($this->Result['status']) && $this->Result['status'] == 0 ? "btn-yellow marked" : ($this->Css == "bg-white" ? "bg-light" : "bg-white"));
        $this->HTML .= '<tr id="tableRow-' . $this->Result['id'] . '" class="ps-relative container ' . $this->Css . '">';

        //Verifica se é editável a tabela
        $this->getEditTable($this->Result['id']);

        //Verifica qual é o modelo de tabela que foi selecionado para mostrar
        if ($this->Modelo > 0):
            $this->ModeloSelect = Check::getPattern("modeloSelecionado_{$this->Title}", 0);
        endif;

        //Obtem os valores de cada coluna passada no table
        $this->getTd();

        $this->HTML .= "</tr>";
    }

    /**
     * para cada valor passado no Table, (column) pega a informação relacionada no Value
     */
    private function getTd() {
        foreach ($this->Table[$this->ModeloSelect] as $i => $table):
            if ($this->Placeholders[$this->ModeloSelect][$i] && preg_match('/#/i', $this->Placeholders[$this->ModeloSelect][$i])):
                $this->Result[$table] = $this->getForeignKeyValues($table, $this->Placeholders[$this->ModeloSelect][$i]);

            else:

                $tt1[] = $table;
                $tt2[] = $this->Result[$table];
                $tt1 = $this->CorrigeValores($tt1, $tt2);
                $this->Result[$table] = $tt1[0];
                unset($tt1, $tt2);
            endif;

            $this->HTML .= "<td class='" . ($this->Style[$this->ModeloSelect][$i] ? $this->Style[$this->ModeloSelect][$i] : $this->getBoxSizes()) . "'>" . $this->Result[$table] . "</td>";

        endforeach;

        $this->PrevTable = "";
    }

    //Verifica se a tabela é um valor de ligação para outro banco
    private function getForeignKeyValues($column, $placeholders) {
        $view = new View();
        $fk = new ForeignKey();
        $fk->setColumn($column);
        $fk->setTable($this->Banco);

        if ($fk->getOneToOne()):
            foreach ($fk->getOneToOne() as $one):
                if (isset($one[$column])):
                    if ($this->Result[$column]):
                        $banco = new Banco(str_replace(PRE, "", $one[$column]));
                        $banco->load('id', $this->Result[$column]);
                        return $view->Retorna($banco->getDados(), $placeholders);
                    else:
                        return "";
                    endif;
                endif;
            endforeach;

        else:

            return $view->Retorna($this->Result, $placeholders);
        endif;

        return "";
    }

    //Corrige respostas de valores padrões como data, titulo, status
    private function CorrigeValores($array1, $array2) {
        foreach ($array1 as $i => $a):
            if (preg_match('/(date|data|inicio|fim)/i', $a) && preg_match('/\d{4}-\d\d-\d\d/i', $array2[$i])):
                $newArray[] = '<div class="al-center">' . Check::getData($array2[$i], 2) . '</div>';
            else:
                $newArray[] = $array2[$i];
            endif;
        endforeach;
        return $newArray;
    }

    //Verifica se a tabela é editável ou não
    private function getEditTable($id) {
        if ($this->Edit):
            $this->HTML .= '<td class="fl-right al-right"><div class="container optionsTable" id="tb-' . $id . '">' . '<button onclick="Delete(\'' . $this->Banco . '\', ' . $id . ', \'' . $this->Title . '\');" title="excluir" class="fl-right btn btn-transparent pd-big color-terceary transition-easy">x</button>';

            if (in_array($this->Banco, array(PRE . "post", PRE . "produto", PRE . "pagina", PRE . "promote"))):
                $this->HTML .= '<button onclick="ChangeStatusPost(\'' . $this->Banco . '\', ' . $id . ', \'' . $this->Title . '\');" title="rascunho" class="fl-right btn btn-transparent pd-big color-terceary transition-easy"><i class="shoticon shoticon-trash font-size07" style="padding-left:10px"></i></button>';
            endif;

            $this->HTML .= '<button onclick="getPage(\'' . $this->Banco . '\' , 50, ' . $id . ');" class="edtButtonTable btn btn-transparent fl-right font-light pd-big transition-easy"><i class="shoticon shoticon-lapis font-size07" style="padding-left: 15px;"></i></button>';

            if ($this->ButtonTableView):
                $this->HTML .= $this->btnView($this->ButtonTableView, $this->ButtonTablePreView, $id);
            endif;

            $this->HTML .= '</div></td>';
        endif;
    }

    //Cria a paginação da tabela caso necessário
    private function Paginator() {
        $read = new Read();
        $read->ExeRead($this->Banco, $this->Where, $this->Places);
        if ($read->getResult()):
            $this->Count = $read->getRowCount();
            $this->HTML = str_replace('id="cont_table">', 'id="cont_table"><div class="container pd-small">' . $this->Count . ' resultados</div>', $this->HTML);
            $paginas = ceil($this->Count / $this->Limit);

            if ($paginas > 1):
                $this->Page = "<ul class='container paginator al-center'>";

                if ($this->Offset > 1):
                    $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover pag pointer bg-light transition-easy'>1</li>";
                endif;

                for ($pag = $this->Offset - 2; $pag < $this->Offset; $pag++):
                    if ($pag > 1):
                        $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover pag pointer bg-light transition-easy'>{$pag}</li>";
                    endif;
                endfor;

                $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover marked bg-white transition-easy'>{$this->Offset}</li>";

                for ($pag = $this->Offset + 1; $pag < $this->Offset + 5; $pag++):
                    if ($pag < $paginas):
                        if ($pag > $this->Offset + 2 && $pag + 10 < $paginas):
                            $npage = ceil((isset($npage) ? ((($paginas - $pag) / 2) + $pag) : ($paginas - $pag) / 2));
                            $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover pag pointer bg-light transition-easy'>" . $npage . "</li>";
                        else:
                            $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover pag pointer bg-light transition-easy'>{$pag}</li>";
                        endif;
                    endif;
                endfor;

                if ($this->Offset != $paginas):
                    $this->Page .= "<li class='fl-left mg-small pd-mediumb radius border border-redhover pag pointer bg-light transition-easy'>{$paginas}</li>";
                endif;

                $this->Page .= "</ul></div><script>$('#subcontent-{$this->Title} div ul').find('.pag').click(function(){" . "$.post('../requests/back/tableGetRows.php', {a: '{$this->Title}' ,b: '{$this->Banco}',c: $('#procurarVars-{$this->Banco}').val(), d: $(this).html(), e: '{$this->Where}', f: '{$this->Places}'}, function(g){ " . "$('#subcontent-{$this->Title}').find('table, ul').remove(); $('#subcontent-{$this->Title}').find('.postBody').append(g);" . "});" . "});</script>";
            endif;
        endif;
    }

    //obtem um tamaho de td padrão para o número de resultados do banco
    private function getBoxSizes() {
        switch ($this->Size):
            case 1:
                $this->Boxes = "box box-1";
                break;
            case 2:
                $this->Boxes = "box box-2";
                break;
            case 3:
                $this->Boxes = "box box-3 s6box-2";
                break;
            case 4:
                $this->Boxes = "table";
                break;
            case 5:
                $this->Boxes = "table";
                break;
            case 6:
                $this->Boxes = "box box-6 s9box-5 s9box-4";
                break;
            default:
        endswitch;
    }

}
