<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 04/12/2016
 * Time: 14:19
 */
class search {

    private $abstracao;
    private $result;

    /**
     * linkPlayer constructor.
     * @param string
     */
    public function __construct($busca) {
        $this->getTemporadaIdRelacionada($busca);
    }

    /**
     * @return mixed
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getAbstracao() {
        $dados = array(
            'titulo' => $this->abstracao->getTitulo(),
            'tituloAlternativo' => $this->abstracao->getTituloAlternativo(),
            'subtitulo' => $this->abstracao->getSubtitulo(),
            'idioma' => $this->abstracao->getIdioma(),
            'resolucao' => $this->abstracao->getResolucao(),
            'posicao' => $this->abstracao->getPosicao()
        );
        return $dados;
    }

    private function getTemporadaIdRelacionada($busca) {
        //$buscaCorrigida = new Corretor($busca, "filme");

        $this->abstracao = new PalavrasChavePlayers($busca);
        $read = new Read();

        $termos = "WHERE title = '{$this->abstracao->getTitulo()}' && subtitle = '{$this->abstracao->getSubtitulo()}'" . ($this->abstracao->getSubtitulo() ? "" : " && posicao = {$this->abstracao->getPosicao()}");

        $read->ExeRead(PRE . "abstracao", $termos);
        if ($read->getResult()):
            $this->getLinkPlayer($read->getResult()[0]);
        else:

            $read->ExeRead(PRE . "alternativo", $termos);
            if ($read->getResult()):
                $read->ExeRead(PRE . "abstracao", "WHERE temporada_id = :p", "p={$read->getResult()[0]['temporada_id']}");
                if ($read->getResult()):
                    $this->getLinkPlayer($read->getResult()[0]);
                endif;
            else:

                if ($this->abstracao->getSubtitulo()):

                    //buscas alternadas entre abstração e alternativo
                    $read->ExeRead(PRE . "abstracao", "WHERE title = '{$this->abstracao->getTitulo()}'");
                    if ($read->getResult()):
                        foreach ($read->getResult() as $item):
                            $read->ExeRead(PRE . "alternativo", "WHERE subtitle = '{$this->abstracao->getSubtitulo()}' && temporada_id = :t", "t={$item['temporada_id']}");
                            if ($read->getResult()):
                                $this->getLinkPlayer($item);
                            endif;
                        endforeach;
                    endif;

                    //buscas alternadas entre alternativo e abstração
                    $read->ExeRead(PRE . "alternativo", "WHERE title = '{$this->abstracao->getTitulo()}'");
                    if ($read->getResult()):
                        foreach ($read->getResult() as $item):
                            $read->ExeRead(PRE . "abstracao", "WHERE subtitle = '{$this->abstracao->getSubtitulo()}' && temporada_id = :t", "t={$item['temporada_id']}");
                            if ($read->getResult()):
                                $this->getLinkPlayer($read->getResult()[0]);
                            endif;
                        endforeach;
                    endif;
                else:

                    $posicao = ($this->abstracao->getPosicao() === 0 ? 1 : $this->abstracao->getPosicao());
                    if ($posicao > 0):

                        $read->ExeRead(PRE . "abstracao", "WHERE title = '{$this->abstracao->getTitulo()}'");
                        if (!$read->getResult()):
                            $read->ExeRead(PRE . "alternativo", "WHERE title = '{$this->abstracao->getTitulo()}'");
                        endif;

                        if ($read->getResult()):
                            foreach ($read->getResult() as $item):
                                $read->ExeRead(PRE . "temporada", "WHERE (posicao = 0 || posicao = :i) && indice = :i && id = :id", "i={$posicao}&id={$item['temporada_id']}");
                                if ($read->getResult()):
                                    $dados['subtitle'] = $read->getResult()[0]['title'];
                                    $dados['posicao'] = $read->getResult()[0]['posicao'];
                                    $dados['temporada_id'] = $item['temporada_id'];

                                    $read->ExeRead(PRE . "titulo", "WHERE id = :ti", "ti={$read->getResult()[0]['titulo_id']}");
                                    if ($read->getResult()):
                                        $dados["title"] = $read->getResult()[0]['title'];
                                        $this->getLinkPlayer($dados);
                                    endif;
                                endif;
                            endforeach;
                        endif;
                    endif;
                endif;
            endif;
        endif;
    }

    private function getLinkPlayer($dados) {
        $player = new linkPlayer($dados['title'], $dados['posicao'], $dados['subtitle'], $this->abstracao->getIdioma(), $this->abstracao->getResolucao());
        if ($player->getResult()):
            $this->result = $player->getResult();
        else:
            $this->result = "nenhum player encontrado";
        endif;
    }
}