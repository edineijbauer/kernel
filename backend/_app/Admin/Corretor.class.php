<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 19/11/2016
 * Time: 16:00
 */
class Corretor {

    private $reference;
    private $busca;
    private $result = false;

    /**
     * Corretor constructor.
     */
    public function __construct($busca, $reference = null) {
        if ($reference):
            $this->reference = (string) trim($reference);
            $this->busca = (string)trim(strip_tags($busca));
        else:
            $this->reference = "";
            $this->busca = (string)trim(strip_tags($busca));
        endif;

        $this->buscaCorrespondencia($this->busca);
    }

    public function getResult() {
        return $this->result;
    }

    private function buscaCorrespondencia($busca) {
        $read = new Read();
        $read->ExeRead(PRE . "corretor_ortografico", "WHERE term = '{$busca}' && reference=:r", "r={$this->reference}");
        if ($read->getResult()):
            $this->result = $read->getResult()[0]['relation'];
        else:
            $this->result = $this->buscaSplitCorrespondencia($busca);

            if (!$this->result):
                $reference = (!empty($this->reference) ? $this->reference . "+" : "");
                $busca = $busca;
                $this->googleSearch("https://www.google.com.br/search?num=1&q={$reference}{$busca}&oq={$reference}{$busca}");
            endif;
        endif;
    }

    private function buscaSplitCorrespondencia($busca) {
        if (preg_match('/ /i', $busca)):
            $read = new Read();
            foreach (explode(" ", $busca) as $term):
                $read->ExeRead(PRE . "corretor_ortografico", "WHERE term = '{$term}' && reference=:r", "r={$this->reference}");
                if ($read->getResult()):
                    $correspondencia = (isset($correspondencia) ? $correspondencia . " " . $read->getResult()[0]['relation'] : $read->getResult()[0]['relation']);
                else:
                    return false;
                endif;
            endforeach;

            return $correspondencia;

        else:
            return false;
        endif;
    }

    private function prepareLinkName($link) {
        return str_replace(array("   ", "  ", " "), array(" ", " ", "+"), $link);
    }

    private function googleSearch($googleLink) {
        $semantica = new Semantica(file_get_contents($this->prepareLinkName($googleLink)));
        $semantica->getTagById("_FQd");
        if ($semantica->getConteudo() !== ""):
            $this->encontrouCorrespondencia($semantica->getConteudo());
        else:
            $this->result = $this->busca;
            $this->saveBuscaCorreta($this->busca);
        endif;
    }

    private function encontrouCorrespondencia($content) {
        $semantica = new Semantica($content);
        $semantica->getTagByClass("spell", 2);
        if ($semantica->getConteudo() !== ""):
            $correspondencia = trim(str_replace($this->reference, "", strip_tags($semantica->getConteudo())));
            $this->result = $correspondencia;

            $this->saveBuscaCorreta($correspondencia);
        else:

            $this->saveBuscaCorreta($this->busca);
        endif;
    }

    private function saveBuscaCorreta($correspondencia) {
        $this->saveCorrespondencia($this->busca, $correspondencia);
        $this->splitCorrespondencia($correspondencia);
    }

    private function splitCorrespondencia($correspondencia) {
        if (preg_match('/ /i', $correspondencia)):
            $busca = explode(" ", $this->busca);
            $correspondencia = explode(" ", $correspondencia);
            if (count($busca) === count($correspondencia)):
                foreach ($correspondencia as $i => $item):
                    $this->saveCorrespondencia($busca[$i], $item);
                endforeach;
            endif;
        endif;
    }

    private function saveCorrespondencia($busca, $correspondencia) {
        $busca = (string)trim($busca);
        $correspondencia = (string)trim($correspondencia);
        $read = new Read();
        $read->ExeRead(PRE . "corretor_ortografico", "WHERE term = '{$busca}' && relation = '{$correspondencia}' && reference = :f", "f={$this->reference}");
        if (!$read->getResult()):
            $dados['term'] = $busca;
            $dados['relation'] = $correspondencia;
            $dados['reference'] = (string)$this->reference;
            $create = new Create();
            $create->ExeCreate(PRE . "corretor_ortografico", $dados);
        endif;
    }
}