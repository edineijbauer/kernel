<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 26/12/2016
 * Time: 20:08
 */
class InputChave {

    private $column;
    private $table;
    private $dados;
    private $retorno;
    private $result;

    /**
     * @param mixed $column
     */
    public function setColumn($column) {
        $this->column = $column;
    }

    /**
     * @param mixed $retorno
     */
    public function setRetorno($retorno) {
        $this->retorno = $retorno;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $this ->dadoss
     */
    public function setDados($dados) {
        $this->dados = $dados;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        $this->checkKeyColumToInputType();
        return $this->result;
    }

    private function checkKeyColumToInputType() {
        switch ($this->dados['chave']):
            case "pk":
                $this->setIdTable();
                break;

            case "indice":
                $this->setNone();
                break;

            case "fk":
                $this->setOneToOne();
                break;

            case "fka":
                $this->setOneToMany();
                break;

            case "fkn":
                $this->setManyToMany();
                break;
        endswitch;
    }

    private function setNone() {
        $this->result = "<input type='hidden' />";
    }

    private function setIdTable() {
        $this->result = "<input type='hidden' value='{$this->dados['value']}' id='{$this->dados['id']}' />";
    }

    private function setOneToOne() {
        $choice = new InputChoice();
        $choice->setTable($this->dados['table']);
        $choice->setTableParent($this->table);
        $choice->setDados($this->dados[$this->dados['table']]);
        $choice->setTitle($this->column);
        $choice->setId($this->dados['id']);

        $input = $choice->getResult();

        $this->result = "<div class='container pd-small'><input type='hidden' value='{$this->dados['value']}' id='{$this->dados['id']}' />" . $input . "</div>";
    }

    private function setOneToMany() {
        $oneToMany = new InputOneToMany();
        $oneToMany->setTable($this->dados['table']);
        $oneToMany->setDados($this->dados);

        $this->result = "<div class='container pd-small'>{$oneToMany->getResult()}</div>";
    }

    private function setManyToMany() {
        $key = str_replace(PRE, "", $this->dados['table']);
        $input = '<input type="hidden" id="' . $this->dados['id'] . '" value="' . ($this->dados['value'] ? ",{$this->dados['value']}" : "") . '," /><div class="container pd-small mg-bottom"><div class="container pd-small font-light font-size13 mg-bottom"><i class="shoticon shoticon-tag-pure"></i>' . Check::getNameBanco($this->dados['table']) . '</div><div class="container pd-small"><input type="text" placeholder="buscar..." id="' . $key . '_field" class="pd-small marcador-input" rel="' . $key . '" alt="' . $this->dados['id'] . '" /></div><div id="div_' . $key . '_field" class="div-marcador-search""></div>';
        $input .= $this->checkValues($this->dados['value'], $key) . '<div class="container boxtag pd-small" id="box' . $key . '">';

        $input .= $this->getValuesMarcadores($this->dados['value'], $key);

        $read = new Read();
        $read->ExeRead($this->dados['table'], "ORDER BY id DESC LIMIT 20");
        if ($read->getResult()):
            foreach ($read->getResult() as $cc):
                $bgcolor = (isset($bgcolor) && $bgcolor == 'bg-white' ? 'bg-secondary' : 'bg-white');
                $input .= '<div onclick="choiceTags(' . $cc['id'] . ', \'' . $key . '\', \'' . $this->dados['id'] . '\')" class="container smart pd-small ' . $bgcolor . '"><div class="fl-left nomargin">' . $cc['title'] . '</div></div>';
            endforeach;
        endif;

        $this->result = $input . '</div></div><div class="container pd-medium clear"></div>';
    }

    private function getValuesMarcadores($value, $key) {
        $input = "";
        if ($value):
            $input .= "<div class='container bg-light radius border pd-small' style='margin: 10px 0;' id='box{$key}_select'>";

            foreach (explode(',', $value) as $tag):
                $tagname = Check::getBanco(PRE . $key, (int)$tag, "title");
                $input .= "<div onclick=\"choiceTags({$tag}, '{$key}', '{$this->dados['id']}')\" class='smart container pd-small selected{$key}' rel='{$tag}' id='selected{$key}-{$tag}'><span class='fl-left nomargin'>{$tagname}</span></div>";
            endforeach;

            $input .= "</div>";

        endif;

        return $input;
    }

    private function checkValues($value, $key) {
        if (is_array($value)):
            $input = '<div class="container bg-light radius border" style="margin: 10px 0;" id="box' . $key . '_select">';
            foreach ($value as $tag => $title):
                $input .= "<div onclick=\"choiceTags({$tag}, '{$key}', '{$this->dados['id']}')\" class='smart container pd-small selected{$key}' rel='{$tag}' id='selected{$key}-{$tag}'><span class='fl-left nomargin'>{$title}</span></div>";
            endforeach;
            return $input . '</div>';
        endif;

        return "";
    }

}