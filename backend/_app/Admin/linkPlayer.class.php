<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 04/12/2016
 * Time: 14:19
 */
class linkPlayer {

    private $result;
    private $titulo;
    private $subtitulo;
    private $posicao;
    private $idioma;
    private $resolucao;

    /**
     * linkPlayer constructor.
     * @param string
     */
    public function __construct($titulo, $posicao, $subtitulo = null, $idioma = null, $resolucao = null) {
        $this->titulo = (string) $titulo;
        $this->posicao = (int) $posicao;
        $this->subtitulo = (string) ($subtitulo ? $subtitulo : "");
        $this->idioma = ($idioma ? $idioma : "");
        $this->resolucao = (string) ($resolucao ? $resolucao : "");
        $this->searchInfoInLinkName();
    }

    /**
     * @return mixed
     */
    public function getResult() {
        return $this->result;
    }

    private function searchInfoInLinkName() {
        $read = new Read();

        $name = Check::Name($this->titulo . " " . ($this->posicao > 0 ? " " . $this->posicao : "") . (!empty($this->subtitulo) ? " " . $this->subtitulo : ""));
        $read->ExeRead(PRE . "link_player", "WHERE link_name LIKE '%-" . implode("-%' && link_name LIKE '%-", explode('-', $name)) . "-%'");
        if ($read->getResult()):

            $i = ($read->getRowCount() > 1 ? $this->getBetterResult($read->getResult()) : 0);

            $player = new Banco("player");
            $player->load("id", $read->getResult()[$i]['player_id']);
            $this->result = json_encode($player->getDados(), 256);
        endif;
    }

    private function getBetterResult($dados) {
        $i = 0;
        $strike = 0;
        foreach ($dados as $e => $link):
            if (!empty($this->idioma) && !isset($this->idioma[1]) && preg_match("/-{$this->idioma[0]}-/i", $link['link_name'])):
                $strike++;
                $i = $e;
            endif;

            if (!empty($this->resolucao) && preg_match("/-{$this->resolucao}-/i", $link['link_name'])):
                $strike++;
                $i = $e;
            endif;

            if ($strike === 2):
                break;
            endif;
            $strike = 0;

        endforeach;

        return $i;
    }
}