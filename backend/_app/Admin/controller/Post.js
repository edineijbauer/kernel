inputs();

$(".lockson").change(function () {
    if ($(this).val() === "0") {
        $(this).siblings(".locksin").find("i").removeClass("shoticon-unlock").addClass("shoticon-lock");
    } else {
        $(this).siblings(".locksin").find("i").removeClass("shoticon-lock").addClass("shoticon-unlock");
    }
});

$(".choiceOneToOne").on('keyup focus', function (e) {
    var $this = $(this);
    var field = $this.parent().siblings(".divoptionsOneToOne").find(".optionsOneToOne");

    if (e.which === 13 && field.html() !== "") {
        field.find("div:eq(0)").trigger("click");
        field.removeClass("boxshadow").html("");
    } else {

        if (($this.val() === "" || $this.val() === "") && $this.parent().siblings(".addOneToOne").length) {
            var button = $this.parent().siblings(".addOneToOne");
            var funcao = button.attr("onclick").split(",");
            funcao = funcao[0] + "," + funcao[1] + ", 0," + funcao[3];
            button.html("adicionar").attr("onclick", funcao);
        }

        $.post('../requests/back/Admin/searchInputOneToOne.php', {
            rel: $this.attr("rel"),
            s: $this.val(),
            e: $this.parent().siblings(".divoptionsOneToOne").attr("rel")
        }, function (g) {
            field.addClass("boxshadow").html(g);
        });
    }
}).focusout(function () {
    var $this = $(this);
    var field = $this.parent().siblings(".divoptionsOneToOne").find(".optionsOneToOne");
    setTimeout(function () {

        if ($this.parent().siblings(".addOneToOne").length) {
            var button = $this.parent().siblings(".addOneToOne");
            if ($this.parent().siblings("input[type=hidden]").val() === "" || $this.val() === "") {
                var funcao = button.attr("onclick").split(",");
                funcao = funcao[0] + "," + funcao[1] + ", 0," + funcao[3];
                button.html("adicionar").attr("onclick", funcao);
            }
        }

        field.removeClass("boxshadow").html("");
    }, 200);
});

$(".marcador-input").keyup(function (e) {
    if (e.which === 13) {
        var marc = $(this).parent().siblings(".div-marcador-search");
        marc.find("div").first().trigger("click");
        $(this).val("");
        marc.html("").removeClass("boxshadow");
    } else {
        changeTags($(this).attr("rel"), $(this).attr("alt"));
    }
});

function selectMonth(valor, id) {
    $(".month").removeClass("weekSelected");
    if ($("#" + id).val() == valor) {
        $("#" + id).val("");
    } else {
        $("#" + id).val(valor);
        $("#" + id + "-" + valor).addClass("weekSelected");
    }
}

function selectWeek(valor, id) {
    $(".weeks").removeClass("weekSelected");
    if ($("#" + id).val() == valor) {
        $("#" + id).val("");
    } else {
        $("#" + id).val(valor);
        $("#" + id + "-" + valor).addClass("weekSelected");
    }
}

function selectOnSwitch(id) {
    $("#" + id).val($("#switch-" + id).is(":checked") ? 1 : 0);
}

function selectOneToOne(banco, id, title, returnId) {console.log('ok');

    if (!$("#" + id).hasClass("choiceOneToMany")) {console.log('ko');
        if(title) {
            $("#" + id).siblings("label").find(".choiceOneToOne").siblings("span").addClass("inputSelect");
            $("#" + id).val(returnId).siblings("label").find(".choiceOneToOne").val(title);
            $(".optionsOneToOne").removeClass("boxshadow").html("");
        } else {
            $("#" + id).val(returnId)
        }

        var button = $("#" + id).siblings(".addOneToOne");
        var funcao = button.attr("onclick").split(",");
        funcao = funcao[0] + "," + funcao[1] + "," + returnId + "," + funcao[3];
        button.html("editar").attr("onclick", funcao);

    } else {
        selectOneToMany(banco, id, title, returnId);
    }
}

function selectOneToMany(banco, id, title, returnId) {

    if ($("#" + id).siblings(".choiceOneToManyDiv").find(".controlOneToMany-" + returnId).length) {
        $("#" + id).siblings(".choiceOneToManyDiv").find(".controlOneToMany-" + returnId).find(".oneToManyTitle").html(title);

    } else {

        var buttons = "<button onclick=\"Delete('" + banco + "', " + returnId + "); deleteOneToMany('" + id + "', " + returnId + ");\" title='excluir' class='fl-right btn btn-transparent pd-small color-terceary transition-easy'>x</button>"
            + "<button onclick=\"getPage('" + banco + "' , 25, " + returnId + ", '" + id + "');\" class='edtButtonTable btn btn-transparent fl-right font-light pd-small transition-easy'><i class='shoticon shoticon-lapis font-size07' style='padding-left: 15px;'></i></button>";
        var dados = "<div class='container controlOneToMany-" + returnId + "'><div class='fl-left pd-small oneToManyTitle'>" + title + "</div><div class='fl-right'>" + buttons + "</div></div>";

        $("#" + id).val("," + returnId + $("#" + id).val()).siblings(".choiceOneToManyDiv").append(dados);
    }
}

function deleteOneToMany(id, returnId) {
    $("#" + id).val($("#" + id).val().replace("," + returnId, "")).siblings(".choiceOneToManyDiv").find(".controlOneToMany-" + returnId).remove();
}

$("input[rel=title]").keyup(function () {
    var url = $(this).parent().parent().find("input[rel=url]");
    url.val(CheckName($(this).val()));
});

$(".star").mouseover(function () {

    $(this).html("&starf;").prevAll().html("&starf;");
    $(this).nextAll("li").html("&star;");

}).mouseout(function () {
    var h = parseInt($("#" + $(this).attr("rel")).val()) - 1;
    if (h && h > -1 && h < 10) {

        $(this).html("&star" + ($(this).index() > h ? "" : "f") + ";");
        $(this).siblings(".star").each(function () {
            $(this).html("&star" + ($(this).index() > h ? "" : "f") + ";");
        });

    } else {
        $(this).html("&star;");
        $(this).siblings(".star").html("&star;");
    }

}).click(function () {

    $("#" + $(this).attr("rel")).val(parseInt($(this).attr("alt")) + 1);
    $("#exibi-" + $(this).attr("rel")).html(parseInt($(this).attr("alt")) + 1);
    $(this).css("color", "goldenrod").prevAll().css("color", "goldenrod");
    $(this).nextAll().css("color", "#555");

});

function createTag(marcador, id) {
    var title = $("#" + marcador + "_field").val();
    $.post('../requests/marcador/create.php', {
        a: title,
        marcador: marcador,
        id: id
    }, function (g) {
        var novaTag = '<div onclick="choiceTags(' + g + ', \'' + marcador + '\', \'' + id + '\')" class="smart container pd-small selectedtag" rel="' + g + '" id="selectedtag-' + g + '"><span class="fl-left nomargin">' + title + '</span></div>';
        if (!$("#box" + marcador + "_select").length) {
            if (!$("#box" + marcador).length) {
                $("#" + marcador + "-sistema").append("<div class='container box" + marcador + " pd-small' id='box" + marcador + "'></div>");
            }
            $("#box" + marcador).prepend("<div class='container bg-light radius border pd-small' style='margin: 10px 0;' id='box" + marcador + "_select'>" + novaTag + "</div>");
        } else {
            $("#box" + marcador + "_select").prepend(novaTag);
        }

        $("#" + id).val("," + g + $("#" + id).val());
        $("#" + marcador + "_field").val("");
        $("#div_" + marcador + "_field").html("");
    });
}

var tagchoice = 0;
function choiceTags(value, key, id) {
    if (tagchoice === 0) {
        tagchoice = 1;
        if (!$("#box" + key + "_select").length) {
            $("#box" + key).prepend("<div class='container bg-light radius border pd-small' style='margin: 10px 0;' id='box" + key + "_select'></div>");
        }

        $.post('../requests/marcador/choice.php', {a: value, marcador: key}, function (g) {
            if (g != '0') {
                if (!$("#selected" + key + "-" + value).length) {

                    //add
                    $("#" + id).val("," + value + $("#" + id).val());
                    $("#box" + key + "_select").prepend('<div onclick="choiceTags(' + value + ', \'' + key + '\', \'' + id + '\')" class="smart container pd-small selected' + key + '" rel="' + value + '" id="selected' + key + '-' + value + '"><span class="fl-left nomargin">' + g + '</span></div>');

                } else {

                    //remove
                    $("#" + id).val($("#" + id).val().replace("," + value, ""));
                    $("#selected" + key + "-" + value).remove();
                    if ($("#box" + key + "_select").html() === "") {
                        $("#box" + key + "_select").remove();
                    }
                }

            } else {
                infor(key + " não encontrada");
            }

            tagchoice = 0;
        });
    }
}

var coverwidth = 0;
$("#cover-width").on('keyup change', function () {
    if (coverwidth === 0) {
        coverwidth = 1;
        $.post('../requests/back/post/updateDirectionImage.php', {
            width: $(this).val(),
            id: $(this).attr("rel")
        }, function (g) {
            coverwidth = 0;
        });
    }
});

function vertImage(id) {
    $("#imgdp").css("transform", ($("#imgdp").css("transform") === "none" ? "rotateX(180deg)" : "none"));
    $.post('../requests/back/post/updateDirectionImage.php', {vertical: 1, id: $("#" + id).val()});
}

function HoriImage(id) {
    $("#imgclass").css("transform", ($("#imgclass").css("transform") === "none" ? "rotateY(180deg)" : "none"));
    $.post('../requests/back/post/updateDirectionImage.php', {horizontal: 1, id: $("#" + id).val()});
}

function sendGallery(folder, e, id) {
    lightBox("favfor-Gallery", "galeria", "<iframe class='container' style='height: 100%;position: absolute;' scrolling='no' frameborder='0' src='../_imageUpload/index.php?f=" + folder + "&e=" + e + "&id=" + id + "'></iframe>");
}

function choiceCover(folder, e, id) {
    lightBox("favfor-Gallery", "galeria", "<iframe class='container' style='height: 100%;position: absolute;' scrolling='no' frameborder='0' src='../_imageUpload/index.php?f=" + folder + "&e=" + e + "&id=" + id + "'></iframe>");
}

function sendVideo(folder, e, id) {
    lightBox("sendvideo", "videos", "<iframe class='container' style='height: 100%;position: absolute;' scrolling='no' frameborder='0' src='../_imageUpload/video.php?f=" + folder + "&e=" + e + "&id=" + id + "'></iframe>");
}

function openGallery(folder, e) {
    lightBox("favfor-Gallery", "galeria", loading);
    $.post("../_imageUpload/gallery.php", {e: e, f: folder}, function (g) {
        lightBoxContent(g);
    });
}