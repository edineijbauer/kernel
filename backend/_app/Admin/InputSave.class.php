<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 26/12/2016
 * Time: 20:08
 */
class InputSave {

    private $result;
    private $id;
    private $inputs;
    private $table;
    private $structTable;
    private $oneToMany;

    /**
     * @param mixed $inputs
     */
    public function setInputs($inputs) {
        $this->inputs = $inputs;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $structTable
     */
    public function setStructTable($structTable) {
        $this->structTable = $structTable;
    }

    public function save() {
        $this->checkTableInInputs();
        if ($this->table):
            $this->checkStructTable();
            $this->checkIdInInputs();
            $this->validaInputValues();
            $this->savePost();
        endif;
    }

    private function savePost() {
        if (!$this->result):

            $post = new Banco($this->table);
            $post->load('id', $this->id);
            $post->setDados($this->inputs);
            $this->result = $post->save();

            $this->checkOneToMany();
        endif;
    }

    private function checkOneToMany() {
        if ($this->oneToMany):
            foreach ($this->oneToMany as $table => $ids):
                foreach ($ids as $id):
                    $fk = new ForeignKey();
                    $fk->setTable($table);
                    if ($fk->getManyToOne()):
                        foreach ($fk->getManyToOne() as $fkMany):
                            $this->setOneToManyDados($fkMany, $table, $id);
                        endforeach;
                    endif;
                endforeach;
            endforeach;
        endif;
    }

    private function setOneToManyDados($fkMany, $table, $id) {
        $read = new Read();
        foreach ($fkMany as $column => $tableFka):
            if ($tableFka === $this->table):

                $read->ExeRead($table, "WHERE id = :id", "id={$id}");
                if ($read->getResult() && $read->getResult()[0][$column] !== $this->result):
                    if (empty($read->getResult()[0][$column])):
                        $up = new Update();
                        $up->ExeUpdate($table, array($column => $this->result), "WHERE id = :id", "id={$id}");
                    else:
                        $dados = $read->getResult()[0];
                        unset($dados['id']);
                        $dados[$column] = $this->result;

                        $create = new Create();
                        $create->ExeCreate($table, $dados);
                    endif;
                endif;

            endif;
        endforeach;
    }

    private function checkStructTable() {
        if (!$this->structTable && $this->table):
            $banco = new BancoGetTable();
            $banco->setTable($this->table);
            $this->structTable = $banco->getKey()[$this->table];
        endif;
    }

    private function validaInputValues() {
        foreach ($this->inputs as $name => $value):
            if (!$this->checkFk($name, $value)):
                $this->checkPermissao($name);
                $this->checkNullInputs($name, $value);
                $this->checkUniqueInputs($name, $value);
                $this->checkTipoInput($name, $value);
                $this->checkTamanhoInput($name, $value);
                $this->checkInformacaoInput($name, $value);
                $this->checkEspecilInput($name, $value);
            endif;
        endforeach;
    }

    private function checkFk($table, $value) {
        if (preg_match('/^' . PRE . '/i', $table) && preg_match("/,/i", $value)):
            $this->oneToMany[$table] = array_filter(explode(",", $value));
            unset($this->inputs[$table]);
            return true;
        endif;

        return false;
    }

    private function checkNullInputs($name, $value) {
        if (!$this->structTable[$name]['null'] && empty($value) && ($this->structTable[$name]['value'] === "" || $this->structTable[$name]['value'] === null)):
            $this->result = "Campo " . $this->nomeAmigavel($name) . " não pode ficar vazio.";
            echo $this->result;
            die;
        endif;
    }

    private function checkUniqueInputs($name, $value) {
        if ($this->structTable[$name]['chave'] === "unique"):
            $read = new Read();
            $read->ExeRead($this->table, "WHERE {$name} = '{$value}'" . ($this->id ? " && id != {$this->id}" : "") . " LIMIT 1");
            if ($read->getResult()):echo $this->id;
                $this->result = "Já existe este " . $this->nomeAmigavel($name) . " '{$value}'";
                echo $this->result;
                die;
            endif;
        endif;
    }

    private function checkPermissao($name) {
        if ($this->id):
            if (!preg_match('/update/i', $this->structTable[$name]['permissao'])):
                $this->result = "Opss! Estas informações não podem ser alteradas no banco.";
                echo $this->result;
                die;
            endif;
        else:
            if (!preg_match('/insert/i', $this->structTable[$name]['permissao'])):
                $this->result = "Opss! Estas informações não podem ser inseridas no banco.";
                echo $this->result;
                die;
            endif;
        endif;
    }

    private function setError($m) {
        echo $m;
        die;
    }

    private function checkInformacaoInput($name, $value) {
        if ($name === "email"):
            if (!empty($value) && !Check::Email($value)):
                $this->setError("Email Inválido");
            endif;
        //        elseif ($name === "cpf" && !Check::Cpf($value)):
        //            $this->setError("Cpf Inválido");
        elseif ($name === "password" || $name === "senha"):
            $this->inputs[$name] = (string)Check::Encrypt($value);
        endif;

    }

    private function checkEspecilInput($name, $value) {

        if ($name === "cover"):

            $control = new ImageControl();
            $control->setImage($value);
            $this->inputs[$name] = $control->getImage();
        endif;
    }

    private function checkTipoInput($name, $value) {
        $tipo = $this->structTable[$name]['tipo'];

        if (!$value && $value !== "0"):
            $this->inputs[$name] = $this->structTable[$name]['value'];

        elseif ($this->structTable[$name]['chave'] === "fk"):
            if (!is_numeric($value)):
                $this->caracteresInvalidos($name);
            else:
                $this->inputs[$name] = (int)$value;
            endif;

        elseif ($tipo === "int" || $tipo === "hidden" || $tipo === "select"):
            if (!is_numeric($value)):
                $this->caracteresInvalidos($name);
            else:
                $this->inputs[$name] = (int)$value;
            endif;

        elseif ($tipo === "float"):
            if (!is_numeric($value)):
                $this->caracteresInvalidos($name);
            else:
                $this->inputs[$name] = (float)number_format($value, $this->structTable[$name]['precisao']);
            endif;

        elseif ($tipo === "datetime"):
            if (!preg_match('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/i', $value)):
                $this->caracteresInvalidos($name);
            endif;

        elseif ($tipo === "date"):
            if (!preg_match('/\d{4}-\d{2}-\d{2}/i', $value)):
                $this->caracteresInvalidos($name);
            endif;

        elseif ($tipo === "time"):
            if (!preg_match('/\d{2}:\d{2}/i', $value)):
                $this->caracteresInvalidos($name);
            endif;

        else:
            $this->inputs[$name] = (string)$value;
        endif;
    }

    private function checkTamanhoInput($name, $value) {
        if (!in_array($this->structTable[$name]['tipo'], array("datetime", "date", "time"))):
            if (strlen($value) > $this->structTable[$name]['tamanho']):
                $this->result = "O campo " . $this->nomeAmigavel($name) . " Excedeu seu tamanho máximo de <b>{$this->structTable[$name]['tamanho']}</b> caracteres.";
                echo $this->result;
                die;
            endif;
        endif;
    }

    private function checkIdInInputs() {
        foreach ($this->structTable as $column => $dados):
            if ($column !== "dados_value" && $dados['chave'] === "pk"):
                if (isset($this->inputs[$column]) && $this->inputs[$column] > 0):
                    $this->id = $this->inputs[$column];
                    unset($this->inputs[$column]);
                    break;
                endif;
            endif;
        endforeach;
    }

    private function checkTableInInputs() {
        if (!$this->table && isset($this->inputs['t'])):
            $table = (preg_match("/^" . PRE . "/i", $this->inputs['t']) ? "" : PRE) . $this->inputs['t'];
            $this->setTable($table);
            unset($this->inputs['t']);
        endif;
    }

    private function nomeAmigavel($valor) {
        $column = ["title", "urlname", "content", "date"];
        $nome = ["titulo", "link", "descrição", "data"];
        return ucwords(str_replace($column, $nome, $valor));
    }

    private function caracteresInvalidos($name) {
        $this->result = "O campo " . $this->nomeAmigavel($name) . " possui caracteres inválidos.";
        echo $this->result;
        echo $this->result;
        die;
    }
}