<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 15/12/2016
 * Time: 19:46
 */
class auth {

    private $token;

    /**
     * auth constructor.
     */
    public function __construct($token) {
        $auth = new Token();
        if ($auth->existToken($token)):
            $this->token = $auth->getNewToken();
        endif;
    }

    /**
     * @return mixed
     */
    protected function getToken() {
        return $this->token;
    }
}