<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 01/12/2016
 * Time: 14:13
 */
class PalavrasChavePlayers {

    private $media = "filme";
    private $acao;
    private $titulo;
    private $posicao = 0;
    private $tituloAlternativo;
    private $subtitulo;
    private $idioma;
    private $resolucao;
    private $temporada;
    private $episodio = 0;
    private $words;
    private $indice;

    /**
     * PalavrasChave constructor.
     * @param $palavra
     */
    public function __construct($palavra) {
        $palavra = trim(mb_strtolower(Check::Codificacao($palavra), "UTF-8"));
        $this->searchWords($palavra);
    }

    /**
     * @return string
     */
    public function getMedia() {
        return $this->media;
    }

    /**
     * @return mixed
     */
    public function getAcao() {
        return $this->acao;
    }

    /**
     * @return string
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * @return mixed
     */
    public function getPosicao() {
        return $this->posicao;
    }

    /**
     * @return string
     */
    public function getTituloAlternativo() {
        return $this->tituloAlternativo;
    }

    /**
     * @return string
     */
    public function getSubtitulo() {
        return $this->subtitulo;
    }

    /**
     * @return mixed
     */
    public function getIdioma() {
        return $this->idioma;
    }

    /**
     * @return mixed
     */
    public function getResolucao() {
        return $this->resolucao;
    }

    /**
     * @return mixed
     */
    public function getTemporada() {
        return $this->temporada;
    }

    /**
     * @return mixed
     */
    public function getEpisodio() {
        return $this->episodio;
    }

    private function setPalavra($palavra) {
        $acentos = array("", "×", "–", "- ", " -", ":", " ?", "|", "/", "\\", "(", ")", "[", "]", "{", "}", "~", " ;", " ,", " .", "=", "_", "+", " &", "*", " %", "$", "#", "@", " !");
        $acentosSpace = array(" - ", "x", "-", " - ", " - ", " : ", " ? ", " | ", " / ", " \\ ", " ( ", " ) ", " [ ", " ] ", " { ", " } ", " ~ ", " ; ", " , ", " . ", " = ", " _ ", " + ", " & ", " * ", " % ", " $ ", " # ", " @ ", " ! ");
        return trim(str_replace(array("     ", "    ", "   ", "  "), " ", str_replace($acentos, $acentosSpace, $palavra)));
    }

    private function searchWords($palavra) {
        $this->words = explode(" ", $this->setPalavra($palavra));
        $this->indice = -1;

        while ($this->next()):
            $this->findWord();
        endwhile;

        $this->getTituloNumero();
        $this->getSubTituloNumero();
    }

    private function findWord() {
        if ($this->isKeyWord()):
            $this->abstraiChave();

        else:
            $this->checkTitle();
        endif;
    }

    private function checkTitle() {

        if (empty($this->titulo)):
            if (!$this->isSeason()):
                if ($this->getWord() === "("):
                    $this->setTituloAlternativo();
                    $this->getTitle();

                else:
                    $this->titulo = $this->getWord();
                    $this->getTitle();

                endif;
            endif;

        elseif (!$this->isSeason() && $this->getWord() === "("):
            $this->setTituloAlternativo();

        endif;
    }

    private function getTitle() {
        if ($this->next()):
            if (!$this->isSeason()):
                if ($this->getWord() === ":" || $this->getWord() === "-" || $this->getWord() === "|"):
                    $this->setSubtitulo();

                elseif ($this->isKeyWord()):
                    $this->abstraiChave();

                elseif ($this->getWord() === "("):
                    $this->setTituloAlternativo();
                    $this->getTitle();

                else:
                    $this->titulo .= " " . $this->getWord();
                    $this->getTitle();

                endif;
            endif;
        endif;
    }

    private function setSubtitulo() {
        if ($this->next()):
            if (!$this->isSeason()):

                if ($this->isKeyWord()):
                    $this->abstraiChave();

                elseif ($this->getWord() === "("):
                    $this->setTituloAlternativo();

                elseif ($this->getWord() === "-" || $this->getWord() === "|"):
                elseif ($this->getWord() === ":"):
                    $this->setSubtitulo();
                else:
                    $this->subtitulo = ($this->subtitulo !== null ? $this->subtitulo . " " . $this->getWord() : $this->getWord());
                    $this->setSubtitulo();

                endif;
            endif;
        endif;
    }

    private function isSeason() {
        $next = (isset($this->words[$this->indice + 1]) ? $this->words[$this->indice + 1] : "");
        $next2 = (isset($this->words[$this->indice + 2]) ? $this->words[$this->indice + 2] : "");

        if (preg_match('/^s*(\d{1,2})[ex](\d{1,2})$/i', $this->getWord(), $matches)):
            $this->temporada = (int)$matches[1];
            $this->episodio = (int)$matches[2];

        elseif (($this->getWord() === "season" || $this->getWord() === "episode") && !empty($next) && is_numeric($next)):
            if ($this->next()):
                if ($this->getWord() === "season"):
                    $this->temporada = (int)$this->getWord();
                else:
                    $this->episodio = (int)$this->getWord();
                endif;
            endif;

        elseif (preg_match('/^(\d{1,2})[°ºª]*$/i', $this->getWord(), $matches) && !empty($next) && $next === "temporada"):
            if ($this->next()):
                $this->temporada = (int)$matches[1];
            endif;

        elseif (($this->getWord() === "episódio" || $this->getWord() === "episodio" || $this->getWord() === "ep") && !empty($next) && is_numeric($next)):
            if ($this->next()):
                $this->episodio = (int)$this->getWord();
            endif;

        elseif (($this->getWord() === "todas" || $this->getWord() === "toda") && !empty($next) && ((($next === "as" || $next === "a") && !empty($next2) && ($next2 === "temporadas" || $next2 === "temporada")) || ($next === "temporadas" || $next === "temporada"))):
            $this->next();
            if ($this->getWord() === "as" || $this->getWord() === "a"):
                $this->next();
            endif;
        else:
            return false;
        endif;

        $this->media = "serie";

        return true;
    }

    private function setTituloAlternativo() {
        if ($this->next()):
            while ($this->getWord() !== ")"):
                if (!$this->isSeason()):
                    $this->tituloAlternativo = ($this->tituloAlternativo === null ? $this->getWord() : $this->tituloAlternativo . " " . $this->getWord());
                    if (!$this->next()):
                        break;
                    endif;
                endif;
            endwhile;
        endif;
    }

    private function abstraiChave() {
        $chave = $this->getWord();

        if (in_array($chave, array("legendado", "dublado", "portugues", "português", "inglês", "ingles", "espanhol", "espanõl", "traduzido"))):
            $this->idioma[] = (in_array($chave, array("legendado", "inglês", "ingles", "espanhol", "espanõl", "traduzido")) ? "legendado" : "dublado");

        elseif (!$this->acao && in_array($chave, array("assistir", "download", "online", "ver", "olhar", "baixar", "abaixar", "torrent"))):
            $this->acao = (in_array($chave, array("assistir", "ver", "online", "olhar")) ? "assitir" : "download");

        elseif (!$this->resolucao && in_array($chave, array("blue-ray", "ld", "360", "480", "sd", "hd", "720", "1080", "fullhd", "uhd", "2160", "4k", "360p", "480p", "720p", "1080p", "2160p"))):
            $this->resolucao = str_replace(array("blue-ray", "ld", "360", "480", "sd", "hd", "720", "1080", "fullhd", "uhd", "2160", "4k", "360pp", "480pp", "720pp", "1080pp", "2160pp"), array("720p", "360p", "360p", "480p", "480p", "720p", "720p", "1080p", "1080p", "2160p", "2160p", "2160p", "360p", "480p", "720p", "1080p", "2160p"), $chave);

        elseif (!$this->media && in_array($chave, array("série", "serie", "filme", "anime", "seriado", "movie"))):
            $this->media = str_replace(array("série", "seriado", "movie"), array("serie", "serie", "filme"), $chave);
        endif;
    }

    private function getTituloNumero() {
        if (preg_match('/ (\d)$/i', $this->titulo, $matches)):
            $this->posicao = (int)$matches[1];
            $this->titulo = trim(str_replace($this->posicao, "", $this->titulo));
        endif;
    }

    private function getSubTituloNumero() {
        if (preg_match('/ (\d)$/i', $this->subtitulo, $matches) || preg_match('/^(\d) /i', $this->subtitulo, $matches)):
            $this->posicao = (int)$matches[1];
            $this->subtitulo = trim(str_replace($this->posicao, "", $this->subtitulo));
        endif;
    }

    private function next() {
        $this->indice++;
        return isset($this->words[$this->indice]);
    }

    private function getWord() {
        return $this->words[$this->indice];
    }

    private function isKeyWord() {
        $chaves = array("blu-ray", "dvd", "série", "serie", "filme", "anime", "seriado", "netflix", "movie", "assistir", "watch", "download", "dublado", "legendado", "hd", "online", "gratis", "full", "fullhd", "sd", "ld", "480p", "360p", "2160p", "uhd", "480", "720p", "1080p", "4k", "360");
        return in_array($this->getWord(), $chaves) && $this->notIsExcecion();
    }

    private function notIsExcecion() {
        $indice = $this->indice;
        if ($this->getWord() === "full" && $this->words[$indice + 1] !== "hd"):
            return false;
        elseif ($this->getWord() === "filme" && $this->words[$indice - 1] === "o"):
            return false;
        elseif ($this->getWord() === "watch" && $this->words[$indice + 1] === "dogs"):
            return false;
        elseif ($this->getWord() === "movie" && $this->words[$indice - 1] === "the"):
            return false;
        elseif (($this->getWord() === "serie" || $this->getWord() === "série") && $this->words[$indice - 1] === "a"):
            return false;
        endif;

        return true;
    }

}