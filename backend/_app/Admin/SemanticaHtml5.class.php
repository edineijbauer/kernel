<?php

abstract class SemanticaHtml5 {

    private $content;
    private $tags;
    private $pilha;
    private $posicao;
    private $indicePilha;
    private $indicePosicaoPilha;
    private $token;
    private $tokenAux;
    private $indiceContent;
    private $max;
    private $change;
    private $erro;
//    private $headers = ["meta", "link", "!doctype", "base", "basefont"];
    private $unClosedTag = ["!--", "br", "hr", "img", "input", "meta", "link", "!DOCTYPE", "!doctype", "area", "base", "basefont", "col", "option", "embed", "frame", "keygen", "param", "source", "track"];
    private $endTag = [">", " ", "\n", "\t", "\r", "\f", "\v", "/"];

    function __construct($content) {
        $this->indicePosicaoPilha = array();
        $this->tags = array();
        $this->pilha = array();
        $this->change = false;
        $this->erro = "";
        $this->token = "";
        $this->tokenAux = "";
        $this->indicePilha = -1;
        $this->indiceContent = -1;
        $this->posicao = -1;

        $this->receiveContent($content);

        if (empty($this->erro)):
            if (strlen($this->content) > 3):
                $this->max = strlen($this->content);
            else:
                $this->erro = "Conteúdo passado muito curto ou vazio";
            endif;
        endif;
    }

    public function getHtml() {
        return html_entity_decode(htmlspecialchars_decode($this->content));
    }

    protected function getUnClosedTag() {
        return $this->unClosedTag;
    }

    protected function getEndTag() {
        return $this->endTag;
    }

    protected function haveChange() {
        return $this->change;
    }

    protected function getChar() {
        return ($this->indiceContent < $this->max ? $this->content[$this->indiceContent] : "");
    }

    protected function getErro() {
        return $this->erro;
    }

    protected function getToken() {
        return $this->token;
    }

    protected function addToken() {
        $this->token .= $this->getChar();
    }

    protected function removeToken() {
        $this->token = "";
    }

    protected function getTokenAux() {
        return $this->tokenAux;
    }

    protected function setTokenAux($tokenAux) {
        if (!empty($tokenAux)):
            $this->tokenAux = $tokenAux;
        endif;
    }

    protected function comment() {
        while (0 === 0):
            if ($this->next()):
                if ($this->getChar() === "-"):
                    if ($this->next()):
                        if ($this->getChar() === "-"):
                            if ($this->next()):
                                if ($this->getChar() === ">"):
                                    break;
                                endif;
                            endif;
                        endif;
                    endif;
                endif;
            endif;
        endwhile;
        $this->token = "";
    }

    protected function addAttr($indice, $name, $value) {
        if (!empty($name) && !empty($indice) && !empty($value)):
            $this->tags[$indice][$name] = $value;
        endif;
    }

    protected function addContent($indice, $value) {
        if (!empty($indice) && !empty($value)):
            $this->tags[$indice]['conteudo'] = $value;
        endif;
    }

    protected function addAttrName($name) {
        if (!empty($name)):
            $this->tokenAux = $name;
            $this->tags[$this->posicao][$this->tokenAux] = "";
            $this->token = "";
        endif;
    }

    protected function addAttrValue($value) {
        if (!empty($value) && !empty($this->tokenAux)):
            if ($this->tokenAux === "class"):
                if (preg_match('/ /i', $value)):
                    $classValue = explode(" ", $value);
                    foreach ($classValue as $v):
                        if (!empty($v)):
                            if (isset($this->tags[$this->posicao][$this->tokenAux])):
                                unset($this->tags[$this->posicao][$this->tokenAux]);
                            endif;
                            $this->tags[$this->posicao][$this->tokenAux][trim($v)] = trim($v);
                        endif;
                    endforeach;
                else:
                    if (isset($this->tags[$this->posicao][$this->tokenAux])):
                        unset($this->tags[$this->posicao][$this->tokenAux]);
                    endif;
                    $this->tags[$this->posicao][$this->tokenAux][trim($value)] = trim($value);
                endif;

            elseif ($this->tokenAux === "style"):
                foreach (explode(";", $value) as $s):
                    if (preg_match('/:/i', $s)):
                        $s = explode(":", trim($s))[0];

                        if (isset($this->tags[$this->posicao]['style'])):
                            unset($this->tags[$this->posicao]['style']);
                        endif;
                        $this->tags[$this->posicao]['style'][trim($s)] = trim($s);
                    endif;
                endforeach;

            else:
                $this->tags[$this->posicao][$this->tokenAux] = $value;
            endif;
        endif;
        $this->token = "";
        $this->tokenAux = "";
    }

    public function getTags() {
        return $this->tags;
    }

    protected function getTag($posicao) {
        return (isset($this->tags[$posicao]) ? $this->tags[$posicao] : null);
    }

    protected function getPosicao() {
        return $this->posicao;
    }

    protected function addTags() {
        $this->posicao++;
        $this->tags[$this->posicao]['position'] = $this->posicao;
        $this->tags[$this->posicao]['tag'] = $this->token;

        if (!in_array($this->token, $this->unClosedTag)):
            $this->indicePilha++;
            $this->pilha[$this->indicePilha] = $this->token;
            $this->indicePosicaoPilha[$this->indicePilha] = $this->posicao;
        endif;

        $this->token = "";
    }

    protected function openContentTag() {
        if ($this->indicePilha > -1 && !isset($this->tags[$this->indicePosicaoPilha[$this->indicePilha]]['conteudo']) && !in_array($this->pilha[$this->indicePilha], $this->unClosedTag)):
            $this->tags[$this->indicePosicaoPilha[$this->indicePilha]]['conteudo'] = "";
        endif;
    }

    protected function getPilha() {
        return $this->pilha;
    }

    protected function removePilha() {
        $num = (strlen($this->pilha[$this->indicePilha]) + 3) * -1;
        $this->tags[$this->indicePosicaoPilha[$this->indicePilha]]['conteudo'] = trim(substr($this->tags[$this->indicePosicaoPilha[$this->indicePilha]]['conteudo'], 0, $num));
        unset($this->pilha[$this->indicePilha]);
        $this->indicePilha--;
    }

    private function receiveContent($content) {
        $content_back = $content;
        $i = 0;
        while (preg_match('/(\&amp;lt;|\&amp;|\&lt;|\&gt;)/i', $content) || $i > 7):
            $content = $content_back;

            if ($i === 0):
                $content = htmlspecialchars($content);
            elseif ($i === 1):
                $content = htmlentities($content);
            elseif ($i === 2):
                $content = htmlspecialchars(htmlentities($content));
            elseif ($i === 3):
                $content = html_entity_decode($content);
            elseif ($i === 4):
                $content = htmlspecialchars_decode($content);
            elseif ($i === 5):
                $content = htmlspecialchars_decode(html_entity_decode($content));
            elseif ($i === 6):
                $content = htmlentities(htmlspecialchars($content));
            elseif ($i === 7):
                $content = html_entity_decode(htmlspecialchars_decode($content));
            endif;

            if (strlen($content) < 3):
                $content = $content_back;
            endif;

            $i++;
        endwhile;

        if ($i < 8):
            $this->content = $content;
        else:
            $this->erro = "Não foi possível decodificar o HTML recebido";
        endif;
    }

    protected function next() {
        $this->indiceContent++;

        for ($i = $this->indicePilha; $i > -1; $i--):
            if (isset($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'])):
                $this->tags[$this->indicePosicaoPilha[$i]]['conteudo'] .= $this->getChar();
            endif;
        endfor;

        return $this->indiceContent < $this->max;
    }

    /**
     * não é esperado uma tag de fechamento, mas encontrou
     * corrige adicionando uma tag afrente da que fechou
     */
    protected function corrigeHtmlTagCloseFaltando() {
        $position = $this->indiceContent - strlen($this->getToken()) - 2;
        $this->content = substr_replace($this->content, "<{$this->getToken()}>", $position, 0);

        for ($i = $this->indicePilha; $i > -1; $i--):
            if (isset($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'])):
                $this->tags[$this->indicePosicaoPilha[$i]]['conteudo'] .= "<{$this->getToken()}>";
            endif;
        endfor;

        $this->change = true;
        $this->indiceContent += strlen($this->getToken()) + 1;
        $this->max = strlen($this->content);
    }

    /**
     * esperado fechar uma tag específica, mas encontrou outra em vez
     */
    protected function corrigeHtmlTagCloseErrada() {
        if ($this->pilha[$this->indicePilha] !== $this->getToken()):
            if (in_array($this->getToken(), $this->pilha)):
                $this->corrigeHtmlTagCloseErradaAdicionando();
                $this->removePilha();
            else:
                $this->corrigeHtmlTagCloseErradaExcluindo();
            endif;
        else:
            $this->removePilha();
        endif;
    }

    /**
     * remove tag não esperada.
     */
    protected function corrigeHtmlTagCloseErradaExcluindo() {
        $position = $this->indiceContent - strlen($this->getToken()) - 2;
        $tamanhoTag = strlen($this->getToken()) + 3;
        $this->content = substr_replace($this->content, '', $position, $tamanhoTag);

        for ($i = $this->indicePilha; $i > -1; $i--):
            if (isset($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'])):
                $this->tags[$this->indicePosicaoPilha[$i]]['conteudo'] = substr_replace($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'], '', $tamanhoTag*-1);
            endif;
        endfor;

        $this->change = true;
        $this->indiceContent -= $tamanhoTag;
        $this->max = strlen($this->content);
    }

    /**
     * corrige fechando a tag esperada, e volta o ponteiro para ler novamente a tag inesperada.
     */
    protected function corrigeHtmlTagCloseErradaAdicionando() {
        $position = $this->indiceContent - strlen($this->getToken()) - 2;
        $this->content = substr_replace($this->content, "</{$this->pilha[$this->indicePilha]}>", $position, 0);
        $tamanhoTag = (strlen($this->getToken()) + 3) *-1;

        for ($i = $this->indicePilha; $i > -1; $i--):
            if (isset($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'])):
                $this->tags[$this->indicePosicaoPilha[$i]]['conteudo'] = substr_replace($this->tags[$this->indicePosicaoPilha[$i]]['conteudo'], "</{$this->pilha[$this->indicePilha]}>", $tamanhoTag);
            endif;
        endfor;

        $this->change = true;
        $this->indiceContent -= strlen($this->getToken()) - strlen($this->pilha[$this->indicePilha]);
        $this->max = strlen($this->content);
    }

    /**
     * Verifica se possui alguma tag que não foi fechada após concluir a varredura do html
     */
    protected function checkErros() {
        if (!empty($this->pilha)):
            //ainda possui tags na pilha
            $this->change = true;

            while ($this->indicePilha > -1):
                $this->content .= "</{$this->pilha[$this->indicePilha]}>";
                $this->removePilha();
            endwhile;
        endif;
    }
}
