<?php

class Semantica extends SemanticaHtml5 {

    private $tag;

    function __construct($content) {
        parent::__construct($content);
        $this->exeRead();
    }

    public function getTagById($id) {
        $this->tag = null;
        foreach ($this->getTags() as $position => $tag):
            if (isset($tag['id']) && $tag['id'] === $id):
                $this->tag = $this->getTag($position);
                break;
            endif;
        endforeach;
    }

    public function getTagByClass($class, $indice = null) {
        $this->tag = null;
        $indice = ($indice && $indice > 0 ? $indice - 1 : null);
        $i = 0;
        foreach ($this->getTags() as $position => $tag):
            if (!$this->tag && isset($tag['class']) && is_array($tag['class'])):
                foreach ($tag['class'] as $c):
                    if (!$this->tag && $c === $class):
                        if (!$indice || ($indice && $indice === $i)):
                            $this->tag = $this->getTag($position);
                            break;
                        endif;
                        $i++;
                    endif;
                endforeach;
            endif;
        endforeach;
    }

    public function getTagByAttr($attr, $value, $indice = null) {
        $this->tag = null;
        $indice = ($indice && $indice > 0 ? $indice - 1 : null);
        $i = 0;
        foreach ($this->getTags() as $position => $tag):
            if (!$this->tag && isset($tag[$attr]) && $tag[$attr] === $value):
                if (!$indice || ($indice && $indice === $i)):
                    $this->tag = $this->getTag($position);
                    break;
                endif;
                $i++;
            endif;
        endforeach;
    }

    public function getTagByName($name, $indice = null) {
        $this->tag = null;
        $indice = ($indice && $indice > 0 ? $indice - 1 : null);
        $i = 0;
        foreach ($this->getTags() as $position => $tag):
            if (!$this->tag && isset($tag['tag']) && $tag['tag'] === $name):
                if (!$indice || ($indice && $indice === $i)):
                    $this->tag = $this->getTag($position);
                    break;
                endif;
                $i++;
            endif;
        endforeach;
    }

    public function getTagByPosition($posicao) {
        $this->tag = $this->getTag($posicao);
    }

    public function getConteudo() {
        return $this->getAttr("conteudo");
    }

    public function getAttr($attr) {
        return ($this->tag ? (isset($this->tag[$attr]) ? $this->tag[$attr] : "") : "");
    }

    public function getTagName() {
        return $this->getAttr("tag");
    }

    private function exeRead() {
        while ($this->next()):
            if ($this->getChar() === "<"):
                $this->openTag();
            endif;
        endwhile;

        $this->checkErros();
    }

    private function openTag() {
        if ($this->next()):

            if ($this->getToken() === "!--"):
                $this->comment();

            else:

                if ($this->getChar() === "/" && empty($this->getToken())):
                    $this->closeTag();

                elseif (in_array($this->getChar(), $this->getEndTag())):

                    //nome da tag definido
                    $this->addTags();
                    $tipo = $this->getToken();

                    while ($this->getChar() !== ">"):
                        if ($this->getChar() !== "/"):
                            $this->getAttrName();
                        else:
                            $this->next();
                        endif;
                    endwhile;

                    $this->openContentTag();

                    if ($tipo === "script" || $tipo === "style"):
                        while (!preg_match("/<\/{$tipo}>/i", $this->getTag($this->getPosicao())['conteudo'])):
                            if (!$this->next()):
                                break;
                            endif;
                        endwhile;

                        $this->removePilha();

                    endif;

                else:

                    //absorve nome da tag
                    $this->addToken();
                    $this->openTag();
                endif;
            endif;
        endif;
    }

    private function getAttrName() {
        if ($this->next()):
            if (preg_match('/[a-zA-Z-]/i', $this->getChar())):
                $this->addToken();

            elseif (preg_match('/\s/i', $this->getChar()) || $this->getChar() === ">"):
                $this->addAttrName($this->getToken());

            elseif ($this->getChar() === "="):
                $this->addAttrName($this->getToken());
                $this->getAttrValue();
            endif;
        endif;
    }

    private function getAttrValue() {
        if ($this->next()):
            if (preg_match('/\s/i', $this->getChar())):
                $this->getAttrValue();
            else:

                if ($this->getChar() === "'" || $this->getChar() === '"'):
                    $this->getAttrValueDelimitor($this->getChar());
                else:
                    $this->addToken();
                    $this->getAttrValueNoDelimitor();
                endif;

                $this->addAttrValue($this->getToken());
            endif;
        endif;
    }

    private function getAttrValueNoDelimitor() {
        if ($this->next()):
            while (!in_array($this->getChar(), $this->getEndTag()) && $this->getChar() !== "/"):
                $this->addToken();
                if (!$this->next()):
                    break;
                endif;
            endwhile;
        endif;
    }

    private function getAttrValueDelimitor($delimiter) {
        if ($this->next()):
            while ($this->getChar() !== $delimiter):
                $this->addToken();
                if (!$this->next()):
                    break;
                endif;
            endwhile;
        endif;
    }

    private function closeTag() {
        if ($this->next()):

            if (in_array($this->getChar(), $this->getEndTag())):

                //fechamento de tag

                if (empty($this->getPilha())):

                    $this->corrigeHtmlTagCloseFaltando();

                elseif (!in_array($this->getToken(), $this->getUnClosedTag())):

                    $this->corrigeHtmlTagCloseErrada();

                endif;

                $this->removeToken();

            else:

                $this->addToken();
                $this->closeTag();

            endif;
        endif;
    }

}
