<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 13/11/2016
 * Time: 17:20
 */
class StyleSemantica {

    private $content;
    private $indice;
    private $max;
    private $style;

    /**
     * StyleSemantica constructor.
     * @param String
     */
    public function __construct($styleContent) {
        $this->content = $styleContent;
        $this->max = strlen($this->content);
        $this->style = array();
        $this->indice = -1;
    }

    public function exeRead() {
        $token = "";
        while ($this->next()):
            if (preg_match("/[@\w\s*,.#:-]/i", $this->getChar())):
                $token .= $this->getChar();
            elseif ($this->getChar() === "{"):
                $this->getValues($token);
                $token = "";
            endif;
        endwhile;

        $this->checkErros();
    }

    public function getStyles(){
        return $this->style;
    }

    public function getStyle($tag){
        return $this->style[$tag];
    }

    private function getValues($tags) {
        $token = "";
        if ($this->next()):
            while ($this->getChar() !== "}"):

                $token .= $this->getChar();

                if (!$this->next()):
                    break;
                endif;
            endwhile;

            $this->addStyle($tags, $token);
        endif;
    }

    private function addStyle($tags, $styles) {
        $listStyle = array();
        foreach (explode(";", $styles) as $style) {
            if (preg_match("/:/i", $style)):
                $style2 = explode(':', trim($style));
                $listStyle[trim($style2[0])] = trim($style2[1]);
            endif;
        }
        foreach (explode(",", $tags) as $tag) {
            $this->style[$tag] = $listStyle;
        }
    }

    private function getChar() {
        return $this->content[$this->indice];
    }

    private function next() {
        $this->indice++;

        if ($this->indice < $this->max && $this->getChar() === "/"):
            if ($this->indice + 1 < $this->max && $this->content[$this->indice + 1] === "*"):
                $this->indice++;
                $this->comentario();
                $this->indice++;
            endif;
        endif;

        return $this->indice < $this->max;
    }

    private function comentario() {
        while (0 === 0):
            $this->indice++;
            if ($this->indice < $this->max):
                if ($this->getChar() === "*"):
                    if ($this->indice + 1 < $this->max && $this->content[$this->indice + 1] === "/"):
                        $this->indice++;
                        break;
                    endif;
                endif;
            else:
                break;
            endif;
        endwhile;
    }

    private function checkErros() {

    }
}