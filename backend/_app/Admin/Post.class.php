<?php

/**
 * Post [ MODEL ]
 * Cria Lista com informações do banco para criação, edição e exclusão.
 *
 * @copyright (c) 2015, Edinei J. Bauer [NENA PRO]
 */
class Post {

    private $id;
    private $retorno;
    private $banco;
    private $title;

    /**
     * @param mixed $retorno
     */
    public function setRetorno($retorno) {
        $this->retorno = $retorno;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @param string $banco
     */
    public function setBanco($banco) {
        $this->banco = $banco;
        $this->title = Check::getNameBanco($banco);
    }

    public function getResult() {
        if ($this->banco):

            $bancoInfo = new BancoGetTable();
            $bancoInfo->setTable($this->banco);
            if ($this->id && $this->haveAccessToEdit($this->id)):
                $bancoInfo->setId($this->id);
            endif;

            $this->startParameters($bancoInfo->getKey());
        else:
            echo "falta informação para poder editar";
        endif;
    }

    private function startParameters($bancoInfo) {
        $input = new Input();
        $input->setRetorno($this->retorno);
        $input->setPrefixo(str_replace(PRE, "", $this->banco));
        $input->setInfoTable($bancoInfo);
        $input->setTitle($this->title);

        if (file_exists('../../tpl/post/' . str_replace(PRE, '', $this->banco) . '.tpl.html')):
            $inputs = $input->getInput();
            $inputs[8000] = $input->getSaveButton();
            $inputs[9000] = $input->getResultJavascript();
            $this->show($inputs);
        else:

            $input->show();
        endif;
    }

    private function haveAccessToEdit($id) {
        if (!ISADMIN && $this->banco == PRE . "user" && $_SESSION['userlogin']['id'] != $id):
            //bloqueia a edição de um usuário que não seja você, considerando que você também não seja o administrador
            WSErro("Opps! Você não tem acesso para isso!", WS_ERROR);
            return false;
        endif;

        return true;
    }

    private function show($inputs) {
        $tpl = new Tpl();
        $tpl->Show($inputs, $tpl->Load("post/" . str_replace(PRE, '', $this->banco)));
    }

}
