<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 26/12/2016
 * Time: 20:08
 */
class Input {

    private $title;
    private $infoTable;
    private $pk;
    private $input;
    private $prefixo;
    private $table;
    private $retorno;
    private $saveFunction;
    private $javascriptCKEditor;

    private $scriptValuesSave;
    private $resultJavascript;
    private $css;
    private $editor;

    /**
     * @param mixed $infoTable
     */
    public function setInfoTable($infoTable) {
        $this->infoTable = $infoTable;
        $this->setInputFields();
        $this->saveFunction = $this->geraSaveFunctionName();
    }

    /**
     * @return mixed
     */
    public function getInput() {
        return $this->input;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getScriptValuesSave() {
        return $this->scriptValuesSave;
    }

    /**
     * @return mixed
     */
    public function getResultJavascript() {
        $this->applySaveJavaScript();
        return $this->resultJavascript;
    }

    /**
     * @return mixed
     */
    public function getCss() {
        return $this->css;
    }

    /**
     * @return bool
     */
    public function isEditor() {
        return $this->editor;
    }

    /**
     * @param mixed $retorno
     */
    public function setRetorno($retorno) {
        $this->retorno = $retorno;
    }

    public function getSaveButton() {
        $retorno = '<button style="margin:3px 0;" class="fl-right font-size11 font-bold border-greenhover transition-fast btn btn-primary" id="btnSavePost" onclick="' . $this->saveFunction . '();">' . (isset($this->infoTable[$this->table]['id']) && $this->infoTable[$this->table]['id']['value'] === 0 ? 'Salvar' : 'Atualizar') . '</button>';

        foreach ($this->infoTable[$this->table] as $info => $d):
            if ($info === "urlname"):
                $retorno .= '<a target="_blank" href="' . HOME . '/' . $d['value'] . '/" style="margin:3px 5px 3px 0;" class="fl-right font-size11 font-bold border-bluehover transition-fast btn hover btn-staticwhite"><i class="shoticon shoticon-eyesb shoticon-button"></i></a>';
            endif;
        endforeach;

        return $retorno . "</div>";
    }

    private function geraSaveFunctionName() {
        return "save" . md5($this->table) . rand(0, 9999999);
    }

    /**
     * @param mixed $prefixo
     */
    public function setPrefixo($prefixo) {
        $this->prefixo = $prefixo;
    }

    public function show() {
        echo $this->getSaveButton() . implode("<div class='container pd-small clear'></div>", $this->getInput());

        echo "<div class='container pd-content30'></div>";

        echo $this->getResultJavascript();
    }

    private function encode($s) {
        return $s;
        //        return str_replace(array("===", "==", "="), array(md5("==="), md5("=="), md5("=")), base64_encode($s));
    }

    private function setInputFields() {
        foreach ($this->infoTable as $table => $infoTable):
            $this->table = $table;

            foreach ($infoTable as $coluna => $dados):
                if ($coluna !== "dados_value" && $this->isEditable($dados)):
                    $dados['id'] = $this->createIdInput($coluna, $dados);

                    $this->setJsSave($coluna, $dados);

                    $customInput = new InputChave();
                    $customInput->setTable($table);
                    $customInput->setColumn($coluna);
                    $customInput->setDados($dados);
                    $customInput->setRetorno($this->retorno);
                    $resultCustom = $customInput->getResult();

                    if (!$resultCustom):
                        if (!$this->checkComentarioColumToInputType($dados, $coluna)):
                            if (!$this->checkNameColumToInputType($coluna, $dados)):
                                $this->checkTypeColumToInputType($coluna, $dados);
                            endif;
                        endif;
                    else:
                        $this->input[] = $resultCustom;
                    endif;

                endif;
            endforeach;

            if (isset($infoTable['dados_value'])):
                if (isset($infoTable['dados_value']['oneToMany'])):
                    foreach ($infoTable['dados_value']['oneToMany'] as $tables => $dados):
                        $this->input[] = $this->checkOneToMany($tables, $dados);
                    endforeach;
                endif;
                if (isset($infoTable['dados_value']['manyToMany'])):
                    foreach ($infoTable['dados_value']['manyToMany'] as $tabless => $dados):
                        $this->input[] = $this->checkManyToMany($tabless, $dados);
                    endforeach;
                endif;
            endif;

        endforeach;
    }

    private function checkOneToMany($table, $dados) {
        $column = $dados['dados_value']['column_link'];
        $dados = $dados[$column];

        $dados[$column]['id'] = $this->createIdInput($column, $dados);
        $this->setJsSave($column, $dados);

        $oneToMany = new InputOneToMany();
        $oneToMany->setTable($table);
        $oneToMany->setDados($dados);

        return "<div class='container pd-small'>{$oneToMany->getResult()}</div>";
    }

    private function checkManyToMany($table, $dados) {
        $key = str_replace(PRE, "", $table);
        $input = '<input type="hidden" id="' . $dados['id'] . '" value="' . ($dados['value'] ? ",{$dados['value']}" : "") . '," /><div class="container pd-small mg-bottom"><div class="container pd-small font-light font-size13 mg-bottom"><i class="shoticon shoticon-tag-pure"></i>' . Check::getNameBanco($table) . '</div><div class="container pd-small"><input type="text" placeholder="buscar..." id="' . $key . '_field" class="pd-small marcador-input" rel="' . $key . '" alt="' . $dados['id'] . '" /></div><div id="div_' . $key . '_field" class="div-marcador-search""></div>';
        $input .= $this->checkValues($dados['value'], $key) . '<div class="container boxtag pd-small" id="box' . $key . '">';

        $input .= $this->getValuesMarcadores($dados['value'], $key);

        $read = new Read();
        $read->ExeRead($table, "ORDER BY id DESC LIMIT 20");
        if ($read->getResult()):
            foreach ($read->getResult() as $cc):
                $bgcolor = (isset($bgcolor) && $bgcolor == 'bg-white' ? 'bg-secondary' : 'bg-white');
                $input .= '<div onclick="choiceTags(' . $cc['id'] . ', \'' . $key . '\', \'' . $dados['id'] . '\')" class="container smart pd-small ' . $bgcolor . '"><div class="fl-left nomargin">' . $cc['title'] . '</div></div>';
            endforeach;
        endif;

        return $input . '</div></div><div class="container pd-medium clear"></div>';
    }

    private function createIdInput($coluna, $dado) {
        return "i" . md5(rand(0, 99999) . $dado['posicao'] . rand(0, 99999) . $coluna);
    }

    private function setJsSave($coluna, $dado) {
        $ordem = 1;
        $table = $this->table;

        if ($dado['chave'] === "fkn"):
            $ordem = 3;
            $table = $dado['tableMiddle'];
            $coluna = $dado['columnTarget'];

        elseif ($dado['chave'] === "pk"):

            $this->pk['name'] = $coluna;
            $this->pk['value'] = $dado['value'];
            $this->pk['id'] = $dado['id'];
        endif;

        $this->setJsSaveFunction($ordem, $table, $dado, $coluna);
    }

    private function setJsSaveFunction($ordem, $table, $dado, $coluna) {
        if (isset($this->scriptValuesSave[$ordem][$table])):
            $this->scriptValuesSave[$ordem][$table] .= ", ";
        else:
            if ($ordem === 3):
                $this->scriptValuesSave[$ordem][$table] = "'{$dado['columnOrigin']}': $('#{$this->pk['id']}').val(), ";
            else:
                $this->scriptValuesSave[$ordem][$table] = "";
            endif;
        endif;

        $this->setJsValue($coluna, $table, $dado, $ordem);
    }

    private function setJsValue($coluna, $table, $dado, $ordem) {
        if ('_content' === $coluna || ($dado['tipo'] === 'text' && $dado['tamanho'] > 499)):

            //CKEditor
            $this->scriptValuesSave[$ordem][$table] .= "'" . $this->encode($coluna) . "': CKEDITOR.instances.{$dado['id']}.getData()";
            $this->javascriptCKEditor[] = 'CKEDITOR.replace(\'' . $dado['id'] . '\', {customConfig: \'\'});';
        else:

            //Outros
            $this->scriptValuesSave[$ordem][$table] .= "'" . $this->encode($coluna) . "': $('#{$dado['id']}').val()";
        endif;
    }

    private function applySaveJavaScript() {
        $this->checkCkEditor();

        $this->resultJavascript = "<script>";
        $this->resultJavascript .= "function resultado(g){console.log(g);if(g> 1){infor('Salvo');}else if(g == '1'){infor('Alterações salvas');}else if(g < 0){infor('Enviamos o relatório de alterações ao autor', 3);}else{infor(g);return false;}return true;}";

        $this->applySaveFunctionValues();

        $this->resultJavascript .= "</script> ";

        $this->resultJavascript .= '<script src="' . HOME . '/_app/Admin/controller/Post.js"></script>';
    }

    private function applySaveFunctionValues() {
        $this->resultJavascript .= "function {$this->saveFunction}(){";

        ksort($this->scriptValuesSave);
        foreach ($this->scriptValuesSave as $order => $script):
            foreach ($script as $table => $campos):

                $this->resultJavascript .= "$.post('" . HOME . "/requests/back/Admin/savePost.php', {t: '{$this->encode(str_replace(PRE, "", $table))}', {$campos}}";

                if ($order < 3):
                    $this->resultJavascript .= ", function(g){" . " var id = (resultado(g)? (g < 0 ? g * -1 : g) : 0); if(id > 0) {getPage('{$table}' , 50, id); ";

                    if ($order === 1 && $this->retorno):
                        $this->resultJavascript .= "$.post('../requests/getBanco.php', {banco: '{$table}', id: id, target: 'title'}, function (result) { if(result) { selectOneToOne('{$this->table}', '{$this->retorno}', result, id); } else { selectOneToOne('{$this->table}', '{$this->retorno}', '', id); } }); closeWindow('{$this->table}');";
                    endif;

                    $this->resultJavascript .= "}}";
                endif;

                $this->resultJavascript .= ");";

            endforeach;
        endforeach;

        $this->resultJavascript .= "}";
    }

    private function checkCkEditor() {

        if ($this->isEditor() && $this->javascriptCKEditor):
            echo '<script src="' . HOME . '/_app/Library/ckeditor/ckeditor.js"></script>';

            echo "<script>";
            echo implode(' ', $this->javascriptCKEditor);
            echo "CKEDITOR.config.contentsCss = '../css/styleP.css'; CKEDITOR.config.allowedContent = true;";
            echo "</script>";

            echo '<style>' . '#cke_editor{float:left; width:100%}.cke_button__underline, .cke_button__strike, .cke_button__horizontalrule, .cke_button__blockquote, .cke_button__removeformat, .cke_button__unlink, .cke_toolbar_break, .cke_button__xdsoft_translater_reverse, .cke_button__xdsoft_translater_settings, .cke_button__specialchar, .cke_button__superscript, .cke_button__subscript, .cke_button__image, .cke_button__anchor, .cke_button__table { display: none !important;}.cke_top {background: #F5F5F5; border-bottom: solid 1px #CCC; padding: 5px 5px 2px;}' . '</style>';
        endif;
    }

    private function checkNameColumToInputType($coluna, $dado) {
        if (preg_match('/mail/i', $coluna)):
            $this->input[] = " <label><input type='mail' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'> email</span></label> ";

        elseif (preg_match('/(password|senha)/i', $coluna)):
            $this->input[] = "<label><input type='password' maxlength='{$dado['tamanho']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'> Nova Senha </span></label> ";

        elseif ("urlname" === $coluna):
            $this->input[] = " <div class='container pd-small'><div class='fl-left' title = 'Link'><i class='shoticon shoticon-link shoticon-button'></i><span class='font-size09 font-light'>&nbsp;</span></div><input type='text' rel='url' placeholder='link-de-acesso' class='fl-left radius nopadding noborder' maxlength = '{$dado['tamanho']}' value = '{$dado['value']}' id = '{$dado['id']}' placeholder = 'link de acesso' style = 'width:65%;' /></div><div class='container pd-small'></div> ";

        elseif ("status" === $coluna):
            $this->input[] = '<div class="container pd-small"><div class="font-light pd-small fl-left locksin" title="status"><i class="shoticon shoticon-button shoticon-' . ($dado['value'] === 1 ? "un" : "") . 'lock"></i>&nbsp;</div><select style="background:transparent;border:0" id="' . $dado['id'] . '" class="pd-small font-size11 fl-left radius color-blackgray lockson">' . "<option class='color-gray font-size11' " . ($dado['value'] === 1 ? "selected='selected'" : "") . "value='1'>Ativado</option><option class='color-gray font-size11' " . ($dado['value'] == "0" ? "selected='selected'" : "") . "value='0'>Desativado</option></select><span class='container pd-small'></span></div>";

        elseif ("nota" === $coluna || "star" === $coluna):
            $this->input[] = '<div class="container pd-small">' . Check::StarL($dado['id'], $dado['value'], 5) . '</div>';

        elseif (preg_match('/acesso/i', $coluna)):
            $this->input[] = '<div class="container pd-small"><div class="font-light pd-small fl-left locksin" title="Acesso"><i class="shoticon shoticon-button shoticon-unlock"></i>&nbsp;</div><select style="background:transparent;border:0" id="' . $dado['id'] . '" class="pd-small font-size11 fl-left radius color-blackgray lockson">' . " <option class='color-gray font-size11' value='1'> Publico</option><option class='color-gray font-size11' value = '0'> Requer Login </option></select> " . '<span class="container pd-small"></span></div>';

        elseif (preg_match('/(preco|price|money|dinheiro)/i', $coluna)):
            $this->input[] = " <label><input type='number' min='1' step='any' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'> Preço</span></label> ";

        elseif ("banco" === $coluna):
            $list = array("Posts" => "post", "Categorias" => "category", "Coleções" => "colecao", "Tags" => "tag", "Desenvolvedores" => "frabricante");
            $opt = " <option class='color-blackgray' value=''> Selecione um Banco </option> ";
            foreach ($list as $nome => $banco):
                $opt .= "<option class='color-blackgray' value='{$banco}'>{$nome}</option> ";
            endforeach;

            $this->input[] = "<div class='container pd-small'><div class=\"font-light pd-small fl-left\" title=\"Acesso\"><i class=\"shoticon shoticon-button shoticon-export-pure\"></i>&nbsp;</div><select id='{$dado['id']}' class='color-blackgray noborder pd-small radius'>{$opt}</select></div>";

        elseif ('_content' === $coluna):
            $this->input[] = $this->getInputEditor($coluna, $dado);

        elseif ('cover' === $coluna):

            $img = "<div class='container'><input type='hidden' value='{$dado['value']}' id='{$dado['id']}' /><div class='container pd-medium' id='imgpreview'>";

            $gallery = new Gallery();
            $gallery->setCover($dado['value']);
            $gallery->setPostId($dado['id']);
            $img .= $gallery->getGallery();

            $this->input[] = "<input type='hidden' id='bancoUso' value='{$this->table}' />{$img}<button onclick=\"getPage('sendGallery', 25, {$this->pk['value']}, '{$this->retorno}');\" class='container btn btn-white pd-medium'><i class='shoticon shoticon-gallery'></i>Editar Imagem</button></div></div>";

        else:
            return false;
        endif;

        return true;
    }

    private function checkComentarioColumToInputType($dado, $coluna) {
        switch ($dado['comentario']):
            case 'week':

                $week = [1 => "D", 2 => "S", 3 => "T", 4 => "Q", 5 => "Q", 6 => "S", 7 => "S"];
                $input = "<div class='container pd-small'><div class='container pd-small font-light font-size13'>Dia da Semana</div>";
                for ($i = 1; $i < 8; $i++):
                    $class = ($dado['value'] === $i ? "weekSelected " : "");
                    $input .= "<button class='btn btn-white {$class}pd-medium fl-left border-primaryhover weeks' id='{$dado['id']}-{$i}' onclick=\"selectWeek({$i}, '{$dado['id']}')\" style='margin-left:2px;'>{$week[$i]}</button>";
                endfor;
                $this->input[] = $input . "<input type='hidden' value='{$dado['value']}' id='{$dado['id']}' /></div>";
                return true;
                break;

            case 'month':

                $input = "<div class='container pd-small'><div class='container pd-small font-light font-size13'>Dia do Mês</div>";
                for ($i = 1; $i < 32; $i++):
                    $class = ($dado['value'] === $i ? "weekSelected " : "");
                    $input .= "<button class='btn btn-white {$class}pd-medium fl-left border-primaryhover month' id='{$dado['id']}-{$i}' onclick=\"selectMonth({$i}, '{$dado['id']}')\">" . ($i < 10 ? "0" : "") . "{$i}</button>";
                endfor;
                $this->input[] = $input . "<input type='hidden' value='{$dado['value']}' id='{$dado['id']}' /></div>";
                return true;
                break;

            case 'on':
                $input = '<div class="container pd-small"><div class="fl-left pd-medium font-light font-size13">' . ucwords(str_replace(array('_', '-'), ' ', $coluna)) . '</div><div class="fl-left pd-smallb"><label class="switch nomargin"><input type="checkbox" id="switch-' . $dado['id'] . '" ' . ($dado['value'] == 1 ? 'checked="checked"' : '') . ' onclick="selectOnSwitch(\'' . $dado['id'] . '\')"><div class="slider round"></div></label></div> </div>';
                $this->input[] = $input . "<input type='hidden' value='{$dado['value']}' id='{$dado['id']}' /></div>";
                return true;
                break;

            case 'star':
                $input = '<div class="container pd-small"><div class="fl-left pd-medium font-light font-size13">' . ucwords(str_replace(array('_', '-'), ' ', $coluna)) . '</div></div>';
                $this->input[] = $input . '<div class="container pd-small">' . Check::Star($dado['id'], $dado['value'], 10) . '</div>';

                return true;
                break;

            case 'invisible':
                $this->input[] = "<input type='hidden' rel='url' maxlength = '{$dado['tamanho']}' value = '{$dado['value']}' id = '{$dado['id']}' />";

                return true;
                break;

            case 'level':
                $this->input[] = "<label class='container pd-small'><div class='fl-left pd-small'>função:</div><select style='background:transparent;border:0' class='pd-small font-size11 fl-left radius color-blackgray' id='{$dado['id']}' class='pd-small radius'><option value='0' " . ($dado['value'] === 0 ? "selected='selected'" : "") . ">Cliente</option><option value='1' " . ($dado['value'] === 1 ? "selected='selected'" : "") . ">Ajudante</option><option value='2' " . ($dado['value'] === 2 ? "selected='selected'" : "") . ">Dealer</option><option value='3' " . ($dado['value'] === 3 ? "selected='selected'" : "") . ">Administrador</option></select></label>";

                return true;
                break;

        endswitch;

        return false;
    }

    private function checkTypeColumToInputType($coluna, $dado) {
        $nome = "<span class='font-light'><div class='fl-left'>" . $this->changeNameColum($coluna) . "</div>" . (!empty($dado['comentario']) ? "<i class='fl-left color-blackgray font-light' style='margin-left: 7px;'>(" . Check::Words($dado['comentario'], 5) . ")</i>" : "") . "</span>";

        switch ($dado['tipo']):
            case 'int':
                $max = (pow(10, $dado['tamanho']) - 1);
                $this->input[] = "<label class='container pd-small'><input type='number' min='0' max='{$max}' id='{$dado['id']}' value='{$dado['value']}' class='pd-small radius' />{$nome}</label>";
                break;

            case 'float':
                $this->input[] = "<label class='container pd-small'><input type='number' min='0' step='0.1' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";
                break;

            case "boolean":
                $this->input[] = "<label class='container pd-small'><select id='{$dado['id']}' class='pd-small radius'><option value='0'>INATIVO</option><option value='1'>ATIVO</option></select>{$nome}</label>";
                break;

            case "datetime":
                if (!$dado['value']):
                    $dado['value'] = date("Y-m-d H:i:s");
                endif;
                $this->input[] = "<div class='container pd-small'><div class='fl-left pd-small font-light font-size14' title='adicionar'><i class='shoticon shoticon-calendario'></i>&nbsp;&nbsp;</div><div class='fl-left' id='datehide'><input type='datetime-local' min='1000-01-01 00:00' max='9999-12-31 23:59' value='" . str_replace(' ', 'T', $dado['value']) . "' id='{$dado['id']}' class='pd-small radius' /></div></div>";
                break;

            case "date":
                $this->input[] = "<label class='container pd-small'><input type='date' min='1000-01-01' max='9999-12-31' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";
                break;

            case "time":
                $this->input[] = "<label class='container pd-small'><input type='time' min='00:00:00' max='23:59:59' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";
                break;

            case "year":
                $this->input[] = "<label class='container pd-small'><input type='number' min='1900' max='2155' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";
                break;

            case "text":
                if ($dado['tamanho'] > 499):
                    $this->input[] = $this->getInputEditor($coluna, $dado);
                else:
                    $this->input[] = "<label class='container pd-small'><input type='text' " . ($coluna === "title" ? "rel='title' " : "") . "maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";
                endif;
                break;

            default:
                $this->input[] = "<label class='container pd-small'><input type='text' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' />{$nome}</label>";

        endswitch;
    }

    private function getInputEditor($coluna, $dado) {
        $this->editor = $dado['id'];
        return '<textarea class="' . $coluna . ' container pd-small" placeholder="descrição..." rows="6" id="' . $dado['id'] . '">' . htmlentities($dado['value']) . '</textarea>';
    }

    private function changeNameColum($name) {
        $arr = ['_', '-', "Smart", "smt", "smart", $this->prefixo, 'price', 'title', ' app', 'title', '_content', 'cover', 'views', 'urlname'];
        $narr = [' ', ' ', "", "", "", '', 'preço', 'titulo', '', 'titulo', 'descricão', 'imagem', 'visualizações', 'link'];
        return ucfirst(str_replace($arr, $narr, $name));
    }

    private function isEditable($dado) {
        if ($dado['comentario'] !== "hidden" && ((isset($dado['permissao']) && preg_match('/select/i', $dado['permissao'])) || !isset($dado['permissao']))):
            return true;
        endif;

        return false;
    }
}