<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 14/11/2016
 * Time: 10:11
 *
 * Calcula a insidencia de vezes que a primeira string aparece na segunda
 * retornando um valor simbólico da probabilidade do primeiro elemento estar relacionado com o segundo
 */
class Insidencia {

    private $first;
    private $second;
    private $result;

    /**
     * Insidencia constructor.
     */
    public function __construct($first, $second) {
        $this->first = explode(' ', trim(strtolower($first)));
        $this->second = explode(' ', trim(strtolower($second)));
        $this->compare();
    }

    public function getResult(){
        return $this->result;
    }

    private function compare() {
        //truque de mestre 2 legendado
        $pontos = 0;
        foreach ($this->second as $e => $fi2):
            //truque de mestre
            foreach ($this->first as $i => $fi):
                if ($fi === $fi2):
                    if ($i === $e):
                        $pontos += 2;
                        break;
                    else:
                        $pontos++;
                        break;
                    endif;
                endif;
            endforeach;
        endforeach;

        $this->result = $pontos * 100 / (count($this->second) * 2);

        $first = preg_quote(implode(" ", $this->first));
        if (preg_match("/{$first}/i", implode(" ", $this->second))):
            $this->result += (100 - $this->result) * 0.5;
        endif;
    }
}