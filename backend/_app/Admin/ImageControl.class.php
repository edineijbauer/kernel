<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 14/02/2017
 * Time: 10:48
 */
class ImageControl {

    private $id;
    private $table;
    private $image;
    private $newImage;
    private $dados;
    private $pureImage;
    private $result;
    private $error;

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = (int)$id;

        if (!$this->image):
            $this->setImage(Check::getBanco($this->table, $this->id, "cover"));
        endif;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image = null) {
        if ($image):
            $this->image = $image;
            $this->pureImage = str_replace("/modi7s-", "/", $this->image);
        endif;
    }

    /**
     * @return mixed
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        $this->start();
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getImage() {
        $this->start();
        return $this->setMod($this->pureImage);
    }

    private function setDados() {
        $banco = new Banco("image_control");
        $banco->loadArray(array("gallery_id" => $this->id, "gallery_table" => $this->table));
        if ($banco->exist()):
            $this->dados = $banco->getDados();
        else:
            $this->dados = array("vertical" => 0, "horizontal" => 0, "turn" => 0, "width" => 700);
        endif;
    }

    private function sendToGallery() {
        $folder = explode("/", $this->pureImage);
        $dados['folder'] = $folder[0];
        $dados['urlname'] = $folder[3];
        $dados['title'] = str_replace(array("-", "_"), " ", $folder[3]);
        $dados['user'] = $_SESSION['userlogin']['id'];
        $dados['cover'] = $this->setMod($this->pureImage);
        $dados['version'] = md5(date('Y-m-d H:i:s') . rand(0, 99));

        $banco = new Banco($this->table);
        $banco->load("id", $this->id);
        $read = new Read();
        $read->ExeRead($this->table, "WHERE title = '{$banco->title}' && id != :id", "id={$this->id}");
        if (!$read->getResult()):
            $banco->setDados($dados);
            $this->result = $banco->save();
        else:
            $this->error = "Titulo da imagem já existe";
        endif;
    }

    private function start() {
        if ($this->image && $this->image !== "demo.jpg"):
            $this->setDados();

            if (file_exists('../../../uploads/' . $this->pureImage)):

                $this->imageEdit();

            endif;
        endif;
    }

    private function setMod($image) {
        return str_replace(array('/01/', '/02/', '/03/', '/04/', '/05/', '/06/', '/07/', '/08/', '/09/', '/10/', '/11/', '/12/'), array('/01/modi7s-', '/02/modi7s-', '/03/modi7s-', '/04/modi7s-', '/05/modi7s-', '/06/modi7s-', '/07/modi7s-', '/08/modi7s-', '/09/modi7s-', '/10/modi7s-', '/11/modi7s-', '/12/modi7s-'), $image);
    }

    private function imageEdit() {
        try {
            require_once('../../../_app/Library/wide/lib/WideImage.php');

            $this->newImage = WideImage::load('../../../uploads/' . $this->pureImage);

            $this->checkRotateMove();
            $this->checkHorizontalMove();
            $this->checkVerticalMove();
            $this->checkDaguaMove();
//            $this->checkWidth();

            $this->newImage->saveToFile('../../../uploads/' . $this->setMod($this->pureImage));
            $this->newImage->destroy();

            array_map('unlink', glob("../../../cache/*"));

            $this->sendToGallery();
        } catch (Exception $e) {
            $this->error = "Imagem não é válida";
        }
    }

    private function checkHorizontalMove() {
        $this->newImage = ($this->dados['horizontal'] === 1 ? ($this->dados['turn'] === 1 ? $this->newImage->flip() : $this->newImage->mirror()) : $this->newImage);
    }

    private function checkVerticalMove() {
        $this->newImage = ($this->dados['vertical'] === 1 ? ($this->dados['turn'] === 1 ? $this->newImage->mirror() : $this->newImage->flip()) : $this->newImage);
    }

    private function checkRotateMove() {
        $this->newImage = ($this->dados['turn'] === 1 ? $this->newImage->rotate(90) : $this->newImage);
    }

    private function checkWidth() {
        $w = (int)Check::getBanco(PRE . "padrao", "width-cover", "padrao", "name");
        $h = ($w > 500 ? ($w > 1200 ? ($w > 1599 ? $w * 0.4 : $w * 0.5) : $w * 0.65) : $w);
//        $this->newImage = $this->newImage->resize($w, $w, 'inside');
        $this->newImage = $this->newImage->crop('center', 'center', $w, $h);
    }

    private function checkDaguaMove() {
        $read = new Read();
        $read->ExeRead(PRE . "padrao", "WHERE user_id=:jjv && name = 'daguaCover' && padrao != '' ", "jjv={$_SESSION['userlogin']['id']}");
        if ($read->getResult()):
            $DaguaImg = $read->getResult()[0]['padrao'];
        endif;

        $read->ExeRead(PRE . "padrao", "WHERE user_id=:jjs && name = 'FrameDagua' && padrao != '' ", "jjs={$_SESSION['userlogin']['id']}");
        if ($read->getResult()):
            $DaguaImg = $read->getResult()[0]['padrao'];
            $del = new Delete();
            $del->ExeDelete(PRE . "padrao", "WHERE user_id=:jjd && name = 'FrameDagua' && padrao != '' ", "jjd={$_SESSION['userlogin']['id']}");
        endif;

        if (isset($DaguaImg)):
            $this->checkDaguaImage($DaguaImg);
        endif;
    }

    private function checkDaguaImage($DaguaImg) {
        $DaguaImg = WideImage::load('../../../uploads/' . $DaguaImg);

        //opacidade dagua
        $read = new Read();
        $read->ExeRead(PRE . "padrao", "WHERE user_id=:jj && name = 'opacidadedaguap' && padrao != '' ", "jj={$_SESSION['userlogin']['id']}");
        $opd = (float)($read->getResult() ? $read->getResult()[0]['padrao'] * 100 : 50);

        //horizontal dagua
        $read->ExeRead(PRE . "padrao", "WHERE user_id=:jj && name = 'horizontaldaguap' && padrao != '' ", "jj={$_SESSION['userlogin']['id']}");
        $dh = ($read->getResult() ? $read->getResult()[0]['padrao'] : 'right');

        //vertical dagua
        $read->ExeRead(PRE . "padrao", "WHERE user_id=:jj && name = 'verticaldaguap' && padrao != '' ", "jj={$_SESSION['userlogin']['id']}");
        $dv = ($read->getResult() ? $read->getResult()[0]['padrao'] : 'bottom');

        $this->newImage = $this->newImage->merge($DaguaImg, "{$dh} - 3", "{$dv} - 3", $opd);
        $DaguaImg->destroy();
    }
}