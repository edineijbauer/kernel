<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 09/01/2017
 * Time: 11:26
 */
class ThemeStyle {

    private $style;
    private $elemento;

    /**
     * ThemeStyle constructor.
     */
    public function __construct() {
        $this->style = "";
        $this->start();
        $this->grava();
    }

    private function start() {
        $read = new Read();
        $read->ExeRead(PRE . "theme_style");
        if ($read->getResult()):
            foreach ($read->getResult() as $item):
                if ($item['elemento'] === "sistem"):
                    $this->checkSistemStyle($item['name'], $item['value']);
                else:
                    $this->elemento[$item['elemento']][$item['name']] = $item['value'];
                endif;
            endforeach;

            $this->setElementos();
        endif;

    }

    private function setElementos() {
        foreach ($this->elemento as $elemento => $estilos):
            $this->style .= "{$elemento} {";
            foreach ($estilos as $name => $value):
                $this->style .= trim($name) . ": " . trim($value) . ";";
            endforeach;
            $this->style .= "}";
        endforeach;
    }

    private function checkSistemStyle($name, $value) {
        if (preg_match('/-color$/i', $name)):
            $cor = str_replace('-color', '', $name);
            $this->setColorPrimary($cor, $value);
        endif;
    }

    private function setColorPrimary($cor, $value) {
        $minus = $this->minus($value);
        $this->style .= ".color-{$cor} { color: {$value}!important;}";
        $this->style .= ".color-{$cor}hover:hover, .color-{$cor}hover:hover a{ color: {$value}!important;}";
        $this->style .= ".btn-{$cor} { background-color: {$value}!important; color: " . Helper::corAuxiliar($value) . "!important; border-color: " . $value . "!important;}";
        $this->style .= ".btn-{$cor}:hover { background-color: " . $minus . "!important; border-color: " . $minus . "!important;}";
        $this->style .= ".bg-{$cor} { background-color: {$value}!important; color: " . Helper::corAuxiliar($value) . "!important;}";
        $this->style .= ".border-{$cor} { border-color: " . $value . "!important;}";
        $this->style .= ".border-{$cor}hover:hover { border-color: " . $value . "!important;}";
    }

    /**
     * Deixa uma cor mais escura
     */
    private function plus($v) {
        return "#" . $this->somaHex(str_replace("#", "", $v), -20);
    }

    /**
     * Deixa uma cor mais clara
     */
    private function minus($v) {
        return "#" . $this->somaHex(str_replace("#", "", $v), 20);
    }

    /**
     * soma ou subtrai um valor inteiro de uma cor hexadecimal, valor positivo deixa mais claro, valores negativos deixa mais escuro
     */
    private function somaHex($style, $plus) {
        $style = strlen($style) === 3 ? $style[0] . $style[0] . $style[1] . $style[1] . $style[2] . $style[2] : $style;
        return $this->decimalToHex(hexdec($style[0] . $style[1]) + $plus) . $this->decimalToHex(hexdec($style[2] . $style[3]) + $plus) . $this->decimalToHex(hexdec($style[4] . $style[5]) + $plus);
    }

    /**
     * garante que o valor decimal exista em 2 termos hexadecimais
     */
    private function decimalToHex($v) {
        return dechex($v > 255 ? 255 : ($v < 0 ? 0 : $v));
    }

    /**
     * grava o arquivo de estilo final
     */
    private function grava() {
        $fp = fopen("../../css/theme/smart.css", "w");
        var_dump($this->style);
        $escreve = fwrite($fp, $this->style);
        fclose($fp);
    }
}