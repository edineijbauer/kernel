<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 26/12/2016
 * Time: 17:37
 *
 * Retorna o modelo da tabela de um banco mysql.
 * exemplo: retorna quais os campos uma tabela possui, o nome, o tipo e os limites
 */
class BancoGetTable {

    private $table;
    private $id;
    private $key;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = (preg_match("/^" . PRE . "/i", $table) ? "" : PRE) . $table;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getKey() {
        $bancoInfo[$this->table] = $this->getTableInfo($this->table, $this->id);
        return $this->getForeignKeys($this->table, $bancoInfo);
    }

    private function getTableInfo($table, $id = 0) {
        $db = DBSA;
        $keys = array();
        $readI = new ReadInfo();
        $readI->ExeRead("COLUMNS", "WHERE TABLE_SCHEMA = :nb && TABLE_NAME = :nt", "nb={$db}&nt={$table}");
        if ($readI->getResult()):
            foreach ($readI->getResult() as $g):
                $keys = array_merge($keys, $this->setKeys($id, $table, $g));
            endforeach;
        endif;

        return $keys;
    }

    private function getForeignKeys($table, $bancoInfo) {
        $fk = new ForeignKey();
        $fk->setTable($table);
        $bancoInfo = $this->setOneToOneForeignKey($table, $bancoInfo, $fk->getOneToOne());
        $bancoInfo = $this->setOneToManyForeignKey($table, $bancoInfo, $fk->getOneToMany(), $fk);
        $bancoInfo = $this->setManyToManyForeignKey($table, $bancoInfo, $fk->getManyToMany(), $fk);

        return $bancoInfo;
    }

    private function setOneToOneForeignKey($table, $bancoInfo, $listColumnsOneToOne = null) {
        if ($listColumnsOneToOne):
            foreach ($listColumnsOneToOne as $tables):
                foreach ($tables as $column => $tableName):
                    $bancoInfo[$table][$column]['tipo'] = "hidden";
                    $bancoInfo[$table][$column]['table'] = $tableName;
                    $bancoInfo[$table][$column]['chave'] = "fk";
                    $bancoInfo[$table][$column]['comentario'] = (isset($bancoInfo[$table][$column]['comentario']) ? $bancoInfo[$table][$column]['comentario'] : "");
                    $bancoInfo = $this->checkComment($bancoInfo, $column, $table);
                    $bancoInfo[$table][$column]['value'] = (!isset($bancoInfo[$table][$column]['value']) || $bancoInfo[$table][$column]['value'] === 0 ? null : $bancoInfo[$table][$column]['value']);
                    unset($bancoOneToOneInfo);
                    $bancoOneToOneInfo[$tableName] = $this->getTableInfo($tableName, $bancoInfo[$table][$column]['value']);
                    $bancoInfo[$table][$column] = array_merge($bancoInfo[$table][$column], $this->getForeignKeys($tableName, $bancoOneToOneInfo));
                endforeach;
            endforeach;
        endif;

        return $bancoInfo;
    }

    private function setOneToManyForeignKey($table, $bancoInfo, $listColumnsOneToMany = null, $middle = null) {
        if ($listColumnsOneToMany):
            foreach ($listColumnsOneToMany as $tables):
                foreach ($tables as $column => $tableName):
                    unset($bancoOneToOneInfo);
                    $bancoOneToOneInfo[$tableName] = $this->getTableInfo($tableName, $bancoInfo[$table][$column]['value']);
                    $bancoInfo[$table]['dados_value']['oneToMany'] = (isset($bancoInfo[$table]['dados_value']['oneToMany']) ? array_merge($bancoInfo[$table]['dados_value']['oneToMany'], $this->getForeignKeys($tableName, $bancoOneToOneInfo)) : $this->getForeignKeys($tableName, $bancoOneToOneInfo));
                    $bancoInfo[$table]['dados_value']['oneToMany'][$tableName]['dados_value']['column_link'] = $middle->getColumnTable($table, $tableName);
                    $bancoInfo[$table]['dados_value']['oneToMany'][$tableName]['dados_value']['value'] = $this->getValueOneToMany($middle, $table, $tableName);
                endforeach;
            endforeach;
        endif;

        return $bancoInfo;
    }

    private function getValueOneToMany($middle, $table, $tableName) {
        if ($this->id && $middle):
            $read = new Read();
            $read->ExeRead($tableName, "WHERE {$middle->getColumnTable($table, $tableName)} = :id", "id={$this->id}");
            if ($read->getResult()):
                foreach ($read->getResult() as $item):
                    $valores[] = $item['id'];
                endforeach;

                return implode(',', $valores);
            endif;
        endif;

        return null;
    }

    private function setManyToManyForeignKey($table, $bancoInfo, $listColumnsManyToMany = null, $middle = null) {
        if ($listColumnsManyToMany):

            $middle->setOrigin($table);

            foreach ($listColumnsManyToMany as $tableName):
                $middle->setTarget($tableName);
                $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['tableMiddle'] = $middle->getRelationTable($table, $tableName);
                $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['columnOrigin'] = $middle->getColumnTable($table, $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['tableMiddle']);
                $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['columnTarget'] = $middle->getColumnTable($bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['tableMiddle'], $tableName);
                $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['value'] = $this->getValueManytoMany($middle, null);
                $bancoInfo[$table]['dados_value']['manyToMany'][$tableName] = array_merge($bancoInfo[$table]['dados_value']['manyToMany'][$tableName], $this->getTableInfo($tableName, $bancoInfo[$table]['dados_value']['manyToMany'][$tableName]['dados_value']['value']));
            endforeach;
        endif;

        return $bancoInfo;
    }

    private function setKeys($id, $table, $g) {
        $key[$g['COLUMN_NAME']]['null'] = ($g['IS_NULLABLE'] === "YES" ? true : false);
        $key[$g['COLUMN_NAME']]['tipo'] = $this->getTipo($g['DATA_TYPE']);
        $key[$g['COLUMN_NAME']]['chave'] = $this->getChave($g);
        $key[$g['COLUMN_NAME']]['posicao'] = $g['ORDINAL_POSITION'];
        $key[$g['COLUMN_NAME']]['permissao'] = $g['PRIVILEGES'];
        $key[$g['COLUMN_NAME']]['comentario'] = $g['COLUMN_COMMENT'];
        $key = $this->checkComment($key, $g['COLUMN_NAME']);
        $key[$g['COLUMN_NAME']]['increase'] = ($g['EXTRA'] === "auto_increment" ? true : false);
        $key[$g['COLUMN_NAME']]['tamanho'] = (int)($key[$g['COLUMN_NAME']]['tipo'] === "int" ? explode(")", explode("(", $g['COLUMN_TYPE'])[1])[0] : ($key[$g['COLUMN_NAME']]['tipo'] === "float" ? 11 : $g['CHARACTER_MAXIMUM_LENGTH']));
        $key[$g['COLUMN_NAME']]['codificação'] = $g['CHARACTER_SET_NAME'];
        $key[$g['COLUMN_NAME']]['precisao'] = (int)$g['NUMERIC_PRECISION'];
        $key[$g['COLUMN_NAME']]['value'] = $this->getValue($id, $table, $key, $g['COLUMN_NAME'], $g['COLUMN_DEFAULT'], $key[$g['COLUMN_NAME']]['tipo']);

        return $key;
    }

    private function getValueManytoMany($middle, $default = 0) {
        if ($this->id):
            $middle->setId($this->id);
            return $middle->getResult();
        endif;

        return $default;
    }

    private function getValue($id = null, $table, $key, $coluna, $default, $tipo) {
        if ($id):
            $read = new Read();
            $read->ExeRead($table, "WHERE id = :id", "id={$id}");
            if ($read->getResult()):
                $valor = (isset($read->getResult()[0][$coluna]) ? $read->getResult()[0][$coluna] : ($default ? $default : $this->getDefaultValue($key, $coluna)));
                if ($tipo === "int"):
                    return (int)$valor;
                elseif ($tipo === "float"):
                    return (float)$valor;
                endif;

                return $valor;
            endif;

        else:
            $valor = ($default ? $default : $this->getDefaultValue($key, $coluna));
            if ($tipo === "int"):
                return (int)$valor;
            elseif ($tipo === "float"):
                return (float)$valor;
            endif;

            return $valor;
        endif;

        return "";
    }

    private function checkComment($key, $column, $table = null) {
        $comment = ($table ? $key[$table][$column]['comentario'] : $key[$column]['comentario']);
        if ($comment):
            $new['comentario'] = $comment;

            if (preg_match('/<\?/i', $comment)):
                foreach (explode("<?", $comment) as $i => $dado):
                    if ($i > 0):
                        $new['exe'] = explode('?>', $dado)[0];
                        $new['comentario'] = trim(str_replace('<?' . $new['exe'] . '?>', '', $new['comentario']));
                    endif;
                endforeach;
            endif;

            if (preg_match('/\[/i', $comment)):
                foreach (explode("[", $comment) as $i => $dado):
                    if ($i > 0):
                        $new['allow'] = explode(']', $dado)[0];
                        $new['comentario'] = trim(str_replace('[' . $new['allow'] . ']', '', $new['comentario']));
                    endif;
                endforeach;
            endif;

            if ($table):
                unset($key[$table][$column]['comentario']);
                $key[$table][$column] = array_merge($key[$table][$column], $new);
            else:
                unset($key[$column]['comentario']);
                $key[$column] = array_merge($key[$column], $new);
            endif;
        endif;

        return $key;
    }

    private function getDefaultValue($key, $coluna) {
        switch ($key[$coluna]['tipo']):
            case 'int':
                return 0;
                break;

            case 'float':
                return 0.00;
                break;

            case 'text':
                return "";
                break;

            case 'datetime':
                return null;
                //                return date("Y-m-d") . "T" . date("H:i:s");
                break;

            case 'date':
                return null;
                //                return date("Y-m-d");
                break;

            case 'time':
                return null;
                //                return ((int)date("H")) - 2 . ':' . date("i");
                break;

            case 'year':
                return date("y");
                break;

            case 'boolean':
                return false;
                break;

            default:
                return "";

        endswitch;
    }

    private function getChave($g) {
        switch ($g['COLUMN_KEY']):
            case "PRI":
                return "pk";
            case "MUL":
                return "indice";
            case "UNI":
                return 'unique';
        endswitch;

        return "";
    }

    private function getTipo($tipo) {
        if (in_array($tipo, array("tinyint", "int", "bigint", "smallint", "mediumint", "bit", "serial"))):
            return "int";
        elseif (in_array($tipo, array("decimal", "double", "real", "float"))):
            return "float";
        elseif (in_array($tipo, array("char", "varchar", "tinytext", "mediumtext", "text", "longtext"))):
            return "text";
        endif;

        return $tipo;
    }

}