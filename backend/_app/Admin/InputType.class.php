<?php

/**
 * Created by PhpStorm.
 * User: nenab
 * Date: 26/12/2016
 * Time: 20:08
 */
class InputType {

    private $infoTable;
    private $input;
    private $prefixo;
    private $table;
    private $tableId;

    private $javascript;
    private $css;
    private $editor = false;

    /**
     * @param mixed $infoTable
     */
    public function setInfoTable($infoTable) {
        $this->infoTable = $infoTable;
        $this->setInputFields();
    }

    /**
     * @return mixed
     */
    public function getInput() {
        return $this->input;
    }

    /**
     * @return mixed
     */
    public function getJavascript() {
        return $this->javascript;
    }

    /**
     * @return mixed
     */
    public function getCss() {
        return $this->css;
    }

    /**
     * @return bool
     */
    public function isEditor() {
        return $this->editor;
    }

    public function getSaveButton() {
        $tipo = 0;

        return '<button style="margin:3px 0;" class="fl-right font-size11 font-bold border-greenhover transition-fast btn btn-primary" id="btnSavePost" onclick="savePost();">' . ($tipo === 0 ? 'Publicar' : 'Atualizar') . '</button>' . '<a target="_blank" style="margin:3px 5px 3px 0;" class="fl-right font-size11 font-bold border-bluehover transition-fast btn hover btn-staticwhite"><i class="shoticon shoticon-eyesb shoticon-button"></i></a></div>';

    }

    /**
     * @param mixed $prefixo
     */
    public function setPrefixo($prefixo) {
        $this->prefixo = $prefixo;
    }

    public function show() {
        echo $this->getSaveButton();

        foreach ($this->getInput() as $item):
            echo $item . "<div class='container pd-small clear'></div>";
        endforeach;

        $this->checkCkEditor();
        $this->applySaveJavaScript();

        echo '<script src="' . HOME . '/_app/Admin/controller/Post.js"></script>';
    }

    private function encode($s) {
        return $s;
        //        return str_replace(array("===", "==", "="), array(md5("==="), md5("=="), md5("=")), base64_encode($s));
    }

    private function applySaveJavaScript() {
        echo "<script>function resultado(g){if(g > 1){infor('Salvo');}else if(g == '1'){infor('Alterações salvas');}else if(g < 0){infor('Enviamos o relatório de alterações ao autor', 3);}else{infor(g);return false;}return true;}";

        ksort($this->javascript);
        echo "function savePost() { ";
        foreach ($this->javascript as $order => $script):
            foreach ($script as $table => $campos):
                echo "$.post('" . HOME . "/requests/back/Admin/savePost.php', {t: '" . $this->encode(str_replace(PRE, "", $table)) . "',{$campos}}, function(g){" . " var id = (resultado(g)? (g < 0 ? g * -1 : g) : 0); " . ($order === 0 && isset($this->tableId[$table]) ? "$('#{$this->tableId[$table]}').val(id);" : "") . " });";
            endforeach;
        endforeach;

        echo "}</script>";
    }

    private function setInputFields() {
        foreach ($this->infoTable as $table => $infoTable):
            $this->table = $table;
            foreach ($infoTable as $coluna => $dados):
                if ($this->isEditable($dados)):

                    $dados['id'] = $this->setJsSave($coluna, $dados);

                    $customInput = new InputCustom();
                    $customInput->setColumn($coluna);
                    $customInput->setDados($dados);
                    $customInput->setJavascript($this->javascript);
                    $resultCustom = $customInput->getResult();
                    if (!$resultCustom):
                        if (!$this->checkNameColumToInputType($coluna, $dados)):
                            $this->checkTypeColumToInputType($coluna, $dados);
                        endif;
                    else:
                        $this->input[] = $resultCustom;
                        if ($customInput->getJavascript()):
                            $this->javascript = $customInput->getJavascript();
                        endif;
                    endif;

                endif;
            endforeach;
        endforeach;
    }

    private function setJsSave($coluna, $dado) {
        $id = md5(rand(0, 99999) . $dado['posicao'] . rand(0, 99999) . $coluna);

        //type -> serve para informar a ordem que os posts devem ser salvos
        $type = 1;
        $table = $this->table;
        $tableId = $table;

        if (isset($dado['table'])):
            //FK
            if ($dado['table'] === $coluna):
                //many to many
                $type = 3;
                $table = $dado['table'];
                $tableId = $table;
            else:
                $tableId = $dado['table'];
            endif;
        endif;

        $this->javascript[$type][$table] = (isset($this->javascript[$type][$table]) ? "{$this->javascript[$type][$table]}, " : "") . "'" . $this->encode($coluna) . "': $('#{$id}').val()";
        if ($type < 3 && !isset($this->tableId[$tableId])):
            $this->tableId[$tableId] = $id;
        endif;
        return $id;
    }

    private function isEditable($dado) {
        if ((isset($dado['permissao']) && preg_match('/select/i', $dado['permissao'])) || !isset($dado['permissao'])):
            return true;
        endif;

        return false;
    }

    private function checkNameColumToInputType($coluna, $dado) {

        if (preg_match('/mail/i', $coluna)):
            $this->input[] = "<label><input type='mail' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>email</span></label>";

        elseif (preg_match('/(password|senha)/i', $coluna)):
            $this->input[] = "<label><input type='password' maxlength='{$dado['tamanho']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>Nova Senha</span></label>";

        elseif ("urlname" === $coluna):
            $this->input[] = "<div class='container pd-small'><div class='fl-left' title='Link'><i class='shoticon shoticon-link shoticon-button'></i><span class='font-size09 font-light'>&nbsp;</span></div><input type='text' placeholder='link-de-acesso' class='fl-left radius nopadding noborder' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' placeholder='link de acesso' style='width:65%;' /></div><span class='container pd-small'></span>";

        elseif ("status" === $coluna):
            $this->input[] = '<div class="container pd-small"><div class="font-light pd-small fl-left locksin" title="status"><i class="shoticon shoticon-button shoticon-unlock"></i>&nbsp;</div><select style="background:transparent;border:0" id="' . $coluna . '" class="pd-small font-size11 fl-left radius color-blackgray lockson">' . "<option class='color-gray font-size11' value='1'>Ativado</option><option class='color-gray font-size11' value='0'>Desativado</option>" . '</select>' . '<span class="container pd-small"></span></div>';

        elseif ("nota" === $coluna || "star" === $coluna):
            $this->input[] = '<div class="container pd-small">' . Check::StarL($coluna, $dado['value'], 10) . '</div>';

        elseif (preg_match('/acesso/i', $coluna)):
            $this->input[] = '<div class="container pd-small"><div class="font-light pd-small fl-left locksin" title="Acesso"><i class="shoticon shoticon-button shoticon-unlock"></i>&nbsp;</div><select style="background:transparent;border:0" id="' . $coluna . '" class="pd-small font-size11 fl-left radius color-blackgray lockson">' . "<option class='color-gray font-size11' value='1'>Publico</option><option class='color-gray font-size11' value='0'>Requer Login</option></select>" . '<span class="container pd-small"></span></div>';

        elseif (preg_match('/(preco|price|money|dinheiro)/i', $coluna)):
            $this->input[] = "<label><input type='number' min='1' step='any' maxlength='{$dado['tamanho']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>Preço</span></label>";

        elseif ("banco" === $coluna):
            $list = array("Posts" => "post", "Categorias" => "category", "Coleções" => "colecao", "Tags" => "tag", "Desenvolvedores" => "frabricante");
            $opt = "<option class='color-blackgray' value=''>Selecione um Banco</option>";
            foreach ($list as $nome => $banco):
                $opt .= "<option class='color-blackgray' value='{$banco}'>{$nome}</option>";
            endforeach;

            $this->input[] = "<div class='container pd-small'><div class=\"font-light pd-small fl-left\" title=\"Acesso\"><i class=\"shoticon shoticon-button shoticon-export-pure\"></i>&nbsp;</div><select id='{$dado['id']}' class='color-blackgray noborder pd-small radius'>{$opt}</select></div>";

        elseif ('_content' === $coluna):
            $this->input[] = $this->getInputEditor($coluna, $dado['value']);

        elseif ('cover' === $coluna):
            //padrões
            $read = new Read();
            $read->ExeRead(PRE . "padrao", "WHERE name='width-cover' && user_id=:uu ORDER BY id DESC", "uu={$_SESSION['userlogin']['id']}");
            $imgWidth = ($read->getResult() ? $read->getResult()[0]['padrao'] : 200);

            $this->input[] = "<div class='container'><div class='fl-left pd-big' id='imgpreview'><div id='Boximgclass' class='ps-relative fl-left'><div id='imgclass' class='transition-easy fl-left ps-relative'><img src='../uploads/{$dado['value']}' id='imgdp' class='transition-easy imgtochangeCover' style='width:" . $imgWidth . "px;' width='" . $imgWidth . "' /></div><div id='edtImgBox' class='fl-left pd-small ps-absolute top left'><input type='number' step='10' placeholder='largura' value='" . $imgWidth . "' class='font-size09' id='cover-width' /><br><div class='fl-left pd-small'><button onclick='vertImage();' class='fl-left btn btn-staticwhite transition-fast border-greenhover pd-small' title='inverter imagem na horizontal' ><i style='padding-left:14px' class='shoticon shoticon-button shoticon-arrowVertical font-size07'></i></button><button onclick='HoriImage();' class='fl-left btn btn-staticwhite transition-fast border-greenhover pd-small' title='inverter imagem na vertical' style='margin:0 3px;'><i style='padding-left:14px' class='shoticon shoticon-button shoticon-arrowHorizontal font-size07'></i></button><button onclick='sendGallery(\"frames\",\"FrameSelect\");' class='fl-left btn btn-staticwhite transition-fast border-greenhover pd-small' title='adicionar máscara' ><i style='padding-left:14px' class='shoticon shoticon-button shoticon-frame font-size07'></i></button><button onclick='sendGallery(\"frames\",\"daguaImageSelect\");' class='fl-left btn btn-staticwhite transition-fast border-greenhover pd-small' title='marca dagua' style='margin:0 3px;'><i style='padding-left:14px'class='shoticon shoticon-button shoticon-gota font-size07'></i></button></div></div></div><input type='hidden' id='{$dado['id']}' value='{$dado['value']}' />" . '<button onclick="sendGallery(\'post\',\'iconSelect\');" class="container btn btn-staticgray border-greenhover transition-fast"><i class="shoticon shoticon-gallery"></i>Enviar Capa</button></div></div>';

        else:
            return false;
        endif;

        return true;
    }

    private function checkTypeColumToInputType($coluna, $dado) {

        $nome = $this->changeNameColum($coluna);

        switch ($dado['tipo']):
            case 'int':
                $max = (pow(10, $dado['tamanho']) - 1);
                $this->input[] = "<label class='container pd-small'><input type='number' min='-{$max}' max='{$max}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                break;

            case 'float':
                $this->input[] = "<label class='container pd-small'><input type='number' step='0.1' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                break;

            case "boolean":
                $this->input[] = "<label class='container pd-small'><select id='{$dado['id']}' class='pd-small radius'><option value='0'>INATIVO</option><option value='1'>ATIVO</option></select><span class='font-light'>" . $nome . "</span></label>";
                break;

            case "datetime":
                $this->input[] = "<div class='container pd-small'><div class='fl-left pd-small font-light font-size14' title='Publicar'><i class='shoticon shoticon-calendario'></i>&nbsp;&nbsp;</div><div class='fl-left' id='datehide'><input type='datetime-local' min='1000-01-01 00:00' max='9999-12-31 23:59' value='" . str_replace(' ', 'T', $dado['value']) . "' id='{$dado['id']}' class='pd-small radius' /></div>";
                break;

            case "date":
                $this->input[] = "<label class='container pd-small'><input type='date' min='1000-01-01' max='9999-12-31' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                break;

            case "time":
                $this->input[] = "<label class='container pd-small'><input type='time' min='00:00:00' max='23:59:59' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                break;

            case "year":
                $this->input[] = "<label class='container pd-small'><input type='number' min='1900' max='2155' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                break;

            case "text":
                if ($dado['tamanho'] > 499):
                    $this->input[] = $this->getInputEditor($coluna, $dado['value']);
                else:
                    $this->input[] = "<label class='container pd-small'><input type='text' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";
                endif;
                break;

            default:
                $this->input[] = "<label class='container pd-small'><input type='text' maxlength='{$dado['tamanho']}' value='{$dado['value']}' id='{$dado['id']}' class='pd-small radius' /><span class='font-light'>" . $nome . "</span></label>";

        endswitch;
    }

    private function getInputEditor($coluna, $value) {
        $this->editor = true;
        return '<textarea class="' . $coluna . ' container pd-small" placeholder="descrição..." rows="6" id="editor">' . htmlentities($value) . '</textarea>';
    }

    private function checkCkEditor() {
        if ($this->isEditor()):
            echo '<script src="' . HOME . '/_app/Library/ckeditor/ckeditor.js"></script>';
            echo '<script>
            CKEDITOR.config.contentsCss = \'../css/styleP.css\';
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.replace(\'editor\', {customConfig: \'\'});
            </script>';

            echo '<style>' . '#cke_editor{float:left; width:100%}.cke_button__underline, .cke_button__strike, .cke_button__horizontalrule, .cke_button__blockquote, .cke_button__removeformat, .cke_button__unlink, .cke_toolbar_break, .cke_button__xdsoft_translater_reverse, .cke_button__xdsoft_translater_settings, .cke_button__specialchar, .cke_button__superscript, .cke_button__subscript, .cke_button__image, .cke_button__anchor, .cke_button__table { display: none !important;}.cke_top {background: #F5F5F5; border-bottom: solid 1px #CCC; padding: 5px 5px 2px;}' . '</style>';
        endif;
    }

    private function changeNameColum($name) {
        $arr = ['_', '-', $this->prefixo, 'price', 'title', ' app', 'title', '_content', 'cover', 'views', 'urlname'];
        $narr = [' ', ' ', '', 'preço', 'titulo', '', 'titulo', 'descricão', 'imagem', 'visualizações', 'link'];
        return ucfirst(str_replace($arr, $narr, $name));
    }

}