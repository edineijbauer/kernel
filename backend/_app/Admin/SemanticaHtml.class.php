<?php

/*
 * Este Software esta licenciado pela ontab.com.br e disponibilizado gratuitamente na web para uso pessoal e profissional
 * Você tem permissão para usá-la como bem entender, mas não comercializá-la. * 
 * A sua distribuição esta proibida sob pena jurídica, a ontab.com.br é o único index com permissão para distribuição.  *
 */

/**
 * Description of SemanticaHtml
 *
 * @author nenab
 */
class SemanticaHtml {

    private $Content;
    private $Pilha = array();
    private $Ia;
    private $Aux;
    private $Indice;
    private $Max;
    private $Change = false;
    private $Erro = "";
    private $Headers = ["meta", "link", "!doctype", "class", "basefont"];
    private $UnClosedTag = ["!--", "br", "hr", "img", "input", "meta", "link", "!doctype", "area", "class", "basefont", "col", "option", "embed", "frame", "keygen", "param", "source", "track"];
    private $EndTagDefinition = [">", " ", "\n"];

    function __construct($content = null) {
        if ($content):
            $this->setContent($content);
        endif;
    }

    public function setContent($content) {
        $this->receiveContent($content);

        if (strlen($this->Content) > 3):
            $this->Max = strlen($this->Content);
            $this->Ia = -1;
            $this->Indice = -1;
            $this->Aux = "";

            while ($this->next()):
                if ($this->Content[$this->Indice] === "<"):
                    $this->openTag();
                endif;
            endwhile;

            $this->checkErros();
        else:
            $this->Erro = "Conteúdo passado muito curto ou vazio";
        endif;
    }

    public function haveChange() {
        return $this->Change;
    }

    public function getResult() {
        return (!empty($this->Pilha) || !empty($this->Erro) ? false : true);
    }

    public function getContent() {
        return html_entity_decode(htmlspecialchars_decode($this->Content));
    }

    public function getErro() {
        return $this->Erro;
    }

    private function receiveContent($content) {
        $content_back = $content;

        $i = 0;
        while (preg_match('/(\&amp;lt;|\&amp;|\&lt;|\&gt;)/i', $content) || $i > 7):
            $content = $content_back;

            if ($i === 0):
                $content = htmlspecialchars($content);
            elseif ($i === 1):
                $content = htmlentities($content);
            elseif ($i === 2):
                $content = htmlspecialchars(htmlentities($content));
            elseif ($i === 3):
                $content = html_entity_decode($content);
            elseif ($i === 4):
                $content = htmlspecialchars_decode($content);
            elseif ($i === 5):
                $content = htmlspecialchars_decode(html_entity_decode($content));
            elseif ($i === 6):
                $content = htmlentities(htmlspecialchars($content));
            elseif ($i === 7):
                $content = html_entity_decode(htmlspecialchars_decode($content));
            endif;
            $i++;
        endwhile;
        if ($i < 8):
            $this->Content = $content;
        else:
            $this->Erro = "Não foi possível decodificar o HTML recebido";
        endif;
    }

    private function openTag() {
        if ($this->next()):
            if ($this->Content[$this->Indice] === "/" && empty($this->Aux)):
                $this->closeTag();

            elseif (in_array($this->Content[$this->Indice], $this->EndTagDefinition)):
                //fechando de tag
                if (!in_array($this->Aux, $this->UnClosedTag)):
                    $this->Ia++;
                    $this->Pilha[$this->Ia] = $this->Aux;
                endif;
                $this->Aux = "";
            else:
                $this->Aux .= $this->Content[$this->Indice];
                $this->openTag();
            endif;
        endif;
    }

    private function closeTag() {
        if ($this->next()):

            if (in_array($this->Content[$this->Indice], $this->EndTagDefinition)):

                //fechando de tag
                if(empty($this->Pilha)):

                    //não é esperado uma tag de fechamento, mas encontrou
                    //corrige adicionando uma tag afrente da que fechou
                    $position = $this->Indice - strlen($this->Aux) - 2;
                    $this->Content = substr_replace($this->Content, "<{$this->Aux}>", $position, 0);
                    $this->Change = true;
                    $this->Indice += strlen($this->Aux) + 1;
                    $this->Max = strlen($this->Content);

                elseif (!in_array($this->Aux, $this->UnClosedTag)):
                    if ($this->Pilha[$this->Ia] !== $this->Aux):

                        //esperado fechar uma tag específica, mas encontrou outra em vez
                        //corrige fechando a tag esperada, e volta o ponteiro para ler novamente a tag inesperada.
                        $position = $this->Indice - strlen($this->Aux) - 2;
                        $this->Content = substr_replace($this->Content, "</{$this->Pilha[$this->Ia]}>", $position, 0);
                        $this->Change = true;
                        $this->Indice -= strlen($this->Aux) - strlen($this->Pilha[$this->Ia]);
                        $this->Max = strlen($this->Content);
                    endif;

                    unset($this->Pilha[$this->Ia]);
                    $this->Ia--;

                endif;

                $this->Aux = "";

            else:

                $this->Aux .= $this->Content[$this->Indice];
                $this->closeTag();

            endif;
        endif;
    }

    private function next() {
        $this->Indice++;
        if ($this->Indice < $this->Max):
            return true;
        endif;
        return false;
    }

    private function checkErros() {
        if (!empty($this->Pilha)):
            //ainda possui tags na pilha

            $this->Change = true;

            while ($this->Ia > -1):
                $this->Content .= "</{$this->Pilha[$this->Ia]}>";
                unset($this->Pilha[$this->Ia]);
                $this->Ia--;
            endwhile;
        endif;
    }

}
