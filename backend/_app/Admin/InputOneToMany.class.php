<?php


class InputOneToMany {

    private $table;
    private $dados;
    private $result;

    /**
     * @param mixed $table
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * @param mixed $dados
     */
    public function setDados($dados) {
        $this->dados = $dados;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        $this->start();
        return $this->result;
    }

    private function start() {
        if ($this->table):

            $this->result = $this->getAddButton(str_replace(PRE, "", $this->table)) . $this->getList();

        endif;
    }

    /**
     * adiciona mais um conteudo desta tabela
     */
    private function getAddButton($titulo) {
        return "<div class='container pd-small'><div class='fl-left pd-small font-size15 font-light'>" . ucwords(str_replace('-', ' ', Check::getNameBanco($titulo))) . "</div><button class='fl-right btn btn-white pd-medium radius addOneToOne' onclick=\"getPage('{$this->table}', 25, 0, '{$this->dados['id']}')\">adicionar</button></div>";
    }

    /**
     * Obtem a lista de conteúdos deste one to many
     */
    private function getList() {
        if (isset($this->dados['value'])):
            $value = $this->getValores($this->dados['value']);
            return "<div class='container pd-small'></div><input type='hidden' value=',{$this->dados['value']},' class='choiceOneToMany' id='{$this->dados['id']}' /><div class='container pd-small choiceOneToManyDiv' rel='{$this->table}'>{$value}</div>";
        else:
            return "<div class='container pd-small'></div><input type='hidden' value=',' class='choiceOneToMany' id='{$this->dados['id']}' /><div class='container pd-small choiceOneToManyDiv' rel='{$this->table}'></div>";
        endif;
    }

    private function getValores($ids) {
        $title = Check::getNameBanco($this->table);
        $dados = "";
        $ids = array_filter(explode(",", $ids));
        $read = new Read();
        foreach ($ids as $id):
            $read->ExeRead($this->table, "WHERE id = :id", "id={$id}");
            $image = ($read->getResult() && isset($read->getResult()[0]['cover']) ? "<div class='fl-left'><img src='" . HOME . "/tim.php?src=" . HOME . "/uploads/{$read->getResult()[0]['cover']}&w=40&h=40' width='40' height='40' style='width:40px' class='fl-left' title='{$read->getResult()[0]['title']}' alt='{$read->getResult()[0]['title']}' /></div>" : "");

            $class = ($image !== "" ? "pd-medium" : "pd-small");

            $buttons = "<button onclick=\"Delete('{$this->table}', {$id}, '{$title}'); deleteOneToMany('{$this->dados['id']}', {$id});\" title='excluir' class='fl-right btn btn-transparent {$class} color-terceary transition-easy'>x</button> <button onclick=\"getPage('{$this->table}' , 25, {$id}, '{$this->dados['id']}');\" class='edtButtonTable btn btn-transparent fl-right font-light {$class} transition-easy'><i class='shoticon shoticon-lapis font-size07' style='padding-left: 15px;'></i></button>";
            $dados .= "<div class='container space controlOneToMany-{$id}'>{$image}<div class='fl-left {$class} oneToManyTitle'>" . Check::getBanco($this->table, (int)$id, "title") . "</div><div class='fl-right'>{$buttons}</div></div>";

        endforeach;

        return $dados;
    }
}