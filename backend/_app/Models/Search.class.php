<?php

/**
 * Search Apps [ MODEL ]
 *
 * @copyright (c) 2016, Edinei J. Bauer NENA PRO
 */
class Search {

    private $busca;
    private $user;
    private $result = "";

    /**
     * @param mixed $busca
     */
    public function setBusca($busca) {
        $this->busca = (string)$busca;
        $this->user = (isset($_SESSION['userlogin']['id']) ? $_SESSION['userlogin']['id'] : 0);

        if (!empty($this->busca)):
            $this->setBuscaHistory();
            $this->getBusca();
        endif;
    }

    /**
     * @return string
     */
    public function getResult() {
        return $this->result;
    }

    private function getBusca() {
        $this->searchInPost();
        $this->searchInSmartphone();
        $this->searchInSite();
        $this->searchInMarca();
        $this->searchInPagina();
        $this->searchInCategoria();
        $this->searchInTag();
    }

    private function searchInPost() {
        $read = new Read();
        $read->ExeRead(PRE . "post", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Artigos</div>";
            foreach ($read->getResult() as $review):
                $view = new View();
                $review['imagem'] = Check::getBanco(PRE . "gallery", (int)$review['imagem'], "cover");
                $review['descricao'] = Check::Words($review['descricao'], 15);
                $this->result .= $view->Retorna($review, $view->Load("artigo"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInSmartphone() {
        $read = new Read();
        $read->ExeRead(PRE . "smartphone", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Smartphone</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $read->ExeRead(PRE . "gallery", "WHERE id = :si", "si={$smart['imagem']}");
                $smart['imagem'] = ($read->getResult() ? $read->getResult()[0]['cover'] : "");
                $smart['imagem_title'] = ($read->getResult() ? $read->getResult()[0]['title'] : "");
                $smart['imagem_urlname'] = ($read->getResult() ? $read->getResult()[0]['urlname'] : "");
                $read->ExeRead(PRE . "preco_beneficio", "WHERE smartphone = :sm LIMIT 1", "sm={$smart['id']}");
                $smart['preco'] = (float) ($read->getResult() ? number_format($read->getResult()[0]["valor"], 1, '.', ''): "-");
                $smart['pontos'] = number_format($read->getResult()[0]["pontos"], 1, '.', '');
                $smart['box'] = 5;
                $this->result .= $view->Retorna($smart, $view->Load("smartphone"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInMarca() {
        $read = new Read();
        $read->ExeRead(PRE . "marca", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Marca</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $read->ExeRead(PRE . "gallery", "WHERE id = :si", "si={$smart['imagem']}");
                $smart['imagem'] = ($read->getResult() ? $read->getResult()[0]['cover'] : "");
                $smart['imagem_title'] = ($read->getResult() ? $read->getResult()[0]['title'] : "");
                $smart['imagem_urlname'] = ($read->getResult() ? $read->getResult()[0]['urlname'] : "");
                $this->result .= $view->Retorna($smart, $view->Load("marca"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInSite() {
        $read = new Read();
        $read->ExeRead(PRE . "index", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Loja</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $read->ExeRead(PRE . "gallery", "WHERE id = :si", "si={$smart['imagem']}");
                $smart['imagem'] = ($read->getResult() ? $read->getResult()[0]['cover'] : "");
                $smart['imagem_title'] = ($read->getResult() ? $read->getResult()[0]['title'] : "");
                $smart['imagem_urlname'] = ($read->getResult() ? $read->getResult()[0]['urlname'] : "");
                $this->result .= $view->Retorna($smart, $view->Load("index"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInPagina() {
        $read = new Read();
        $read->ExeRead(PRE . "pagina", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Pagina</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $read->ExeRead(PRE . "gallery", "WHERE id = :si", "si={$smart['imagem']}");
                $smart['imagem'] = ($read->getResult() ? $read->getResult()[0]['cover'] : "");
                $smart['imagem_title'] = ($read->getResult() ? $read->getResult()[0]['title'] : "");
                $smart['imagem_urlname'] = ($read->getResult() ? $read->getResult()[0]['urlname'] : "");
                $this->result .= $view->Retorna($smart, $view->Load("pagina"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInCategoria() {
        $read = new Read();
        $read->ExeRead(PRE . "category", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Categoria</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $this->result .= $view->Retorna($smart, $view->Load("categoria"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function searchInTag() {
        $read = new Read();
        $read->ExeRead(PRE . "tag", "WHERE (title LIKE '%" . $this->busca . "%' || urlname LIKE '%" . $this->busca . "%') ORDER BY id DESC LIMIT 7");
        if ($read->getResult()):
            $this->result .= "<div class='container boxshadow-heavy mg-small pd-medium'><div class='container pd-medium'>{$read->getRowCount()} " . ($read->getRowCount() > 1 ? "Resultados" : "Resultado") . " em Tag</div>";
            foreach ($read->getResult() as $smart):
                $view = new View();
                $this->result .= $view->Retorna($smart, $view->Load("tag"));
            endforeach;
            $this->result .= "</div>";
        endif;
    }

    private function setBuscaHistory() {
        $read = new Read();
        $read->ExeRead(PRE . "search", "WHERE search='{$this->busca}' && user_id=:ui", "ui={$this->user}");
        if (!$read->getResult()):
            $create = new Create();
            $Dados['search'] = $this->busca;
            $Dados['user_id'] = $this->user;
            $Dados['date'] = date('Y-m-d H:i:s');
            $create->ExeCreate(PRE . "search", $Dados);
        else:
            $Dados['number'] = $read->getResult()[0]['number'] + 1;
            $Dados['date'] = date('Y-m-d H:i:s');
            $Dados['user_id'] = (isset($_SESSION['userlogin']['user_id']) ? $_SESSION['userlogin']['user_id'] : 0);
            $up = new Update();
            $up->ExeUpdate(PRE . "search", $Dados, "WHERE search='{$this->busca}' && user_id=:ui", "ui={$this->user}");
        endif;
    }

}
