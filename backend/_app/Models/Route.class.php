<?php

/**
 * Created by PhpStorm.
 * User: Edinei
 * Date: 18/01/2017
 * Time: 10:41
 */
abstract class Route extends Seo {

    private $File;
    private $Link;

    private $Data = array();

    private $Limite = 25;
    private $Offset;

    /**
     * Route constructor.
     * @param $File
     * @param $Link
     * @param $Offset
     */
    public function __construct($File, $Link = null, $Offset = 0) {
        $this->File = $File;
        $this->Link = $Link;
        $this->Offset = $Offset;

        parent::__construct($this->File, $this->Link, $this->Offset);
        parent::setUrl($this->getUrl());

        $this->checkRoute();
    }

    /**
     * @return mixed
     */
    protected function getFile() {
        return $this->File;
    }

    /**
     * @return null
     */
    protected function getLink() {
        return $this->Link;
    }

    /**
     * @return int
     */
    public function getOffset() {
        return $this->Offset;
    }

    /**
     * <b>Obter Dados:</b> Este será automaticamente povoado com valores de uma tabela single para arquivos
     * como categoria, artigo, etc. Basta usar um extract para obter as variáveis da tabela!
     *
     * @return ARRAY = Dados da tabela
     */
    protected function getData() {
        return $this->Data;
    }


    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    private function checkRoute() {

        $routes = array();
        $defaultFile = null;
        $routesTable = array();
        include_once ADM ? '../_app/routes.php' : '_app/routes.php';

        if (in_array($this->File, $routes)):
            $title = isset($routesTitle[$this->File]) ? $routesTitle[$this->File] : $this->getTitle();
            $descricao = isset($routesDescricao[$this->File]) ? $routesDescricao[$this->File] : $this->getDescricao();
            $tabela = isset($routesTable[$this->File]) ? $routesTable[$this->File] : null;
            $this->File = ($tabela ? $tabela : $this->File);

            $this->checkSubContent($title, $descricao, $tabela);

        else:

            if (!$this->checkPost($defaultFile)):
                $this->notAllowed();
            endif;
        endif;
    }

    private function checkSubContent($title, $descricao, $table = null) {
        if (!$this->Link || is_numeric($this->Link)):
            if ($this->Link):
                $this->Offset = (int)$this->Link;
            endif;

            if ($this->readContent($table)):
                parent::setTitle($title);
                parent::setDescricao($descricao);
            else:
                $this->notAllowed();
            endif;

        else:
            if (!$this->readSubContent($table, $descricao)):
                $this->notAllowed();
            endif;

        endif;
    }

    /**
     * Le as informações deste conteúdo no banco
     * Se encontrar, retorna um lista de resultados
     * senão, notAllowed
     */
    private function readContent($table = null) {
        if ($table):
            $read = new Read();
            $read->ExeRead(PRE . $table, "ORDER BY id DESC LIMIT {$this->Offset},{$this->Limite}");
            if ($read->getResult()):
                $this->Data = $read->getResult();
            else:
                return false;
            endif;
        endif;

        return true;
    }

    /**
     * Le as informações deste conteúdo no banco
     * Se encontrar, retorna a informação relacionada com o Link
     * senão, notAllowed
     */
    private function readSubContent($table = null, $descricao) {
        if ($table):
            $read = new Read();
            $read->ExeRead(PRE . $table, "WHERE urlname = :un", "un={$this->Link}");
            if ($read->getResult()):
                $this->sendTitleDescricao($read->getResult()[0], $read->getResult()[0]['title'], (isset($read->getResult()[0]['descricao']) ? $this->getDescricao($read->getResult()[0]['descricao']) : $descricao));
            else:
                return false;
            endif;

        endif;

        return true;
    }

    /**
     * Verifica se este caminho é um post
     * Caso seja, retorna as informações do post
     * senão, notAllowed
     */
    private function checkPost($table = null) {
        if ($table):
            $read = new Read();
            $read->ExeRead(PRE . $table, "WHERE urlname = :un", "un={$this->File}");
            if ($read->getResult()):
                $this->Link = $this->File;
                $this->File = $table;
                $this->sendTitleDescricao($read->getResult()[0], $read->getResult()[0]['title'], ($read->getResult()[0]['descricao'] ? $this->getDescricao($read->getResult()[0]['descricao']) : $this->getDescricao()));
                return true;
            endif;
        endif;

        return false;
    }

    private function sendTitleDescricao($dados, $title, $descricao) {
        $this->Data = $dados;

        foreach ($this->Data as $name => $value):
            if (preg_match('/_title$/i', $name)):
                $title = ucwords($value) . ($this->Offset > 1 ? " Pagina {$this->Offset}" : "") . ' | ' . SITENAME;
            elseif (preg_match('/_content$/i', $name)):
                $descricao = ucfirst($value);
            endif;
        endforeach;

        if (isset($dados['imagem'])):
            parent::setImage(HOME . "/uploads/" . Check::getBanco(PRE . "gallery", (int)$dados['imagem'], "cover"));
        endif;

        parent::setTitle($title);
        parent::setDescricao($descricao);
    }

    private function notAllowed() {
        if ($this->File !== "404"):
            header("Location: " . HOME . "/404");
        else:
            echo "<title>404 não encontrado</title>";
            WSErro("404 não encontrado", WS_ERROR, true);
        endif;
    }

    private function getTitle() {
        $title = ucwords(str_replace(array("index", "-", "_", ".", "+"), array("", " ", " ", " ", " "), $this->File));
        return empty($title) ? SITENAME : $title . " | " . SITENAME;
    }

    private function getDescricao($desc = null) {
        $desc = (!$desc ? SITEDESC : $desc);
        return Check::Words(html_entity_decode($desc), 25);
    }

    private function getUrl() {
        return HOME . ($this->File !== "index" ? DIRECTORY_SEPARATOR . $this->File : "") . (!empty($this->Link) ? DIRECTORY_SEPARATOR . $this->Link : "") . (!empty($this->Offset) ? DIRECTORY_SEPARATOR . $this->Offset : "");
    }
}