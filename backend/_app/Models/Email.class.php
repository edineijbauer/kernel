<?php
require_once '../../vendor/autoload.php';
use Mailgun\Mailgun;

/**
 * Email [ MODEL ]
 * Modelo responável por configurar a PHPMailer, validar os dados e disparar e-mails do sistema!
 *
 * @copyright (c) year, Robson V. Leite UPINSIDE TECNOLOGIA
 */

class Email{

    /** CORPO DO E-MAIL */
    private $assunto;
    private $mensagem;
    private $html;
    private $nome;
    private $email;

    private $emailRemetente;
    private $nomeRemetente;

    /** CONSTROLE */
    private $Error;
    private $Result;

    /**
     * Email constructor.
     */
    public function __construct() {

    }

    /**
     * @param mixed $assunto
     */
    public function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    /**
     * @param mixed $mensagem
     */
    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html) {
        $this->html = $html;
    }

    /**
     * @param mixed $emailRemetente
     */
    public function setEmailRemetente($emailRemetente) {
        $this->emailRemetente = $emailRemetente;
    }

    /**
     * @param mixed $nomeRemetente
     */
    public function setNomeRemetente($nomeRemetente) {
        $this->nomeRemetente = $nomeRemetente;
    }


    public function enviar() {
        $this->Clear();
        $this->PreFormat();

        $mgClient = new Mailgun('key-0786754334d08fedfd9317e7b2298359');
        $domain = "buscaphone.com";

        $this->nomeRemetente = !$this->nomeRemetente ? "Contato BuscaPhone" : $this->nomeRemetente;
        $this->emailRemetente = !$this->emailRemetente ? "contato@buscaphone.com" : $this->emailRemetente;

        $this->Result = $mgClient->sendMessage("$domain", array('from' => "{$this->nomeRemetente} <{$this->emailRemetente}>", 'to' => "{$this->nome} <{$this->email}>", 'subject' => $this->assunto, 'text' => $this->mensagem, 'html' => $this->html
                //            'recipient-variables' => '{"name-1@gmail.com": {"first":"Name-1", "id":1}, "name-2@gmail.com": {"first":"Name-2", "id": 2}}'),
                //        array('inline' => 'Pad-Thai-1.jpg')
            ));
    }

    /**
     * <b>Verificar Envio:</b> Executando um getResult é possível verificar se foi ou não efetuado
     * o envio do e-mail. Para mensagens execute o getError();
     * @return BOOL $Result = TRUE or FALSE
     */
    public function getResult() {
        return $this->Result;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    //Limpa código e espaços!
    private function Clear() {
        $this->html = trim($this->html);
        $this->assunto = trim(strip_tags($this->assunto));
        $this->mensagem = trim(strip_tags($this->mensagem));
    }

    //Formatar ou Personalizar a Mensagem!
    private function PreFormat() {
        $this->Mensagem = "{$this->html}<hr><small>Recebida em: " . date('d/m/Y H:i') . "</small>";
    }

}
