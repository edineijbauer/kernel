<?php

/**
 * Seo [ MODEL ]
 * Classe de apoio para o modelo LINK. Pode ser utilizada para gerar SSEO para as páginas do sistema!
 *
 * @copyright (c) 2016, Edinei J. Bauer INMANAGER
 */
abstract class Seo {

    private $File;
    private $Link;
    private $Redir;

    private $title;
    private $descricao;
    private $image;
    private $url;
    private $favicon;

    /* DADOS POVOADOS */
    private $seoTags;

    function __construct($File, $Link = null, $Redir = 1) {
        $this->File = strip_tags(trim($File));
        $this->Link = strip_tags(trim($Link));
        $this->Redir = str_replace('.html', '', strip_tags(trim($Redir)));
        $this->favicon = INCLUDE_PATH . '/img/favicon.png';
    }

    /**
     * <b>Obter MetaTags:</b> Execute este método informando os valores de navegação para que o mesmo obtenha
     * todas as metas como title, description, og, itemgroup, etc.
     *
     * <b>Deve ser usada com um ECHO dentro da tag HEAD!</b>
     * @return STRING TAGS =  Retorna todas as tags HEAD
     */
    protected function getTags() {
        $this->setMetadados();

        return $this->seoTags;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = trim(strip_tags($title));
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao) {
        $this->descricao = trim(strip_tags($descricao));
    }

    /**
     * @param mixed $image
     */
    public function setImage($image) {
        $this->image = $image;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url) {
        $this->url = trim(strip_tags($url));
    }


    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    //Monta e limpa as tags para alimentar as tags
    private function setMetadados() {
        $this->image = ($this->image ? $this->image : $this->favicon);

        if ($this->File === "index" && empty($this->Link)):
            $this->seoIndex();
        else:

            if ($this->File === "smartphone"):
                $this->seoSmartphones();
            elseif ($this->File === "marca"):
                $this->seoMarca();
            else:
                $this->seoPost();
            endif;
            $this->seoNoIndex();
        endif;

        //NORMAL PAGE
        $this->seoTags .= '<meta name="robots" _content="index, follow" />' . "\n";
        $this->seoTags .= '<meta name="viewport" _content="width=device-width">' . "\n";
        $this->seoTags .= '<link rel="shortcut icon" href="' . $this->favicon . '">' . "\n";

        //FACEBOOK
        $this->seoTags .= '<meta property="og:url" _content="' . $this->url . '" />' . "\n";
        $this->seoTags .= '<meta property="og:type" _content="article" />' . "\n";
        $this->seoTags .= '<meta property="og:title" _content="' . $this->title . '" />' . "\n";
        $this->seoTags .= '<meta property="og:description" _content="' . $this->descricao . '" />' . "\n";
        $this->seoTags .= '<meta property="og:image" _content="' . HOME . '/tim.php?src=' . $this->image . '&w=400" />' . "\n";
        $this->seoTags .= '<meta property="og:site_name" _content="' . SITENAME . '" />' . "\n";
        $this->seoTags .= '<meta property="og:locale" _content="pt_BR" />' . "\n";
        $this->seoTags .= "\n";

        //ITEM GROUP (TWITTER)
        $this->seoTags .= '<meta name="twitter:card" _content="summary_large_image" />' . "\n";
        $this->seoTags .= '<meta name="twitter:index" _content="@BuscaPhone" />' . "\n";
        $this->seoTags .= '<meta name="twitter:creator" _content="@BuscaPhone" />' . "\n";
        $this->seoTags .= '<meta name="twitter:description" _content="' . $this->descricao . '" />' . "\n";
        $this->seoTags .= '<meta name="twitter:title" _content="' . $this->title . '" />' . "\n";
        $this->seoTags .= '<meta name="twitter:image" _content="' . $this->image . '" />' . "\n";
        $this->seoTags .= '<meta name="twitter:url" _content="' . $this->url . '" />' . "\n";

        //ITEM GROUP (GERAL)
        $this->seoTags .= '<meta itemprop="name" _content="' . $this->title . '">' . "\n";
        $this->seoTags .= '<meta itemprop="description" _content="' . $this->descricao . '">' . "\n";
        $this->seoTags .= '<meta itemprop="url" _content="' . $this->url . '">' . "\n";
        $this->seoTags .= '<meta itemprop="image" _content="' . $this->image . '">' . "\n";
        $this->seoTags .= '<meta name="description" _content="' . $this->descricao . '"/>' . "\n";
    }

    private function seoNoIndex() {
        $this->seoTags = '<title>' . $this->title . '</title> ' . "\n";
        $this->seoTags .= '<link rel="canonical" href="' . $this->url . '">' . "\n";
    }

    private function seoIndex() {
        $this->title .= " - " . SITESUB;
        $this->seoTags .= '<title itemprop="name">' . $this->title . '</title>' . "\n";
        $this->seoTags .= '<link rel="canonical" href="' . HOME . '" itemprop="url">' . "\n";
    }

    private function seoPost() {
        $this->seoTags .= '<meta property="og:type" _content="article" />' . "\n";
    }

    private function seoSmartphones() {
        $this->descricao = "Especificações Técnicas do Smartphone " . $this->title . ". Compare suas informações técnicas e encontre o menor preço.";
        $this->title .= " - Informações Técnicas";
    }

    private function seoMarca() {
        $this->descricao = ($this->descricao !== SITEDESC ? $this->descricao : "A marca " . ucfirst($this->title) . " é confiável? é boa? Informações, descrição, smartphones e comentários sobre a marca " . ucfirst($this->title) . " na " . SITENAME . " resumem sua qualidade.");
        $this->title = "Sobre a Marca " . ucfirst($this->title) . ", é boa?";
    }

}
