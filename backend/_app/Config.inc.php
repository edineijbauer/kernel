<?php
require('Connection.inc.php');

define("DOLAR", 3.15);

// DEFINE IDENTIDADE DO SITE ################
define('SITENAME', "buscaPhone");
define('SITESUB', "Saiba Tudo sobre Smartphones");
define('SITEDESC', "Compare smartphones para buscar o menor preço, encontre novidades do mundo dos celulares e descubra o melhor smartphone custo/benefício dentro de seu orçamento na " . SITENAME . ".");
define('TIPO', "site");
define('USER_TABLE', "user");

// DEFINE A BASE DO SITE ####################
define('THEME', "smart");
define('HTTP', 'http://');
define('HOME', HTTP . DOMINIO);
define('HOME_IMAGE', HTTP . "craw.buscaphone.com");
define('INCLUDE_PATH', HOME . '/themes/' . THEME);
define('REQUIRE_PATH', 'themes/' . THEME);
define('RSS', HTTP . DOMINIO . '/feed'); // padrão: HOME/feed

// DEFINE SERVIDOR DE E-MAIL ################
//define('MAILUSER', $read->getResult()[0]['sistema_email']);
//define('MAILPASS', $read->getResult()[0]['sistema_senha']);
//define('MAILPORT', $read->getResult()[0]['sistema_porta']);
//define('MAILHOST', $read->getResult()[0]['sistema_smtp']);

// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {
    $cDir = ['Conn', 'Helpers', 'Models', 'Template', "PageNavigation", 'formCrud', 'Admin', 'Trigger', 'Vo', 'Smartphone', 'Api', 'DataMining', 'Smartphone', 'Smartphone/Specout'];
    $iDir = null;

    if ($Class == 'check'):
        $Class = 'Check';
    endif;

    foreach ($cDir as $dirName):
        if (!$iDir && file_exists(__DIR__ . '/' . $Class . '.class.php') && !is_dir(__DIR__ . '/' . $Class . '.class.php')):
            include_once(__DIR__ . '/' . $Class . '.class.php');
            $iDir = true;
        endif;
    endforeach;

    if (!$iDir):
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    endif;
}

function CustomErro($mensagem, $erro) {
    if ($erro === "22003"):
        $a = ["(Update) Erro ao Ler:</b> SQLSTATE[22003]:", " Numeric value out of range: 1264 Out of range value for column", "at row 1"];
        $b = ["", "campo ", "Excedeu o tamanho máximo"];
        echo str_replace($a, $b, $mensagem);

        return true;
    endif;

    return false;
}

//WSErro :: Exibe erros lançados :: Front
function WSErro($ErrMsg, $ErrNo, $ErrDie = null) {

    if (!CustomErro($ErrMsg, $ErrNo)):
        //(Update) Erro ao Ler: SQLSTATE[22003]: Numeric value out of range: 1264 Out of range value for column 'limite_rebuy' at row 11

        $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
        echo "<p class=\"trigger shoticon shoticon-{$CssClass} shoticon-sectiontitle {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

    else:
        die;
    endif;

    if ($ErrDie):
        die;
    endif;
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));

    echo "<p class=\"trigger {$CssClass}\">";
    echo "<b>Erro na Linha: #{$ErrLine} ::</b> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}

//(Update) Erro ao Ler:</b> SQLSTATE[22003]: Numeric value out of range: 1264 Out of range value for column 'limite_rebuy' at row 1

set_error_handler('PHPErro');
